import 'dart:io';

class Signup {
  File? file;
  String name = '';
  String email = '';
  String birthdate = '';
  String gender = '';
  String height = '';
  int category = -1;
  int box = -1;
  String password = '';
  String? firstSurname = '';
  String? secondSurname = '';
  String? weight;
  String? instagram;
  String? facebook;
  String? twitter;
  String? whatsapp;
  bool? showFirstSurname = true;
  bool? showSecondSurname = true;
  bool? showBirthdate = true;
  bool? showGender = true;
  bool? showAge = true;
  bool? showHeight = true;
  bool? showWeight = true;
  bool? showCategory = true;
  bool? showInstagram = true;
  bool? showFacebook = true;
  bool? showTwitter = true;
  bool? showWhatsapp = true;

  Signup();

  Signup.fromSignUp(
      {required this.file,
      required this.name,
      required this.email,
      required this.birthdate,
      required this.gender,
      required this.height,
      required this.password,
      required this.category,
      required this.box,
      this.firstSurname,
      this.secondSurname,
      this.weight,
      this.instagram,
      this.facebook,
      this.whatsapp,
      this.twitter,
      this.showFirstSurname,
      this.showSecondSurname,
      this.showBirthdate,
      this.showGender,
      this.showAge,
      this.showHeight,
      this.showWeight,
      this.showCategory,
      this.showInstagram,
      this.showFacebook,
      this.showTwitter,
      this.showWhatsapp});

  Signup copyWith(
      {File? file,
      String? name,
      String? firstSurname,
      String? secondSurname,
      String? email,
      String? birthdate,
      String? height,
      String? weight,
      String? gender,
      String? password,
      int? category,
      int? box,
      String? instagram,
      String? facebook,
      String? twitter,
      String? whatsapp,
      bool? showFirstSurname,
      bool? showSecondSurname,
      bool? showBirthdate,
      bool? showGender,
      bool? showAge,
      bool? showHeight,
      bool? showWeight,
      bool? showCategory,
      bool? showInstagram,
      bool? showFacebook,
      bool? showTwitter,
      bool? showWhatsapp}) {
    return Signup.fromSignUp(
        file: file ?? this.file,
        name: name ?? this.name,
        email: email ?? this.email,
        firstSurname: firstSurname ?? this.firstSurname,
        secondSurname: secondSurname ?? this.secondSurname,
        birthdate: birthdate ?? this.birthdate,
        gender: gender ?? this.gender,
        height: height ?? this.height,
        weight: weight ?? this.weight,
        category: category ?? this.category,
        box: box ?? this.box,
        password: password ?? this.password,
        instagram: instagram ?? this.instagram,
        facebook: facebook ?? this.facebook,
        whatsapp: whatsapp ?? this.whatsapp,
        twitter: twitter ?? this.twitter,
        showFirstSurname: showFirstSurname ?? this.showFirstSurname,
        showSecondSurname: showSecondSurname ?? this.showSecondSurname,
        showBirthdate: showBirthdate ?? this.showBirthdate,
        showGender: showGender ?? this.showGender,
        showAge: showAge ?? this.showAge,
        showHeight: showHeight ?? this.showHeight,
        showWeight: showWeight ?? this.showWeight,
        showCategory: showCategory ?? this.showCategory,
        showInstagram: showInstagram ?? this.showInstagram,
        showFacebook: showFacebook ?? this.showFacebook,
        showTwitter: showTwitter ?? this.showTwitter,
        showWhatsapp: showWhatsapp ?? this.showWhatsapp);
  }
}
