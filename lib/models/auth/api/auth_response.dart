class AuthResponse {
  final bool success;
  final int status;
  final DataLoginResponse data;

  AuthResponse(
      {required this.success, required this.status, required this.data});

  factory AuthResponse.fromJson(Map<String, dynamic> json) {
    return AuthResponse(
        success: json['success'],
        status: json['status'],
        data: DataLoginResponse.fromJson(json['data']));
  }
}

class DataLoginResponse {
  final int id;
  int? boxID;
  final String accessToken;

  DataLoginResponse(this.id, this.boxID, this.accessToken);

  factory DataLoginResponse.fromJson(Map<String, dynamic> json) =>
      DataLoginResponse(json['id'], json['box_id'], json['access_token']);

  Map<String, dynamic> toJson() {
    return {'id': id, 'box_id': boxID, 'access_token': accessToken};
  }
}
