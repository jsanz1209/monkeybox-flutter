class ResetPasswordRequest {
  final String email;
  final String frontendBaseUrl;

  ResetPasswordRequest({
    required this.email,
    required this.frontendBaseUrl,
  });

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'frontendBaseUrl': frontendBaseUrl,
    };
  }
}
