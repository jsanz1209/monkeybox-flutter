
import 'dart:io';

import 'package:monkeybox/models/user/api/privacy_data_request.dart';

class SignUpRequest {
  final String email;
  final String username;
  final String password;
  final String name;
  final String? firstSurname;
  final String? secondSurname;
  final String birthdate;
  final String gender;
  final double? weight;
  final String height;
  final int intensityCategoryID;
  final int boxID;
  final PrivacyData? privacy;
  final File? file;

  SignUpRequest({
      required this.email,
      required this.username,
      required this.password,
      required this.name,
      this.firstSurname,
      this.secondSurname,
      required this.birthdate,
      required this.gender,
      required this.weight,
      required this.height,
      required this.intensityCategoryID,
      required this.boxID,
      required this.file,
      this.privacy});

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'username': username,
      'password': password,
      'name': name,
      'first_surname': firstSurname ?? '',
      'second_surname': secondSurname ?? '',
      'birthdate': birthdate,
      'gender': gender,
      'weight': weight,
      'height': height,
      'intensity_category_id': intensityCategoryID,
      'box_id': boxID,
      'privacy': privacy!.toJson()
    };
  }
}