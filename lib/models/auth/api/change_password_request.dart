class ChangePasswordRequest {
  final int id;
  final String code;
  final String password;

  ChangePasswordRequest(
      {required this.id, required this.code, required this.password});

  Map<String, dynamic> toJson() {
    return {'id': id, 'code': code, 'password': password};
  }
}
