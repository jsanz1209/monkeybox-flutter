import 'package:flutter/material.dart';
import 'package:monkeybox/enums/menu_options_enum.dart';

class ItemMenu {
  final String? svgName;
  final IconData? iconData;
  final EdgeInsetsGeometry? contentPadding;
  final String routeName;
  final MenuOptionsMenuEnum id;
  final String title;
  bool? isVisible;

  ItemMenu({required this.id, required this.title, required  this.routeName, this.svgName, this.iconData, this.contentPadding, this.isVisible = true });
}