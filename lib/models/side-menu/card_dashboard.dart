
class CardDashboard {
  final String backgroundName;
  final String foregroundName;
  final String title;
  final String routeName;

  CardDashboard({ required this.backgroundName, required this.foregroundName, required this.title, required this.routeName });
  
}