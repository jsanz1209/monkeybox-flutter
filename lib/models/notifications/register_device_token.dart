class RegisterDeviceTokenRequest {
  final String deviceToken;

  RegisterDeviceTokenRequest({required this.deviceToken});

  factory RegisterDeviceTokenRequest.fromJson(Map<String, dynamic> json) {
    return RegisterDeviceTokenRequest(deviceToken: json['device_token']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['device_token'] = this.deviceToken;
    return data;
  }
}
