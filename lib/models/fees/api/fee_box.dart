class FeesBox {
  final int boxID;
  final int competitor;
  final String createdAt;
  final int createdBy;
  final String currencyCode;
  final String? description;
  final int? fortnight;
  final int id;
  final int isDeleted;
  final String name;
  final int quantity;
  final int? sessionsPerWeek;
  final int temporal;
  final int period;
  final String status;
  final int private;
  final String updatedAt;
  final int updatedBy;

  FeesBox({
    required this.boxID,
    required this.competitor,
    required this.currencyCode,
    required this.description,
    required this.fortnight,
    required this.createdAt,
    required this.createdBy,
    required this.id,
    required this.isDeleted,
    required this.name,
    required this.quantity,
    required this.sessionsPerWeek,
    required this.temporal,
    required this.period,
    required this.status,
    required this.private,
    required this.updatedAt,
    required this.updatedBy,
  });

  factory FeesBox.fromJson(Map<String, dynamic> json) {
    return FeesBox(
      boxID: json['box_id'],
      competitor: json['competitor'],
      currencyCode: json['currency_code'],
      description: json['description'],
      fortnight: json['fortnight'],
      createdAt: json['created_at'],
      createdBy: json['created_by'],
      id: json['id'],
      isDeleted: json['is_deleted'],
      name: json['name'],
      quantity: json['quantity'],
      sessionsPerWeek: json['sessions_per_week'],
      temporal: json['temporal'],
      period: json['period'],
      status: json['status'],
      private: json['private'],
      updatedAt: json['updated_at'],
      updatedBy: json['updated_by'],
    );
  }
}
