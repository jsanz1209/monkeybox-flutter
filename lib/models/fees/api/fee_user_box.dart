import 'package:monkeybox/models/fees/api/bonus_user_selected.dart';

class FeeUserBox {
  final BonusUserSelected? bonus;
  final int? boxID;
  final int? feeID;
  final int id;
  final bool isCompetitor;
  final int isDeleted;
  final String status;
  final int userID;

  FeeUserBox({
    required this.bonus,
    required this.boxID,
    required this.feeID,
    required this.id,
    required this.isCompetitor,
    required this.isDeleted,
    required this.status,
    required this.userID,
  });

  factory FeeUserBox.fromJson(Map<String, dynamic> json) {
    return FeeUserBox(
      bonus: json['bonus'] != null ? BonusUserSelected.fromJson(json['bonus']) : null,
      boxID: json['box_id'],
      feeID: json['fee_id'],
      id: json['id'],
      isCompetitor: json['isCompetitor'],
      isDeleted: json['is_deleted'],
      status: json['status'],
      userID: json['user_id'],
    );
  }
}