
import 'package:monkeybox/models/fees/api/bonus_user.dart';

class BonusUserSelected {
  final bool administrationFees;
  final String createdAt;
  final int createdBy;
  final String endDate;
  final int id;
  final int isDeleted;
  final String? issueDate;
  final String? name;
  final int? origin;
  final int quantity;
  final int serviceID;
  final int? sessionsPerWeek;
  final String startDate;
  final String status;
  final String type;
  final String updatedAt;
  final int updatedBy;
  final int userBoxID;
  final int consumedSessions;
  final int sessionsNumber;
  final BonusUser? fee;

  BonusUserSelected({
    required this.administrationFees,
    required this.createdAt,
    required this.createdBy,
    required this.endDate,
    required this.id,
    required this.isDeleted,
    required this.issueDate,
    required this.name,
    required this.origin,
    required this.quantity,
    required this.serviceID,
    required this.sessionsPerWeek,
    required this.startDate,
    required this.status,
    required this.type,
    required this.updatedAt,
    required this.updatedBy,
    required this.userBoxID,
    required this.consumedSessions,
    required this.sessionsNumber,
    required this.fee
  });

  factory BonusUserSelected.fromJson(Map<String, dynamic> json) {
    return BonusUserSelected(
      administrationFees: json['administrationFees'],
      consumedSessions: json['consumed_sessions'],
      sessionsNumber: json['sessions_number'],
      createdAt: json['created_at'],
      createdBy: json['created_by'],
      endDate: json['end_date'],
      id: json['id'],
      isDeleted: json['is_deleted'],
      issueDate: json['issue_date'],
      name: json['name'],
      origin: json['origin'],
      quantity: json['quantity'],
      serviceID: json['service_id'],
      sessionsPerWeek: json['sessions_per_week'],
      startDate: json['start_date'],
      status: json['status'],
      type: json['type'],
      updatedAt: json['updated_at'],
      updatedBy: json['updated_by'],
      userBoxID: json['user_box_id'],
      fee: json['fee'] != null ? BonusUser.fromJson(json['fee']) : null
    );
  }
}
