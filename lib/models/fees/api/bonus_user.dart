class BonusUser {
  final int boxID;
  final int? consumedSessions;
  final String createdAt;
  final int createdBy;
  final String currencyCode;
  final int expirationMonths;
  final int id;
  final int isDeleted;
  final String languageCode;
  final String name;
  final int quantity;
  final int sessionsNumber;
  final int? sessionsPerWeek;
  final String updatedAt;
  final int updatedBy;

  BonusUser({
    required this.boxID,
    required this.consumedSessions,
    required this.createdAt,
    required this.createdBy,
    required this.currencyCode,
    required this.expirationMonths,
    required this.id,
    required this.isDeleted,
    required this.languageCode,
    required this.name,
    required this.quantity,
    required this.sessionsNumber,
    required this.sessionsPerWeek,
    required this.updatedAt,
    required this.updatedBy,
  });

  factory BonusUser.fromJson(Map<String, dynamic> json) {
    return BonusUser(
      boxID: json['box_id'],
      consumedSessions: json['consumed_sessions'],
      createdAt: json['created_at'],
      createdBy: json['created_by'],
      currencyCode: json['currency_code'],
      expirationMonths: json['expiration_months'],
      id: json['id'],
      isDeleted: json['is_deleted'],
      languageCode: json['language_code'],
      name: json['name'],
      quantity: json['quantity'],
      sessionsNumber: json['sessions_number'],
      sessionsPerWeek: json['sessions_per_week'],
      updatedAt: json['updated_at'],
      updatedBy: json['updated_by'],
    );
  }
}
