class FeeUser {
  final bool administrationFees;
  final String createdAt;
  final int createdBy;
  final String endDate;
  final int id;
  final int isDeleted;
  final String? name;
  final String? issueDate;
  final int quantity;
  final int? origin;
  final int serviceID;
  final int? sessionsPerWeek;
  final String startDate;
  final String status;
  final String? type;
  final int userBoxID;
  final String updatedAt;
  final int updatedBy;

  FeeUser(
      {required this.administrationFees,
      required this.createdAt,
      required this.createdBy,
      required this.endDate,
      required this.id,
      required this.isDeleted,
      required this.name,
      required this.issueDate,
      required this.quantity,
      required this.origin,
      required this.serviceID,
      required this.sessionsPerWeek,
      required this.startDate,
      required this.status,
      required this.type,
      required this.userBoxID,
      required this.updatedAt,
      required this.updatedBy});

  factory FeeUser.fromJson(Map<String, dynamic> json) {
    return FeeUser(
      administrationFees: json['administrationFees'],
      createdAt: json['created_at'],
      createdBy: json['created_by'],
      endDate: json['end_date'],
      id: json['id'],
      isDeleted: json['is_deleted'],
      name: json['name'],
      issueDate: json['issue_date'],
      quantity: json['quantity'],
      origin: json['origin'],
      serviceID: json['service_id'],
      sessionsPerWeek: json['sessions_per_week'],
      startDate: json['start_date'],
      status: json['status'],
      type: json['types'],
      userBoxID: json['user_box_id'],
      updatedAt: json['updated_at'],
      updatedBy: json['updated_by'],
    );
  }
}
