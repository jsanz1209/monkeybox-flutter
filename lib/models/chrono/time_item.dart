class TimeItem {
  late int time;

  TimeItem({required this.time});

  String getTime() {
    if (time == -1) {
      return 'NO';
    }
    if (time < 60) {
      return '$time segundos';
    } else {
      int minutes = (time ~/ 60);
      int seconds = time % 60;

      if (seconds == 0) {
        return '$minutes ${minutes > 1 ? 'minutos' : 'minuto'}';
      } else {
      return '${getFormatBase10(minutes)}:${getFormatBase10(seconds)} minutos';
      }
    }
  }

  String geTimeMinutes() {
    if (time == -1) {
      return 'NO';
    }
    if (time < 60) {
      return '00:${getFormatBase10(time)}';
    } else {
      int minutes = (time ~/ 60);
      int seconds = time % 60;

      return '${getFormatBase10(minutes)}:${getFormatBase10(seconds)}';
    }
  }

  getFormatBase10(int value) {
    if (value < 10) {
      return '0$value';
    }
    return value;
  }
}
