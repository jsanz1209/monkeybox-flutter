class Weight {
  String? startDate = '';
  String? endDate = '';
  double? imc = -1;
  num? weight;

  Weight();

  Weight.fromWeight(
      {required this.startDate,
      required this.endDate,
      required this.weight,
      required this.imc});

  Weight copyWith({String? startDate, String? endDate, double? imc, num? weight}) {
    return Weight.fromWeight(
        imc: imc ?? this.imc,
        weight: weight ?? this.weight,
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate);
  }
}
