

import 'package:monkeybox/models/user/api/user.dart';

class Reservation {
  late int id;
  late int userId;
  late int trainingId;
  late String date;
  int? queue;
  late int boxId;
  late String startTime;
  late String endTime;
  User? user;

  Reservation(
      {required this.id,
      required this.userId,
      required this.trainingId,
      required this.date,
      this.queue,
      required this.boxId,
      required this.startTime,
      required this.endTime,
      required this.user});

  factory Reservation.fromJson(Map<String, dynamic> map) {
    User? user;
    if (map.containsKey('user')) {
      user = User.fromJson(map['user']);
    }
    return Reservation(
      id: map['id'],
      userId: map['user_id'],
      trainingId: map['training_id'],
      date: map['date'],
      queue: map['queue'],
      boxId: map['box_id'],
      startTime: map['start_time'],
      endTime: map['end_time'],
      user: user,
    );
  }
}
