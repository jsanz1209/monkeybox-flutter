import 'package:monkeybox/models/user/api/user.dart';

class TrainingReservation {
  final dynamic cancelTime;
  final String createdAt;
  final int createdBy;
  final String date;
  final int id;
  final bool lessThanMin;
  final dynamic minAthletes;
  final int queue;
  final String queueAutomatic;
  final int trainingType;
  final int trainingId;
  final String updatedAt;
  final int updatedBy;
  final User? user;

  TrainingReservation(
      {required this.cancelTime,
      required this.createdAt,
      required this.createdBy,
      required this.date,
      required this.id,
      required this.lessThanMin,
      required this.minAthletes,
      required this.queue,
      required this.queueAutomatic,
      required this.trainingType,
      required this.trainingId,
      required this.updatedAt,
      required this.updatedBy,
      required this.user});

  factory TrainingReservation.fromJson(Map<String, dynamic> map) {
    return TrainingReservation(
        cancelTime: map['cancelTime'],
        createdAt: map['created_at'] is String ? map['created_at'] : '',
        createdBy: map['created_by'],
        date: map['date'],
        id: map['id'],
        lessThanMin: map['lessThanMin'],
        minAthletes: map['minAthletes'],
        queue: map['queue'] is bool
            ? (map['queue'] as bool)
                ? 1
                : 0
            : map['queue'],
        queueAutomatic: map['queue_automatic'] is bool
            ? (map['queue_automatic'] as bool)
                ? '1'
                : '0'
            : map['queue_automatic'],
        trainingId: map['training_id'],
        trainingType: map['trainingType'],
        updatedAt: map['updated_at'] is String ? map['updated_at'] : '',
        updatedBy: map['updated_by'],
        user: map['user'] != null ? User.fromJson(map['user']) : null);
  }
}
