
class ReservationWODS {
  final String createdAt;
  final int createdBy;
  final String date;
  final int id;
  final int queue;
  final String queueAutomatic;
  final int trainingId;
  final String updatedAt;
  final int updatedBy;
  final int userId;

  ReservationWODS(
      {required this.createdAt,
      required this.createdBy,
      required this.date,
      required this.id,
      required this.queue,
      required this.queueAutomatic,
      required this.trainingId,
      required this.updatedAt,
      required this.updatedBy,
      required this.userId});

  factory ReservationWODS.fromJson(Map<String, dynamic> json) {
    return ReservationWODS(
        createdAt: json['created_at'],
        createdBy: json['created_by'],
        date: json['date'],
        id: json['id'],
        queue: json['queue'],
        queueAutomatic: json['queue_automatic'],
        trainingId: json['training_id'],
        updatedAt: json['updated_at'],
        updatedBy: json['updated_by'],
        userId: json['user_id']);
  }
}
