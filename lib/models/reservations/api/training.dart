import 'package:monkeybox/models/reservations/api/training_reservation.dart';

class Training {
  final int boxId;
  final int capacity;
  final String createdAt;
  final int createdBy;
  final String? customDate;
  final String endTime;
  final int id;
  final int isDeleted;
  final String startTime;
  final int type;
  final String updatedAt;
  final int updatedBy;
  final int? weekDay;
  final List<TrainingReservation> reservations;

  Training(
      {required this.boxId,
      required this.capacity,
      required this.createdAt,
      required this.createdBy,
      required this.customDate,
      required this.endTime,
      required this.id,
      required this.isDeleted,
      required this.startTime,
      required this.type,
      required this.updatedAt,
      required this.updatedBy,
      required this.weekDay,
      required this.reservations});

  factory Training.fromJson(Map<String, dynamic> map) {
    return Training(
        boxId: map['box_id'],
        capacity: map['capacity'],
        createdAt: map['created_at'],
        createdBy: map['created_by'],
        customDate: map['custom_date'],
        endTime: map['end_time'],
        id: map['id'],
        isDeleted: map['is_deleted'],
        reservations: (map['reservations'] as List<dynamic>).map((item) {
          return TrainingReservation.fromJson(item);
        }).toList(),
        startTime: map['start_time'],
        type: map['type'],
        updatedAt: map['updated_at'],
        updatedBy: map['updated_by'],
        weekDay: map['weed_day']);
  }
}
