import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:monkeybox/constants/settings.dart';

class InfoBOX {
  final int id;
  final String? name;
  final String? logo;
  final String? address;
  final String? zipCode;
  final String? phoneNumber;
  final String? facebookAccount;
  final String? twitterAccount;
  final String? instagramAccount;
  final String? privacyPolicy;
  final String? rules;
  final String? normative;
  final String? logoLoginUrl;
  final String? logoLoadingUrl;
  final String? logoContactUrl;
  final String? logoLogin;
  final String? logoLoading;
  final String? logoContact;
  final String? rankingBackgroundUrl;
  final String? wodsBackgroundUrl;
  final String? reservationsBackgroundUrl;
  final String? rankingBackground;
  final String? wodsBackground;
  final String? reservationsBackground;
  final bool? isOpen;
  final int? onlinePayment;
  final String? infoEmail;
  final int? reserveUntil;
  final String? stripeCallbackToken;
  final String? stripePk;
  final int? administrationFees;

  InfoBOX(
      {required this.id,
      this.name,
      this.logo,
      this.address,
      this.zipCode,
      this.phoneNumber,
      this.facebookAccount,
      this.twitterAccount,
      this.instagramAccount,
      this.privacyPolicy,
      this.rules,
      this.normative,
      this.logoLoginUrl,
      this.logoLoadingUrl,
      this.logoContactUrl,
      this.logoLogin,
      this.logoLoading,
      this.logoContact,
      this.rankingBackgroundUrl,
      this.wodsBackgroundUrl,
      this.reservationsBackgroundUrl,
      this.rankingBackground,
      this.wodsBackground,
      this.reservationsBackground,
      this.isOpen,
      this.onlinePayment,
      this.infoEmail,
      this.reserveUntil,
      this.stripeCallbackToken,
      this.stripePk,
      this.administrationFees});

  factory InfoBOX.fromJson(Map<String, dynamic> json) {
    return InfoBOX(
        id: json['id'],
        address: json['address'],
        facebookAccount: json['facebook_account'],
        infoEmail: json['info_email'],
        instagramAccount: json['instagram_account'],
        isOpen: json['is_open'],
        logo: json['logo'],
        logoContactUrl: json['logoContactUrl'],
        logoLoadingUrl: json['logoLoadingUrl'],
        logoLoginUrl: json['logoLoginUrl'],
        logoContact: json['logo_contact'],
        logoLoading: json['logo_loading'],
        logoLogin: json['logo_login'],
        name: json['name'],
        normative: json['normative'],
        onlinePayment: json['online_payment'],
        phoneNumber: json['phone_number'],
        privacyPolicy: json['privacy_policy'],
        rankingBackgroundUrl: json['rankingBackgroundUrl'],
        rankingBackground: json['ranking_background'],
        reservationsBackgroundUrl: json['reservationsBackgroundUrl'],
        reservationsBackground: json['reservations_background'],
        rules: json['rules'],
        twitterAccount: json['twitter_account'],
        wodsBackgroundUrl: json['wodsBackgroundUrl'],
        wodsBackground: json['wods_background'],
        reserveUntil: json['reserve_until'],
        zipCode: json['zip_code'],
        stripeCallbackToken: json['stripe_callback_token'],
        stripePk: json['stripe_pk'],
        administrationFees: json['administration_fees']);
  }

  getWodsBackgroundAsWidget() {
    if (this.wodsBackground != null && this.wodsBackgroundUrl != null) {
      try {
        return CachedNetworkImageProvider(
            Settings.apiURL + this.wodsBackgroundUrl!);
      } on Exception catch (_) {
        return AssetImage('assets/images/dash1.jpg');
      }
    } else {
      return AssetImage('assets/images/dash1.jpg');
    }
  }

  getReservationsBackgroundAsWidget() {
    if (this.reservationsBackground != null &&
        this.reservationsBackgroundUrl != null) {
      try {
        return CachedNetworkImageProvider(
          Settings.apiURL + this.reservationsBackgroundUrl!,
        );
      } on Exception catch (_) {
        return AssetImage('assets/images/dash2.jpg');
      }
    } else {
      return AssetImage('assets/images/dash2.jpg');
    }
  }

  getLogoContactAsWidget() {
    if (this.logoContact != null && this.logoContactUrl != null) {
      try {
        return CachedNetworkImageProvider(
          Settings.apiURL + this.logoContactUrl!,
        );
      } on Exception catch (_) {
        return AssetImage('assets/images/logo_login.png');
      }
    } else {
      return AssetImage('assets/images/logo_login.png');
    }
  }
}
