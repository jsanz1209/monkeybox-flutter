class Box {
  final int id;
  final String name;

  Box({required this.id, required this.name});

  factory Box.fromJson(Map<String, dynamic> json) {
    return Box(id: json['id'], name: json['name']);
  }
}
