class AccountRequest {
  final String email;
  final String username;

  AccountRequest({ required this.email, required this.username});

    Map<String, dynamic> toJson() {
    return {
      'email': email,
      'username': username,
    };
  }
}