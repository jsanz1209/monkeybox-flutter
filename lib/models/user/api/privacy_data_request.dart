import 'package:monkeybox/models/user/api/profile_data_request.dart';
import 'package:monkeybox/models/user/api/social_network_data_request.dart';

class PrivacyData {
  final SocialNetworkData socialNetworks;
  final ProfileData profile;

  PrivacyData({required this.socialNetworks, required this.profile});

  Map<String, dynamic> toJson() {
    return {
      'social_networks': socialNetworks.toJson(),
      'profile': profile.toJson()
    };
  }
}