
import 'package:monkeybox/models/user/api/privacy_data_request.dart';

class UserRequest {
  final String name;
  final String firstSurname;
  final String secondSurname;
  final String birthdate;
  final String gender;
  final String? weight;
  final String height;
  final int? intensityCategoryID;
  final PrivacyData privacy;

  UserRequest(
      {
      required this.name,
      required this.firstSurname,
      required this.secondSurname,
      required this.birthdate,
      required this.gender,
      required this.weight,
      required this.height,
      required this.intensityCategoryID,
      required this.privacy});

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'first_surname': firstSurname,
      'second_surname': secondSurname,
      'birthdate': birthdate,
      'gender': gender,
      'weight': weight,
      'height': height,
      'intensity_category_id': intensityCategoryID,
      'privacy': privacy.toJson()
    };
  }
}
