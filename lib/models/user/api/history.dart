class History {
  dynamic startDate;
  dynamic endDate;
  String status;

  History(
      {required this.startDate, required this.endDate, required this.status});

  factory History.fromJson(Map<String, dynamic> json) => History(
      startDate: json['start_date'],
      endDate: json['end_date'],
      status: json['status']);

  Map<String, dynamic> toJson() {
    return {'start_date': startDate, 'end_date': endDate, 'status': status};
  }
}
