import 'package:monkeybox/models/user/api/profile.dart';
import 'package:monkeybox/models/user/api/role.dart';
import 'package:monkeybox/models/user/api/user_boxes.dart';

class User {
  final String email;
  final int id;
  final String? username;
  final Profile? profile;
  final List<Role> roles;
  final List<UserBoxes> userBoxes;

  User(
      {required this.email,
      required this.id,
      required this.username,
      required this.profile,
      required this.roles,
      required this.userBoxes});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        email: json['email'],
        id: json['id'] ?? json['user_id'],
        username: json['username'],
        profile: json['profile'] != null ? Profile.fromJson(json['profile']) : null,
        roles: json['roles'] != null
            ? (json['roles'] as List<dynamic>).map((item) {
                return Role.fromJson(item);
              }).toList()
            : [],
        userBoxes: json['userBoxes'] != null
            ? (json['userBoxes'] as List<dynamic>).map((item) {
                return UserBoxes.fromJson(item);
              }).toList()
            : []);
  }
}
