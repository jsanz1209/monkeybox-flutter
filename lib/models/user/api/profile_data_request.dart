class ProfileData {
  final bool firstSurname;
  final bool secondSurname;
  final bool gender;
  final bool birthdate;
  final bool age;
  final bool weight;
  final bool height;
  final bool favoriteCategory;
  final bool instagram;
  final bool facebook;
  final bool twitter;
  final bool whatsapp;

  ProfileData({
      required this.firstSurname,
      required this.secondSurname,
      required this.gender,
      required this.birthdate,
      required this.age,
      required this.weight,
      required this.height,
      required this.favoriteCategory,
      required this.instagram,
      required this.facebook,
      required this.twitter,
      required this.whatsapp
  });

  Map<String, dynamic> toJson() {
    return {
      'first_surname': firstSurname,
      'second_surname': secondSurname,
      'gender': gender,
      'birthdate': birthdate,
      'age': age,
      'weight': weight,
      'height': height,
      'favorite_category': favoriteCategory,
      'instagram': instagram,
      'facebook': facebook,
      'twitter': twitter,
      'whatsapp': whatsapp
    };
  }
}
