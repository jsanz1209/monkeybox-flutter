class Privacy {
  late bool? firstSurname;
  late bool? secondSurname;
  late bool? gender;
  late bool? birthdate;
  late bool? age;
  late bool? weight;
  late bool? height;
  late bool? favoriteCategory;
  late dynamic whatsapp;
  late String? instagram;
  late String? facebook;
  late String? twitter;
  late bool? showInstagram;
  late bool? showFacebook;
  late bool? showTwitter;
  late bool? showWhatsapp;

  Privacy(
      {required this.firstSurname,
      required this.secondSurname,
      required this.gender,
      required this.birthdate,
      required this.age,
      required this.weight,
      required this.height,
      required this.favoriteCategory,
      required this.whatsapp,
      this.instagram,
      this.facebook,
      this.twitter,
      required this.showInstagram,
      required this.showFacebook,
      required this.showTwitter,
      required this.showWhatsapp});

  factory Privacy.fromJson(Map<String, dynamic> json) {
    return Privacy(
        firstSurname: json['first_surname'],
        secondSurname: json['second_surname'],
        gender: json['gender'],
        birthdate: json['birthdate'],
        age: json['age'],
        weight: json['weight'],
        height: json['height'],
        favoriteCategory: json['favorite_category'],
        whatsapp: json['whatsapp'],
        showInstagram: json['showInstagram'],
        showFacebook: json['showFacebook'],
        facebook: json['facebook'],
        twitter: json['twitter'],
        instagram: json['instagram'],
        showTwitter: json['showTwitter'],
        showWhatsapp: json['showWhatsapp']);
  }

    factory Privacy.fromJsonRanking(Map<String, dynamic> map) {
    return Privacy(
        instagram: (map['social_networks']['instagram'] ?? ''),
        facebook: (map['social_networks']['facebook'] ?? ''),
        twitter: (map['social_networks']['twitter'] ?? ''),
        whatsapp: map['social_networks']['whatsapp'],
        firstSurname: map['profile']['first_surname'],
        secondSurname: map['profile']['second_surname'],
        gender: map['profile']['gender'],
        birthdate: map['profile']['birthdate'],
        age: map['profile']['age'],
        weight: map['profile']['weight'],
        height: map['profile']['height'],
        favoriteCategory: map['profile']['favorite_category'],
        showInstagram: map['profile']['instagram'],
        showFacebook: map['profile']['facebook'],
        showTwitter: map['profile']['twitter'],
        showWhatsapp: map['profile']['whatsapp']);
  }
}
