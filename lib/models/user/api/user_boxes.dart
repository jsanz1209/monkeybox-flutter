import 'package:monkeybox/models/fees/api/bonus_user_selected.dart';

class UserBoxes {
  final int boxID;
  final int? feeID;
  final int id;
  final bool isCompetitor;
  final int isDeleted;
  final String status;
  final int userID;
  final BonusUserSelected? bonus;

  UserBoxes(
      {required this.boxID,
      required this.feeID,
      required this.id,
      required this.isCompetitor,
      required this.isDeleted,
      required this.status,
      required this.userID,
      this.bonus});

  factory UserBoxes.fromJson(Map<String, dynamic> json) {
    return UserBoxes(
        boxID: json['box_id'],
        feeID: json['fee_id'],
        id: json['id'],
        isCompetitor: json['isCompetitor'],
        isDeleted: json['is_deleted'],
        status: json['status'],
        userID: json['user_id'],
        bonus:  json['bonus'] != null ? BonusUserSelected.fromJson(json['bonus']) : null);
  }
}
