import 'dart:convert';

import 'package:monkeybox/models/user/api/privacy.dart';
import 'package:monkeybox/models/wods/api/intensity_category.dart';

class Profile {
  final String birthdate;
  final int? blocksVictories;
  final String? countryCode;
  final String email;
  final String firstSurname;
  final String gender;
  final double height;
  final IntensityCategory? intensityCategory;
  final String name;
  final String? phone;
  final String? picture;
  final String? pictureUrl;
  final Privacy? privacy;
  final String? resizedPictureUrl;
  final String secondSurname;
  final int userID;
  final double weight;
  final String wodCount;
  final int wodsVictories;

  Profile({
    required this.birthdate,
    required this.blocksVictories,
    required this.email,
    required this.firstSurname,
    required this.gender,
    required this.height,
    required this.intensityCategory,
    required this.name,
    required this.picture,
    required this.pictureUrl,
    required this.privacy,
    required this.resizedPictureUrl,
    required this.secondSurname,
    required this.userID,
    required this.weight,
    required this.wodCount,
    required this.wodsVictories,
    this.countryCode,
    this.phone,
  });

  Profile copyWith({
    String? birthdate,
    int? blocksVictories,
    String? email,
    String? firstSurname,
    String? gender,
    double? height,
    IntensityCategory? intensityCategory,
    String? name,
    String? picture,
    String? pictureUrl,
    Privacy? privacy,
    String? resizedPictureUrl,
    String? secondSurname,
    int? userID,
    double? weight,
    String? wodCount,
    int? wodsVictories,
    String? countryCode,
    String? phone,
  }) {
    return Profile(
        birthdate: birthdate ?? this.birthdate,
        blocksVictories: blocksVictories ?? this.blocksVictories,
        email: email ?? this.email,
        firstSurname: firstSurname ?? this.firstSurname,
        gender: gender ?? this.gender,
        height: height ?? this.height,
        intensityCategory: intensityCategory ?? this.intensityCategory,
        name: name ?? this.name,
        picture: picture ?? this.picture,
        pictureUrl: pictureUrl ?? this.pictureUrl,
        privacy: privacy ?? this.privacy,
        resizedPictureUrl: resizedPictureUrl ?? this.resizedPictureUrl,
        secondSurname: secondSurname ?? this.secondSurname,
        userID: userID ?? this.userID,
        weight: weight ?? this.weight,
        wodCount: wodCount ?? this.wodCount,
        wodsVictories: wodsVictories ?? this.wodsVictories,
        countryCode: countryCode ?? this.countryCode,
        phone: phone ?? this.phone);
  }

  factory Profile.fromJson(Map<String, dynamic> json) {
    return Profile(
        birthdate: json['birthdate'],
        blocksVictories: json['blocksVictories'],
        email: json['email'],
        firstSurname: json['first_surname'],
        gender: json['gender'],
        height: json['height'] != null
            ? (json['height'] is double)
                ? json['height']
                : (json['height'] is int)
                    ? (json['height'] as int).toDouble()
                    : double.parse(json['height'])
            : 0,
        intensityCategory:
            json['intensityCategory'] != null ? IntensityCategory.fromJson(json['intensityCategory']) : null,
        name: json['name'],
        picture: json['picture'],
        pictureUrl: json['pictureUrl'],
        privacy: json['privacy'] != null ? json['privacy'] is String
            ? Privacy.fromJsonRanking(jsonDecode(json['privacy']))
            : Privacy.fromJson(json['privacy']) : null,
        resizedPictureUrl: json['resizedPictureUrl'],
        secondSurname: json['second_surname'],
        userID: json['user_id'],
        weight: double.parse((json['weight'] ?? 0.0).toString()),
        wodCount: json['wodCount'],
        countryCode: json['countryCode'],
        wodsVictories: json['wodsVictories'] == null ? 0 : json['wodsVictories']
    );
  }

  factory Profile.fromJsonRanking(Map<String, dynamic> map) {
    return Profile(
        userID: map['user_id'],
        name: (map['name'] ?? ''),
        firstSurname: (map['first_surname'] ?? ''),
        secondSurname: (map['second_surname'] ?? ''),
        birthdate: (map['birthdate'] ?? ''),
        gender: (map['gender'] ?? ''),
        picture: (map['picture'] ?? ''),
        pictureUrl: (map['pictureUrl'] ?? ''),
        resizedPictureUrl: (map['resizedPictureUrl'] ?? ''),
        height: double.parse((map['height'] ?? 0.0).toString()),
        wodCount: map['wodCount'],
        email: map['email'],
        weight: double.parse((map['weight'] ?? 0.0).toString()),
        wodsVictories: map['wodsVictories'],
        blocksVictories: map['blocksVictories'],
        phone: (map['phone'] ?? ''),
        countryCode: (map['countryCode'] ?? ''),
        intensityCategory: IntensityCategory.fromJson(map['intensityCategory']),
        privacy: Privacy.fromJson(map['privacy']));
  }

  getFullName() {
    return '${this.name} ${this.firstSurname} ${this.secondSurname}';
  }

  DateTime getBirthdateAsDateTime() {
    return DateTime.parse(birthdate);
  }

  getAge() {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - getBirthdateAsDateTime().year;
    int month1 = currentDate.month;
    int month2 = getBirthdateAsDateTime().month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = getBirthdateAsDateTime().day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }
}
