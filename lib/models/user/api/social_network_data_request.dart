class SocialNetworkData {
  final String? instagram;
  final String? facebook;
  final String? twitter;
  final String? whatsapp;

  SocialNetworkData({
      this.instagram, this.facebook, this.twitter, this.whatsapp});

  Map<String, dynamic> toJson() {
    return {
      'instagram': instagram,
      'facebook': facebook,
      'twitter': twitter,
      'whatsapp': whatsapp
    };
  }
}