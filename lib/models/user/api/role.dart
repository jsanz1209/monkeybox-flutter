class Role {
  final int? boxId;
  final String roleName;

  Role({required this.boxId, required this.roleName});

  factory Role.fromJson(Map<String, dynamic> json) {
    return Role(boxId: json['boxId'], roleName: json['roleName']);
  }
}