class UpdateRMRequest {
  final String date;
  final double kgLb;
  final int exerciseId;
  final int repetitions;
  final double weight;

  UpdateRMRequest(
      {required this.date,
      required this.kgLb,
      required this.exerciseId,
      required this.repetitions,
      required this.weight});

  factory UpdateRMRequest.fromJson(Map<String, dynamic> json) {
    return UpdateRMRequest(
        date: json['date'],
        kgLb: json['kg_lb'],
        exerciseId: json['exercise_id'],
        repetitions: json['repetitions'],
        weight: json['weight']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['kg_lb'] = this.kgLb;
    data['exercise_id'] = this.exerciseId;
    data['repetitions'] = this.repetitions;
    data['weight'] = this.weight;
    return data;
  }
}
