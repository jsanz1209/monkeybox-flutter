class DescriptionExercise {
  final int? categoryID;
  final String createdAt;
  final int createdBy;
  final int exerciseID;
  final String explanation;
  final int id;
  final int isDeleted;
  final String languageCode;
  final String name;
  final String updatedAt;
  final int updatedBy;

  DescriptionExercise(
      {required this.categoryID,
      required this.createdAt,
      required this.createdBy,
      required this.exerciseID,
      required this.explanation,
      required this.id,
      required this.isDeleted,
      required this.languageCode,
      required this.name,
      required this.updatedAt,
      required this.updatedBy});

  factory DescriptionExercise.fromJson(Map<String, dynamic> json) {
    return DescriptionExercise(
      categoryID: json['category_id'],
      createdAt: json['created_at'],
      createdBy: json['created_by'],
      exerciseID: json['exercise_id'],
      explanation: json['explanation'],
      isDeleted: json['is_deleted'],
      id: json['id'],
      languageCode: json['language_code'],
      name: json['name'],
      updatedAt: json['updated_at'],
      updatedBy: json['updated_by'],
    );
  }
}
