class BodyWeightsData {
  int? id;
  int? userId;
  double? imc;
  num? weight;
  String? createdAt;
  String? updatedAt;
  int? createdBy;
  int? updatedBy;
  int? isDeleted;
  String? date;

  BodyWeightsData(
      {this.id,
      this.userId,
      this.imc,
      this.weight,
      this.createdAt,
      this.updatedAt,
      this.createdBy,
      this.updatedBy,
      this.isDeleted,
      this.date});

  factory BodyWeightsData.fromJson(Map<String, dynamic> json) {
    return BodyWeightsData(
        id: json['id'],
        userId: json['user_id'],
        imc: double.parse(json['imc'].toString()),
        weight: json['weight'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        createdBy: json['created_by'],
        updatedBy: json['updated_by'],
        isDeleted: json['is_deleted'],
        date: json['date']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['imc'] = this.imc;
    data['weight'] = this.weight;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['is_deleted'] = this.isDeleted;
    data['date'] = this.date;
    return data;
  }
}
