class AddUpdateWeightRequest {
  final String date;
  final num weight;

  AddUpdateWeightRequest(
      {required this.date,     
      required this.weight});

  factory AddUpdateWeightRequest.fromJson(Map<String, dynamic> json) {
    return AddUpdateWeightRequest(
        date: json['date'],      
        weight: json['weight']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;  
    data['weight'] = this.weight;
    return data;
  }
}
