import 'package:monkeybox/models/sport-data/api/rm_data.dart';

class RMResponse {
  final List<RMData> data;
  final int status;
  final bool success;

  RMResponse(
      {required this.data, required this.status, required this.success});

  factory RMResponse.fromJson(Map<String, dynamic> json) {
    return RMResponse(
        data: json['data'] != null
            ? (json['data'] as List<dynamic>).map((item) {
                return RMData.fromJson(item);
              }).toList()
            : [],
        status: json['status'],
        success: json['success']);
  }
}
