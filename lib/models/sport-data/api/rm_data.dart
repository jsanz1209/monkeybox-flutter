class RMData {
  int id;
  int distance;
  int exerciseID;
  int repetitions;
  int? time;
  int unbroken;
  double kgLb;
  double weight;
  String date;
  String createdAt;
  String updatedAt;
  int createdBy;
  int updatedBy;
  int isDeleted;

  RMData(
      {required this.id,
      required this.distance,
      required this.exerciseID,
      required this.time,
      required this.unbroken,
      required this.repetitions,
      required this.kgLb,
      required this.weight,
      required this.date,
      required this.createdAt,
      required this.updatedAt,
      required this.createdBy,
      required this.updatedBy,
      required this.isDeleted});

  factory RMData.fromJson(Map<String, dynamic> json) {
    return RMData(
        id: json['id'],
        distance: json['distance'],
        exerciseID: json['exercise_id'],
        repetitions: json['repetitions'],
        time: json['time'],
        unbroken: json['unbroken'],
        kgLb: double.parse(json['kg_lb'].toString()),
        weight: double.parse(json['weight'].toString()),
        date: json['date'],
        createdAt: json['created_at'],
        createdBy: json['created_by'],
        isDeleted: json['is_deleted'],
        updatedAt: json['updated_at'],
        updatedBy: json['updated_by']);
  }
}
