import 'package:monkeybox/models/sport-data/api/exercise_data.dart';

class ExercisesResponse {
  final List<ExerciseData> data;
  final int status;
  final bool success;

  ExercisesResponse(
      {required this.data, required this.status, required this.success});

  factory ExercisesResponse.fromJson(Map<String, dynamic> json) {
    return ExercisesResponse(
        data: json['data'] != null
            ? (json['data'] as List<dynamic>).map((item) {
                return ExerciseData.fromJson(item);
              }).toList()
            : [],
        status: json['status'],
        success: json['success']);
  }
}
