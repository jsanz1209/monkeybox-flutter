class AddRMRequest {
  final String date;
  final int userId;
  final double kgLb;
  final int exerciseId;
  final int repetitions;
  final double weight;

  AddRMRequest(
      {required this.date,
      required this.userId,
      required this.kgLb,
      required this.exerciseId,
      required this.repetitions,
      required this.weight});

  factory AddRMRequest.fromJson(Map<String, dynamic> json) {
    return AddRMRequest(
        date: json['date'],
        userId: json['user_id'],
        kgLb: json['kg_lb'],
        exerciseId: json['exercise_id'],
        repetitions: json['repetitions'],
        weight: json['weight']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['user_id'] = this.userId;
    data['kg_lb'] = this.kgLb;
    data['exercise_id'] = this.exerciseId;
    data['repetitions'] = this.repetitions;
    data['weight'] = this.weight;
    return data;
  }
}
