import 'package:monkeybox/models/sport-data/api/category_exercise.dart';
import 'package:monkeybox/models/sport-data/api/description_exercise.dart';

class ExerciseData {
  final int boxID;
  final CategoryExercise? category;
  final String code;
  final String createdAt;
  final int createdBy;
  final DescriptionExercise description;
  final int id;
  final int isDeleted;
  final String updatedAt;
  final int updatedBy;

  ExerciseData(
      {required this.boxID,
      required this.category,
      required this.code,
      required this.createdAt,
      required this.createdBy,
      required this.id,
      required this.description,
      required this.isDeleted,
      required this.updatedAt,
      required this.updatedBy});

  factory ExerciseData.fromJson(Map<String, dynamic> json) {
    return ExerciseData(
        boxID: json['box_id'],
        category: json['category'] != null ? CategoryExercise.fromJson(json['category']) : null,
        code: json['code'],
        id: json['id'],
        createdAt: json['created_at'],
        createdBy: json['created_by'],
        description: DescriptionExercise.fromJson(json['description']),
        isDeleted: json['is_deleted'],
        updatedAt: json['updated_at'],
        updatedBy: json['updated_by']);
  }
}
