import 'body_weights_data.dart';

class BodyWeightsResponse {
  final bool success;
  final int status;
  final List<BodyWeightsData> data;

  BodyWeightsResponse({required this.success, required this.status, required this.data});

  factory BodyWeightsResponse.fromJson(Map<String, dynamic> json) {
    return BodyWeightsResponse(
        success: json['success'],
        status: json['status'],
        data: json['data'] != null
            ? (json['data'] as List<dynamic>).map((item) {
                return BodyWeightsData.fromJson(item);
              }).toList()
            : []);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['data'] = this.data.map((v) => v.toJson()).toList();  
    return data;
  }
}
