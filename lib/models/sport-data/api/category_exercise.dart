class CategoryExercise {
  final String category;
  final String createdAt;
  final int createdBy;
  final int id;
  final int isDeleted;
  final String um;
  final String updatedAt;
  final int updatedBy;

  CategoryExercise(
      {required this.category,
      required this.createdAt,
      required this.createdBy,
      required this.id,
      required this.isDeleted,
      required this.um,
      required this.updatedAt,
      required this.updatedBy});

  factory CategoryExercise.fromJson(Map<String, dynamic> json) {
    return CategoryExercise(
      category: json['category'],
      createdAt: json['created_at'],
      createdBy: json['created_by'],
      id: json['id'],
      isDeleted: json['is_deleted'],
      um: json['um'],
      updatedAt: json['updated_at'],
      updatedBy: json['updated_by'],
    );
  }
}
