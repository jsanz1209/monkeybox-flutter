class RM {
  int? exercise;
  int? reps;
  String? startDate = '';
  String? endDate = '';
  double? weight = 0;
  double? bodyWeight = 0;

  RM();

  RM.fromRM(
      {required this.exercise,
      required this.reps,
      required this.startDate,
      required this.endDate,
      required this.weight,
      required this.bodyWeight});

  RM copyWith({
    int? exercise,
    int? reps,
    String? startDate,
    String? endDate,
    double? weight,
    double? bodyWeight
  }) {
    return RM.fromRM(
        exercise: exercise ?? this.exercise,
        reps: reps ?? this.reps,
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate,
        weight: weight ?? this.weight,
        bodyWeight: bodyWeight ?? this.bodyWeight);
  }
}
