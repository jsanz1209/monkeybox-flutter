import 'package:flutter/material.dart';

class EffortButton {
  final int id;
  final String text;
  final Color color;

  EffortButton({required this.id, required this.text, required this.color});
}
