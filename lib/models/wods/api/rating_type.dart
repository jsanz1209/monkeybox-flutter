class RatingType {
  late int id;
  late int boxId;
  late String description;
  late String name;
  late String order;

  static const TIME = 1;
  static const TIME_ALBACETE = 9;

  static const REPS = 2;
  static const REPS_ALBACETE = 10;

  static const ROUNDS = 3;
  static const ROUNDS_ALBACETE = 11;

  static const CALORIES = 7;
  static const CALORIES_ALBACETE = 15;

  static const KG = 4;
  static const KG_ALBACETE = 12;

  static const KM = 6;
  static const KM_ALBACETE = 14;

  static const M = 5;
  static const M_ALBACETE = 13;

  RatingType(
      {required this.id,
      required this.boxId,
      required this.description,
      required this.name,
      required this.order});

  factory RatingType.fromJson(Map<String, dynamic> map) {
    return RatingType(
      id: map['id'],
      boxId: map['box_id'],
      description: map['description'],
      name: map['name'],
      order: map['order'],
    );
  }
}
