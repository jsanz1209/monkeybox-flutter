class FeedbackUserWod {
  final String createdAt;
  final int createdBy;
  final int effort;
  final int fromCompetitor;
  final int id;
  final int isDeleted;
  final int mood;
  final String updatedAt;
  final int updatedBy;
  final int userID;
  final int wodID;

  FeedbackUserWod(
      {required this.createdAt,
      required this.createdBy,
      required this.effort,
      required this.fromCompetitor,
      required this.id,
      required this.isDeleted,
      required this.mood,
      required this.updatedAt,
      required this.updatedBy,
      required this.userID,
      required this.wodID});

  factory FeedbackUserWod.fromJson(Map<String, dynamic> map) {
    return FeedbackUserWod(
        createdAt: map['created_at'],
        createdBy: map['created_by'],
        effort: map['effort'],
        fromCompetitor: map['from_competitor'],
        id: map['id'],
        isDeleted: map['is_deleted'],
        mood: map['mood'],
        updatedAt: map['updated_at'],
        updatedBy: map['updated_by'],
        userID: map['user_id'],
        wodID: map['wod_id']);
  }
}
