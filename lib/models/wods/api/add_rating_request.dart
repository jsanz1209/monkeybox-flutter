class AddRatingRequest {
  final int userID;
  final int ratingTypeID;
  final int blockID;
  final int intensityCategoryID;
  final int rating;
  final int isComplete;
  final String observations;
  final List<int>? team;

  AddRatingRequest(
      {required this.userID,
      required this.ratingTypeID,
      required this.blockID,
      required this.intensityCategoryID,
      required this.rating,
      required this.isComplete,
      required this.observations,
      this.team});

  Map<String, dynamic> toJson() {
    return {
      'user_id': userID,
      'rating_type_id': ratingTypeID,
      'block_id': blockID,
      'intensity_category_id': intensityCategoryID,
      'rating': rating,
      'is_complete': isComplete,
      'observations': observations,
      'team': team
    };
  }
}
