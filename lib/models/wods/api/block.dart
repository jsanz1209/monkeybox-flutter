import 'package:monkeybox/models/wods/api/rating_type.dart';
class Block {
  late int id;
  late String capTime;
  late String goal;
  late int isTeam;
  late String restText;
  late String title;
  late int wodId;
  late String work;
  late List<RatingType> ratingTypes;

  Block(
      {required this.id,
      required this.capTime,
      required this.goal,
      required this.isTeam,
      required this.restText,
      required this.title,
      required this.wodId,
      required this.work,
      required this.ratingTypes});

  factory Block.fromJson(Map<String, dynamic> map) {
    List<RatingType> _ratingTypes = [];
    for (var item in map['ratingTypes']) {
      _ratingTypes.add(RatingType.fromJson(item));
    }
    return Block(
      id: map['id'],
      capTime: map['cap_time'],
      goal: map['goal'],
      isTeam: map['is_team'],
      restText: map['rest_text'],
      title: map['title'],
      wodId: map['wod_id'],
      work: map['work'],
      ratingTypes: _ratingTypes,
    );
  }

  getParsedRatingTypes() {
    return ratingTypes.map((e) => e.name.toUpperCase()).toList().join(' | ');
  }
}
