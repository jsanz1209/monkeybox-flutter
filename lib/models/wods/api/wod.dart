import 'package:intl/intl.dart';
import 'package:monkeybox/models/wods/api/block.dart';

class Wod {
  late int id;
  late int boxId;
  late int competitor;
  late String date;
  int? flexibilityIntensity;
  String? flexibilityTime;
  int? resistenceIntensity;
  String? resistanceTime;
  int? strengthIntensity;
  String? strengthTime;
  late String title;
  late String warmUp;
  late List<Block> blocks;

  Wod({
    required this.id,
    required this.boxId,
    required this.competitor,
    required this.date,
    this.flexibilityIntensity,
    this.flexibilityTime,
    this.resistenceIntensity,
    this.resistanceTime,
    this.strengthIntensity,
    this.strengthTime,
    required this.title,
    required this.warmUp,
    required this.blocks,
  });

  factory Wod.fromJson(Map<String, dynamic> map) {
    List<Block> _blocks = [];
    for (var item in map['blocks']) {
      _blocks.add(Block.fromJson(item));
    }

    return Wod(
        id: map['id'],
        boxId: map['box_id'],
        competitor: map['competitor'],
        date: map['date'],
        flexibilityIntensity: map['flexibility_intensity'],
        flexibilityTime: map['flexibility_time'],
        resistenceIntensity: map['resistance_intensity'],
        resistanceTime: map['resistance_time'],
        strengthIntensity: map['strength_intensity'],
        strengthTime: map['strength_time'],
        title: map['title'],
        warmUp: map['warm_up'],
        blocks: _blocks);
  }

  getDate() {
    return DateTime.parse(date);
  }

  getFormattedDate(String? format) {
    return format == null
        ? DateFormat('dd/MM/yyy').format(getDate())
        : DateFormat(format).format(getDate());
  }
}
