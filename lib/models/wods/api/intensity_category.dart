class IntensityCategory {
  late int id;
  late String category;

  IntensityCategory({required this.id, required this.category});

  factory IntensityCategory.fromJson(Map<String, dynamic> map) {
    return IntensityCategory(id: map['id'], category: map['category']);
  }
}
