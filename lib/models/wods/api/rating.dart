import 'package:monkeybox/models/user/api/profile.dart';

class Rating {
  int blockID;
  String createdAt;
  int createdBy;
  int id;
  int intensityCategoryID;
  int isComplete;
  int isDeleted;
  String? observations;
  num rating;
  int ratingTypeID;
  List<Profile>? team;
  String updatedAt;
  int updatedBy;
  int userID;
  Profile userInfo;

  Rating(
      {required this.blockID,
      required this.createdAt,
      required this.createdBy,
      required this.id,
      required this.intensityCategoryID,
      required this.isComplete,
      required this.isDeleted,
      this.observations,
      required this.rating,
      required this.ratingTypeID,
      this.team,
      required this.updatedAt,
      required this.updatedBy,
      required this.userID,
      required this.userInfo});

  factory Rating.fromJson(Map<String, dynamic> map) {
    return Rating(
        blockID: map['block_id'],
        createdAt: map['created_at'],
        createdBy: map['created_by'],
        id: map['id'],
        intensityCategoryID: map['intensity_category_id'],
        isComplete: map['is_complete'],
        isDeleted: map['is_deleted'],
        observations: map['observations'],
        rating: map['rating'],
        ratingTypeID: map['rating_type_id'],
        team: map['team'] != null
            ? (map['team'] as List<dynamic>).map((item) {
                return Profile.fromJson(item);
              }).toList()
            : null,
        updatedAt: map['updated_at'],
        updatedBy: map['updated_by'],
        userID: map['user_id'],
        userInfo: Profile.fromJson(map['user_info']));
  }
}
