class FeedbackRequest {
  final String wodID;
  final int effort;
  final int mood;
  final int userID;

  FeedbackRequest(this.wodID, this.effort, this.mood, this.userID);

  Map<String, dynamic> toJson() {
    return {'wod_id': wodID, 'effort': effort, 'mood': mood, 'user_id': userID};
  }
}
