import 'package:flutter/material.dart';

class MoodButton {
  final int id;
  final String text;
  final Color color;
  final String icon;

  MoodButton({required this.id, required this.text, required this.color, required this.icon});
}
