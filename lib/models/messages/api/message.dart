class Message {
  final String createdAt;
  final int createdBy;
  final int id;
  final int idBox;
  final int isDeleted;
  final String message;
  final bool read;
  final String title;
  final String updatedAt;
  final int updatedBy;

  Message(
      {required this.createdAt,
      required this.createdBy,
      required this.id,
      required this.idBox,
      required this.isDeleted,
      required this.message,
      required this.read,
      required this.title,
      required this.updatedAt,
      required this.updatedBy});

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
        createdAt: json['created_at'],
        createdBy: json['created_by'],
        id: json['id'],
        idBox: json['id_box'],
        isDeleted: json['is_deleted'],
        message: json['message'],
        read: json['read'],
        title: json['title'],
        updatedAt: json['updated_at'],
        updatedBy: json['updated_by']);
  }
}
