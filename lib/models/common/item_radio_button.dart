class ItemRadioButton {
  final String id;
  final String label;

  ItemRadioButton(this.id, this.label);
}