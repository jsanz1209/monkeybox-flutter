class ItemDropdownMultiple {
  int id;
  String name;
  String? imageUrl;
  bool isChecked = false;

  ItemDropdownMultiple(this.id, this.name, this.imageUrl, this.isChecked);

}