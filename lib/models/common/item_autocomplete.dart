import 'package:flutter/material.dart';

class ItemAutocomplete {
  int id;
  String name;

  ItemAutocomplete({required this.id, required this.name});

 @override
  String toString() {
    return '$id, $name';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is ItemAutocomplete && other.id == id && other.name == name;
  }

  @override
  int get hashCode => hashValues(id, name);
}