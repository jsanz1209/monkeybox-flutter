import 'package:flutter/material.dart';

class BottomSheet {
  final String title;
  final String subtitle;
  final IconData icon;
  final dynamic value;

  BottomSheet(
      {required this.title,
      required this.subtitle,
      required this.icon,
      required this.value});
}
