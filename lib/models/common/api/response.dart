class ApiResponse {
  bool? success;
  int? status;
  dynamic data;

  ApiResponse({this.success, this.status, this.data});

  factory ApiResponse.fromJson(Map<String, dynamic> map) {
    return ApiResponse(
        success: map['success'], status: map['status'], data: map['data']);
  }
}
