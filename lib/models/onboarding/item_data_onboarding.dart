import 'package:flutter/material.dart';

///Class to hold data for itembuilder in Withbuilder app.
class ItemDataOnboarding {
  final Color color;
  final String image;
  final String title;
  final String description;

  ItemDataOnboarding(this.color, this.image, this.title, this.description);
}