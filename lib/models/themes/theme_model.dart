import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';

enum ThemeType { Chinchilla, Albacete }

class ThemeModel extends ChangeNotifier {
  ThemeData currentTheme = CustomExtraThemeData.getChinchillaTheme();

  toggleTheme(context) {
    currentTheme = CustomExtraThemeData.getThemeData(context);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return notifyListeners();
  }
}