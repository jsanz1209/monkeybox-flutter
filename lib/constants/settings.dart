// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:monkeybox/helpers/hex_color.dart';

class Settings {
  static const String APP_NAME = 'MonkeyBox';

  static const host = 'api.monkeybox.club';
  static const version = 'v1';
  static const apiURLBase = 'https://${Settings.host}';
  static const apiURL = '${Settings.apiURLBase}/${Settings.version}';
  static const apiURLUploads = '${Settings.apiURLBase}/uploads';

  static const COMPLETED_PAYMENT = 'COMPLETED';
  static const CREATED_PAYMENT = 'CREATED';
  static const PROCESSING_PAYMENT = 'PROCESSING';

  static const int CHRONO_TYPE_AMRAP = 1;
  static const int CHRONO_TYPE_FOR_TIME = 2;
  static const int CHRONO_TYPE_EMOM = 3;
  static const int CHRONO_TYPE_TABATA = 4;

  static Color LILAC = HexColor("dc4367");
  static Color DEEP_LILAC= HexColor("b33553");
  static Color ORANGE = HexColor("f0b04a");
  static Color DEEP_ORANGE = HexColor("be8d3e");

}
