import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/common/custom_loading.dart';

class LoadingHelper {
  static bool isShowingLoading = false;

  static showLoading(BuildContext context, [String? message]) {
    if (!LoadingHelper.isShowingLoading) {
      LoadingHelper.isShowingLoading = true;
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
              backgroundColor: Colors.transparent,
              elevation: 12,
              insetPadding: EdgeInsets.zero,
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomLoading(),
                    if (message != null)
                      Padding(
                        padding: const EdgeInsets.all(32.0),
                        child: Text(
                          message,
                          style: Theme.of(context).textTheme.headline2,
                          textAlign: TextAlign.center,
                        ),
                      )
                  ],
                ),
              ));
        },
      );
    }
  }

  static hideLoading(BuildContext context) {
    if (LoadingHelper.isShowingLoading) {
      LoadingHelper.isShowingLoading = false;
      Navigator.of(context).pop();
    }
  }
}
