import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:monkeybox/pages/app.dart';

class Dialogs {
  static void showConfirmDialogCupertino(title, message) {
    showCupertinoDialog(
        context: appNavigatorKey.currentContext!,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(title),
            content: Text(message),
            actions: [
              CupertinoDialogAction(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  static Future<bool> showConfirmDialogMaterial(title, message,
      {withActions = true,
      confirmButtonText = 'Aceptar',
      cancelButtonText = 'Cancelar'}) async {
    List<Widget> actions = [];
    if (cancelButtonText != null) {
      actions.add(TextButton(
          onPressed: () {
            Navigator.of(appNavigatorKey.currentContext!).pop(false);
          },
          child: Text(cancelButtonText,
              style: Theme.of(appNavigatorKey.currentContext!)
                  .textTheme
                  .bodyText2!
                  .copyWith(color: Colors.blue))));
    }
    if (confirmButtonText != null) {
      actions.add(TextButton(
          onPressed: () {
            Navigator.of(appNavigatorKey.currentContext!).pop(true);
          },
          child: Text(confirmButtonText,
              style: Theme.of(appNavigatorKey.currentContext!)
                  .textTheme
                  .bodyText2!
                  .copyWith(color: Colors.blue))));
    }
    return await showDialog(
        context: appNavigatorKey.currentContext!,
        builder: (context) {
          return AlertDialog(
            title: Text(
              title,
              style: Theme.of(appNavigatorKey.currentContext!)
                  .textTheme
                  .headline2!
                  .copyWith(color: Colors.black),
            ),
            content: Text(message),
            actions: withActions ? actions : null,
          );
        });
  }

  static void showDialogCupertinoAndGoTo(title, message,
      {confirmButtonText,
      confirmButtonRoute,
      cancelButtonText,
      cancelButtonRoute}) {
    showCupertinoDialog(
        context: appNavigatorKey.currentContext!,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(title),
            content: Text(message),
            actions: [
              CupertinoDialogAction(
                child: Text(confirmButtonText),
                onPressed: () {
                  Navigator.pushNamed(
                      appNavigatorKey.currentContext!, confirmButtonRoute);
                },
              ),
              CupertinoDialogAction(
                child: Text(cancelButtonText),
                onPressed: () {
                  if (cancelButtonRoute == null) {
                    Navigator.of(appNavigatorKey.currentContext!).pop();
                    return;
                  }
                  Navigator.pushNamed(
                      appNavigatorKey.currentContext!, cancelButtonRoute);
                },
              ),
            ],
          );
        });
  }
}
