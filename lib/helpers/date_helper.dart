import 'package:intl/intl.dart';

class DateHelper {
  static getDateWithFirstDayOfMonth(String date) {
    DateTime dateTimeFrom = DateTime.parse(date);
    DateTime firstDayDateFrom =
        DateTime.utc(dateTimeFrom.year, dateTimeFrom.month, 1);
    return DateFormat('yyyy-MM-dd').format(firstDayDateFrom);
  }

  static getDateWithLastDayOfMonth(String date) {
    DateTime dateTimeTo = DateTime.parse(date);
    DateTime lastDayDateTo = DateTime.utc(
      dateTimeTo.year,
      dateTimeTo.month + 1,
    ).subtract(Duration(days: 1));
    return DateFormat('yyyy-MM-dd').format(lastDayDateTo);
  }
}
