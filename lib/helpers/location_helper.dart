import 'package:http/http.dart' as http;
import 'package:monkeybox/models/common/api/http_exception.dart';
import 'dart:convert' as convert;

import 'package:monkeybox/models/side-menu/place_location.dart';

const googleAPIKey = 'AIzaSyAygKtNh5pfsWpAeNnB0ONEHmI_81yesUg';

class LocationHelper {
  static String generateLocationPreviewImage(
      double latitude, double longitude) {
    return 'https://maps.googleapis.com/maps/api/staticmap?center=$latitude, $longitude&zoom=13&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:%7C$latitude,$longitude&key=$googleAPIKey';
  }

  static Future<String> getPlaceAddress(double lat, double long) async {
    try {
      var url = Uri.parse(
          'https://maps.googleapis.com/maps/api/geocode/json?key=$googleAPIKey&latlng=$lat,$long');
      var response = await http.get(url);
      var jsonCode = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return jsonCode['results'][0]['formatted_address'].toString();
      }
      throw HttpException(jsonCode['error_message']);
    } catch (error) {
      rethrow;
    }
  }

  static Future<PlaceLocation> getPlaceFromAddress(String address) {
    try {
      return Future.delayed(const Duration(milliseconds: 500), () async {
        var url = Uri.parse(
            'https://maps.googleapis.com/maps/api/geocode/json?key=$googleAPIKey&address=$address');
        var response = await http.get(url);
        var jsonCode = convert.jsonDecode(response.body);
        if (response.statusCode == 200) {
          return PlaceLocation(
              latitude: jsonCode['results'][0]['geometry']['location']['lat'],
              longitude: jsonCode['results'][0]['geometry']['location']['lng'],
              address: address);
        }

        throw HttpException(jsonCode['error_message']);
      });
    } catch (error) {
      rethrow;
    }
  }
}
