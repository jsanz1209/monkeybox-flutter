import 'package:flutter/material.dart';

class FormHelper {
  static Map<String, GlobalKey<FormFieldState<dynamic>>> getFormFields(
      List<String> fieldNames) {
    var result = {for (var v in fieldNames) v: GlobalKey<FormFieldState>()};
    return result;
  }

  static getFirstKeyError(
      Map<String, GlobalKey<FormFieldState<dynamic>>> fields) {
    for (var key in fields.entries) {
      if (key.value.currentState != null && key.value.currentState!.hasError) {
        return key.value;
      }
    }
    return null;
  }
}
