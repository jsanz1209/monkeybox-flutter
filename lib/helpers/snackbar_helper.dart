import 'package:flutter/material.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:monkeybox/pages/app.dart';

class SnackBarHelper {
  static Color _getBackgroundColor(
      BuildContext context, TypeSnackBarEnum type) {
    if (type == TypeSnackBarEnum.error) {
      return Theme.of(context).errorColor;
    } else if (type == TypeSnackBarEnum.success) {
      return HexColor('#20dc6a');
    } else if (type == TypeSnackBarEnum.info) {
      return HexColor('#00AFFF');
    }
    return Colors.black;
  }

  static showSnackBar(String title, String message, TypeSnackBarEnum type) {
    var context = appNavigatorKey.currentContext!;
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: const Duration(milliseconds: 8000),
        content: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 3,
              child: Html(data: '<p>$title</p><p>$message</p>', style: {
                'p': Style(
                    margin: EdgeInsets.zero,
                    color: Colors.white,
                    fontFamily: 'Oswald',
                    fontSize: FontSize.rem(0.9),
                    lineHeight: LineHeight.rem(1.15)),
                'a': Style(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                )
              }),
            ),
            Flexible(
              flex: 1,
              child: TextButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  },
                  child: Text('CERRAR',
                      style: Theme.of(context)
                          .textTheme
                          .headline3!
                          .copyWith(color: Colors.white))),
            )
          ],
        ),
        backgroundColor: _getBackgroundColor(context, type)));
  }
}
