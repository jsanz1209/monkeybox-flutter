import 'dart:ui';

extension WindowUnitsHelper on num {
  double get vh =>
      this * window.physicalSize.height / window.devicePixelRatio / 100;
}
