import 'package:monkeybox/pages/app.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FormValidators {
  static const infoIconUnicode = '\u24d8  ';
  static var enabledUnicode = true;

  static String? validatorEmail(String? value) {
    var emailPattern = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    if (value!.isEmpty) {
      return _getMessageText(AppLocalizations.of(appNavigatorKey.currentContext!).emailMandatory);
    } else if (!emailPattern.hasMatch(value)) {
      return _getMessageText(AppLocalizations.of(appNavigatorKey.currentContext!).emailFormat);
    }
    return null;
  }

  static String? validatorPassword(String? value) {
    if (value!.isEmpty) {
      return _getMessageText(AppLocalizations.of(appNavigatorKey.currentContext!).passwordMandatory);
    } else if (value.length < 5) {
      return _getMessageText(
          AppLocalizations.of(appNavigatorKey.currentContext!).passwordFormat);
    }
    return null;
  }

  static String? validatorRequired(Object? value, String message) {
    if (value == null || value is List) {
      return _getMessageText(message);
    } else if (value is String?) {
      if ((value as String?)!.isEmpty) {
        return _getMessageText(message);
      }
    }

    return null;
  }

  static String? validatorTrueRequired(bool? value, String message) {
    if (value == null || value == false) {
      return _getMessageText(message);
    }
    return null;
  }

  static String? validatorMatch(
      String? firstValue, String? secondValue, String message) {
    if (firstValue != secondValue) {
      return _getMessageText(message);
    }
    return null;
  }

  static _getMessageText(String message) {
    if (enabledUnicode) {
      return '$infoIconUnicode $message';
    } else {
      return message;
    }
  }
}
