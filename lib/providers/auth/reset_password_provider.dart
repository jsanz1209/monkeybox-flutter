import 'package:flutter/material.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/auth/api/reset_password_request.dart';
import 'package:monkeybox/providers/auth/api_auth.dart';
import 'package:monkeybox/models/auth/api/change_password_request.dart';

class ResetPasswordProvider with ChangeNotifier {
  Future<void> resetPassword(String email) async {
    ResetPasswordRequest resetPasswordRequest = ResetPasswordRequest(
        email: email, frontendBaseUrl: Settings.apiURLBase);
    try {
      await APIAuth().resetPassword(resetPasswordRequest);
    } catch (error) {
      rethrow;
    }
  }

  Future<void> changePassword(int id, String code, String password) async {
    ChangePasswordRequest changePasswordRequest =
        ChangePasswordRequest(id: id, code: code, password: password);
    try {
      await APIAuth().changePassword(changePasswordRequest);
    } catch (error) {
      rethrow;
    }
  }
}
