import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:monkeybox/models/auth/api/auth_response.dart';
import 'package:monkeybox/models/auth/api/login_request.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/providers/auth/api_auth.dart';
import 'package:monkeybox/providers/user/api_user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider with ChangeNotifier {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
  String? _accessToken;
  int? _userID;
  int? _boxID;

  GlobalKey<NavigatorState> get navigatorKey {
    return _navigatorKey;
  }

  bool get isLoggedIn {
    return _accessToken != null && _boxID != null;
  }

  set token(String? value) {
    _accessToken = value;
    notifyListeners();
  }

  String? get token {
    return _accessToken;
  }

  set userID(int? value) {
    _userID = value;
    notifyListeners();
  }

  int? get userID {
    return _userID;
  }

  set boxID(int? value) {
    _boxID = value;
    notifyListeners();
  }

  int? get boxID {
    return _boxID;
  }

  Future<void> login(LoginRequest loginRequest) async {
    try {
      var authResponse = await APIAuth().login(loginRequest);
      if (authResponse.status == 200) {
        _accessToken = authResponse.data.accessToken;
        _userID = authResponse.data.id;
        _boxID = authResponse.data.boxID;
        notifyListeners();
        saveCurrentUser(authResponse.data);
      }
    } catch (error) {
      rethrow;
    }
  }

  Future<User?> getProfile() async {
    return await APIUser().getProfile();
  }

  Future<bool> tryAutoLogin() async {
    var extractedUserAuth = await currentUser;
    if (extractedUserAuth == null) {
      return false;
    } else {
      _accessToken = extractedUserAuth.accessToken;
      _userID = extractedUserAuth.id;
      _boxID = extractedUserAuth.boxID;
      notifyListeners();
      return true;
    }
  }

  void saveCurrentUser(DataLoginResponse dataLoginResponse) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final userAuth = convert.json.encode(dataLoginResponse.toJson());
    prefs.setString('userAuth', userAuth);
  }

  Future<DataLoginResponse?> get currentUser async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userAuth')) {
      return null;
    }
    final extractedUserAuth = convert.json.decode(prefs.getString('userAuth')!)
        as Map<String, dynamic>;
    return DataLoginResponse.fromJson(extractedUserAuth);
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userAuth');
    _accessToken = null;
    _userID = null;
    _boxID = null;
    notifyListeners();
  }
}
