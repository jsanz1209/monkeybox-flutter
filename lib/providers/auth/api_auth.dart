import 'dart:convert';
import 'dart:convert' as convert;

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/auth/api/auth_response.dart';
import 'package:monkeybox/models/auth/api/change_password_request.dart';
import 'package:monkeybox/models/auth/api/login_request.dart';
import 'package:monkeybox/models/auth/api/reset_password_request.dart';
import 'package:monkeybox/models/auth/api/signup_request.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/providers/http/http_client.dart';

class APIAuth {
  final Client client = HttpClient.client;

  Future<AuthResponse> login(LoginRequest loginRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/login');
      var response = await HttpClient.client
          .post(url, body: convert.jsonEncode(loginRequest));
      var jsonCode = convert.jsonDecode(response.body);
      AuthResponse authResponse = AuthResponse.fromJson(jsonCode);
      return authResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<AuthResponse> signup(SignUpRequest signUpRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/signup');
      var response = await client.post(url, body: jsonEncode(signUpRequest));
      var jsonCode = jsonDecode(response.body);
      AuthResponse authResponse = AuthResponse.fromJson(jsonCode);
      return authResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> resetPassword(
      ResetPasswordRequest resetPasswordRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/password-reset-request');
      var response =
          await client.post(url, body: jsonEncode(resetPasswordRequest));
      var jsonCode = jsonDecode(response.body);
      ApiResponse resetPasswordResponse = ApiResponse.fromJson(jsonCode);
      return resetPasswordResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> changePassword(
      ChangePasswordRequest changePasswordRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/password-reset');
      var response =
          await client.post(url, body: jsonEncode(changePasswordRequest));
      var jsonCode = jsonDecode(response.body);
      ApiResponse resetPasswordResponse = ApiResponse.fromJson(jsonCode);
      return resetPasswordResponse;
    } catch (error) {
      rethrow;
    }
  }
}
