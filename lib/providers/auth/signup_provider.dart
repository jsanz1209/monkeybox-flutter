import 'dart:io';

import 'package:flutter/material.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/auth/api/auth_response.dart';
import 'package:monkeybox/models/auth/api/signup_request.dart';
import 'package:monkeybox/models/box/api/infobox.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/app/app_provider.dart';
import 'package:monkeybox/providers/auth/api_auth.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/box/api_box.dart';
import 'package:monkeybox/providers/user/api_user.dart';
import 'package:provider/provider.dart';

class SignUpProvider with ChangeNotifier {

  InfoBOX? _infoBOX;

  set infoBOX(InfoBOX? value) {
    _infoBOX = value;
    notifyListeners();
  }

  InfoBOX? get infoBOX {
    return _infoBOX;
  }

  Future<void> getInfoBox(int boxID) async {
    try {
      var response = await APIBOX().getInfoBOX(boxID);
      _infoBOX = response;
      notifyListeners();      
    } catch (error) {
      rethrow;
    }
  }

  Future<bool> setData(SignUpRequest signUpRequest) async {
    try {
      var authProvider = Provider.of<AuthProvider>(appNavigatorKey.currentContext!,
          listen: false);
      AuthResponse authResponse = await APIAuth().signup(signUpRequest);
      if (authResponse.success) {
        authProvider.userID = authResponse.data.id;
        authProvider.token = authResponse.data.accessToken;
        await setProfileImage(
            signUpRequest.file, authResponse.data.accessToken);
        var userResponse = await authProvider.getProfile();
        var boxID = userResponse!.userBoxes[0].boxID;
        authProvider.boxID = boxID;
        authResponse.data.boxID = boxID;
        authProvider.saveCurrentUser(authResponse.data);
      }
      return true;
    } catch (error) {
      rethrow;
    }
  }

  Future<void> setProfileImage(File? data, String token) async {
    try {
      var response = await APIUser().setProfileImage(data, token);
      if (response.statusCode == 200) {
        debugPrint('Uploaded!');
      }
    } catch (error) {
      SnackBarHelper.showSnackBar(
          'ERROR',
          'Se ha producido un error al subir la imagen: $error.',
          TypeSnackBarEnum.error);
      rethrow;
    }
  }

  Future<List<List<dynamic>>> fetchData() async {
    var appProvider =
        Provider.of<AppProvider>(appNavigatorKey.currentContext!, listen: false);
    return await appProvider.fetchData();
  }
}
