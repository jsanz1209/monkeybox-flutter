import 'dart:async';

import 'package:flutter/material.dart';
import 'package:monkeybox/models/box/api/infobox.dart';
import 'package:monkeybox/models/messages/api/message.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/app/app_provider.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/box/api_box.dart';
import 'package:monkeybox/providers/loading/loading_provider.dart';
import 'package:monkeybox/providers/messages/api_messages.dart';
import 'package:monkeybox/providers/user/api_user.dart';
import 'package:provider/provider.dart';
import 'package:stripe_sdk/stripe_sdk.dart';

class MenuProvider with ChangeNotifier {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();

  InfoBOX? _infoBOX;
  User? _user;
  String? _status;
  List<Message>? _messages;
  List<Message> _notReadMessages = [];
  String _tagProfileAnimationHero = '';

  GlobalKey<NavigatorState> get navigatorKey {
    return _navigatorKey;
  }

  set tagProfileAnimationHero(value) {
    _tagProfileAnimationHero = value;
  }

  get tagProfileAnimationHero {
    return _tagProfileAnimationHero;
  }

  set infoBOX(InfoBOX? value) {
    _infoBOX = value;
    notifyListeners();
  }

  InfoBOX? get infoBOX {
    return _infoBOX;
  }

  set user(User? value) {
    _user = value;
    notifyListeners();
  }

  User? get user {
    return _user!;
  }

  set status(String? value) {
    _status = value;
    notifyListeners();
  }

  String get status {
    return _status!;
  }

  set messages(List<Message> value) {
    _messages = value;
  }

  List<Message> get messages {
    return _messages!;
  }

  set notReadMessages(List<Message> value) {
    _notReadMessages = value;
    notifyListeners();
  }

  List<Message> get notReadMessages {
    return _notReadMessages;
  }

  Future<void> refreshProfile() async {
      var userProfile = await APIUser().getProfile();
      user = userProfile;
  }

  Future<void> markAsViewedNotification(List<Message> notifications) async {
    BuildContext context = appNavigatorKey.currentContext!;
    var loadingProvider = Provider.of<LoadingProvider>(context, listen: false);
    var authProvider = Provider.of<AuthProvider>(context, listen: false);

    loadingProvider.setIgnoreActiveLoading(true, true);
    loadingProvider.setIgnoreDeactiveLoading(true, true);
    loadingProvider.showLoading();
    try {
      var requests = notifications.map((notification) => APIMessages()
          .markAsViewedNotification(notification.id, authProvider.userID!));
      await Future.wait(requests);
      notReadMessages = [];
    } catch (error) {
      rethrow;
    } finally {
      loadingProvider.setIgnoreActiveLoading(false, true);
      loadingProvider.setIgnoreDeactiveLoading(false, true);
      loadingProvider.hideLoading();
    }
  }

  Future<void> fetchMessages() async {
    BuildContext context = appNavigatorKey.currentContext!;
    var loadingProvider = Provider.of<LoadingProvider>(context, listen: false);
    loadingProvider.setIgnoreActiveLoading(true, true);
    loadingProvider.setIgnoreDeactiveLoading(true, true);
    try {
      var response = await APIMessages().getMessages();
      _messages = response;
      notReadMessages = _messages!.where((element) => !element.read).toList();
    } catch (error) {
      rethrow;
    } finally {
      loadingProvider.setIgnoreActiveLoading(false, true);
      loadingProvider.setIgnoreDeactiveLoading(false, true);
    }
  }

  Future<void> fetchUserData() async {
    BuildContext context = appNavigatorKey.currentContext!;
    var loadingProvider = Provider.of<LoadingProvider>(context, listen: false);
    try {
      loadingProvider.setIgnoreActiveLoading(true, true);
      loadingProvider.setIgnoreDeactiveLoading(true, true);
      loadingProvider.showLoading('Iniciando aplicación...');
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      var appProvider = Provider.of<AppProvider>(context, listen: false);
      var userID = authProvider.userID!;
      var boxID = authProvider.boxID!;
      var responses = await Future.wait([
        APIBOX().getInfoBOX(boxID),
        APIUser().getStatus(userID, boxID),
        APIUser().getProfile(),
        APIMessages().getMessages(),
        appProvider.fetchData()
      ]);
      _infoBOX = responses[0] as InfoBOX;
      Provider.of<AppProvider>(context, listen: false).infoBOX = _infoBOX;
      _status = responses[1] as String;
      _user = responses[2] as User;
      _messages = responses[3] as List<Message>;
      notReadMessages = _messages!.where((element) => !element.read).toList();
      Stripe.init(_infoBOX!.stripePk!);
      notifyListeners();
    } catch (error) {
      rethrow;
    } finally {
      loadingProvider.setIgnoreActiveLoading(false, true);
      loadingProvider.setIgnoreDeactiveLoading(false, true);
      loadingProvider.hideLoading();
    }
  }
}
