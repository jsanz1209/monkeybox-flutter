import 'dart:convert';

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/fees/api/bonus_user.dart';
import 'package:monkeybox/models/fees/api/fee_box.dart';
import 'package:monkeybox/models/fees/api/fee_user.dart';
import 'package:monkeybox/models/fees/api/fee_user_box.dart';
import 'package:monkeybox/providers/http/http_client.dart';

class APIFees {
  final Client client = HttpClient.client;

  Future<List<FeeUser>> getFees(bool isCompetitor) async {
    try {
      List<FeeUser> _feesUser = [];
      var param = isCompetitor ? '?filter[competitor]=1' : '';
      var url = Uri.parse('${Settings.apiURL}/me/payments$param');
      var response = await client.get(url, );
      var data = json.decode(response.body)['data'];
      for (var item in data) {
        _feesUser.add(FeeUser.fromJson(item));
      }
      return _feesUser;
    } catch (error) {
      rethrow;
    }
  }

  Future<List<BonusUser>> getBonus(int boxID) async {
    try {
      List<BonusUser> _feesBox = [];
      var url = Uri.parse('${Settings.apiURL}/bonuses');
      var response = await client.get(url);
      var data = json.decode(response.body)['data'];
      for (var item in data) {
        _feesBox.add(BonusUser.fromJson(item));
      }
      return _feesBox;
    } catch (error) {
      rethrow;
    }
  }

  Future<List<FeesBox>> getFeesBox(int boxID) async {
    try {
      List<FeesBox> _feesBox = [];
      var url = Uri.parse('${Settings.apiURL}/fees/box/$boxID');
      var response = await client.get(url);
      var data = json.decode(response.body)['data'];
      for (var item in data) {
        _feesBox.add(FeesBox.fromJson(item));
      }
      return _feesBox;
    } catch (error) {
      rethrow;
    }
  }

  Future<FeeUserBox> getFeeUserBox(int userID, int boxID) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/$userID/box/$boxID');
      var response = await client.get(url);
      var data = json.decode(response.body)['data'];
      return FeeUserBox.fromJson(data);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> updateFeeType(
      int feeID, int userID, int boxID, String status) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/$userID}/box/$boxID');
      var body = {'fee_id': feeID, 'status': status};
      var response = await client.post(url, body: jsonEncode(body));
      var map = json.decode(response.body);
      return ApiResponse.fromJson(map);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> buyBonus(
      int bonusID, String startDate, String endDate) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/bonuses/buy/$bonusID');
      var body = {
        'start_date': startDate,
        'end_date': endDate,
        'completed': 0,
        'issue_date': ''
      };
      var response = await client.post(url, body: jsonEncode(body));
      var map = json.decode(response.body);
      return ApiResponse.fromJson(map);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> setPaymentInProgress(int paymentID) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/payments/$paymentID');
      var body = {'status': 'PROCESSING'};
      var response = await client.post(url, body: jsonEncode(body));
      var map = json.decode(response.body);
      return ApiResponse.fromJson(map);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> generateStripePayment(int paymentID) async {
    try {
      var url =
          Uri.parse('${Settings.apiURL}/payments/complete-payment/$paymentID');
      var response = await client.get(url);
      var map = json.decode(response.body);
      return ApiResponse.fromJson(map);
    } catch (error) {
      rethrow;
    }
  }
}
