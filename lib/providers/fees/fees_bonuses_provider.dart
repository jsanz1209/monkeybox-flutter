import 'package:flutter/material.dart';
// import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/models/fees/api/bonus_user.dart';
import 'package:monkeybox/models/fees/api/fee_box.dart';
import 'package:monkeybox/models/fees/api/fee_user.dart';
import 'package:monkeybox/models/fees/api/fee_user_box.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/fees/api_fees.dart';
import 'package:monkeybox/providers/loading/loading_provider.dart';
import 'package:provider/provider.dart';
import 'package:stripe_sdk/stripe_sdk.dart';
import 'package:stripe_sdk/stripe_sdk_ui.dart';

class FeesBonusesProvider with ChangeNotifier {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
  List<FeeUser> _listFeesUser = [];
  List<BonusUser> _listBonusesUser = [];
  List<FeesBox> _listFeesBox = [];
  FeeUserBox? _infoFeeUserBox;

  GlobalKey<NavigatorState> get navigatorKey {
    return _navigatorKey;
  }

  List<FeeUser> get listFeesUser {
    return _listFeesUser;
  }

  List<BonusUser> get listBonusesUser {
    return _listBonusesUser;
  }

  List<FeesBox> get listFeesBox {
    return _listFeesBox;
  }

  FeeUserBox? get infoFeeUserBox {
    return _infoFeeUserBox;
  }

  Future<List<FeeUser>> getFees(bool isCompetitor) async {
    try {
      return await APIFees().getFees(isCompetitor);
    } catch (error) {
      rethrow;
    }
  }

  Future<List<BonusUser>> getBonus(int boxID) async {
    try {
      return await APIFees().getBonus(boxID);
    } catch (error) {
      rethrow;
    }
  }

  Future<List<FeesBox>> getFeesBox(int boxID) async {
    try {
      return await APIFees().getFeesBox(boxID);
    } catch (error) {
      rethrow;
    }
  }

  Future<FeeUserBox> getFeeUserBox(int userID, int boxID) async {
    try {
      return await APIFees().getFeeUserBox(userID, boxID);
    } catch (error) {
      rethrow;
    }
  }

  Future<void> updateFeeType(
      int feeID, int userID, int boxID, String status) async {
    try {
      await APIFees().updateFeeType(feeID, userID, boxID, status);
    } catch (error) {
      rethrow;
    }
  }

  Future<void> buyBonus(int bonusID, int expirationMoths) async {
    try {
      DateTime currentDate = DateTime.now();
      String startDate = DateFormat('yyyy-MM-dd').format(currentDate);
      String endDate = DateFormat('yyyy-MM-dd')
          .format(currentDate.add(Duration(days: expirationMoths * 30)));
      await APIFees().buyBonus(bonusID, startDate, endDate);
    } catch (error) {
      rethrow;
    }
  }

  Future getAPICallsBackend(int userID, int boxID, bool isCompetitor) async {
    try {
      var response = await Future.wait([
        getFees(isCompetitor),
        getBonus(boxID),
        getFeesBox(boxID),
        getFeeUserBox(userID, boxID)
      ]);
      _listFeesUser = response[0] as List<FeeUser>;
      _listBonusesUser = response[1] as List<BonusUser>;
      _listFeesBox = response[2] as List<FeesBox>;
      _infoFeeUserBox = response[3] as FeeUserBox;

      notifyListeners();
      return response;
    } catch (error) {
      rethrow;
    }
  }

  Future setPaymentAPICalls(int paymentID, StripeCard stripeCard, User user,
      BuildContext context) async {
    var loadingProvider = Provider.of<LoadingProvider>(
        appNavigatorKey.currentContext!,
        listen: false);
    try {
      loadingProvider.setIgnoreActiveLoading(true, true);
      loadingProvider.setIgnoreDeactiveLoading(true, true);
      loadingProvider.showLoading(
          'Por favor no cierres la aplicación, si se interrumpe este proceso el pago no se completará correctamente.');
      var apiResponse = await APIFees().generateStripePayment(paymentID);
      var clientSecret = apiResponse.data['clientSecret'];
      var paymentMethod =
          await Stripe.instance.api.createPaymentMethodFromCard(stripeCard);
      var responsePayment = await Stripe.instance.confirmPayment(
          clientSecret, context,
          paymentMethodId: paymentMethod['id']);
      if (responsePayment['status'] == 'succeeded') {
        return APIFees().setPaymentInProgress(paymentID);
      } else {
        return responsePayment;
      }
    } catch (error) {
      rethrow;
    } finally {
      loadingProvider.setIgnoreActiveLoading(false, true);
      loadingProvider.setIgnoreDeactiveLoading(false, true);
      loadingProvider.hideLoading();
    }
  }
}

class PaymentMethodParams {}
