import 'package:flutter/material.dart';
import 'package:monkeybox/models/box/api/box.dart';
import 'package:monkeybox/models/box/api/infobox.dart';
import 'package:monkeybox/models/wods/api/intensity_category.dart';
import 'package:monkeybox/providers/box/api_box.dart';
import 'package:monkeybox/providers/wods/api_wods.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppProvider with ChangeNotifier {
  List<IntensityCategory> _intensitiesCategory = [];
  List<Box> _boxes = [];
  InfoBOX? _infoBOX;

  List<IntensityCategory> get intensitiesCategory {
    return _intensitiesCategory;
  }

  List<Box> get boxes {
    return _boxes;
  }

  setFinishOnBoarding() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('onBoarding', true);
  }

  set infoBOX(InfoBOX? value) {
    _infoBOX = value;
    notifyListeners();
  }

  InfoBOX? get infoBOX {
    return _infoBOX;
  }

  Future<bool> isOnBoardingFinish() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('onBoarding')) {
      return false;
    }
    return true;
  }

  Future<List<List<dynamic>>> fetchData() async {
    if (_intensitiesCategory.isEmpty || _boxes.isEmpty) {
      var responses = await Future.wait(
          [APIWods().intensityCategories(), APIBOX().boxList()]);
      _intensitiesCategory = responses[0] as List<IntensityCategory>;
      _boxes = responses[1] as List<Box>;
      notifyListeners();
    }
    return [_intensitiesCategory, _boxes];
  }
}
