import 'package:flutter/material.dart';
import 'package:monkeybox/models/sport-data/api/body_weights_data.dart';

class WeightProvider with ChangeNotifier {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
  BodyWeightsData? dataUpdate;

  GlobalKey<NavigatorState> get navigatorKey {
    return _navigatorKey;
  }
}