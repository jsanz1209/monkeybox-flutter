import 'dart:convert';

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/box/api/box.dart';
import 'package:monkeybox/models/box/api/infobox.dart';
import 'package:monkeybox/providers/http/http_client.dart';

class APIBOX {
  final Client client = HttpClient.client;

  Future<List<Box>> boxList() async {
    try {
      var url = Uri.parse('${Settings.apiURL}/boxes/list');
      var response = await client.get(url);
      List<Box> list = [];
      if (response.statusCode != 200) {
        return list;
      }
      var data = json.decode(response.body)['data'];
      for (var element in data) {
        list.add(Box.fromJson(element));
      }
      return list;
    } catch (error) {
      rethrow;
    }
  }

  Future<InfoBOX?> getInfoBOX(int boxID) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/boxes/$boxID/info');
      var response = await client.get(url);
      if (response.statusCode != 200) {
        return null;
      }
      var data = json.decode(response.body)['data'];
      return InfoBOX.fromJson(data);
    } catch (error) {
      rethrow;
    }
  }
}
