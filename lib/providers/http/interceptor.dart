import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/common/api/http_exception.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/loading/loading_provider.dart';
import 'package:provider/provider.dart';

class LoggingInterceptor implements InterceptorContract {

  @override
  Future<RequestData> interceptRequest({required RequestData data}) async {
    try {
      BuildContext context = appNavigatorKey.currentContext!;
      var loadingProvider =
          Provider.of<LoadingProvider>(context, listen: false);
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      data.headers['content-type'] = 'application/json';
      if (authProvider.token != null) {
        data.headers['authorization'] = 'Bearer ${authProvider.token}';
      }
      if (!loadingProvider.ignoreActiveLoading) {
        Future.delayed(const Duration(milliseconds: 0), () {
          loadingProvider.showLoading();
        });
      }
    } catch (error) {
      rethrow;
    }
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({required ResponseData data}) async {
    BuildContext context = appNavigatorKey.currentContext!;
    var loadingProvider = Provider.of<LoadingProvider>(context, listen: false);
    try {
      var jsonCode = convert.jsonDecode(data.body!);
      if (!jsonCode['success'] &&
          !(data.url!.contains('me/user-wod-feedback') &&
              jsonCode['data']['status'] == 404)) {
        var errorMessage = '';
        if (!(jsonCode['data'] is List<dynamic>) &&
            jsonCode['data']['message'] != null &&
            (jsonCode['data']['message'] as String).indexOf('[') != -1 &&
            jsonCode['data']['message'] != '[]') {
          var errorMessageObject =
              convert.jsonDecode(jsonCode['data']['message']) as Map;
          for (var key in errorMessageObject.keys) {
            if (errorMessage.isNotEmpty) {
              errorMessage += '\n';
            }
            errorMessage += '${errorMessageObject[key][0]}';
          }
        } else if (!(jsonCode['data'] is List<dynamic>) &&
            jsonCode['data']['message'] != null &&
            (jsonCode['data']['message'] as String).indexOf('[') == -1) {
          errorMessage = jsonCode['data']['message'];
        } else if (jsonCode['data'] is List<dynamic> &&
            (jsonCode['data'] as List<dynamic>).isNotEmpty) {
          errorMessage = jsonCode['data'][0]['message'];
        } else {
          errorMessage = jsonCode['data']['name'];
        }
        SnackBarHelper.showSnackBar(
            'ERROR', errorMessage, TypeSnackBarEnum.error);
        throw HttpException(errorMessage);
      }
    } catch (error) {
      rethrow;
    } finally {
      if (!loadingProvider.ignoreDeactiveLoading) {
        loadingProvider.hideLoading();
      }
    }
    return data;
  }
}
