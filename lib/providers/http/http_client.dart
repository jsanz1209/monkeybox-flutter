import 'package:http/http.dart';
import 'package:http_interceptor/http/intercepted_client.dart';
import 'package:monkeybox/providers/http/interceptor.dart';

class HttpClient {
  static Client client = InterceptedClient.build(interceptors: [
    LoggingInterceptor(),
  ]);
  
}
