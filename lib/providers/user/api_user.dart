import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/user/api/account_request.dart';
import 'package:monkeybox/models/user/api/change_password_request.dart';
import 'package:monkeybox/models/user/api/history.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/models/user/api/user_request.dart';
import 'package:monkeybox/providers/http/http_client.dart';
import 'package:http/http.dart' as http;

class APIUser {
  final Client client = HttpClient.client;

  Future<String?> getStatus(int userID, int boxID) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/$userID/box/$boxID/status');
      var response = await client.get(url);
      if (response.statusCode != 200) {
        return null;
      }
      var data = json.decode(response.body)['data'];
      return data;
    } catch (error) {
      rethrow;
    }
  }

  Future<User?> getProfile() async {
    try {
      var url =
          Uri.parse('${Settings.apiURL}/me?expand=profile, roles, userBoxes');
      var response = await client.get(url);
      if (response.statusCode != 200) {
        return null;
      }
      var data = json.decode(response.body)['data'];
      return User.fromJson(data);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> updateProfile(UserRequest userRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/me/update-profile');
      var response = await client.post(url, body: jsonEncode(userRequest));
      var jsonCode = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonCode);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> updateAccount(AccountRequest accountRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/me/update-account');
      var response = await client.post(url, body: jsonEncode(accountRequest));
      var jsonCode = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonCode);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> changePassword(
      ChangePasswordRequest changePasswordRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/me/change-password');
      var response =
          await client.post(url, body: jsonEncode(changePasswordRequest));
      var jsonCode = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonCode);
    } catch (error) {
      rethrow;
    }
  }

  // TO-DO: Implementing interceptor: https://github.com/CodingAleCR/http_interceptor/issues/95
  Future<StreamedResponse> setProfileImage(File? data, String token) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/me/set-profile-image');
      var request = http.MultipartRequest('POST', url);
      request.headers['authorization'] = 'Bearer $token';
      request.files
          .add(await http.MultipartFile.fromPath('picture', data!.path));
      var response = await request.send();
      return response;
    } catch (error) {
      rethrow;
    }
  }

  Future<List<History>> getHistoryByBoxId(int userID, int boxID) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/$userID/box/$boxID/history');
      var response = await client.get(url);
      if (response.statusCode == 200) {
        var jsonCode = jsonDecode(response.body);
        List<History> histories = (jsonCode['data'] as List<dynamic>)
            .map((item) => History.fromJson(item))
            .toList();
        return histories;
      } else {
        return [];
      }
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> deleteUser(int userID) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/delete/$userID');
      var response = await client.delete(url);
      var jsonCode = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonCode);
    } catch (error) {
      rethrow;
    }
  }
}
