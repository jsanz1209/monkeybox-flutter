import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/user/api/account_request.dart';
import 'package:monkeybox/models/user/api/change_password_request.dart';
import 'package:monkeybox/models/user/api/history.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/models/user/api/user_request.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/user/api_user.dart';
import 'package:monkeybox/widgets/auth/auth_page.dart';
import 'package:provider/provider.dart';

class UserProvider with ChangeNotifier {
  Future<User?> updateProfileData(
      UserRequest userRequest,
      AccountRequest accountRequest,
      ChangePasswordRequest? changePasswordRequest) async {
    try {
      var callsToAPI = [
        APIUser().updateAccount(accountRequest),
        APIUser().updateProfile(userRequest)
      ];
      if (changePasswordRequest != null) {
        callsToAPI.add(APIUser().changePassword(changePasswordRequest));
      }
      await Future.wait(callsToAPI);
      return await APIUser().getProfile();
    } catch (error) {
      rethrow;
    }
  }

  Future<User?> setProfileImage(File? data) async {
    try {
      var authProvider = Provider.of<AuthProvider>(
          appNavigatorKey.currentContext!,
          listen: false);
      var response = await APIUser().setProfileImage(data, authProvider.token!);
      if (response.statusCode == 200) {
        debugPrint('Uploaded!');
        return await APIUser().getProfile();
      } else {
        return null;
      }
    } catch (error) {
      SnackBarHelper.showSnackBar(
          'ERROR',
          'Se ha producido un error al subir la imagen: $error.',
          TypeSnackBarEnum.error);
      rethrow;
    }
  }

  Future<List<History>> getHistoryByBoxId(int userID, int boxID) async {
    try {
      var response = await APIUser().getHistoryByBoxId(userID, boxID);
      response.removeWhere((data) {
        var status = data.status.split('-').length > 1
            ? data.status.split('-')[1]
            : data.status.split('-')[0];
        return [
              'habilitado',
              'deshablitado',
              'alta',
              'baja',
              'enabled',
              'disabled'
            ].join().indexOf(status.toLowerCase()) ==
            -1;
      });

      var formatResponse = response.map((data) {
        var dataFormat = data;
        var status = data.status.split('-').length > 1
            ? data.status.split('-')[1]
            : data.status.split('-')[0];
        dataFormat.startDate = data.startDate.isNotEmpty
            ? DateTime.parse(data.startDate.split(' ')[0])
            : null;
        dataFormat.endDate = data.endDate.isNotEmpty
            ? DateTime.parse((data.endDate).split(' ')[0])
            : null;
        dataFormat.status = ['habilitado', 'alta', 'enabled']
                    .join()
                    .indexOf(status.toLowerCase()) !=
                -1
            ? 'ALTA'
            : 'BAJA';
        return dataFormat;
      }).toList();
      return formatResponse.reversed.toList();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> deleteUser(int userID) async {
    try {
      var context = appNavigatorKey.currentContext!;
      await APIUser().deleteUser(userID);
      Provider.of<AuthProvider>(context, listen: false).logout();
      Navigator.of(appNavigatorKey.currentContext!)
          .pushReplacementNamed(AuthPage.routeName);
    } catch (error) {
      rethrow;
    }
  }
}
