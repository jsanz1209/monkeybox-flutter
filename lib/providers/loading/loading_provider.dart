import 'package:flutter/material.dart';
import 'package:monkeybox/helpers/loading_helper.dart';
import 'package:monkeybox/pages/app.dart';

class LoadingProvider with ChangeNotifier {
  bool _ignoreActiveLoading = false;
  bool _ignoreDeactiveLoading = false;

  setIgnoreActiveLoading(bool value, [bool? notifiy]) {
    _ignoreActiveLoading = value;
    if (notifiy != null) {
      notifyListeners();
    }
  }

  setIgnoreDeactiveLoading(bool value, [bool? notify]) {
    _ignoreDeactiveLoading = value;
    if (notify != null) {
      notifyListeners();
    }
  }

  bool get ignoreActiveLoading {
    return _ignoreActiveLoading;
  }

  bool get ignoreDeactiveLoading {
    return _ignoreDeactiveLoading;
  }

  showLoading([String? message]) {
    BuildContext context = appNavigatorKey.currentContext!;
    LoadingHelper.showLoading(context, message);
  }

  hideLoading() {
    BuildContext context = appNavigatorKey.currentContext!;
    LoadingHelper.hideLoading(context);
  }
}
