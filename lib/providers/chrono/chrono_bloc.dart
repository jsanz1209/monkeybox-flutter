// ignore_for_file: close_sinks

import 'dart:async';

class ChronoBloc {
  static final ChronoBloc _singletonChronoBloc = ChronoBloc._internal();
  StreamController? _chronoGenBackEventController;
  StreamController? _chronoWidgetBackEventController;

  bool fromWODs = false;
  bool isInChronoSection = false;

  factory ChronoBloc() {
    return _singletonChronoBloc;
  }

  ChronoBloc._internal();

  initControllersGenBack() {
    disposeChronoGenBackEvent();
    _chronoGenBackEventController = StreamController<void>();
  }

  initControllersChronoWidgetGenBack() {
    disposeChronoWidgetBackEvent();
    _chronoWidgetBackEventController = StreamController<void>();
  }

  initAllControllers() {
    initControllersGenBack();
    initControllersChronoWidgetGenBack();
  }

  // For events, exposing only a stream which is an input
  StreamSink<void> get chronoGenBackEventSink =>
      _chronoGenBackEventController!.sink;
  Stream<void> get chronoGenBackEventStream =>
      _chronoGenBackEventController!.stream;

  // For events, exposing only a stream which is an input
  StreamSink<void> get chronoWidgetBackEventSink =>
      _chronoWidgetBackEventController!.sink;
  Stream<void> get chronoWidgetBackEventStream =>
      _chronoWidgetBackEventController!.stream;

  void disposeChronoGenBackEvent() {
    // dispose StreamController
    if (_chronoGenBackEventController != null) {
      _chronoGenBackEventController!.close();
    }
  }

  void disposeChronoWidgetBackEvent() {
    // dispose StreamController
    if (_chronoWidgetBackEventController != null) {
      _chronoWidgetBackEventController!.close();
    }
  }
}
