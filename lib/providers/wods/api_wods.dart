import 'dart:convert';

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/wods/api/add_rating_request.dart';
import 'package:monkeybox/models/wods/api/feedback_request.dart';
import 'package:monkeybox/models/wods/api/intensity_category.dart';
import 'package:monkeybox/providers/http/http_client.dart';

class APIWods {
  final Client client = HttpClient.client;

  Future<List<IntensityCategory>> intensityCategories() async {
    try {
      var url = Uri.parse('${Settings.apiURL}/ratings/intensity-categories');
      var response = await client.get(url);
      List<IntensityCategory> list = [];
      if (response.statusCode != 200) {
        return list;
      }
      var data = json.decode(response.body)['data'];
      for (var element in data) {
        list.add(IntensityCategory.fromJson(element));
      }
      return list;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> getWod(String date, int boxId, int competitor) async {
    try {
      var url = Uri.https(Settings.host, Settings.version + '/wods', {
        'filter[date]': date,
        'filter[box_id]': boxId.toString(),
        'filter[competitor]': competitor.toString(),
        'expand': 'blocks'
      });
      var response = await client.get(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> getReservationAndTraining(int boxId, String date) async {
    try {
      var url = Uri.https(
          Settings.host,
          Settings.version +
              '/reservations/andtraining/now/date/$date/box/$boxId',
          <String, dynamic>{'expand': 'user.profile'});
      var response = await client.get(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> getParticipants(String date) async {
    try {
      var url = Uri.https(
          Settings.host,
          Settings.version + '/reservations/andtraining',
          <String, dynamic>{'expand': 'user.profile', 'filter[date]': date});
      var response = await client.get(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> getUserWodFeedback(int wodID) async {
    try {
      var url = Uri.https(
          Settings.host, Settings.version + '/me/user-wod-feedback/$wodID');
      var response = await client.get(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> createFeedBack(FeedbackRequest feedbackRequest) async {
    try {
      var url =
          Uri.https(Settings.host, Settings.version + '/user-wod-feedback');
      var response = await client.post(url, body: jsonEncode(feedbackRequest));
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> updateFeedBack(
      int feedbackID, FeedbackRequest feedbackRequest) async {
    try {
      var url = Uri.https(
          Settings.host, Settings.version + '/user-wod-feedback/$feedbackID');
      var response = await client.post(url, body: jsonEncode(feedbackRequest));
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> getRankings(int wodId) async {
    try {
      var url = Uri.https(
          Settings.host,
          Settings.version + '/ratings/rankings/${wodId.toString()}',
          <String, dynamic>{'expand': 'user.profile'});
      var response = await client.get(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> addRating(AddRatingRequest addRatingRequest) async {
    try {
      var url = Uri.https(Settings.host, Settings.version + '/ratings');
      var response = await client.post(url, body: jsonEncode(addRatingRequest));
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> updateRating(
      int idRating, AddRatingRequest updateRatingRequest) async {
    try {
      var url =
          Uri.https(Settings.host, Settings.version + '/ratings/$idRating');
      var response =
          await client.post(url, body: jsonEncode(updateRatingRequest));
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> deleteRating(int idRating) async {
    try {
      var url =
          Uri.https(Settings.host, Settings.version + '/ratings/$idRating');
      var response = await client.delete(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }
}
