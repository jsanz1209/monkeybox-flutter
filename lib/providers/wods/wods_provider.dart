import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/common/item_dropdown_multiple.dart';
import 'package:monkeybox/models/reservations/api/reservation.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/models/wods/api/add_rating_request.dart';
import 'package:monkeybox/models/wods/api/block.dart';
import 'package:monkeybox/models/wods/api/feedback_request.dart';
import 'package:monkeybox/models/wods/api/feedback_user_wod.dart';
import 'package:monkeybox/models/wods/api/rating.dart';
import 'package:monkeybox/models/wods/api/rating_type.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/loading/loading_provider.dart';
import 'package:monkeybox/providers/wods/api_wods.dart';
import 'package:monkeybox/widgets/common/form/custom_dropdown_multiple.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';
import 'package:provider/provider.dart';

class WodsProvider with ChangeNotifier {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
  Map<String, dynamic> _ratings = {};
  bool _isAddingRatingMode = false;
  bool _isFeedbackFilled = false;
  List<Block> _blocksWithRatingTypes = [];
  GlobalKey rankingWidgetKey = GlobalKey();

  GlobalKey<NavigatorState> get navigatorKey {
    return _navigatorKey;
  }

  Map<String, dynamic> get ratings {
    return _ratings;
  }

  List<Block> get blocksWithRatingTypes {
    return _blocksWithRatingTypes;
  }

  set blocksWithRatingTypes(List<Block> value) {
    _blocksWithRatingTypes = value;
    notifyListeners();
  }

  bool get isFeedbackFilled {
    return _isFeedbackFilled;
  }

  setIsFeedbackFilled(bool value, bool notify) {
    _isFeedbackFilled = value;
    if (notify) {
      notifyListeners();
    }
  }

  set isAddingRatingMode(bool value) {
    _isAddingRatingMode = value;
    notifyListeners();
  }

  bool get isAddingRatingMode {
    return _isAddingRatingMode;
  }

  Future<ApiResponse> getWod(String date, int boxId, int competitor) async {
    try {
      return await APIWods().getWod(date, boxId, competitor);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> getReservationAndTraining(int boxId, String date) async {
    BuildContext context = appNavigatorKey.currentContext!;
    var loadingProvider = Provider.of<LoadingProvider>(context, listen: false);
    try {
      loadingProvider.setIgnoreActiveLoading(true);
      return await APIWods().getReservationAndTraining(boxId, date);
    } catch (error) {
      rethrow;
    } finally {
      loadingProvider.setIgnoreActiveLoading(false);
    }
  }

  Future<ApiResponse> getRankings(int wodId) async {
    BuildContext context = appNavigatorKey.currentContext!;
    var loadingProvider = Provider.of<LoadingProvider>(context, listen: false);
    try {
      loadingProvider.setIgnoreActiveLoading(true);
      var response = await APIWods().getRankings(wodId);
      _ratings = response.data as Map<String, dynamic>;
      notifyListeners();
      return response;
    } catch (error) {
      rethrow;
    } finally {
      loadingProvider.setIgnoreActiveLoading(false);
    }
  }

  Future<List<Reservation>> getParticipants(String date) async {
    var response = await APIWods().getParticipants(date);
    if (response.success!) {
      return (response.data as List)
          .map((map) => Reservation.fromJson(map))
          .toList();
    } else {
      return [];
    }
  }

  Future<FeedbackUserWod?> getUserWodFeedback(int wodID) async {
    var response = await APIWods().getUserWodFeedback(wodID);
    if (response.success!) {
      return FeedbackUserWod.fromJson(response.data);
    } else {
      return null;
    }
  }

  Future<void> createUserWodFeedback(
      String wodID, int effort, int mood, int userID) async {
    FeedbackRequest feedbackRequest =
        FeedbackRequest(wodID, effort, mood, userID);
    await APIWods().createFeedBack(feedbackRequest);
  }

  Future<void> updateUserWodFeedback(
      int feedbackID, String wodID, int effort, int mood, int userID) async {
    FeedbackRequest feedbackRequest =
        FeedbackRequest(wodID, effort, mood, userID);
    await APIWods().updateFeedBack(feedbackID, feedbackRequest);
  }

  Future<ApiResponse> addRating(AddRatingRequest addRatingRequest) async {
    var response = await APIWods().addRating(addRatingRequest);
    return response;
  }

  Future<ApiResponse> updateRating(
      int idRating, AddRatingRequest addRatingRequest) async {
    var response = await APIWods().updateRating(idRating, addRatingRequest);
    return response;
  }

  Future<ApiResponse> deleteRating(int idRating) async {
    var response = await APIWods().deleteRating(idRating);
    return response;
  }

  Future<void> crudRatings(List<Future> futures) async {
    await Future.wait(futures);
  }

  filterRakingsNoCompleted(Map originalRatings, Map clonedRatings) {
    try {
      originalRatings.entries.forEach((entry) {
        var category = originalRatings[entry.key];
        if (category is Map<String, dynamic>) {
          category.entries.forEach((entryCategory) {
            var block = originalRatings[entry.key][entryCategory.key];
            block.entries.forEach((entryBlock) {
              var listBlock = [...entryBlock.value as List];
              listBlock.removeWhere((rating) {
                var ratingModel = Rating.fromJson(rating);
                return ratingModel.isComplete == 0;
              });
              clonedRatings[entry.key][entryCategory.key][entryBlock.key] =
                  listBlock;
              if (listBlock.isEmpty) {
                (clonedRatings[entry.key][entryCategory.key] as Map)
                    .removeWhere((key, value) {
                  return key == entryBlock.key;
                });
              }
              if ((clonedRatings[entry.key][entryCategory.key] as Map)
                  .isEmpty) {
                (clonedRatings[entry.key] as Map).removeWhere((key, value) {
                  return key == entryCategory.key;
                });
              }
            });
          });
        }
      });
    } catch (error) {
      debugPrint(error.toString());
    }
    return clonedRatings;
  }

  int? getSelectedIntesityCategory(User? user) {
    int? _intensityCategory;
    _ratings.entries.forEach((entry) {
      var category = _ratings[entry.key];
      if (category is Map<String, dynamic>) {
        category.entries.forEach((entryCategory) {
          var block = _ratings[entry.key][entryCategory.key];
          block.entries.forEach((entryBlock) {
            for (var rating in entryBlock.value) {
              var ratingModel = Rating.fromJson(rating);
              var userIDBlockRating = ratingModel.userID;
              var intensityCategory = ratingModel.intensityCategoryID;
              var teams = ratingModel.team ?? [];
              var isTeamIncludingCurrentUser = teams.firstWhereOrNull(
                      (element) => element.userID == user!.id) !=
                  null;
              if (userIDBlockRating == user!.id || isTeamIncludingCurrentUser) {
                _intensityCategory = intensityCategory;
                break;
              }
            }
          });
        });
      }
    });
    return _intensityCategory;
  }

  List<Rating> getRatingUser(User? user) {
    List<Rating> blockWithRatings = [];
    _ratings.entries.forEach((entry) {
      var category = _ratings[entry.key];
      if (category is Map<String, dynamic>) {
        category.entries.forEach((entryCategory) {
          var block = _ratings[entry.key][entryCategory.key];
          block.entries.forEach((entryBlock) {
            for (var rating in entryBlock.value) {
              var ratingModel = Rating.fromJson(rating);
              var userIDBlockRating = ratingModel.userID;
              var teams = ratingModel.team ?? [];
              var isTeamIncludingCurrentUser = teams.firstWhereOrNull(
                      (element) => element.userID == user!.id) !=
                  null;
              if (userIDBlockRating == user!.id || isTeamIncludingCurrentUser) {
                blockWithRatings.add(ratingModel);
              }
            }
          });
        });
      }
    });
    return blockWithRatings;
  }

  void setupInputsBasedOnRating(
      RatingType ratingType,
      String? ratingFormat,
      bool isTeam,
      Map<String, GlobalKey<FormFieldState<dynamic>>> _fields,
      List<Widget> widgets,
      Map _data,
      List<ItemDropdownMultiple> users,
      GlobalKey<FormState> formkey,
      FocusScopeNode focusNode) {
    if (ratingType.id == RatingType.TIME ||
        ratingType.id == RatingType.TIME_ALBACETE) {
      _fields['minutes'] = GlobalKey<FormFieldState>();
      _fields['seconds'] = GlobalKey<FormFieldState>();

      int? minutes;
      int? seconds;
      if (ratingFormat != null) {
        int ratingInteger = int.parse(ratingFormat);
        minutes = (ratingInteger / 60).floor();
        seconds = ratingInteger % 60;
      }

      widgets.add(CustomTextFormField(
        formKey: _fields['minutes'],
        label: 'Minutos',
        fieldName: 'minutes',
        isBlackTheme: true,
        maxLength: 3,
        initialValue:
            minutes != null && seconds != null ? minutes.toString() : '',
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        focusNode: focusNode,
        onSave: (value) {
          _data['minutes'] = value!;
        },
        onBlur: () {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value!.isEmpty &&
              _fields['seconds']!.currentState!.value.toString().isEmpty &&
              _restOfFieldsAreEmpty(_fields, 'minutes')) {
            return null;
          } else if (value.isEmpty) {
            return FormValidators.validatorRequired(
                value, 'El campo Minutos es obligatorio');
          } else {
            return null;
          }
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
      widgets.add(CustomTextFormField(
        formKey: _fields['seconds'],
        label: 'Segundos',
        fieldName: 'seconds',
        isBlackTheme: true,
        maxLength: 2,
        initialValue:
            seconds != null && minutes != null ? seconds.toString() : '',
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.done,
        onSave: (value) {
          _data['seconds'] = value!;
        },
        onBlur: () {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value!.isEmpty &&
              _fields['minutes']!.currentState!.value.toString().isEmpty &&
              _restOfFieldsAreEmpty(_fields, 'seconds')) {
            return null;
          } else if (value.isEmpty) {
            return FormValidators.validatorRequired(
                value, 'El campo Segundos es obligatorio');
          } else if (int.parse(value) > 59) {
            return 'El máximo de Segundos es 59';
          } else {
            return null;
          }
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
    }

    if (ratingType.id == RatingType.REPS ||
        ratingType.id == RatingType.REPS_ALBACETE) {
      _fields['reps'] = GlobalKey<FormFieldState>();

      widgets.add(CustomTextFormField(
        formKey: _fields['reps'],
        label: 'Repeticiones',
        fieldName: 'reps',
        isBlackTheme: true,
        initialValue:
            ratingFormat != null && ratingFormat != '0' ? ratingFormat : '',
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.done,
        onSave: (value) {
          _data['reps'] = value!;
        },
        onBlur: () {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value!.isEmpty && !_restOfFieldsAreEmpty(_fields, 'reps')) {
            return FormValidators.validatorRequired(
                value, 'El campo Repeticiones es obligatorio');
          }
          return null;
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
    }

    if (ratingType.id == RatingType.ROUNDS ||
        ratingType.id == RatingType.ROUNDS_ALBACETE) {
      _fields['rounds'] = GlobalKey<FormFieldState>();

      widgets.add(CustomTextFormField(
        formKey: _fields['rounds'],
        label: 'Rondas',
        fieldName: 'rounds',
        isBlackTheme: true,
        initialValue:
            ratingFormat != null && ratingFormat != '0' ? ratingFormat : '',
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.done,
        onSave: (value) {
          _data['rounds'] = value!;
        },
        onBlur: () {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value!.isEmpty && !_restOfFieldsAreEmpty(_fields, 'rounds')) {
            return FormValidators.validatorRequired(
                value, 'El campo Rondas es obligatorio');
          }
          return null;
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
    }

    if (ratingType.id == RatingType.KG ||
        ratingType.id == RatingType.KG_ALBACETE) {
      _fields['weight'] = GlobalKey<FormFieldState>();
      widgets.add(CustomTextFormField(
        formKey: _fields['weight'],
        label: 'Peso',
        fieldName: 'weight',
        isBlackTheme: true,
        initialValue:
            ratingFormat != null && ratingFormat != '0' ? ratingFormat : '',
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        textInputAction: TextInputAction.done,
        onSave: (value) {
          _data['weight'] = value!;
        },
        onBlur: () {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value!.isEmpty && !_restOfFieldsAreEmpty(_fields, 'weight')) {
            return FormValidators.validatorRequired(
                value, 'El campo Peso es obligatorio');
          }
          return null;
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
    }

    if (ratingType.id == RatingType.M ||
        ratingType.id == RatingType.M_ALBACETE) {
      _fields['distancem'] = GlobalKey<FormFieldState>();

      widgets.add(CustomTextFormField(
        formKey: _fields['distancem'],
        label: 'Distancia (m)',
        fieldName: 'distancem',
        isBlackTheme: true,
        initialValue:
            ratingFormat != null && ratingFormat != '0' ? ratingFormat : '',
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        textInputAction: TextInputAction.done,
        onSave: (value) {
          _data['distancem'] = value!;
        },
        onBlur: () {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value!.isEmpty && !_restOfFieldsAreEmpty(_fields, 'distancem')) {
            return FormValidators.validatorRequired(
                value, 'El campo Distancia (m) es obligatorio');
          }
          return null;
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
    }

    if (ratingType.id == RatingType.KM ||
        ratingType.id == RatingType.KM_ALBACETE) {
      _fields['distancekm'] = GlobalKey<FormFieldState>();

      widgets.add(CustomTextFormField(
        formKey: _fields['distancekm'],
        label: 'Distancia (km)',
        fieldName: 'distancekm',
        initialValue:
            ratingFormat != null && ratingFormat != '0' ? ratingFormat : '',
        isBlackTheme: true,
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        textInputAction: TextInputAction.done,
        onSave: (value) {
          _data['distancekm'] = value!;
        },
        onBlur: () {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value!.isEmpty && !_restOfFieldsAreEmpty(_fields, 'distancekm')) {
            return FormValidators.validatorRequired(
                value, 'El campo Distancia (km) es obligatorio');
          }
          return null;
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
    }

    if (ratingType.id == RatingType.CALORIES ||
        ratingType.id == RatingType.CALORIES_ALBACETE) {
      _fields['calories'] = GlobalKey<FormFieldState>();

      widgets.add(CustomTextFormField(
        formKey: _fields['calories'],
        label: 'Calorías',
        fieldName: 'calories',
        initialValue:
            ratingFormat != null && ratingFormat != '0' ? ratingFormat : '',
        isBlackTheme: true,
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        textInputAction: TextInputAction.done,
        onSave: (value) {
          _data['calories'] = value!;
        },
        onBlur: () {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value!.isEmpty && !_restOfFieldsAreEmpty(_fields, 'calories')) {
            return FormValidators.validatorRequired(
                value, 'El campo Calorías es obligatorio');
          }
          return null;
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
    }

    if (isTeam) {
      var teamID = _getTeamID(ratingType);
      _fields[teamID] = GlobalKey<FormFieldState>();
      widgets.add(CustomMultipleDropdown(
        formKey: _fields[teamID],
        listOptions: users,
        title: 'Selecciona los participantes',
        onSave: (value) {
          _data[teamID] = value!.where((element) => element.isChecked).toList();
        },
        onChange: (_) {
          formkey.currentState!.validate();
        },
        validator: (value) {
          if (value == null) {
            return null;
          }
          if (value.where((element) => element.isChecked).toList().isEmpty &&
              !_restOfFieldsAreEmpty(_fields, teamID)) {
            return FormValidators.validatorRequired(
                value, 'Es obligatorio seleccionar algún participante');
          }
          return null;
        },
      ));
      widgets.add(SizedBox(
        height: 24.sp,
      ));
    }
  }

  String _getTeamID(RatingType ratingType) {
    return 'team_${ratingType.name}';
  }

  AddRatingRequest _getRatingRequest(
    List<RatingType> ratingTypes,
    int indexRatingType,
    int intensityCategory,
    int userID,
    Map data,
    String field,
  ) {
    var teamID = _getTeamID(ratingTypes[indexRatingType]);
    return AddRatingRequest(
        blockID: data['blockID'],
        ratingTypeID: ratingTypes[indexRatingType].id,
        intensityCategoryID: intensityCategory,
        isComplete: data['not_completed'] == null
            ? 1
            : (data['not_completed'] as bool)
                ? 0
                : 1,
        observations: data['observations'] ?? '',
        rating: (data['not_completed'] != null && data['not_completed'] as bool)
            ? 0
            : (field == 'time' &&
                    data['minutes'] != null &&
                    data['seconds'] != null &&
                    data['minutes'].isNotEmpty &&
                    data['seconds'].isNotEmpty)
                ? (int.parse(data['minutes']) * 60 + int.parse(data['seconds']))
                : data[field] != null && data[field].isNotEmpty
                    ? int.parse(data[field])
                    : 0,
        userID: userID,
        team: data[teamID] == null
            ? []
            : (data[teamID] as List<ItemDropdownMultiple>)
                .map((e) => e.id)
                .toList());
  }

  List<AddRatingRequest> getRatingRequests(
      dynamic datas, int intensityCategory, int userID) {
    List<AddRatingRequest> requests = [];

    for (var data in datas) {
      var indexRatingType = 0;
      List<RatingType> ratingTypes = data['ratingTypes'];
      if (ratingTypes[indexRatingType].id == RatingType.TIME ||
          ratingTypes[indexRatingType].id == RatingType.TIME_ALBACETE &&
              (data['minutes'] != null &&
                      data['seconds'] != null &&
                      data['minutes'].isNotEmpty &&
                      data['seconds'].isNotEmpty ||
                  data['not_completed'] != false)) {
        requests.add(_getRatingRequest(ratingTypes, indexRatingType,
            intensityCategory, userID, data, 'time'));
        if (indexRatingType < ratingTypes.length - 1) {
          indexRatingType++;
        }
      }

      if (ratingTypes[indexRatingType].id == RatingType.REPS ||
          ratingTypes[indexRatingType].id == RatingType.REPS_ALBACETE &&
              (data['reps'] != null && data['reps'].isNotEmpty ||
                  data['not_completed'] != false)) {
        requests.add(_getRatingRequest(ratingTypes, indexRatingType,
            intensityCategory, userID, data, 'reps'));

        if (indexRatingType < ratingTypes.length - 1) {
          indexRatingType++;
        }
      }

      if (ratingTypes[indexRatingType].id == RatingType.ROUNDS ||
          ratingTypes[indexRatingType].id == RatingType.ROUNDS_ALBACETE &&
              (data['rounds'] != null && data['rounds'].isNotEmpty ||
                  data['not_completed'] != false)) {
        requests.add(_getRatingRequest(ratingTypes, indexRatingType,
            intensityCategory, userID, data, 'rounds'));

        if (indexRatingType < ratingTypes.length - 1) {
          indexRatingType++;
        }
      }

      if (ratingTypes[indexRatingType].id == RatingType.KG ||
          ratingTypes[indexRatingType].id == RatingType.KG_ALBACETE &&
              (data['weight'] != null && data['weight'].isNotEmpty ||
                  data['not_completed'] != false)) {
        requests.add(_getRatingRequest(ratingTypes, indexRatingType,
            intensityCategory, userID, data, 'weight'));

        if (indexRatingType < ratingTypes.length - 1) {
          indexRatingType++;
        }
      }

      if (ratingTypes[indexRatingType].id == RatingType.M ||
          ratingTypes[indexRatingType].id == RatingType.M_ALBACETE &&
              (data['distancem'] != null && data['distancem'].isNotEmpty ||
                  data['not_completed'] != false)) {
        requests.add(_getRatingRequest(ratingTypes, indexRatingType,
            intensityCategory, userID, data, 'distancem'));

        if (indexRatingType < ratingTypes.length - 1) {
          indexRatingType++;
        }
      }
      if (ratingTypes[indexRatingType].id == RatingType.KM ||
          ratingTypes[indexRatingType].id == RatingType.KM_ALBACETE &&
              (data['distancekm'] != null && data['distancekm'].isNotEmpty ||
                  data['not_completed'] != false)) {
        requests.add(_getRatingRequest(ratingTypes, indexRatingType,
            intensityCategory, userID, data, 'distancekm'));

        if (indexRatingType < ratingTypes.length - 1) {
          indexRatingType++;
        }
      }
      if (ratingTypes[indexRatingType].id == RatingType.CALORIES ||
          ratingTypes[indexRatingType].id == RatingType.CALORIES_ALBACETE &&
              (data['calories'] != null && data['calories'].isNotEmpty ||
                  data['not_completed'] != false)) {
        requests.add(_getRatingRequest(ratingTypes, indexRatingType,
            intensityCategory, userID, data, 'calories'));

        if (indexRatingType < ratingTypes.length - 1) {
          indexRatingType++;
        }
      }
    }

    return requests;
  }

  _restOfFieldsAreEmpty(Map<String, GlobalKey<FormFieldState<dynamic>>> _fields,
      String currentField) {
    var restAreEmpty = true;
    for (var element in _fields.entries) {
      if (element.key != currentField &&
          element.value.currentState != null &&
          element.value.currentState!.value.toString().isNotEmpty) {
        restAreEmpty = false;
        break;
      }
    }
    return restAreEmpty;
  }
}
