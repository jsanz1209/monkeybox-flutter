import 'package:flutter/material.dart';
import 'package:monkeybox/helpers/date_helper.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/common/charts/chat_data.dart';
import 'package:monkeybox/models/rm/rm.dart';
import 'package:monkeybox/models/sport-data/api/add_rm_request.dart';
import 'package:monkeybox/models/sport-data/api/add_update_weight_request.dart';
import 'package:monkeybox/models/sport-data/api/body_weights_data.dart';
import 'package:monkeybox/models/sport-data/api/exercise_data.dart';
import 'package:monkeybox/models/sport-data/api/rm_data.dart';
import 'package:monkeybox/models/sport-data/api/update_rm_request.dart';
import 'package:monkeybox/models/weight/weight.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/sport-data/api_sport_data.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SportDataProvider with ChangeNotifier {
  List<ExerciseData> _exercises = [];

  List<ExerciseData> get exercises {
    return _exercises;
  }

  set exercises(List<ExerciseData> value) {
    _exercises = value;
    notifyListeners();
  }

  Future<List<ExerciseData>> getExercises(int boxID) async {
    try {
      if (exercises.isEmpty) {
        var response = await APISportData().getExercises(boxID);
        if (response.success) {
          exercises = response.data;
          return response.data;
        } else {
          return [];
        }
      } else {
        return _exercises;
      }
    } catch (error) {
      rethrow;
    }
  }

  Future<List<RMData>> getRMData(
      int exerciseID, String dateFrom, String dateTo, int? repetitions) async {
    try {
      var authProvider = Provider.of<AuthProvider>(
          appNavigatorKey.currentContext!,
          listen: false);
      var formattedDateFrom = DateHelper.getDateWithFirstDayOfMonth(dateFrom);
      var formattedDateTo = DateHelper.getDateWithLastDayOfMonth(dateTo);
      var response = await APISportData().getAthleticsData(
          exerciseID,
          authProvider.userID!,
          formattedDateFrom,
          formattedDateTo,
          repetitions);
      if (response.success) {
        return response.data;
      } else {
        return [];
      }
    } catch (error) {
      rethrow;
    }
  }

  Future<void> addAthleticData(RM data) async {
    try {
      var authProvider = Provider.of<AuthProvider>(
          appNavigatorKey.currentContext!,
          listen: false);
      var request = AddRMRequest(
          date: data.startDate!,
          userId: authProvider.userID!,
          kgLb: data.weight!,
          exerciseId: data.exercise!,
          repetitions: data.reps!,
          weight: data.bodyWeight!);
      await APISportData().addAthleticData(request);
    } catch (error) {
      rethrow;
    }
  }

  Future<void> updateAthleticData(RM data, int idRM) async {
    try {
      var request = UpdateRMRequest(
          date: data.startDate!,
          kgLb: data.weight!,
          exerciseId: data.exercise!,
          repetitions: data.reps!,
          weight: data.bodyWeight!);
      await APISportData().updateAthleticData(idRM, request);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> deleteAthleticData(int id) async {
    try {
      return APISportData().deleteAthleticData(id);
    } catch (error) {
      rethrow;
    }
  }

  Future<List<BodyWeightsData>> getBodyWeights(
      [String? dateFrom, String? dateTo]) async {
    try {
      var authProvider = Provider.of<AuthProvider>(
          appNavigatorKey.currentContext!,
          listen: false);
      var formattedDateFrom;
      var formattedDateTo;
      if (dateFrom != null && dateTo != null) {
        formattedDateFrom = DateHelper.getDateWithFirstDayOfMonth(dateFrom);
        formattedDateTo = DateHelper.getDateWithLastDayOfMonth(dateTo);
      }
      var response = await APISportData().getBodyWeights(
          authProvider.userID!, formattedDateFrom, formattedDateTo);
      if (response.success) {
        return response.data;
      } else {
        return [];
      }
    } catch (error) {
      rethrow;
    }
  }

  Future<List<List<dynamic>>> getExerciseBodyWeights() async {
    BuildContext context = appNavigatorKey.currentContext!;
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    try {
      var response = await Future.wait(
          [getExercises(authProvider.boxID!), getBodyWeights()]);
      return response;
    } catch (error) {
      rethrow;
    }

  }

  Future<void> addWeightData(Weight data) async {
    try {
      var authProvider = Provider.of<AuthProvider>(
          appNavigatorKey.currentContext!,
          listen: false);
      var addWeightDataRequest =
          AddUpdateWeightRequest(date: data.startDate!, weight: data.weight!);
      await APISportData()
          .addWeight(authProvider.userID!, addWeightDataRequest);
    } catch (error) {
      rethrow;
    }
  }

  Future<void> updateWeightData(Weight data, int idWeight) async {
    try {
      var addWeightDataRequest =
          AddUpdateWeightRequest(date: data.startDate!, weight: data.weight!);
      await APISportData().updateWeight(idWeight, addWeightDataRequest);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> deleteBodyWeightsData(int id) async {
    try {
      return APISportData().deleteWeight(id);
    } catch (error) {
      rethrow;
    }
  }

  /// Returns the list of chart series which need to render on the spline chart.
  List<SplineSeries<ChartData, DateTime>> getDefaultSplineSeries(
      List<ChartData> listChartData) {
    return <SplineSeries<ChartData, DateTime>>[
      SplineSeries<ChartData, DateTime>(
        dataSource: <ChartData>[...listChartData],
        xValueMapper: (ChartData item, _) => item.x as DateTime,
        yValueMapper: (ChartData item, _) => item.y,
        markerSettings: const MarkerSettings(isVisible: true),
        dataLabelMapper: (ChartData data, int value) {
          return '${data.y}kg\n${data.label}';
        },
        dataLabelSettings: const DataLabelSettings(
            isVisible: true,
            textStyle: TextStyle(fontSize: 10),
            labelAlignment: ChartDataLabelAlignment.bottom),
      )
    ];
  }

  /// Get default column series
  /// Get default column series
  List<ColumnSeries<ChartData, DateTime>> getDefaultColumnSeries(
      List<ChartData> listChartData) {
    return <ColumnSeries<ChartData, DateTime>>[
      ColumnSeries<ChartData, DateTime>(
        dataSource: <ChartData>[...listChartData],
        xValueMapper: (ChartData sales, _) => sales.x as DateTime,
        yValueMapper: (ChartData sales, _) => sales.y,
        dataLabelMapper: (ChartData data, int value) {
          return '${data.y}kg\n${data.label}';
        },
        dataLabelSettings: const DataLabelSettings(
            isVisible: true,
            textStyle: TextStyle(fontSize: 10),
            labelAlignment: ChartDataLabelAlignment.top),
      )
    ];
  }
}
