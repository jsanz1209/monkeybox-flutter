import 'dart:convert';

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/sport-data/api/add_rm_request.dart';
import 'package:monkeybox/models/sport-data/api/add_update_weight_request.dart';
import 'package:monkeybox/models/sport-data/api/body_weights_response.dart';
import 'package:monkeybox/models/sport-data/api/exercises_response.dart';
import 'package:monkeybox/models/sport-data/api/rm_response.dart';
import 'package:monkeybox/models/sport-data/api/update_rm_request.dart';
import 'package:monkeybox/providers/http/http_client.dart';

class APISportData {
  final Client client = HttpClient.client;

  Future<ExercisesResponse> getExercises(int boxID) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/exercises?filter[box_id]=$boxID');
      var response = await client.get(url);
      var data = json.decode(response.body);
      var exerciseResponse = ExercisesResponse.fromJson(data);
      return exerciseResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<RMResponse> getAthleticsData(int exerciseID, int userID,
      String dateFrom, String dateTo, int? repetitions) async {
    try {
      var httpParams =
          'filter[user_id]=$userID&filter[exercise_id]=$exerciseID&filter[dateFrom]=$dateFrom&filter[dateTo]=$dateTo';
      if (repetitions != null) {
        httpParams =
            'filter[user_id]=$userID&filter[exercise_id]=$exerciseID&filter[repetitions]=$repetitions&filter[dateFrom]=$dateFrom&filter[dateTo]=$dateTo';
      }
      var url = Uri.parse('${Settings.apiURL}/athletic-datas?$httpParams');
      var response = await client.get(url);
      var data = json.decode(response.body);
      var rmResponse = RMResponse.fromJson(data);
      return rmResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> addAthleticData(AddRMRequest addRMRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/athletic-datas');
      var response =
          await HttpClient.client.post(url, body: jsonEncode(addRMRequest));
      var jsonCode = jsonDecode(response.body);
      ApiResponse addRMResponse = ApiResponse.fromJson(jsonCode);
      return addRMResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> updateAthleticData(
      int id, UpdateRMRequest updateRMRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/athletic-datas/$id');
      var response =
          await HttpClient.client.post(url, body: jsonEncode(updateRMRequest));
      var jsonCode = jsonDecode(response.body);
      ApiResponse addRMResponse = ApiResponse.fromJson(jsonCode);
      return addRMResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> deleteAthleticData(int id) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/athletic-datas/$id');
      var response = await client.delete(url);
      var data = json.decode(response.body);
      var deleteAthleticDataRes = ApiResponse.fromJson(data);
      return deleteAthleticDataRes;
    } catch (error) {
      rethrow;
    }
  }

  Future<BodyWeightsResponse> getBodyWeights(int userID,
      [String? dateFrom, String? dateTo]) async {
    try {
      var url;
      if (dateFrom != null && dateTo != null) {
        url = Uri.parse(
            '${Settings.apiURL}/body-weights/$userID/$dateFrom/$dateTo');
      } else {
        url = Uri.parse('${Settings.apiURL}/body-weights/$userID');
      }
      var response = await client.get(url);
      var data = json.decode(response.body);
      var bodyWeightsResponse = BodyWeightsResponse.fromJson(data);
      return bodyWeightsResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> addWeight(int userID, AddUpdateWeightRequest addUpdateWeightRequest) async {
    var url = Uri.parse('${Settings.apiURL}/body-weights/$userID');
    var response =
        await HttpClient.client.post(url, body: jsonEncode(addUpdateWeightRequest));
    var jsonCode = jsonDecode(response.body);
    ApiResponse addWeightResponse = ApiResponse.fromJson(jsonCode);
    return addWeightResponse;
  }

  Future<ApiResponse> updateWeight(
      int idWeight, AddUpdateWeightRequest addUpdateWeightRequest) async {
    var url = Uri.parse('${Settings.apiURL}/body-weights/update/$idWeight');
    var response =
        await HttpClient.client.post(url, body: jsonEncode(addUpdateWeightRequest));
    var jsonCode = jsonDecode(response.body);
    ApiResponse updatedWeightResponse = ApiResponse.fromJson(jsonCode);
    return updatedWeightResponse;
  }

  Future<ApiResponse> deleteWeight(int idWeight) async {
    var url = Uri.parse('${Settings.apiURL}/body-weights/$idWeight');
    var response = await HttpClient.client.delete(url);
    var jsonCode = jsonDecode(response.body);
    ApiResponse deletedWeightResponse = ApiResponse.fromJson(jsonCode);
    return deletedWeightResponse;
  }
}
