import 'package:flutter/material.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/reservations/api/training.dart';
import 'package:monkeybox/models/reservations/api/training_reservation.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/reservations/api_reservations.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:provider/provider.dart';

class ReservationsProvider with ChangeNotifier {
  int _chipIndexSelected = 0;
  int _cancelTime = 0;
  DateTime _selectedDate = DateTime.now();

  set selectedDate(DateTime value) {
    _selectedDate = value;
    notifyListeners();
  }

  DateTime get selectedDate {
    return _selectedDate;
  }

  set chipIndexSelected(int value) {
    _chipIndexSelected = value;
    notifyListeners();
  }

  int get chipIndexSelected {
    return _chipIndexSelected;
  }

  set cancelTime(int value) {
    _cancelTime = value;
    notifyListeners();
  }

  int get cancelTime {
    return _cancelTime;
  }

  _compareHourFn(Training a, Training b) {
    return a.startTime.compareTo(b.startTime);
  }

  _isQueueAuto(List<TrainingReservation> reservations) {
    var userID =
        Provider.of<AuthProvider>(appNavigatorKey.currentContext!).userID;
    var userFound = reservations.indexWhere((reservation) =>
        reservation.user!.id == userID && reservation.queueAutomatic == '1');
    return userFound != -1;
  }

  _isThereMoreThanTime(DateTime date, String hours) {
    var currentTime = DateTime.now();
    var time = hours.split(':');

    date = DateTime(date.year, date.month, date.day, int.parse(time[0]),
        int.parse(time[2]));

    return (date.millisecondsSinceEpoch - currentTime.millisecondsSinceEpoch) >
        (_cancelTime);
  }

  bool isUserReserverd(
      List<TrainingReservation> reservations, BuildContext context) {
    var userID = Provider.of<MenuProvider>(context, listen: false).user!.id;
    return (reservations
            .lastIndexWhere((element) => element.user!.id == userID) !=
        -1);
  }

  bool isPassedDate(DateTime expirationDate) {
    final now = DateTime.now();
    return expirationDate.isBefore(now);
  }

  isThereMoreThanCancelTime(
      Training training, BuildContext context, DateTime dateSelected) {
    if (isUserReserverd(training.reservations, context) &&
        _isQueueAuto(training.reservations)) {
      return true;
    }
    return _isThereMoreThanTime(dateSelected, training.startTime);
  }

  Future<List<Training>> getTrainingsReservations(
      String date, int boxId, int type) async {
    try {
      var response =
          await APIReservations().getTrainingsReservations(date, boxId);
      var formatResponse =
          response.where((element) => element.type == type).toList();
      formatResponse.sort((a, b) => _compareHourFn(a, b));
      return formatResponse;
    } catch (error) {
      rethrow;
    }
  }

  Future<TrainingReservation> setReservation(
      int trainingID, String date) async {
    try {
      var response = await APIReservations().setReservation(trainingID, date);
      return response;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> cancelReservation(int id) async {
    try {
      var response = await APIReservations().cancelReservation(id);
      return response;
    } catch (error) {
      rethrow;
    }
  }
}
