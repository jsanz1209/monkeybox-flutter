import 'dart:convert';

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/reservations/api/reservation_wods.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/reservations/api/training.dart';
import 'package:monkeybox/models/reservations/api/training_reservation.dart';
import 'package:monkeybox/providers/http/http_client.dart';

class APIReservations {
  final Client client = HttpClient.client;

  Future<List<ReservationWODS>> getReservations(int userID) async {
    try {
      var url = Uri.parse(
          '${Settings.apiURL}/reservations?filter[user_id]=$userID&filter[queue]=0');
      var response = await client.get(url);
      List<ReservationWODS> list = [];
      if (response.statusCode != 200) {
        return [];
      }
      var data = json.decode(response.body)['data'];
      for (var element in data) {
        list.add(ReservationWODS.fromJson(element));
      }
      return list;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> getReservationAndTraining(int boxId, String date) async {
    try {
      var url = Uri.https(
          Settings.host,
          Settings.version +
              '/reservations/andtraining/now/date/$date/box/$boxId',
          <String, dynamic>{'expand': 'user.profile'});
      var response = await client.get(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }

  Future<List<Training>> getTrainingsReservations(
      String date, int boxId) async {
    try {
      var url = Uri.https(
          Settings.host,
          Settings.version + '/trainings/date/$date/box/$boxId',
          <String, dynamic>{'expand': 'reservations.user.profile'});

      var response = await client.get(url);
      var jsonResponse = jsonDecode(response.body);
      return (jsonResponse['data'] as List<dynamic>).map((item) {
        return Training.fromJson(item);
      }).toList();
    } catch (error) {
      rethrow;
    }
  }

  Future<TrainingReservation> setReservation(int trainingID, String date) async {
    try {
      var params = {'training_id': trainingID, 'date': date};
      var url = Uri.parse('${Settings.apiURL}/reservations');
      var response = await client.post(url, body: jsonEncode(params));
      var jsonResponse = jsonDecode(response.body);
      return TrainingReservation.fromJson(jsonResponse['data']);
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> cancelReservation(int id) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/reservations/$id/cancel');
      var response = await client.post(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }
}
