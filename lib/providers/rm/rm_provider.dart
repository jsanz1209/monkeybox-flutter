import 'package:flutter/material.dart';
import 'package:monkeybox/models/common/item_autocomplete.dart';
import 'package:monkeybox/models/sport-data/api/rm_data.dart';

class RmProvider with ChangeNotifier {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
  RMData? dataUpdate;
  ItemAutocomplete? itemAutocomplete;

  GlobalKey<NavigatorState> get navigatorKey {
    return _navigatorKey;
  }
}