import 'dart:convert';

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/common/api/response.dart';
import 'package:monkeybox/models/messages/api/message.dart';
import 'package:monkeybox/providers/http/http_client.dart';

class APIMessages {
  final Client client = HttpClient.client;

  Future<List<Message>> getMessages() async {
    try {
      var url = Uri.parse('${Settings.apiURL}/messages');
      var response = await client.get(url);
      List<Message> list = [];
      if (response.statusCode != 200) {
        return [];
      }
      var data = json.decode(response.body)['data'];
      for (var element in data) {
        list.add(Message.fromJson(element));
      }
      return list;
    } catch (error) {
      rethrow;
    }
  }

  Future<ApiResponse> markAsViewedNotification(
      int messageId, int userID) async {
    try {
      var url =
          Uri.parse('${Settings.apiURL}/messages/see/$messageId/user/$userID');
      var response = await client.get(url);
      var jsonResponse = jsonDecode(response.body);
      return ApiResponse.fromJson(jsonResponse);
    } catch (error) {
      rethrow;
    }
  }
}
