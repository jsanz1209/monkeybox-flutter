import 'dart:convert';

import 'package:http/http.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/notifications/register_device_token.dart';
import 'package:monkeybox/providers/http/http_client.dart';

class APINotifications {
  final Client client = HttpClient.client;

  Future<bool?> registerDeviceToken(
      RegisterDeviceTokenRequest registerDeviceTokenRequest) async {
    try {
      var url = Uri.parse('${Settings.apiURL}/user/register-device');
      var response =
          await client.post(url, body: jsonEncode(registerDeviceTokenRequest));
      if (response.statusCode != 200) {
        return null;
      }
      var data = json.decode(response.body)['data'];
      return data;
    } catch (error) {
      rethrow;
    }
  }
}
