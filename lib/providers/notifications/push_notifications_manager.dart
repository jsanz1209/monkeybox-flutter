import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:monkeybox/enums/app_state_enum.dart';
import 'package:monkeybox/helpers/dialogs.dart';
import 'package:monkeybox/models/notifications/register_device_token.dart';
import 'package:monkeybox/providers/notifications/api_notifications.dart';

class PushNotificationManager {
  PushNotificationManager._();

  factory PushNotificationManager() => _instance;

  static final PushNotificationManager _instance = PushNotificationManager._();

  FirebaseMessaging messaging = FirebaseMessaging.instance;

  late StreamSubscription<RemoteMessage> _onMessageSubscription;
  late StreamSubscription<RemoteMessage> _onMessageOpenedAppSubscription;

  /// Function to ask user for push notification permissions and if provided, save FCM Token in persisted local storage.
  void setFCMToken() async {
    /// requesting permission for [alert], [badge] & [sound]. Only for iOS
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      badge: true,
      sound: true,
    );

    /// saving token only if user granted access.
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      String? token = await messaging.getToken();
      debugPrint('FirebaseMessaging token: $token');
      await APINotifications().registerDeviceToken(
          new RegisterDeviceTokenRequest(deviceToken: token!));
      _configure();
    }
  }

  /// Function to configure the functionality of displaying and tapping on notifications.
  void _configure() async {
    /// For iOS only, setting values to show the notification when the app is in foreground state.
    _showForegroundNotificationIniOS();

    /// handler when notification arrives. This handler is executed only when notification arrives in foreground state.
    /// For iOS, OS handles the displaying of notification
    /// For Android, we push local notification
    _onMessageSubscription =
        FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // _showForegroundNotificationInAndroid(message);
      _handleNotification(message, AppStateEnum.foreground);
    });

    /// handler when user taps on the notification.
    /// For iOS, it gets executed when the app is in [foreground] / [background] state.
    /// For Android, it gets executed when the app is in [background] state.
    _onMessageOpenedAppSubscription =
        FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      _handleNotification(message, AppStateEnum.foreground);
    });

    /// If the app is launched from terminated state by tapping on a notification, [getInitialMessage] function will return the
    /// [RemoteMessage] only once.
    RemoteMessage? initialMessage = await messaging.getInitialMessage();

    /// if [RemoteMessage] is not null, this means that the app is launched from terminated state by tapping on the notification.
    if (initialMessage != null) {
      _handleNotification(initialMessage, AppStateEnum.terminated);
    }
  }

  // void _showForegroundNotificationInAndroid(RemoteMessage message) async {}

  void _showForegroundNotificationIniOS() async {
    await messaging.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  void _handleNotification(RemoteMessage message, AppStateEnum appState) {
    debugPrint('title ' + message.notification!.title.toString());
    debugPrint('body ' + message.notification!.body.toString());
    Dialogs.showConfirmDialogMaterial(
        message.notification!.title, message.notification!.body,
        withActions: true, confirmButtonText: "Ok", cancelButtonText: null);
  }

  void cancelSubscription() {
    _onMessageSubscription.cancel();
    _onMessageOpenedAppSubscription.cancel();
    messaging.deleteToken();
  }
}
