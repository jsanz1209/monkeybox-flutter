/// Flutter icons MonkeyboxIcons
/// Copyright (C) 2021 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  MonkeyboxIcons
///      fonts:
///       - asset: fonts/MonkeyboxIcons.ttf
///
/// 
///
import 'package:flutter/widgets.dart';

class MonkeyboxIcons {
  MonkeyboxIcons._();

  static const _kFontFam = 'MonkeyboxIcons';
  static const String? _kFontPkg = null;

  static const IconData custom_stats = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData custom_skull = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData body = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData custom_wods = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
