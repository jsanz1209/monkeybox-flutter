enum MenuOptionsMenuEnum {
  home,
  paymentManagement,
  reservations,
  wods,
  wodsCompetitors,
  rm,
  weight,
  timer,
  fees,
  myStats,
  notifications,
  profile,
  boxContact,
  boxConditions,
  tutorial,
  logout
}