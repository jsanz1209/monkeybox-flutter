import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liquid_swipe/liquid_swipe.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/onboarding/item_data_onboarding.dart';
import 'package:monkeybox/pages/side-menu/dashboard/dashboard_page.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/widgets/auth/auth_page.dart';
import 'package:provider/provider.dart';

/// Example of LiquidSwipe with itemBuilder
class OnBoarding extends StatefulWidget {
  @override
  _OnBoarding createState() => _OnBoarding();
}

class _OnBoarding extends State<OnBoarding> {
  int page = 0;
  late LiquidController liquidController;
  late UpdateType updateType;

  List<ItemDataOnboarding> data = [
    ItemDataOnboarding(
      HexColor('#233645'),
      'assets/images/onboarding/wodcellent-consulta-wod-diarios.gif',
      'Consulta WODs diarios',
      '<p>Desde la sección <strong>"WODs"</strong> podrás consultar en <strong>cualquier momento</strong> los <strong>WODs diarios</strong> y <strong>WODs antiguos</strong>.</p>',
    ),
    ItemDataOnboarding(
      HexColor('#141D24'),
      'assets/images/onboarding/wodcellent-anotación-resultados-wods.gif',
      'Anotación de resultados de WODs',
      '<p>Desde la sección <strong>"WODs"</strong> puedes establecer o editar tus resultados.</p>',
    ),
    ItemDataOnboarding(
      Colors.grey,
      'assets/images/onboarding/wodcellent-ranking-diario.gif',
      'Consultar ranking diario',
      '<p>Desde la sección <strong>"WODs"</strong> puedes consultar la <strong>clasificación en vivo</strong> de los participantes a lo largo del día.</p>',
    ),
    ItemDataOnboarding(
      Colors.blueGrey,
      'assets/images/onboarding/wodcellent-videos-explicativos-ejercicios.gif',
      'Vídeos explicativos de ejercicios',
      '<p>Desde la sección <strong>"WODs"</strong> habrá <strong>ejercicios enlazados</strong>, al <strong>pulsar los enlaces</strong> aparecerá información adicional del ejercicio, como <strong>descripción o vídeo</strong>.</p>',
    ),
    ItemDataOnboarding(
      Colors.blue,
      'assets/images/onboarding/wodcellent-reservas.gif',
      'Reserva de clase',
      '<p>Desde la sección <strong>"Reservas"</strong> puedes <strong>reservar</strong> tu hora para <strong>WOD, OpenBOX o Competidores</strong> (si tu BOX ha activado esta modalidad).',
    ),
    ItemDataOnboarding(
      Colors.green,
      'assets/images/onboarding/frontend-cuotas-bonos.gif',
      'Paga tu cuota desde la APP',
      '<p>Si tu BOX lo permite, verás la sección <strong>"Cuotas y bonos"</strong> desde la cual puedes realizar el <strong>pago</strong> de tu cuota con tu <strong>tarjeta</strong>.</p>',
    ),
    ItemDataOnboarding(
      Colors.greenAccent,
      'assets/images/onboarding/frontend-perfil.gif',
      'Perfil',
      '<p>Establece tus datos personales y privacidad desde <strong>"Perfil"</strong>.</p>',
    ),
  ];

  @override
  void initState() {
    super.initState();
    liquidController = LiquidController();
  }

  Widget _buildDot(int index) {
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((page) - index).abs(),
      ),
    );
    double zoom = 1.0 + (2.0 - 1.0) * selectedness;
    return new Container(
      width: 25.0,
      child: new Center(
        child: new Material(
          color: Colors.white,
          type: MaterialType.circle,
          child: new Container(
            width: 8.0 * zoom,
            height: 8.0 * zoom,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        LiquidSwipe.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            return Container(
              width: double.infinity,
              color: data[index].color,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                // mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    data[index].image,
                    fit: BoxFit.contain,
                    width: 230.sp,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(28.0),
                    child: Column(
                      children: <Widget>[
                        Text(
                          data[index].title,
                          style: Theme.of(context).textTheme.headline1,
                          textAlign: TextAlign.center,
                        ),
                        Html(data: data[index].description, style: {
                          'p': Style(
                              margin: EdgeInsets.zero,
                              color: Colors.white,
                              fontFamily: 'WorkSans',
                              fontSize: FontSize.rem(0.9),
                              lineHeight: LineHeight.rem(1.15))
                        }),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
          positionSlideIcon: 0.05,
          slideIconWidget: IconButton(
            onPressed: () {
              liquidController.animateToPage(
                  page: liquidController.currentPage + 1);
            },
            icon: CircleAvatar(
              backgroundColor: data[
                      liquidController.currentPage < data.length - 1
                          ? (liquidController.currentPage + 1)
                          : 0]
                  .color,
              radius: 30,
              child: Icon(
                Icons.navigate_next_rounded,
                color: Colors.white,
              ),
            ),
          ),
          onPageChangeCallback: pageChangeCallback,
          waveType: WaveType.liquidReveal,
          liquidController: liquidController,
          fullTransitionValue: 600,
          // enableSideReveal: true,
          enableLoop: true,
          ignoreUserGestureWhileAnimating: true,
        ),
        Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Expanded(child: SizedBox()),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List<Widget>.generate(data.length, _buildDot),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: TextButton(
              style: Theme.of(context).textButtonTheme.style!.copyWith(
                  foregroundColor: MaterialStateProperty.resolveWith(
                      (states) => Colors.white)),
              onPressed: () {
                if (liquidController.currentPage < data.length - 1) {
                  liquidController.animateToPage(
                      page: data.length - 1, duration: 700);
                } else {
                  var authProvider =
                      Provider.of<AuthProvider>(context, listen: false);
                  if (authProvider.isLoggedIn) {
                    Navigator.of(context)
                        .pushReplacementNamed(DashboardPage.routeName);
                  } else {
                    Navigator.of(context)
                        .pushReplacementNamed(AuthPage.routeName);
                  }
                }
              },
              child: liquidController.currentPage < data.length - 1
                  ? Text('Ir al final')
                  : Text('Finalizar'),
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: TextButton(
              style: Theme.of(context).textButtonTheme.style!.copyWith(
                  foregroundColor: MaterialStateProperty.resolveWith(
                      (states) => Colors.white)),
              onPressed: () {
                liquidController.jumpToPage(
                    page: liquidController.currentPage + 1 > data.length - 1
                        ? 0
                        : liquidController.currentPage + 1);
              },
              child: Text(
                'Siguiente',
              ),
            ),
          ),
        )
      ],
    ));
  }

  pageChangeCallback(int lpage) {
    setState(() {
      page = lpage;
    });
  }
}
