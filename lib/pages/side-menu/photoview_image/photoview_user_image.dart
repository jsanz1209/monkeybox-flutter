import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:photo_view/photo_view.dart';

class PhotoViewUserImage extends StatelessWidget {
  static const routeName = 'photoview-user-image';
  const PhotoViewUserImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var arguments = ModalRoute.of(context)!.settings.arguments as dynamic;
    var tag = arguments['tag'] as String;
    var imageUrl = arguments['imageUrl'] as String;
    return Scaffold(
      backgroundColor: Colors.black,
      appBar:
          CustomHeader(title: '', withBackButton: true, isTransparent: true),
      body: Hero(
        tag: tag,
        transitionOnUserGestures: true,
        child: Center(
          child: PhotoView(
            imageProvider: CachedNetworkImageProvider(imageUrl),
          ),
        ),
      ),
    );
  }
}
