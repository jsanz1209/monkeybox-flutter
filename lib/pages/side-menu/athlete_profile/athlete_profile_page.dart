import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/enums/category_intensity_enum.dart';
import 'package:monkeybox/models/user/api/profile.dart';
import 'package:monkeybox/models/wods/api/intensity_category.dart';
import 'package:monkeybox/pages/side-menu/photoview_image/photoview_user_image.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:url_launcher/url_launcher.dart';

class AthleteProfilePage extends StatefulWidget {
  static const routeName = 'profile';
  const AthleteProfilePage({Key? key}) : super(key: key);

  @override
  _AthleteProfilePageState createState() => _AthleteProfilePageState();
}

class _AthleteProfilePageState extends State<AthleteProfilePage> {
  @override
  Widget build(BuildContext context) {
    final String languageCode = Localizations.localeOf(context).languageCode;
    Size screenSize = MediaQuery.of(context).size;
    // force max container height to 300
    double containerHeight = screenSize.height * 0.35;
    if (containerHeight > 300) {
      containerHeight = 300;
    }
    var arguments =
        (ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>);
    Profile profile = arguments['profile'] as Profile;
    var tag = arguments['tag'];

    return Scaffold(
      appBar: CustomHeader(
        title: 'Perfil del Atleta'.toUpperCase(),
        withBackButton: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                height: containerHeight,
                decoration:
                    BoxDecoration(color: Theme.of(context).primaryColor),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        color: Theme.of(context).colorScheme.secondary,
                        height: containerHeight,
                        child: Hero(
                          tag: tag,
                          transitionOnUserGestures: true,
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).pushNamed(
                                  PhotoViewUserImage.routeName,
                                  arguments: {
                                    'tag': tag,
                                    'imageUrl': profile.pictureUrl,
                                  });
                            },
                            child: CachedNetworkImage(
                              imageUrl: profile.pictureUrl!,
                              fit: BoxFit.cover,
                              placeholder: (ctx, url) =>
                                  placeholderErrorImageWidget,
                              errorWidget: (ctx, url, _) =>
                                  placeholderErrorImageWidget,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 16.0, horizontal: 12),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              FittedBox(
                                alignment: Alignment.centerLeft,
                                child: Text(profile.getFullName(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline2!
                                        .copyWith(color: Colors.white)),
                              ),
                              if (profile.blocksVictories != null &&
                                  profile.blocksVictories != 0)
                                Container(
                                    margin: EdgeInsets.only(top: 10.sp),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary,
                                        borderRadius:
                                            BorderRadius.circular(25)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text.rich(
                                          TextSpan(
                                            text: '${profile.blocksVictories} ',
                                            children: [
                                              TextSpan(
                                                  text: 'VICTORIAS BLOQUES',
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.normal)),
                                            ],
                                          ),
                                          style: TextStyle(
                                              color: CustomExtraThemeData
                                                      .getExtraThemeData()
                                                  .textSideNavColor,
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.bold)),
                                    )),
                              if (profile.wodsVictories != 0)
                                Container(
                                    margin: EdgeInsets.only(top: 10.sp),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary,
                                        borderRadius:
                                            BorderRadius.circular(25)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text.rich(
                                          TextSpan(
                                            text: '${profile.wodsVictories} ',
                                            children: [
                                              TextSpan(
                                                  text: 'VICTORIAS WODS',
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.normal)),
                                            ],
                                          ),
                                          style: TextStyle(
                                              color: CustomExtraThemeData
                                                      .getExtraThemeData()
                                                  .textSideNavColor,
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.bold)),
                                    )),
                              // WHATSAPP
                              profile.privacy != null &&
                                      profile.privacy!.showWhatsapp! &&
                                      profile.privacy!.whatsapp != null &&
                                      profile.privacy!.whatsapp.isNotEmpty
                                  ? InkWell(
                                      onTap: () {
                                        _launchURL(
                                            'whatsapp://send?phone=+34${profile.privacy!.whatsapp!}',
                                            'https://api.whatsapp.com/send?phone=${profile.privacy!.whatsapp!}');
                                      },
                                      child: Container(
                                          margin: EdgeInsets.only(top: 12.sp),
                                          padding: EdgeInsets.zero,
                                          width: 124,
                                          decoration: BoxDecoration(
                                            border:
                                                Border.all(color: Colors.white),
                                            borderRadius:
                                                BorderRadius.circular(80),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 6.0, vertical: 4),
                                            child: Row(
                                              children: [
                                                SvgPicture.asset(
                                                  'assets/images/icons/whatsapp.svg',
                                                  color: Colors.white,
                                                  height: 20.sp,
                                                  width: 20.sp,
                                                ),
                                                SizedBox(
                                                  width: 7.sp,
                                                ),
                                                Text(
                                                  profile.privacy!.whatsapp,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText2!
                                                      .copyWith(
                                                          color: Colors.white,
                                                          fontSize: 12.sp,
                                                          height: 1.12.sp),
                                                )
                                              ],
                                            ),
                                          )),
                                    )
                                  : SizedBox(),
                              SizedBox(
                                height: 12.sp,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  //Instagram
                                  profile.privacy != null &&
                                          profile.privacy!.showInstagram! &&
                                          profile.privacy!.instagram != null &&
                                          profile.privacy!.instagram!.isNotEmpty
                                      ? Container(
                                          margin: EdgeInsets.only(right: 6.sp),
                                          child: IconButton(
                                            constraints:
                                                BoxConstraints(maxWidth: 62.sp),
                                            color: Colors.red,
                                            padding: EdgeInsets.zero,
                                            visualDensity:
                                                VisualDensity.compact,
                                            alignment: Alignment.centerLeft,
                                            onPressed: () {
                                              _launchURL(
                                                  'instagram://user?username=${profile.privacy!.instagram!}',
                                                  'https://instagram.com/${profile.privacy!.instagram!}');
                                            },
                                            icon: SvgPicture.asset(
                                              'assets/images/icons/instagram.svg',
                                              color: Colors.white,
                                              height: 24.sp,
                                              width: 24.sp,
                                            ),
                                          ),
                                        )
                                      : SizedBox.shrink(),
                                  // Facebook
                                  profile.privacy != null &&
                                          profile.privacy!.showFacebook! &&
                                          profile.privacy!.facebook!.isNotEmpty
                                      ? Container(
                                          margin: EdgeInsets.only(right: 6.sp),
                                          child: IconButton(
                                            constraints:
                                                BoxConstraints(maxWidth: 62.sp),
                                            padding: EdgeInsets.zero,
                                            visualDensity:
                                                VisualDensity.compact,
                                            alignment: Alignment.centerLeft,
                                            onPressed: () {
                                              _launchURL(
                                                  'fb://page/page_id/${profile.privacy!.facebook!}',
                                                  'https://facebook.com/${profile.privacy!.facebook!}');
                                            },
                                            icon: SvgPicture.asset(
                                              'assets/images/icons/facebook.svg',
                                              color: Colors.white,
                                              height: 24.sp,
                                              width: 24.sp,
                                            ),
                                          ),
                                        )
                                      : SizedBox.shrink(),
                                  // Twitter
                                  profile.privacy != null &&
                                          profile.privacy!.showTwitter! &&
                                          profile.privacy!.twitter != null &&
                                          profile.privacy!.twitter!.isNotEmpty
                                      ? IconButton(
                                          constraints:
                                              BoxConstraints(maxWidth: 62.sp),
                                          padding: EdgeInsets.zero,
                                          visualDensity: VisualDensity.compact,
                                          alignment: Alignment.centerLeft,
                                          onPressed: () {
                                            _launchURL(
                                                'tw://${profile.privacy!.twitter!}',
                                                'https://twitter.com/${profile.privacy!.twitter!}');
                                          },
                                          icon: SvgPicture.asset(
                                            'assets/images/icons/twitter.svg',
                                            color: Colors.white,
                                            height: 24.sp,
                                            width: 24.sp,
                                          ),
                                        )
                                      : SizedBox.shrink(),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                )),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
              color: Colors.grey.shade300,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Email',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 12.sp),
                  ),
                  InkWell(
                    onTap: () {
                      _launchURL('mailto:${profile.email}', 'mailto:${profile.email}');
                    },
                    child: Text(
                      profile.email,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.black54),
                    ),
                  ),
                  profile.privacy != null && profile.privacy!.birthdate!
                      ? Container(
                          margin: EdgeInsets.only(top: 15.sp),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Fecha de nacimiento',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(fontSize: 12.sp),
                              ),
                              Text(
                                  DateFormat('dd MMMM yyyy', languageCode)
                                      .format(profile.getBirthdateAsDateTime())
                                      .toString(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(color: Colors.black54))
                            ],
                          ),
                        )
                      : SizedBox(),
                  profile.privacy != null && profile.privacy!.age!
                      ? Container(
                          margin: EdgeInsets.only(top: 15.sp),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Edad',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(fontSize: 12.sp),
                              ),
                              Text(
                                profile.getAge().toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(color: Colors.black54),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                  profile.privacy != null && profile.privacy!.gender!
                      ? Container(
                          margin: EdgeInsets.only(top: 15.sp),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Sexo',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(fontSize: 12.sp),
                              ),
                              Text(
                                profile.gender == 'M' ? 'Hombre' : 'Mujer',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(color: Colors.black54),
                              ),
                            ],
                          ),
                        )
                      : SizedBox()
                ],
              ),
            ),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  profile.privacy != null && profile.privacy!.height!
                      ? Container(
                          margin: EdgeInsets.only(top: 16.sp),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/icons/height.svg',
                                  color: Colors.black45,
                                  height: 32.sp,
                                  width: 32.sp,
                                ),
                                SizedBox(
                                  width: 8.sp,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Estatura',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(fontSize: 12.sp),
                                    ),
                                    Text(
                                      '${profile.height} m',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(color: Colors.black),
                                    ),
                                  ],
                                ),
                              ]),
                        )
                      : SizedBox(),
                  profile.privacy != null && profile.privacy!.weight!
                      ? Container(
                          margin: EdgeInsets.only(top: 16.sp),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 6.0),
                                  child: SvgPicture.asset(
                                    'assets/images/icons/weight.svg',
                                    color: Colors.black45,
                                    height: 22.sp,
                                    width: 22.sp,
                                  ),
                                ),
                                SizedBox(
                                  width: 12.sp,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Peso',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(fontSize: 12.sp),
                                    ),
                                    Text(
                                      '${profile.weight} kg',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(color: Colors.black),
                                    ),
                                  ],
                                ),
                              ]),
                        )
                      : SizedBox(),
                  profile.privacy != null && profile.privacy!.favoriteCategory!
                      ? Container(
                          margin: EdgeInsets.only(top: 16.sp),
                          child: Row(children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 6.0),
                              child: _getIconIntensityCategory(
                                  profile.intensityCategory!),
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Categoría Favorita',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2!
                                      .copyWith(fontSize: 12.sp),
                                ),
                                Text(
                                  profile.intensityCategory!.category,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(color: Colors.black),
                                ),
                              ],
                            ),
                          ]),
                        )
                      : SizedBox(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _launchURL(String url, String optionalURL) async {
    if (Platform.isAndroid) {
      try {
        await launchUrl(Uri.parse(url));
      } catch (_) {
        await launchUrl(Uri.parse(optionalURL));
      }
    } else {
      await launchUrl(Uri.parse(optionalURL));
    }
  }

  Icon get placeholderErrorImageWidget {
    return Icon(
      Icons.image,
      color: Colors.white,
      size: 116.sp,
    );
  }

  SvgPicture _getIconIntensityCategory(IntensityCategory intensityCategory) {
    if ((intensityCategory.id - 1) == CategoryIntensityEnum.rx.index) {
      return SvgPicture.asset(
        'assets/images/icons/trophy.svg',
        color: Colors.black45,
        height: 24.sp,
        width: 24.sp,
      );
    } else if ((intensityCategory.id - 1) ==
        CategoryIntensityEnum.scaled.index) {
      return SvgPicture.asset(
        'assets/images/icons/medal.svg',
        color: Colors.black45,
        height: 24.sp,
        width: 24.sp,
      );
    } else if ((intensityCategory.id - 1) ==
        CategoryIntensityEnum.rookie.index) {
      return SvgPicture.asset(
        'assets/images/icons/ribbon.svg',
        color: Colors.black45,
        height: 24.sp,
        width: 24.sp,
      );
    } else {
      return SvgPicture.asset(
        'assets/images/icons/happy.svg',
        color: Colors.black45,
        height: 24.sp,
        width: 24.sp,
      );
    }
  }
}
