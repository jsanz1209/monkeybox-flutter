import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';
import 'package:provider/provider.dart';

class NotificationsPage extends StatefulWidget {
  static const routeName = 'notifications';
  const NotificationsPage({Key? key}) : super(key: key);

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      var menuProvider = Provider.of<MenuProvider>(context, listen: false);
      var notReadMessages = menuProvider.notReadMessages;
      await menuProvider.markAsViewedNotification(notReadMessages);
    });
  }

  @override
  Widget build(BuildContext context) {
    final String languageCode = Localizations.localeOf(context).languageCode;
    var messages = Provider.of<MenuProvider>(context, listen: false).messages;
    return Scaffold(
        backgroundColor: HexColor('#e5e5e5'),
        appBar: CustomMenuHeader(title: 'NOTIFICACIONES'),
        drawer: NavigationDrawer(),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(8.sp),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ...messages.map((e) => Card(
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 16.sp,
                            right: 16.sp,
                            top: 32.sp,
                            bottom: 16.sp),
                        child: Container(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 8.sp),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      DateFormat('EEEE dd MMMM yyyy, HH:mm',
                                              languageCode)
                                          .format(DateTime.parse(e.updatedAt)),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(fontSize: 12.sp),
                                    ),
                                    SizedBox(
                                      height: 4.sp,
                                    ),
                                    Text(
                                      e.title,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              Html(
                                data: e.message,
                                style: {
                                  'span': Style(),
                                  'p': Style(lineHeight: LineHeight.normal)
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ))
              ],
            ),
          ),
        ));
  }
}
