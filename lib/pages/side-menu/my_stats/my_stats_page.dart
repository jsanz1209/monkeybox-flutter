import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';

class MyStatsPage extends StatelessWidget {
  static const routeName = 'my-stats';
  const MyStatsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomMenuHeader(title: 'MIS ESTADÍSTICAS'),
        drawer: NavigationDrawer(),
        body: Center(child: Text('My Stats works')));
  }
}
