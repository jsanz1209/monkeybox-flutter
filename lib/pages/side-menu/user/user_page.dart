import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/providers/user/user_provider.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';
import 'package:monkeybox/widgets/side_menu/user/profile_page.dart';
import 'package:monkeybox/widgets/side_menu/user/user_history.dart';
import 'package:provider/provider.dart';

class UserPage extends StatefulWidget {
  static const routeName = 'user';
  const UserPage({Key? key}) : super(key: key);
  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;
  late PageController _pageController;
  List<Widget> _widgetOptions = [];

  @override
  void initState() {
    super.initState();
    _pageController =
        PageController(initialPage: 0, keepPage: true, viewportFraction: 1);
    _widgetOptions = <Widget>[
      UserProfile(scaffoldKey: _scaffoldKey),
      const UserHistory()
    ];
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => UserProvider(),
      child: Scaffold(
        backgroundColor: Colors.white,
        extendBody: true,
        endDrawerEnableOpenDragGesture: false,
        key: _scaffoldKey,
        appBar: _selectedIndex == 1
            ? CustomMenuHeader(title: 'MI HISTORIAL')
            : null,
        drawer: NavigationDrawer(),
        body: _buildPageView(),
        bottomNavigationBar: CurvedNavigationBar(
          backgroundColor: Colors.transparent,
          color: Theme.of(context).primaryColor,
          index: _selectedIndex,
          items: _bottomNavigationItems,
          onTap: (index) {
            //Handle button tap
            setState(() {
              _selectedIndex = index;
              _pageController.animateToPage(index,
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.linear);
            });
          },
        ),
      ),
    );
  }

  List<Widget> get _bottomNavigationItems {
    return <Widget>[
      _selectedIndex == 1
          ? Container(
              margin: EdgeInsets.only(top: 20.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Icon(
                    Icons.account_circle,
                    size: 28.w,
                    color: Colors.white,
                  ),
                  Text(
                    'Mi Cuenta',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 14.sp, color: Colors.white),
                  ),
                ],
              ),
            )
          : Icon(
              Icons.account_circle,
              size: 32.sp,
              color: Colors.white,
            ),
      _selectedIndex == 0
          ? Container(
              margin: EdgeInsets.only(top: 20.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SvgPicture.asset(
                    'assets/images/icons/list.svg',
                    color: Colors.white,
                    height: 28.w,
                    width: 28.w,
                  ),
                  Text(
                    'Mi Historial',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 14.sp, color: Colors.white),
                  ),
                ],
              ),
            )
          : SvgPicture.asset(
              'assets/images/icons/list.svg',
              color: Colors.white,
              height: 32.sp,
              width: 32.sp,
            ),
    ];
  }

  Widget _buildPageView() {
    return PageView(
        clipBehavior: Clip.none,
        controller: _pageController,
        onPageChanged: (index) {
          _pageChanged(index);
        },
        // allowImplicitScrolling: true,
        children: _widgetOptions);
  }

  void _pageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
