import 'package:flutter/material.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:monkeybox/widgets/side_menu/wods/wods_nested_routes.dart';
import 'package:provider/provider.dart';

class MainWodsPage extends StatelessWidget {
  static const routeName = 'wods';
  static const routeNameCompetitors = 'wods-competitors';
  final String setupPageRoute;
  final bool? isCompetitor;
  const MainWodsPage(
      {Key? key, required this.setupPageRoute, this.isCompetitor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('Main WODs build');
    return ChangeNotifierProvider(
        create: (_) => WodsProvider(),
        child: Scaffold(
            body: WodsNestedRoutes(
          setupPageRoute: setupPageRoute,
          isCompetitor: isCompetitor,
        )));
  }
}
