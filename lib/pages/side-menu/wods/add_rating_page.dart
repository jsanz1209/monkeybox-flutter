import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/dialogs.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/wods/api/add_rating_request.dart';
import 'package:monkeybox/models/wods/api/feedback_user_wod.dart';
import 'package:monkeybox/models/wods/api/rating.dart';
import 'package:monkeybox/models/wods/api/wod.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:monkeybox/widgets/side_menu/wods/add_rating/effort_buttons.dart';
import 'package:monkeybox/widgets/side_menu/wods/add_rating/blocks_rating.dart';
import 'package:monkeybox/widgets/side_menu/wods/add_rating/mood_buttons.dart';
import 'package:provider/provider.dart';

class AddRatingPage extends StatefulWidget {
  static const routeName = 'add-rating';
  const AddRatingPage({Key? key}) : super(key: key);

  @override
  State<AddRatingPage> createState() => _AddRatingPageState();
}

class _AddRatingPageState extends State<AddRatingPage> {
  late Future<FeedbackUserWod?> _futureUserFeedbackWod;
  int? _effortSelected;
  int? _moodSelected;
  bool _isUpdatedFeedbackMode = false;
  int? _feedBackID;

  void setFilledFeedback(bool notify) {
    Provider.of<WodsProvider>(context, listen: false).setIsFeedbackFilled(
        _moodSelected != null && _effortSelected != null, notify);
  }

  bool get isFeedBackFilled {
    return Provider.of<WodsProvider>(context, listen: false).isFeedbackFilled;
  }

  @override
  void deactivate() {
    super.deactivate();
    _moodSelected = null;
    _effortSelected = null;
    setFilledFeedback(false);
  }

  @override
  Widget build(BuildContext context) {
    var wod = (ModalRoute.of(context)!.settings.arguments) as Wod;
    _futureUserFeedbackWod = Provider.of<WodsProvider>(context, listen: false)
        .getUserWodFeedback(wod.id);
    return Scaffold(
        appBar: CustomHeader(
          title: 'ALTA DE RANKINGS',
          withBackButton: true,
        ),
        body: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Container(
              margin: EdgeInsets.only(
                  right: 16.sp, left: 16.sp, top: 32.sp, bottom: 8.sp),
              child: FutureBuilder(
                  future: _futureUserFeedbackWod,
                  builder: (ctx, asyncSnapShot) {
                    if (asyncSnapShot.connectionState == ConnectionState.done) {
                      FeedbackUserWod? feedbackUserWod =
                          asyncSnapShot.data as FeedbackUserWod?;
                      if (feedbackUserWod != null) {
                        _isUpdatedFeedbackMode = true;
                        _feedBackID = feedbackUserWod.id;
                        _effortSelected = feedbackUserWod.effort;
                        _moodSelected = feedbackUserWod.mood;
                        Future.delayed(const Duration(milliseconds: 100), () {
                          setFilledFeedback(true);
                        });
                      }
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Percepción de esfuerzo',
                            style: Theme.of(context)
                                .textTheme
                                .headline1!
                                .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 16.sp,
                          ),
                          EffortButtons(
                            effort: feedbackUserWod != null
                                ? feedbackUserWod.effort
                                : null,
                            onEffortSelected: (value) {
                              var beforeEffortValue = _effortSelected;
                              _effortSelected = value;
                              if (beforeEffortValue == null) {
                                setFilledFeedback(true);
                              }
                            },
                          ),
                          SizedBox(
                            height: 32.sp,
                          ),
                          Text(
                            'Estado anímico',
                            style: Theme.of(context)
                                .textTheme
                                .headline1!
                                .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                          ),
                          MoodButtons(
                            mood: feedbackUserWod != null
                                ? feedbackUserWod.mood
                                : null,
                            onMoodSelected: (value) {
                              var beforeMoodValue = _moodSelected;
                              _moodSelected = value;
                              if (beforeMoodValue == null) {
                                setFilledFeedback(true);
                              }
                            },
                          ),
                        ],
                      );
                    } else {
                      return SizedBox();
                    }
                  }),
            ),
            BlocksRatings(
                wod: wod,
                onDeleteRatings: _deleteRatings,
                onSubmit: (List datas, int intensityCategor) =>
                    _submit(wod.id, datas, intensityCategor))
          ]),
        ));
  }

  Future<bool> _showDialogDeletingRating() async {
    return await Dialogs.showConfirmDialogMaterial('ELIMINAR RESULTADOS',
        'Vas a proceder a eliminar los resultados guardados, ¿estás seguro de que deseas eliminarlos?');
  }

  void _deleteRatings(List<Rating> ratings) async {
    var userWantsDeletingRatings = await _showDialogDeletingRating();
    if (userWantsDeletingRatings) {
      List<Future> futuresAPIDeleteRating = [];
      var wodsProvider = Provider.of<WodsProvider>(context, listen: false);

      for (var rating in ratings) {
        futuresAPIDeleteRating.add(wodsProvider.deleteRating(rating.id));
      }

      await wodsProvider.crudRatings(futuresAPIDeleteRating);
      SnackBarHelper.showSnackBar(
          'RESULTADOS',
          'Se han borrado los resultados correctamente',
          TypeSnackBarEnum.success);
      Navigator.of(context).pop(true);
    }
  }

  void _submit(int wodID, List datas, int intensityCategory) async {
    var user = Provider.of<MenuProvider>(context, listen: false).user;
    var wodsProvider = Provider.of<WodsProvider>(context, listen: false);
    if (isFeedBackFilled) {
      var user = Provider.of<MenuProvider>(context, listen: false).user;
      if (_isUpdatedFeedbackMode) {
        await Provider.of<WodsProvider>(context, listen: false)
            .updateUserWodFeedback(_feedBackID!, wodID.toString(),
                _effortSelected!, _moodSelected!, user!.id);
      } else {
        await Provider.of<WodsProvider>(context, listen: false)
            .createUserWodFeedback(
                wodID.toString(), _effortSelected!, _moodSelected!, user!.id);
      }

      SnackBarHelper.showSnackBar(
          'FEEDBACK',
          _isUpdatedFeedbackMode
              ? 'El feeback del WOD se ha actualizado correctamente'
              : 'El feedback del WOD se ha añadido correctamente',
          TypeSnackBarEnum.success);
    }

    List<AddRatingRequest> requests =
        wodsProvider.getRatingRequests(datas, intensityCategory, user!.id);

    if (requests.isNotEmpty) {
      List<Future> futuresAPIUpgradeRating = [];
      var indexRequest = 0;
      for (var i = 0; i < datas.length; i++) {
        var data = datas[i];
        var isUpdateRating = data['isUpdateRating'] != null;
        for (var j = 0; j < data['ratingTypes'].length; j++) {
          if (indexRequest < requests.length) {
            if (isUpdateRating) {
              futuresAPIUpgradeRating.add(wodsProvider.updateRating(
                  data['idRatings'][j], requests[indexRequest]));
            } else {
              futuresAPIUpgradeRating
                  .add(wodsProvider.addRating(requests[indexRequest]));
            }
            indexRequest++;
          }
        }
      }
      await wodsProvider.crudRatings(futuresAPIUpgradeRating);
      Navigator.of(context).pop(true);
    }
  }
}
