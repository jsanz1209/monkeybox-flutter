import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/models/wods/api/wod.dart';
import 'package:monkeybox/pages/side-menu/chronometer/chronometer_page.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/chrono/chrono_bloc.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:monkeybox/widgets/common/custom_floating_button.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:monkeybox/widgets/common/date_picker.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';
import 'package:monkeybox/widgets/side_menu/wods/wod_widget.dart';
import 'package:provider/provider.dart';

class WodsPage extends StatefulWidget {
  static const routeName = 'wods-by-date';
  final bool? isCompetitor;
  const WodsPage({Key? key, this.isCompetitor}) : super(key: key);

  @override
  _WodsPageState createState() => _WodsPageState();
}

class _WodsPageState extends State<WodsPage> {
  DateTime currentDate = DateTime.now();
  Wod? wod;
  late ScrollController _scrollController;
  int _sensitivity = 8;
  final _chronoBloc = ChronoBloc();

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    loadWod();
    loadExercises();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    var rankingWidgetKey =
        Provider.of<WodsProvider>(context, listen: false).rankingWidgetKey;
    return Scaffold(
      appBar: CustomMenuHeader(
        title: widget.isCompetitor != null ? 'WODS COMPETIDORES' : 'WODS',
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.sp, vertical: 12.sp),
            child: ElevatedButton.icon(
                style: OutlinedButton.styleFrom(
                  backgroundColor: CustomExtraThemeData.getExtraThemeData().buttonBackground,
                  primary: Colors.black,
                  elevation: 0,
                  visualDensity: VisualDensity.compact,
                  maximumSize: Size(200.sp, 42.sp),
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: CustomExtraThemeData.getExtraThemeData().buttonBorderColor!),
                      borderRadius: BorderRadius.circular(30)),
                ),
                onPressed: () {
                  var menuProvider =
                      Provider.of<MenuProvider>(context, listen: false);
                  _chronoBloc.fromWODs = true;
                  Navigator.of(menuProvider.navigatorKey.currentContext!)
                      .pushNamed(ChronometerPage.routeName);
                },
                icon: Icon(
                  Icons.timer,
                  size: 24.sp,
                ),
                label: Text('CRONO',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Oswald',
                        fontSize: 16.sp))),
          )
        ],
      ),
      drawer: NavigationDrawer(),
      body: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          controller: _scrollController,
          child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > _sensitivity) {
                changeDateTime(currentDate.subtract(const Duration(days: 1)));
                // Right Swipe
              } else if (details.delta.dx < -_sensitivity) {
                changeDateTime(currentDate.add(const Duration(days: 1)));
              } //Left Swipe
            },
            child: Column(
              children: [
                DatePickerWidget(currentDate, changeDateTime, CustomExtraThemeData.getExtraThemeData().wodsColor, CustomExtraThemeData.getExtraThemeData().wodsLabelColor),
                wod != null
                    ? WodWidget(wod!, () {
                        Scrollable.ensureVisible(
                            rankingWidgetKey.currentContext!,
                            duration: const Duration(milliseconds: 700),
                            curve: Curves.easeInOut);
                      })
                    : Container(
                        height: screenSize.height * .7,
                        width: double.infinity,
                        color: Colors.transparent,
                        child: Center(
                            child: Text(
                                'No se encuentran resultados para esta fecha',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 20.sp,
                                    fontFamily: 'Oswald'))))
              ],
            ),
          ),
        ),
      ),
      floatingActionButton:
          CustomFloatingButton(scrollControllerMain: _scrollController),
    );
  }

  changeDateTime(DateTime selectedDate) {
    currentDate = selectedDate;
    loadWod();
  }

  getSelectedDate() {
    return DateFormat('yyyy-MM-dd').format(currentDate);
  }

  loadExercises() async {
    var boxID = Provider.of<AuthProvider>(context, listen: false).boxID!;
    try {
      await Provider.of<SportDataProvider>(context, listen: false)
          .getExercises(boxID);
    } catch (error) {
      rethrow;
    }
  }

  loadWod() async {
    var boxID = Provider.of<AuthProvider>(context, listen: false).boxID;
    var wodsProvider = Provider.of<WodsProvider>(context, listen: false);
    wodsProvider
        .getWod(getSelectedDate(), boxID!, widget.isCompetitor != null ? 1 : 0)
        .then((response) {
      if (response.status == 200 && response.success!) {
        if (response.data.length > 0) {
          var wodData = Wod.fromJson(response.data[0]);
          wodsProvider.blocksWithRatingTypes = wodData.blocks
              .where((element) => element.ratingTypes.isNotEmpty)
              .toList();
          setState(() {
            wod = wodData;
          });
        } else {
          setState(() {
            wod = null;
          });
        }
      }
    });
  }
}
