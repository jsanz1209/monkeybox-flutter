import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/fees/api/fee_user.dart';
import 'package:monkeybox/pages/side-menu/dashboard/dashboard_page.dart';
import 'package:monkeybox/providers/fees/fees_bonuses_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:provider/provider.dart';
import 'package:stripe_sdk/stripe_sdk_ui.dart';

class PaymentGatewayStripePage extends StatelessWidget {
  static const routeName = 'payment-gateway-stripe';
  final formKey = GlobalKey<FormState>();
  final stripeCard = StripeCard();

  PaymentGatewayStripePage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var arguments =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    var name = arguments['name'] as String;
    var fee = arguments['fee'] as FeeUser;
    return Scaffold(
      appBar: CustomHeader(
        title: 'PAGO',
        withBackButton: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: 32.sp, vertical: 48.sp),
          child: Column(
            children: [
              Text(
                'CUOTA/BONO PENDIENTE',
                style: Theme.of(context).textTheme.bodyText2!.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 24.sp,
                    fontFamily: 'Oswald',
                    color: Colors.black),
              ),
              SizedBox(
                height: 24.sp,
              ),
              Text.rich(TextSpan(
                  text: 'Completa tu pago ',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2!
                      .copyWith(color: Colors.black, height: 1.2),
                  children: [
                    TextSpan(
                        text: '$name | ${fee.quantity}€,',
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            height: 1.2)),
                    TextSpan(
                        text:
                            ' para poder seguir disfrutando de todas las funcionalidades de la aplicación.',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: Colors.black, height: 1.2))
                  ])),
              SizedBox(
                height: 24.sp,
              ),
              Container(
                padding:
                    EdgeInsets.symmetric(vertical: 32.sp, horizontal: 0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.cyan[50],
                  border: Border.all(
                    color: Colors.grey.shade300,
                    width: 2.sp,
                  ),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 2,
                        color: Colors.grey.shade300,
                        spreadRadius: 1)
                  ],
                ),
                child: Column(
                  children: [
                    Text(
                      'Número de tarjeta de pago',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.sp,
                          color: Colors.black),
                    ),
                    SizedBox(height: 8.sp),
                    CardForm(
                      card: stripeCard,
                      formKey: formKey,
                      displayAnimatedCard: true,
                      cardHolderTextName: 'Titular de la tarjeta',
                      textExpDate: 'Válida hasta',
                      textExpiry: 'MM/AA',
                      cardNumberErrorText:
                          '\u24d8 El número de tarjeta no es válido',
                      cardExpiryErrorText:
                          '\u24d8 La fecha de caducidad no es válida',
                      cardCvcErrorText: '\u24d8 El CVC/CVV no es válido',
                      cardNumberDecoration: getTextfieldInputDecoration(
                          'Número de tarjeta', context),
                      cardExpiryDecoration:
                          getTextfieldInputDecoration('Válida hasta', context),
                      cardCvcDecoration:
                          getTextfieldInputDecoration('CVC/CVV', context),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 30.sp),
                            padding: EdgeInsets.only(
                                top: 32.sp,
                                bottom: 24.sp,
                                left: 16.sp,
                                right: 16.sp),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2),
                                border: Border.all(
                                  color: Colors.black45,
                                  width: 2.sp,
                                )),
                            child: Column(
                              children: [
                                Text.rich(
                                  TextSpan(
                                      text: '•  Válida hasta: ',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14.sp,
                                              height: 1.3),
                                      children: [
                                        TextSpan(
                                            text:
                                                'Fecha de caducidad de tu tarjeta en formato mes/año (MM/AA).',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2!
                                                .copyWith(
                                                    color: Colors.black,
                                                    fontSize: 14.sp,
                                                    height: 1.3))
                                      ]),
                                ),
                                SizedBox(
                                  height: 10.sp,
                                ),
                                Text.rich(
                                  TextSpan(
                                      text: '•  CVC/CVV : ',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14.sp,
                                              height: 1.3),
                                      children: [
                                        TextSpan(
                                            text:
                                                'Código de seguridad numérico en el dorso de tu tarjeta (3/4 dígitos).',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2!
                                                .copyWith(
                                                    color: Colors.black,
                                                    fontSize: 14.sp,
                                                    height: 1.3))
                                      ]),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 22.sp,
                            left: 24.sp,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.cyan[50],
                              ),
                              child: Text(
                                'Ayuda',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 18.sp),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8.sp,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: CustomElevatedButton(
                          text: 'PAGAR $name | ${fee.quantity}€',
                          backgroundColor: HexColor('#ff7f50'),
                          textColor: Colors.white,
                          fontSize: 20.sp,
                          pressEvent: () {
                            // _setPayment(context, fee.id);
                            if (formKey.currentState!.validate()) {
                              formKey.currentState!.save();
                              _setPayment(context, fee.id);
                            }
                          }),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  InputDecoration getTextfieldInputDecoration(
      String placeholder, BuildContext context) {
    return InputDecoration(
      errorStyle: TextStyle(
          fontFamily: 'Oswald',
          fontSize: 14.sp,
          overflow: TextOverflow.visible,
          color: Theme.of(context).errorColor),
      errorMaxLines: 2,
      isDense: true,
      labelText: placeholder,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.black, width: 2.sp),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.black, width: 2.sp),
      ),
      errorBorder: UnderlineInputBorder(
        borderSide:
            BorderSide(color: Theme.of(context).errorColor, width: 2.sp),
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 4.sp),
      labelStyle:
          Theme.of(context).textTheme.bodyText2!.copyWith(height: 1.1875.sp),
    );
  }

  void _setPayment(BuildContext context, int paymentID) async {
    try {
      var menuProvider = Provider.of<MenuProvider>(context, listen: false);
      var feesProvider =
          Provider.of<FeesBonusesProvider>(context, listen: false);
      await feesProvider.setPaymentAPICalls(
          paymentID, stripeCard, menuProvider.user!, context);
      SnackBarHelper.showSnackBar(
          'El pago se ha realizado con éxito',
          'Ya puedes volver a disfrutar de todas las funcionalidades de MonkeyBox.',
          TypeSnackBarEnum.success);
      menuProvider.refreshProfile();
      Navigator.of(menuProvider.navigatorKey.currentContext!)
          .pushReplacementNamed(DashboardPage.routeName);
    } catch (error) {
      SnackBarHelper.showSnackBar('Error al realizar el pago', error.toString(),
          TypeSnackBarEnum.error);
    }
  }
}
