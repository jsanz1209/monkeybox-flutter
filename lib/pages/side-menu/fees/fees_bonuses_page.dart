import 'package:flutter/material.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/fees/api/bonus_user.dart';
import 'package:monkeybox/models/fees/api/fee_box.dart';
import 'package:monkeybox/providers/fees/fees_bonuses_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/fees/fees_header.dart';
import 'package:monkeybox/widgets/side_menu/fees/list_fees.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';
import 'package:provider/provider.dart';

class FeesBonusesPage extends StatefulWidget {
  static const routeName = 'fees';
  const FeesBonusesPage({Key? key}) : super(key: key);

  @override
  State<FeesBonusesPage> createState() => _FeesBonusesPageState();
}

class _FeesBonusesPageState extends State<FeesBonusesPage> {
  late ScrollController _scrollController;
  late Future _futureAPIBackend;
  String? _stateFeeSelected = Settings.CREATED_PAYMENT;
  bool _isFirstLoad = true;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    var menuProvider = Provider.of<MenuProvider>(context, listen: false);
    _futureAPIBackend = Provider.of<FeesBonusesProvider>(context, listen: false)
        .getAPICallsBackend(menuProvider.user!.id, menuProvider.infoBOX!.id,
            menuProvider.user!.userBoxes[0].isCompetitor);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomMenuHeader(title: 'CUOTAS Y BONOS'),
        drawer: NavigationDrawer(),
        backgroundColor: Colors.white,
        body: FutureBuilder(
            future: _futureAPIBackend,
            builder: (ctx, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                _showDialogNoneFeeBonusSelected();
                return CustomScrollView(
                  controller: _scrollController,
                  shrinkWrap: true,
                  slivers: [
                    FeesHeader(
                      onChangeFeeBonus: (isBonus, feeBonusObject) {
                        _changeFeeBonus(isBonus, feeBonusObject);
                      },
                      onStateFeeSelected: (value) {
                        setState(() {
                          _stateFeeSelected = value;
                        });
                      },
                    ),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                          (_, index) => ListFees(
                                filter: _stateFeeSelected,
                              ),
                          childCount: 1),
                    )
                  ],
                );
              } else {
                return const SizedBox();
              }
            }));
  }

  void _changeFeeBonus(bool isBonus, dynamic feeBonusObject) async {
    var feesBonusProvider =
        Provider.of<FeesBonusesProvider>(context, listen: false);
    var menuProvider = Provider.of<MenuProvider>(context, listen: false);

    var futureAPICall;

    if (isBonus) {
      var bonusObject = feeBonusObject as BonusUser;
      futureAPICall = feesBonusProvider.buyBonus(
          bonusObject.id, bonusObject.expirationMonths);
    } else {
      var menuProvider = Provider.of<MenuProvider>(context, listen: false);
      var feeObject = feeBonusObject as FeesBox;
      futureAPICall = feesBonusProvider.updateFeeType(
          feeObject.id,
          menuProvider.user!.id,
          menuProvider.infoBOX!.id,
          menuProvider.user!.userBoxes[0].status);
    }
    try {
      await futureAPICall;
      SnackBarHelper.showSnackBar(
          'Tipo de cuota/bono',
          'El tipo de cuota/bono se ha cambiado correctamente',
          TypeSnackBarEnum.success);
      setState(() {
        _futureAPIBackend =
            Provider.of<FeesBonusesProvider>(context, listen: false)
                .getAPICallsBackend(
                    menuProvider.user!.id,
                    menuProvider.infoBOX!.id,
                    menuProvider.user!.userBoxes[0].isCompetitor);
      });
    } catch (error) {
      rethrow;
    }
  }

  void _showDialogNoneFeeBonusSelected() {
    var feesBonusProvider =
        Provider.of<FeesBonusesProvider>(context, listen: false);
    var menuProvider = Provider.of<MenuProvider>(context, listen: false);
    var infoBox = menuProvider.infoBOX;
    var infoFeeUserBox = feesBonusProvider.infoFeeUserBox!;
    if (infoFeeUserBox.feeID == null &&
        infoFeeUserBox.bonus == null &&
        _isFirstLoad) {
      _isFirstLoad = false;
      Future.delayed(const Duration(milliseconds: 100), () {
        showDialog(
            useSafeArea: false,
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text(
                  'Ninguna cuota/bono seleccionado',
                  style: Theme.of(context)
                      .textTheme
                      .headline2!
                      .copyWith(color: Colors.black),
                ),
                content: Text(
                    'Debe seleccionar una cuota/bono en el selector superior para poder realizar una reserva.\n\n La cuota/bono se puede pagar online usando la APP (${infoBox!.administrationFees}€ de gastos de gestión) o pagarla en metálico en el BOX. \n\n Se permite 1 reserva aunque la cuota no esté pagada.'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                      child: Text('Aceptar',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2!
                              .copyWith(color: Colors.blue))),
                ],
              );
            });
      });
    }
  }
}
