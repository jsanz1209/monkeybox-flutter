import 'package:flutter/material.dart';
import 'package:monkeybox/providers/fees/fees_bonuses_provider.dart';
import 'package:monkeybox/widgets/side_menu/fees/fees_nested_routes.dart';
import 'package:provider/provider.dart';

class MainFeesPage extends StatelessWidget {
  static const routeName = 'fees';
  final String setupPageRoute;
  const MainFeesPage(
      {Key? key, required this.setupPageRoute})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('Main Fees build');
    return ChangeNotifierProvider(
        create: (_) => FeesBonusesProvider(),
        child: Scaffold(
            body: FeesNestedRoutes(
          setupPageRoute: setupPageRoute
        )));
  }
}
