import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/enums/reservation_type_enum.dart';
import 'package:monkeybox/providers/reservations/reservations_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/reservations/content_reservation.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';
import 'package:provider/provider.dart';

class ReservationsPage extends StatefulWidget {
  static const routeName = 'reservations';
  const ReservationsPage({Key? key}) : super(key: key);

  @override
  _ReservationsPageState createState() => _ReservationsPageState();
}

class _ReservationsPageState extends State<ReservationsPage> {
  int _selectedIndex = 0;
  late PageController _pageController;
  List<Widget> _widgetOptions = [
    ContentReservation(
        titleHeader: 'WOD',
        typeReservation: ReservationTypeEnum.wod,
        color: CustomExtraThemeData.getExtraThemeData().headerReservationPrimaryColorWod!,
        colorSecondary: CustomExtraThemeData.getExtraThemeData().headerReservationSecondaryColorWod!),
    ContentReservation(
        titleHeader: 'OPEN BOX',
        typeReservation: ReservationTypeEnum.openBOX,
        color: CustomExtraThemeData.getExtraThemeData().headerReservationPrimaryColorOpenBox!,
        colorSecondary: CustomExtraThemeData.getExtraThemeData().headerReservationSecondaryColorOpenBox!),
  ];

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 0, viewportFraction: 1);
    if (_isCompetitorUser) {
      _widgetOptions.add(ContentReservation(
          titleHeader: 'COMPETIDOR',
          typeReservation: ReservationTypeEnum.competitors,
          color: CustomExtraThemeData.getExtraThemeData().headerReservationPrimaryColorCompetitor!,
          colorSecondary: CustomExtraThemeData.getExtraThemeData().headerReservationSecondaryColorCompetitor!));
    }
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ReservationsProvider(),
      child: Scaffold(
        extendBody: true,
        appBar: CustomMenuHeader(title: 'RESERVA DE LA CLASE'),
        drawer: NavigationDrawer(),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [Expanded(child: _buildPageView())]),
        bottomNavigationBar: CurvedNavigationBar(
          backgroundColor: Colors.transparent,
          color: _selectedIndex == 0
              ? CustomExtraThemeData.getExtraThemeData().navigationBarWodBackgroundColor!
              : (_selectedIndex == 1)
                ? CustomExtraThemeData.getExtraThemeData().navigationBarOpenBoxBackgroundColor!
                : CustomExtraThemeData.getExtraThemeData().navigationBarCompetitorBackgroundColor!,
          index: _selectedIndex,
          items: _bottomNavigationItems,
          onTap: (index) {
            //Handle button tap
            setState(() {
              _selectedIndex = index;
              _pageController.animateToPage(index,
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.linear);
            });
          },
        ),
      ),
    );
  }

  void _pageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget _buildPageView() {
    return PageView(
        controller: _pageController,
        onPageChanged: (index) {
          _pageChanged(index);
        },
        // allowImplicitScrolling: true,
        children: _widgetOptions);
  }

  List<Widget> get _bottomNavigationItems {
    List<Widget> _tabs = [
      _selectedIndex != 0
          ? Container(
              margin: EdgeInsets.only(top: 20.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Icon(
                    Icons.crop_square_rounded,
                    size: 28.w,
                    color: CustomExtraThemeData.getExtraThemeData().navigationBarWodIconColor,
                  ),
                  Text(
                    'WOD',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(color: CustomExtraThemeData.getExtraThemeData().navigationBarWodIconColor),
                  ),
                ],
              ),
            )
          : Icon(
              Icons.crop_square_rounded,
              size: 32.sp,
              color: CustomExtraThemeData.getExtraThemeData().navigationBarWodIconColor,
            ),
      _selectedIndex != 1
          ? Container(
              margin: EdgeInsets.only(top: 20.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Icon(
                    Icons.fullscreen_rounded,
                    size: 28.w,
                    color: CustomExtraThemeData.getExtraThemeData().navigationBarOpenBoxIconColor,
                  ),
                  Text(
                    'Open BOX',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 14.sp, color: CustomExtraThemeData.getExtraThemeData().navigationBarOpenBoxIconColor),
                  ),
                ],
              ),
            )
          : Icon(
              Icons.fullscreen_rounded,
              size: 32.sp,
              color: CustomExtraThemeData.getExtraThemeData().navigationBarOpenBoxIconColor,
            ),
    ];

    if (_isCompetitorUser) {
      _tabs.add(_selectedIndex != 2
          ? Container(
              margin: EdgeInsets.only(top: 20.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SvgPicture.asset(
                    'assets/images/icons/trophy-outline.svg',
                    height: 28.w,
                    width: 28.w,
                    color: CustomExtraThemeData.getExtraThemeData().navigationBarCompetitorIconColor,
                  ),
                  Text(
                    'Competidores',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 14.sp, color: CustomExtraThemeData.getExtraThemeData().navigationBarCompetitorLabelColor),
                  ),
                ],
              ),
            )
          : SvgPicture.asset(
              'assets/images/icons/trophy-outline.svg',
              height: 32.sp,
              width: 32.sp,
              color: CustomExtraThemeData.getExtraThemeData().navigationBarCompetitorIconColor,
            ));
    }


    return _tabs;
  }

  bool get _isCompetitorUser {
    var user = Provider.of<MenuProvider>(context, listen: false).user;
    return user!.userBoxes.indexWhere((element) => element.isCompetitor) != -1;
  }
}
