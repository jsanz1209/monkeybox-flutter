import 'package:flutter/material.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/side_menu/side_menu_nested_routes.dart';
import 'package:provider/provider.dart';

class SideMenuPage extends StatelessWidget {
  static const routeName = 'side-menu';
  final String setupPageRoute;
  const SideMenuPage({Key? key, required this.setupPageRoute})
      : super(key: key);

  Widget build(BuildContext context) {
    debugPrint('Side menu build');
    return (MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => MenuProvider()),
          ChangeNotifierProvider(create: (_) => SportDataProvider()),
        ],
        child: Scaffold(
            body: SideMenuNestedRoutes(setupPageRoute: setupPageRoute))));
  }
}
