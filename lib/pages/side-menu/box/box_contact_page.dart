import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/side_menu/contacts/header_box_contact.dart';
import 'package:monkeybox/widgets/side_menu/contacts/map.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';

class BoxContactPage extends StatelessWidget {
  static const routeName = 'box-contact';
  const BoxContactPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomMenuHeader(title: 'CONTACTO BOX'),
        drawer: NavigationDrawer(),
        body: Column(
          children: const [HeaderBoxContact(), Expanded(child: Map())],
        ));
  }
}
