import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';
import 'package:provider/provider.dart';

class BoxConditionsPage extends StatefulWidget {
  static const routeName = 'box-conditions';

  const BoxConditionsPage({Key? key}) : super(key: key);

  @override
  _BoxConditionsPageState createState() => _BoxConditionsPageState();
}

class _BoxConditionsPageState extends State<BoxConditionsPage> {
  @override
  Widget build(BuildContext context) {
    var infoBOX = Provider.of<MenuProvider>(context, listen: false).infoBOX;
    return Scaffold(
      appBar: CustomMenuHeader(title: 'CONDICIONES DEL BOX'),
      drawer: NavigationDrawer(),
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Html(
          data: infoBOX!.normative,
          style: {
            'body': Style(
              color: Colors.black,
              fontFamily: 'Oswald',
              // lineHeight: LineHeight.rem(1.24),
            ),
            'span': Style(
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
            ),
            'p': Style(
                margin: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                lineHeight: LineHeight.normal),
            'ol': Style(
                color: Colors.black, margin: EdgeInsets.only(bottom: 16.sp)),
            'li': Style(
                color: Colors.black,
                margin: EdgeInsets.all(8.sp),
                lineHeight: LineHeight.normal),
          },
        ),
      )),
    );
  }
}
