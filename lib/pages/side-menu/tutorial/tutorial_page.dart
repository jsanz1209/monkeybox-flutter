import 'package:flutter/material.dart';
import 'package:monkeybox/pages/onboarding/onboarding.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';

class TutorialPage extends StatelessWidget {
  static const routeName = 'tutorial';
  const TutorialPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomMenuHeader(title: 'TUTORIAL'),
        drawer: NavigationDrawer(),
        body: OnBoarding());
  }
}
