import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';

class PaymentManagementPage extends StatelessWidget {
  static const routeName = 'payment-management';
  const PaymentManagementPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomMenuHeader(title: 'GESTIÓN DE PAGOS'),
        drawer: NavigationDrawer(),
        body: Center(child: Text('Payment management works')));
  }
}
