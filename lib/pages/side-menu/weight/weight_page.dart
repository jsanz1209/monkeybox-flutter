import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/models/sport-data/api/body_weights_data.dart';
import 'package:monkeybox/providers/weight/weight_provider.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';
import 'package:monkeybox/widgets/side_menu/weight/add_weight_widget.dart';
import 'package:monkeybox/widgets/side_menu/weight/historical_weight_widget.dart';
import 'package:provider/provider.dart';

class WeightPage extends StatefulWidget {
  static const routeName = 'weight';
  const WeightPage({Key? key}) : super(key: key);

  @override
  State<WeightPage> createState() => _WeightPageState();
}

class _WeightPageState extends State<WeightPage> {
  int _selectedIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 0, viewportFraction: 1);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => WeightProvider(),
      builder: (context, _) => Scaffold(
        extendBody: true,
        backgroundColor: Colors.white,
        appBar: CustomMenuHeader(
            title: _selectedIndex == 0 ? 'HISTÓRICO PESO' : 'AÑADIR PESO'),
        drawer: NavigationDrawer(),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [Expanded(child: _buildPageView(context))]),
        bottomNavigationBar: CurvedNavigationBar(
          backgroundColor: Colors.transparent,
          color: Theme.of(context).primaryColor,
          index: _selectedIndex,
          items: _bottomNavigationItems,
          onTap: (index) {
            //Handle button tap
            _selectIndex(index, context);
          },
        ),
      ),
    );
  }

  Widget _buildPageView(BuildContext context) {
    return PageView(
        controller: _pageController,
        onPageChanged: (index) {
          _selectIndex(index, context);
        },
        // allowImplicitScrolling: true,
        children: [
          HistoricalWeightWidget(
            onUpdateWeight: (BodyWeightsData data) {
              Provider.of<WeightProvider>(context, listen: false).dataUpdate =
                  data;
              _selectIndex(1, context);
            },
          ),
          AddWeightWidget(
            onBack: () {
              FocusManager.instance.primaryFocus?.unfocus();
              _selectIndex(0, context);
            },
          )
        ]);
  }

  void _selectIndex(int index, BuildContext context) {
    setState(() {
      if (index == 0) {
        Provider.of<WeightProvider>(context, listen: false).dataUpdate = null;
      }
      _selectedIndex = index;
      _pageController.animateToPage(index,
          duration: const Duration(milliseconds: 10), curve: Curves.linear);
    });
  }

  List<Widget> get _bottomNavigationItems {
    return <Widget>[
      _selectedIndex == 1
          ? Container(
              margin: EdgeInsets.only(top: 20.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SvgPicture.asset(
                    'assets/images/icons/list.svg',
                    color: Colors.white,
                    height: 28.w,
                    width: 28.w,
                  ),
                  Text(
                    'Histórico Peso',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 14.sp, color: Colors.white),
                  ),
                ],
              ),
            )
          : SvgPicture.asset(
              'assets/images/icons/list.svg',
              color: Colors.white,
              height: 32.sp,
              width: 32.sp,
            ),
      _selectedIndex == 0
          ? Container(
              margin: EdgeInsets.only(top: 20.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Icon(
                    Icons.add_rounded,
                    size: 28.w,
                    color: Colors.white,
                  ),
                  Text(
                    'Añadir Peso',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 14.sp, color: Colors.white),
                  ),
                ],
              ),
            )
          : Icon(
              Icons.add_rounded,
              size: 32.sp,
              color: Colors.white,
            ),
    ];
  }
}
