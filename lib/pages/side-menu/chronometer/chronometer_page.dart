import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:monkeybox/providers/chrono/chrono_bloc.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/side_menu/chrono/chrono_settings.dart';
import 'package:monkeybox/widgets/side_menu/chrono/chrono_type_selection.dart';
import 'package:monkeybox/widgets/side_menu/chrono/chrono_widget.dart';
import 'package:provider/provider.dart';

class ChronometerPage extends StatefulWidget {
  static const routeName = 'chronometer';

  final Function? onBack;
  const ChronometerPage({Key? key, this.onBack}) : super(key: key);

  @override
  _ChronometerPageState createState() => _ChronometerPageState();
}

class _ChronometerPageState extends State<ChronometerPage> {
  Widget? _currentWidget;
  bool _fromWODS = false;
  late StreamSubscription<void> _streamSubscription;
  late MenuProvider menuProvider;
  final _chronoBloc = ChronoBloc();

  @override
  void initState() {
    super.initState();
    _chronoBloc.isInChronoSection = true;
    menuProvider = Provider.of<MenuProvider>(context, listen: false);
    _fromWODS = _chronoBloc.fromWODs;
    _currentWidget = widgetTypeSelection;
    _chronoBloc.initAllControllers();
    _streamSubscription = _chronoBloc.chronoGenBackEventStream.listen((_) {
      if (_currentWidget.runtimeType == ChronoTypeSelectionWidget) {
        Navigator.of(context).pop();
      } else if (_currentWidget.runtimeType == ChronoSettingsWidget) {
        onCancel();
      } else if (_currentWidget.runtimeType == ChronoWidget) {
        _chronoBloc.chronoWidgetBackEventSink.add(VoidCallback);
      }
    });
  }

  @override
  void dispose() {
    _chronoBloc.fromWODs = false;
    _chronoBloc.isInChronoSection = false;
    _streamSubscription.cancel();
    _chronoBloc.disposeChronoGenBackEvent();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (_currentWidget.runtimeType == ChronoTypeSelectionWidget) {
          return Future<bool>.value(false);
        } else if (_currentWidget.runtimeType == ChronoSettingsWidget) {
          onCancel();
        }
        return Future<bool>.value(true);
      },
      child: Scaffold(
          body: _currentWidget == null
              ? Center(child: CircularProgressIndicator())
              : PageTransitionSwitcher(
                  transitionBuilder:
                      (child, primaryAnimation, secondaryAnimation) =>
                          FadeThroughTransition(
                            animation: primaryAnimation,
                            secondaryAnimation: secondaryAnimation,
                            child: child,
                          ),
                  child: _currentWidget!)),
    );
  }

  void onSelect(int type) {
    setState(() {
      _currentWidget = ChronoSettingsWidget(type, onCancel, onStart);
    });
  }

  void onStart(String typeName, int type, int timeWork, int rounds,
      int timeRepeat, int timeRest) {
    setState(() {
      _currentWidget = ChronoWidget(
          type: type,
          timeWork: timeWork,
          rounds: rounds,
          timeRepeat: timeRepeat,
          timeRest: timeRest,
          name: typeName,
          onCancel: onCancel);
    });
  }

  void onCancel() {
    setState(() {
      _currentWidget = widgetTypeSelection;
    });
  }

  get widgetTypeSelection {
    return ChronoTypeSelectionWidget(onSelect, () {
      if (_fromWODS) {
        Navigator.of(context).pop();
      }
    }, _fromWODS);
  }
}
