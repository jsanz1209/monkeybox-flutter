
import 'package:flutter/material.dart';
import 'package:monkeybox/models/side-menu/card_dashboard.dart'
    as dashboard_item;
import 'package:monkeybox/pages/side-menu/reservations/reservations_page.dart';
import 'package:monkeybox/pages/side-menu/wods/main_wods_page.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';
import 'package:monkeybox/widgets/side_menu/dashboard/card_dashboard.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';
import 'package:provider/provider.dart';

class DashboardPage extends StatelessWidget {
  static const routeName = 'dashboard';
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var infoBox = Provider.of<MenuProvider>(context, listen: true).infoBOX;
    return Scaffold(
      appBar: CustomMenuHeader(title: 'HOME'),
      drawer: NavigationDrawer(),
      body: Container(
        color: Theme.of(context).primaryColor,
        child: Column(children: [
          CardDashboard(
            item: dashboard_item.CardDashboard(
                backgroundName: infoBox != null ? infoBox.wodsBackgroundUrl! : 'dash1',
                foregroundName: 'wods',
                title: 'WODs',
                routeName: MainWodsPage.routeName),
          ),
          CardDashboard(
            item: dashboard_item.CardDashboard(
                backgroundName: infoBox != null ? infoBox.reservationsBackgroundUrl! : 'dash2',
                foregroundName: 'bookmark',
                title: 'RESERVAS',
                routeName: ReservationsPage.routeName),
          )
        ]),
      ),
    );
  }
}
