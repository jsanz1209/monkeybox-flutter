import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/themes/theme_model.dart';
import 'package:monkeybox/pages/auth/change_password_page.dart';
import 'package:monkeybox/pages/auth/login_page.dart';
import 'package:monkeybox/pages/onboarding/onboarding.dart';
import 'package:monkeybox/pages/side-menu/dashboard/dashboard_page.dart';
import 'package:monkeybox/pages/side-menu/side_menu_page.dart';
import 'package:monkeybox/providers/app/app_provider.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/widgets/auth/auth_page.dart';
import 'package:provider/provider.dart';
import 'package:uni_links/uni_links.dart';

final GlobalKey<NavigatorState> appNavigatorKey = GlobalKey<NavigatorState>();

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  Future<bool>? _futureLogin;
  Future<bool>? _futureOnBoarding;

  /// Handle incoming links - the ones that the app will recieve from the OS
  /// while already started.
  StreamSubscription? _sub;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 500), () {
      Provider.of<ThemeModel>(appNavigatorKey.currentContext!, listen: false)
          .toggleTheme(appNavigatorKey.currentContext!);
    });
    Future.delayed(const Duration(milliseconds: 3000), () {
      _initUniLinks();
      _handleIncomingLinks();
    });
    _futureLogin =
        Provider.of<AuthProvider>(context, listen: false).tryAutoLogin();
    _futureOnBoarding =
        Provider.of<AppProvider>(context, listen: false).isOnBoardingFinish();
  }

  @override
  void dispose() {
    _sub?.cancel();
    super.dispose();
  }

  Future<void> _handleIncomingLinks() async {
    // ... check initialUri
    // Attach a listener to the stream
    _sub = uriLinkStream.listen((Uri? uri) {
      _proccessUri(uri);
      // Use the uri and warn the user, if it is not correct
    }, onError: (err) {
      // Handle exception by warning the user their action did not succeed
    });

    // NOTE: Don't forget to call _sub.cancel() in dispose()
  }

  Future<void> _initUniLinks() async {
    try {
      Uri? initialLink = await getInitialUri();
      _proccessUri(initialLink);
    } on PlatformException {
      debugPrint('platfrom exception unilink');
    }
  }

  _proccessUri(Uri? uri) {
    if (uri != null && uri.path.contains(ChangePasswordPage.routeName)) {
      Map<String, List<String>> queryParameters = uri.queryParametersAll;
      var authNavigatorKey =
          Provider.of<AuthProvider>(context, listen: false).navigatorKey;
      Navigator.of(authNavigatorKey.currentContext!)
          .pushNamed(ChangePasswordPage.routeName, arguments: queryParameters);
    }
  }

  Scaffold get _loadingWidget {
    return Scaffold(
        body: Container(
            color: HexColor('#233645'),
            child: Center(
              child: Text(
                'Loading...',
                style: TextStyle(color: Colors.white),
              ),
            )));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _futureOnBoarding,
      builder: (ctx, snapshot) {
        return ScreenUtilInit(
            designSize: Size(375, 812),
            minTextAdapt: true,
            splitScreenMode: true,
            builder: () {
              return MaterialApp(
                builder: (BuildContext context, Widget? child) {
                  return MediaQuery(
                    data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
                    child: child!,
                  );
                },
                title: Settings.APP_NAME,
                navigatorKey: appNavigatorKey,
                debugShowCheckedModeBanner: false,
                theme: Provider.of<ThemeModel>(context).currentTheme,
                localizationsDelegates: [
                  AppLocalizations.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate
                ],
                supportedLocales: [
                  Locale('es', ''),
                  Locale('en', ''),
                ],
                home: (snapshot.connectionState == ConnectionState.waiting)
                    ? _loadingWidget
                    : !snapshot.data!
                        ? OnBoarding()
                        : FutureBuilder<bool>(
                            future: _futureLogin,
                            builder: (ctx, snapshot) {
                              return (snapshot.connectionState ==
                                      ConnectionState.waiting)
                                  ? _loadingWidget
                                  : snapshot.data!
                                      ? const SideMenuPage(
                                          setupPageRoute:
                                              DashboardPage.routeName)
                                      : const AuthPage(
                                          setupPageRoute: LoginPage.routeName);
                            }),
                onGenerateRoute: (settings) {
                  late Widget page;
                  if (settings.name!.startsWith(AuthPage.routeName)) {
                    var setupPageRoute = settings.name!.contains('/')
                        ? settings.name!.split('/')[1]
                        : '';
                    page = AuthPage(setupPageRoute: setupPageRoute);
                  } else if (settings.name!
                      .startsWith(SideMenuPage.routeName)) {
                    var setupPageRoute = settings.name!.contains('/')
                        ? settings.name!.split('/')[1]
                        : '';
                    page = SideMenuPage(setupPageRoute: setupPageRoute);
                  } else {
                    throw Exception('Unknown route: ${settings.name}');
                  }
                  return MaterialPageRoute<dynamic>(
                    builder: (context) {
                      return page;
                    },
                    settings: settings,
                  );
                },
              );
            });
      },
    );
  }
}
