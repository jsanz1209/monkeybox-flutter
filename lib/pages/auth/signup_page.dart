import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/auth/api/signup_request.dart';
import 'package:monkeybox/models/auth/signup.dart';
import 'package:monkeybox/models/user/api/privacy_data_request.dart';
import 'package:monkeybox/models/user/api/profile_data_request.dart';
import 'package:monkeybox/models/user/api/social_network_data_request.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/pages/side-menu/side_menu_page.dart';
import 'package:monkeybox/providers/auth/signup_provider.dart';
import 'package:monkeybox/widgets/auth/signup/form_signup.dart';
import 'package:monkeybox/widgets/auth/signup/header_signup.dart';
import 'package:monkeybox/widgets/common/custom_floating_button.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:monkeybox/widgets/common/user_image_sliver_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SignUpPage extends StatefulWidget {
  static const routeName = 'signup';
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  File? _imageFile;
  late ScrollController _scrollController;
  bool _isBeenFormSubmitted = false;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    isSelectedImage = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = CustomExtraThemeData.getChinchillaTheme();
    return Scaffold(
      backgroundColor: theme.colorScheme.primary,
      bottomNavigationBar: Container(height: 10.h, color: theme.colorScheme.primary),
      body: Container(
        color: Theme.of(context).colorScheme.secondary,      
        child: SafeArea(
          child: Container(
            color: theme.colorScheme.primary,
            child: CustomScrollView(
                controller: _scrollController,
                slivers: <Widget>[
                  HeaderSignup(onChange: (value) {
                    setState(() {
                      _imageFile = value;
                    });
                  }),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (_, index) => Column(
                              children: [
                                _showMessageImageRequired(),
                                Container(
                                    width: double.infinity,
                                    padding: const EdgeInsets.only(
                                        top: 10,
                                        bottom: 0,
                                        left: 30,
                                        right: 30),
                                    child: FormSignup(
                                        submitEvent: (data) =>
                                            _submitEvent(context, data))),
                              ],
                            ),
                        childCount: 1),
                  ),
                ]),
          ),
        ),
      ),
      floatingActionButton: CustomFloatingButton(
        scrollControllerMain: _scrollController,
        isWhiteTheme: true,
      ),
    );
  }

  void _submitEvent(BuildContext context, Signup? data) async {
    setState(() {
      _isBeenFormSubmitted = true;
    });
    if (_imageFile != null) {
      if (data != null) {
        try {
          await Provider.of<SignUpProvider>(context, listen: false).setData(
              SignUpRequest(
                  file: _imageFile,
                  username: data.email,
                  email: data.email,
                  name: data.name,
                  firstSurname: data.firstSurname,
                  secondSurname: data.secondSurname,
                  birthdate: data.birthdate,
                  intensityCategoryID: data.category,
                  boxID: data.box,
                  gender: data.gender,
                  password: data.password,
                  height: data.height.replaceFirst(',', '.'),
                  weight: double.tryParse(data.weight!.replaceFirst(',', '.')),
                  privacy: PrivacyData(
                      socialNetworks: SocialNetworkData(
                          instagram: data.instagram,
                          facebook: data.facebook,
                          twitter: data.twitter,
                          whatsapp: data.whatsapp),
                      profile: ProfileData(
                        age: data.showAge!,
                        birthdate: data.showBirthdate!,
                        favoriteCategory: data.showCategory!,
                        firstSurname: data.showFirstSurname!,
                        secondSurname: data.showSecondSurname!,
                        gender: data.showGender!,
                        weight: data.showWeight!,
                        height: data.showHeight!,
                        instagram: data.showInstagram!,
                        twitter: data.showTwitter!,
                        whatsapp: data.showWhatsapp!,
                        facebook: data.showFacebook!,
                      ))));
          SnackBarHelper.showSnackBar(
              'Registro realizado con éxito',
              'El administrador validará tu cuenta. Una vez validada, recibirás la confirmación al email que solicitaste y podrás a empezar a usar el resto de funcionalidades de la APP.',
              TypeSnackBarEnum.success);
          Navigator.of(appNavigatorKey.currentContext!)
              .pushReplacementNamed(SideMenuPage.routeName);
        } catch (err) {
          debugPrint(err.toString());
        }
      }
    } else {
      Future.delayed(const Duration(milliseconds: 100), () {
        _scrollController.animateTo(0,
            duration: const Duration(milliseconds: 700),
            curve: Curves.easeInOut);
      });
    }
  }

  Widget _showMessageImageRequired() {
    return _isBeenFormSubmitted && _imageFile == null
        ? Padding(
            padding: EdgeInsets.only(top: 16.sp, bottom: 8.sp),
            child: Text(
                FormValidators.validatorRequired(
                    null, AppLocalizations.of(context).youMustSelectAProfileImage)!,
                style: TextStyle(
                    fontFamily: 'Oswald',
                    fontSize: 14.sp,
                    overflow: TextOverflow.visible,
                    color: Theme.of(context).errorColor)),
          )
        : const SizedBox();
  }
}
