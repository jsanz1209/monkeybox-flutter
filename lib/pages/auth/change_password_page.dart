import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/pages/auth/login_page.dart';
import 'package:monkeybox/providers/auth/reset_password_provider.dart';
import 'package:monkeybox/widgets/auth/reset-password/form_change_password.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class ChangePasswordPage extends StatelessWidget {
  static const routeName = 'change-password';
  ChangePasswordPage({Key? key}) : super(key: key);

  late int _id;
  late String _code;

  @override
  Widget build(BuildContext context) {
    var arguments = (ModalRoute.of(context)!.settings.arguments
        as Map<String, List<String>>);
    _id = int.parse(arguments['id']![0]);
    _code = arguments['code']![0];
    return Scaffold(
        appBar: CustomHeader(
          title: 'Cambia tu contraseña',
          onPress: () {
            Navigator.of(context).pop();
          },
        ),
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Nueva contraseña',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontSize: 24.sp,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 16,
                ),
                Text(
                    'Cambia tu contraseña más abajo, por una que te sea fácil recordar.',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: Colors.black,
                        )),
                SizedBox(
                  height: 40.sp,
                ),
                FormChangePassword(
                    submitEvent: (password) => _submit(context, password))
              ],
            ),
          ),
        ));
  }

  void _submit(BuildContext context, String password) async {
    try {
      await Provider.of<ResetPasswordProvider>(context, listen: false)
          .changePassword(_id, _code, password);
      SnackBarHelper.showSnackBar(
          'Contraseña modificada correctamente',
          'Ya puedes iniciar sesión con tu nueva contraseña',
          TypeSnackBarEnum.success);
      Navigator.of(context).popAndPushNamed(LoginPage.routeName);
    } catch (error) {
      rethrow;
    }
  }
}
