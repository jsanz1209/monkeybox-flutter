import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/providers/auth/reset_password_provider.dart';
import 'package:monkeybox/widgets/auth/reset-password/form_reset_password.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:provider/provider.dart';

class ResetPasswordPage extends StatefulWidget {
  static const routeName = 'reset-password';
  ResetPasswordPage({Key? key}) : super(key: key);

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomHeader(
          title: 'Olvidaste tu contraseña?',
          withBackButton: true,
          onPress: () {
            Navigator.of(context).pop();
          },
        ),
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Recupera tu contraseña',
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: Colors.black,
                    fontSize: 24.sp,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                  'Por favor, introduce tu email y te enviaremos un correo para reestablecer tu contraseña.',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: Colors.black,
                      )),
              SizedBox(
                height: 40.sp,
              ),
              FormResetPassword(
                submitEvent: _submit,
              )
            ],
          ),
        ));
  }

  void _submit(String email) async {
    try {
      await Provider.of<ResetPasswordProvider>(context, listen: false)
          .resetPassword(email);
      SnackBarHelper.showSnackBar(
          'Email enviado correctamente',
          'Te hemos enviado un correo electrónico a <strong>$email</strong> para que puedas reestablecer tu contraseña.',
          TypeSnackBarEnum.success);
      Navigator.of(context).pop();
    } catch (error) {
      rethrow;
    }
  }
}
