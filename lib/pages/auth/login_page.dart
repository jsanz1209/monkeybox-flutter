import 'dart:async';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/auth/api/login_request.dart';
import 'package:monkeybox/models/themes/theme_model.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/pages/side-menu/side_menu_page.dart';
import 'package:monkeybox/providers/app/app_provider.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/widgets/auth/login/form_login.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  static const routeName = 'login';
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  bool _isEndAnimation = true;

  void _submit(BuildContext context, String email, String password) async {
    try {
      await Provider.of<AuthProvider>(context, listen: false)
          .login(LoginRequest(login: email, password: password));
      Provider.of<ThemeModel>(context, listen: false).toggleTheme(context);
      Navigator.of(appNavigatorKey.currentContext!)
          .pushReplacementNamed(SideMenuPage.routeName);
    } catch (err) {
      debugPrint(err.toString());
    }
  }

  Widget defaultLoadingImage(BuildContext context) {
    return Image.asset(
      'assets/images/logo_login.png',
      fit: BoxFit.contain,
    );
  }

  @override
  void initState() {
    super.initState();
    Provider.of<AppProvider>(context, listen: false)
        .isOnBoardingFinish()
        .then((isOnBoardingFinished) {
      setState(() {
        _isEndAnimation = isOnBoardingFinished;
      });
    });
    Future.delayed(const Duration(milliseconds: 1800), () async {
      var isOnBoarding = await Provider.of<AppProvider>(context, listen: false)
          .isOnBoardingFinish();
      if (!isOnBoarding) {
        Provider.of<AppProvider>(appNavigatorKey.currentContext!, listen: false)
            .setFinishOnBoarding();
        setState(() {
          _isEndAnimation = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var infoBOX =
        Provider.of<AppProvider>(appNavigatorKey.currentContext!, listen: false)
            .infoBOX;
    return Scaffold(
      backgroundColor: CustomExtraThemeData.getExtraThemeData().primary,
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
              padding: EdgeInsets.only(top: 77.02.sp),
              child: AnimatedContainer(
                height: _isEndAnimation ? 250.h : 350.h,
                duration: Duration(milliseconds: 1500),
                curve: Curves.ease,
                child: infoBOX != null
                    ? CachedNetworkImage(
                        imageUrl:
                            '${Settings.apiURLBase}${infoBOX.logoLoginUrl!}',
                        errorWidget: (ctx, _, __) {
                          return defaultLoadingImage(context);
                        },
                        fit: BoxFit.contain,
                      )
                    : defaultLoadingImage(context),
              )),
          Container(
            width: 250.0.sp,
            padding: EdgeInsets.zero,
            height: 106.sp,
            margin: const EdgeInsets.only(top: 23.8, bottom: 0),
            child: DefaultTextStyle(
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
              softWrap: true,
              textAlign: TextAlign.center,
              child: AnimatedTextKit(
                  isRepeatingAnimation: false,
                  repeatForever: false,
                  animatedTexts: [
                    TyperAnimatedText(AppLocalizations.of(context).welcomeTitleLogin,
                        curve: Curves.easeInOut,
                        speed: const Duration(milliseconds: 100),
                        textStyle: Theme.of(context)
                            .textTheme
                            .headline3!
                            .copyWith(fontWeight: FontWeight.bold, 
                            color: Colors.white,
                            fontSize: 32.sp,
                            shadows: [
                          Shadow(
                              offset: Offset(4.0, 4.0),
                              blurRadius: 3.0,
                              color: CustomExtraThemeData.getChinchillaTheme().colorScheme.secondary),
                        ]),
                        textAlign: TextAlign.center),
                  ]),
            ),
          ),
          Container(
              margin: const EdgeInsets.only(
                  top: 20, left: 30, right: 30, bottom: 20),
              child: FormLogin(
                  submitEvent: (email, password) =>
                      _submit(context, email, password)))
        ]),
      ),
    );
  }
}
