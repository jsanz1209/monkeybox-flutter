import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';

class CustomHeader extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final AppBar appBar = AppBar();
  final Function? onPress;
  final bool? withBackButton;
  final bool? withMenuButton;
  final bool? isTransparent;
  final List<Widget>? actions;
  final Function? behaviourBackButton;

  CustomHeader(
      {Key? key,
      required this.title,
      this.onPress,
      this.withBackButton,
      this.withMenuButton,
      this.actions,
      this.behaviourBackButton,
      this.isTransparent = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var appBar = AppBar(
        toolbarHeight: 64.h,
        elevation: 4,
        leading: IconButton(
          padding: EdgeInsets.zero,
          icon: withBackButton != null
              ? Container(
                  height: double.infinity,
                  width: double.infinity,
                  child: Transform.translate(
                    offset: const Offset(-6, 0),
                    child: Icon(
                      Icons.arrow_back_rounded,
                      color: CustomExtraThemeData.getThemeData(context)
                          .appBarTheme
                          .actionsIconTheme!
                          .color,
                      size: 24.sp,
                    ),
                  ),
                )
              : withMenuButton != null
                  ? SvgPicture.asset(
                      'assets/images/icons/menu.svg',
                      alignment: Alignment.centerLeft,
                      color:
                          Theme.of(context).appBarTheme.actionsIconTheme!.color,
                      width: 24.sp,
                      fit: BoxFit.contain,
                    )
                  : SizedBox(),
          onPressed: () {
            if (onPress != null) {
              onPress!();
            } else if (withBackButton!) {
              Navigator.of(context).pop();
            }
          },
        ),
        actions: actions,
        centerTitle: true,
        title: Text(title,
            style:
                CustomExtraThemeData.getThemeData(context).textTheme.headline2),
        backgroundColor: isTransparent!
            ? Colors.transparent
            : CustomExtraThemeData.getThemeData(context).colorScheme.secondary);
    return appBar;
  }

  @override
  Size get preferredSize => Size.fromHeight(64.h);
}
