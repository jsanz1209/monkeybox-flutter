import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';

class FieldSet extends StatelessWidget {
  final String title;
  final bool withoutMarginChild;
  final List<Widget> widgets;

  const FieldSet(
      {Key? key,
      required this.title,
      required this.widgets,
      this.withoutMarginChild = false})
      : super(key: key);

  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 30),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: Colors.black,
                  width: 2.sp,
                )),
            child: Container(
              margin: EdgeInsets.only(top: withoutMarginChild ? 12 : 16),
              padding: EdgeInsets.only(
                  top: withoutMarginChild ? 0 : 24,
                  bottom: 24,
                  left: 20,
                  right: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [...widgets],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 24),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: CustomExtraThemeData.getExtraThemeData().fieldSetColor,
                border: Border.all(
                  color: Colors.black,
                  width: 2.sp,
                )),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                title,
                style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
