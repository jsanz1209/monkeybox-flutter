import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/models/common/charts/chat_data.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class BarsChartWidget extends StatelessWidget {
  final String title;
  final String labelAxisX;
  final String labelAxisY;
  final List<ColumnSeries<ChartData, dynamic>> series;

  const BarsChartWidget(
      {Key? key,
      required this.title,
      required this.series,
      required this.labelAxisX,
      required this.labelAxisY})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String languageCode = Localizations.localeOf(context).languageCode;
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      margin: EdgeInsets.only(left: 0, top: 8.sp, right: 16.sp),
      title: ChartTitle(
          text: title,
          alignment: ChartAlignment.center,
          textStyle:
              Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 12.sp)),
      primaryXAxis: DateTimeCategoryAxis(
        title: AxisTitle(
            text: labelAxisX,
            textStyle: Theme.of(context)
                .textTheme
                .bodyText2!
                .copyWith(fontSize: 12.sp, color: Colors.black)),
        dateFormat: DateFormat('dd MMM yyyy', languageCode),
        labelRotation: 45,
        axisLine: const AxisLine(width: 0.5),
        minorTickLines: const MinorTickLines(width: 0.5),
        minorGridLines: MinorGridLines(
          width: 0.5,
        ),
        majorTickLines: const MajorTickLines(width: 0.5),
        majorGridLines: const MajorGridLines(width: 0.5),
      ),
      primaryYAxis: NumericAxis(
          minimum: 0,
          interval: 20,
          title: AxisTitle(
              text: labelAxisY,
              textStyle: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(fontSize: 12.sp, color: Colors.black)),
          axisLine: const AxisLine(width: 0),
          majorGridLines: const MajorGridLines(width: 0.5),
          majorTickLines: const MajorTickLines(size: 0)),
      series: series,
      tooltipBehavior: TooltipBehavior(
          enable: true,
          canShowMarker: true,
          builder: (data, point, series, pointIndex, seriesIndex) {
            var chartData = data as ChartData;
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                '${DateFormat('dd MMM yyyy').format(chartData.x)}: ${chartData.y}kg (${data.label})',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(fontSize: 8, color: Colors.white)
              ),
            );
          },
          format: 'point.x: point.y kg point'),
    );
  }
}
