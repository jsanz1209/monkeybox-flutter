import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:monkeybox/widgets/common/input_image_silver.dart';
import 'package:monkeybox/widgets/side_menu/custom_menu_header.dart';

bool isSelectedImage = false;

class UserImageSliverAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight = 200.h;
  final String title;
  String? imageUrl;
  double? minExtentCustom;
  final File? imageFile;
  final bool? withBackButton;
  final Function(File?) onChange;
  final bool? isFixedBackground;
  final bool? isBlackTheme;
  final GlobalKey<ScaffoldState>? scaffoldKey;

  UserImageSliverAppBar(
      {required this.title,
      required this.imageUrl,
      required this.imageFile,
      required this.onChange,
      this.withBackButton,
      this.minExtentCustom,
      this.isFixedBackground,
      this.isBlackTheme,
      this.scaffoldKey});

  double getOpacity(double shrinkOffset) {
    var fixOpacity = (shrinkOffset / expandedHeight);
    return 1 - fixOpacity < 0
        ? 0
        : 1 - fixOpacity > 1
            ? 1
            : 1 - fixOpacity;
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SafeArea(
      bottom: false,
      top: false,
      maintainBottomViewPadding: false,
      child: Container(
        height: 220.h,
        width: MediaQuery.of(context).size.width,
        clipBehavior: Clip.none,
        color:
            isFixedBackground != null ? Theme.of(context).primaryColor : null,
        child: Stack(children: [
          SafeArea(
            child: Container(
              height: expandedHeight - 50,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                clipBehavior: Clip.none,
                fit: StackFit.expand,
                children: [
                  withBackButton != null
                      ? CustomHeader(title: title, withBackButton: true)
                      : CustomMenuHeader(
                          title: title,
                          onClick: (ctx) {
                            if (scaffoldKey != null) {
                              scaffoldKey!.currentState!.openDrawer();
                            }
                          },
                        ),
                  Opacity(
                    opacity: getOpacity(shrinkOffset),
                    child: Opacity(
                      opacity: 0.5,
                      child: imageFile != null
                          ? Image.file(imageFile!, fit: BoxFit.cover)
                          : imageUrl != null
                              ? CachedNetworkImage(
                                  imageUrl: imageUrl!,
                                  fit: BoxFit.cover,
                                )
                              : Container(
                                  color:
                                  CustomExtraThemeData.getThemeData(context).colorScheme.secondary,
                                ),
                    ),
                  ),
                  Positioned(
                      child: withBackButton != null
                          ? CustomHeader(
                              title: title,
                              withBackButton: true,
                              isTransparent: shrinkOffset < 100 &&
                                  (shrinkOffset > 0 || isSelectedImage),
                            )
                          : CustomMenuHeader(
                              title: title,
                              isTransparent: shrinkOffset < 100 &&
                                  (shrinkOffset > 0 || isSelectedImage),
                              onClick: (ctx) {
                                if (scaffoldKey != null) {
                                  scaffoldKey!.currentState!.openDrawer();
                                }
                              },
                            )),
                ],
              ),
            ),
          ),
          Positioned(
            top: (expandedHeight / 3) - shrinkOffset,
            left: MediaQuery.of(context).size.width / 3.35,
            child: Opacity(
                opacity: getOpacity(shrinkOffset),
                child: InputImageSilver(
                    isBlackTheme: isBlackTheme,
                    fontSize: 18.sp,
                    initValue: imageUrl,
                    expandedHeight: expandedHeight,
                    onChange: (value) {
                      if (value != null) {
                        onChange(value);
                        isSelectedImage = true;
                      }
                    })),
          ),
        ]),
      ),
    );
  }

  @override
  double get maxExtent => 220.h;

  @override
  double get minExtent => minExtentCustom ?? 64.h;

  @override
  bool shouldRebuild(UserImageSliverAppBar old) => true;
}
