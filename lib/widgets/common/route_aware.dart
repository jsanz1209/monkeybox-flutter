import 'package:flutter/material.dart';

class RouteAwareWidget extends StatefulWidget {
  final String? name;
  final Widget child;
  final Function onChangeRoute;
  final RouteObserver<ModalRoute<void>> routeObserver;
  const RouteAwareWidget(
      {Key? key,
      this.name,
      required this.routeObserver,
      required this.child,
      required this.onChangeRoute})
      : super(key: key);

  @override
  State<RouteAwareWidget> createState() => RouteAwareWidgetState();
}

// Implement RouteAware in a widget's state and subscribe it to the RouteObserver.
class RouteAwareWidgetState extends State<RouteAwareWidget> with RouteAware {
  _changeRoute() {
    Future.delayed(const Duration(milliseconds: 100), () {
      widget.onChangeRoute();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    widget.routeObserver.subscribe(this, ModalRoute.of(context)!);
  }

  @override
  void dispose() {
    widget.routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    // Route was pushed onto navigator and is now topmost route.
    _changeRoute();
  }

  @override
  void didPopNext() {
    // Covering route was popped off the navigator.
    _changeRoute();
  }

  @override
  Widget build(BuildContext context) => widget.child;
}
