import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/models/common/item_dropdown_multiple.dart';
import 'package:monkeybox/pages/app.dart';

class CustomMultipleDropdown extends StatefulWidget {
  final String title;
  final Function(List<ItemDropdownMultiple>?)? onSave;
  final String? Function(List<ItemDropdownMultiple>?)? validator;
  final GlobalKey<FormFieldState>? formKey;
  final bool? isBlackTheme;
  final Function(List<ItemDropdownMultiple>)? onChange;
  final List<ItemDropdownMultiple> listOptions;

  const CustomMultipleDropdown(
      {Key? key,
      required this.title,
      required this.listOptions,
      this.onSave,
      this.validator,
      this.formKey,
      this.isBlackTheme,
      this.onChange})
      : super(key: key);

  @override
  State<CustomMultipleDropdown> createState() => _CustomMultipleDropdownState();
}

class _CustomMultipleDropdownState extends State<CustomMultipleDropdown> {
  @override
  Widget build(BuildContext context) {
    return FormField<List<ItemDropdownMultiple>>(
      key: widget.formKey,
      validator: widget.validator,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onSaved: (value) => widget.onSave != null ? widget.onSave!(value) : null,
      initialValue: widget.listOptions,
      builder: (FormFieldState<List<ItemDropdownMultiple>> state) => InkWell(
        onTap: () {
          _showAlertCheckBoxList(state);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(width: 2.sp, color: Colors.black))),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8.sp),
                child: _getSelectedText.isNotEmpty
                    ? Text(
                        _getSelectedText,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: Colors.black),
                      )
                    : Text(widget.title),
              ),
            ),
            state.hasError
                ? Padding(
                    padding: const EdgeInsets.only(left: 5, top: 12),
                    child: Text(
                      state.errorText!,
                      style: TextStyle(
                          fontFamily: 'Oswald',
                          fontSize: 14.sp,
                          overflow: TextOverflow.visible,
                          color: Theme.of(context).errorColor),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  String get _getSelectedText {
    List<String?> selectedOptions = widget.listOptions.map((item) {
      if ((item.isChecked)) {
        return item.name;
      } else {
        return null;
      }
    }).toList();
    selectedOptions.removeWhere((element) => element == null);
    return selectedOptions.join(', ');
  }

  Padding get _buildImagePlaceHolderError {
    return Padding(
      padding: EdgeInsets.all(12.sp),
      child: SvgPicture.asset(
        'assets/images/icons/person.svg',
        color: Colors.white,
        width: double.infinity,
        fit: BoxFit.contain,
      ),
    );
  }

  void _showAlertCheckBoxList(FormFieldState<List> state) {
    showDialog(
        context: appNavigatorKey.currentContext!,
        builder: (ctx) {
          return StatefulBuilder(builder: (context, setStateDialog) {
            return AlertDialog(
              title: Text(
                widget.title,
                style: Theme.of(context)
                    .textTheme
                    .headline2!
                    .copyWith(color: Colors.black),
              ),
              actionsPadding:
                  EdgeInsets.symmetric(horizontal: 16.sp, vertical: 0),
              contentPadding: EdgeInsets.all(16.sp),
              content: Container(
                height: 300.0, // Change as per your requirement
                width: 500.0,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: widget.listOptions.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        elevation: 3,
                        margin:
                            EdgeInsets.symmetric(horizontal: 0, vertical: 8.sp),
                        child: Container(
                          padding: EdgeInsets.all(8.sp),
                          child: Column(
                            children: <Widget>[
                              CheckboxListTile(
                                  activeColor: Theme.of(context).primaryColor,
                                  dense: true,
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 8.sp, horizontal: 0),
                                  //font change
                                  title: Row(
                                    children: [
                                      Container(
                                        height: 50.sp,
                                        width: 50.sp,
                                        alignment: Alignment.centerLeft,
                                        decoration: BoxDecoration(
                                          color: Theme.of(context).primaryColor,
                                          //Here goes the same radius, u can put into a var or function
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(100)),
                                        ),
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            child: widget.listOptions[index]
                                                        .imageUrl !=
                                                    null
                                                ? CachedNetworkImage(
                                                    imageUrl: widget
                                                        .listOptions[index]
                                                        .imageUrl
                                                        .toString(),
                                                    height: 50.sp,
                                                    width: 50.sp,
                                                    fit: BoxFit.cover,
                                                    placeholder: (ctx, _) =>
                                                        _buildImagePlaceHolderError,
                                                    alignment: Alignment.center,
                                                    errorWidget:
                                                        (ctx, error, _) {
                                                      return _buildImagePlaceHolderError;
                                                    },
                                                  )
                                                : _buildImagePlaceHolderError),
                                      ),
                                      SizedBox(width: 8.sp),
                                      Expanded(
                                        child: Text(
                                          widget.listOptions[index].name
                                              .toString(),
                                          overflow: TextOverflow.clip,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText2!
                                              .copyWith(
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                        ),
                                      ),
                                    ],
                                  ),
                                  value: widget.listOptions[index].isChecked,
                                  onChanged: (val) {
                                    _itemChange(val!, index, setStateDialog);
                                    state.didChange(widget.listOptions);
                                    if (widget.onChange != null) {
                                      widget.onChange!(widget.listOptions);
                                    }
                                  })
                            ],
                          ),
                        ),
                      );
                    }),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(appNavigatorKey.currentContext!).pop(false);
                    },
                    child: Text('Aceptar',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: Theme.of(context).primaryColor))),
                TextButton(
                    onPressed: () {
                      Navigator.of(appNavigatorKey.currentContext!).pop(true);
                    },
                    child: Text('Cancelar',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: Theme.of(context).primaryColor)))
              ],
            );
          });
        });
  }

  void _itemChange(
      bool value, int index, Function(void Function()) setStateDialog) {
    setState(() {
      widget.listOptions[index].isChecked = value;
    });
    setStateDialog(() {
      widget.listOptions[index].isChecked = value;
    });
  }
}
