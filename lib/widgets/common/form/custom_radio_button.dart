import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/common/item_radio_button.dart';

class CustomRadioButton extends StatefulWidget {
  final List<ItemRadioButton> labels;
  final String fieldName;
  final Function(String?)? onSave;
  final GlobalKey<FormFieldState>? formKey;
  final String? Function(String?)? validator;
  final bool? isBlackTheme;
  final String? initValue;

  const CustomRadioButton(
      {Key? key,
      required this.labels,
      required this.fieldName,
      this.formKey,
      this.onSave,
      this.validator,
      this.isBlackTheme,
      this.initValue})
      : super(key: key);

  @override
  State<CustomRadioButton> createState() => _CustomRadioButtonState();
}

class _CustomRadioButtonState extends State<CustomRadioButton> {
  String? _groupValue;

  _valueChangedHandler(String? value) {
    setState(() {
      _groupValue = value;
    });
  }

  Color get themeColor {
    return widget.isBlackTheme != null ? Colors.black : Colors.white;
  }

  @override
  void initState() {    
    super.initState();
    if (widget.initValue != null) {
      _valueChangedHandler(widget.initValue);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FormField<String>(
        key: widget.formKey,
        validator: widget.validator,
        initialValue: widget.initValue,
        onSaved: (value) =>
            widget.onSave != null ? widget.onSave!(_groupValue) : null,
        builder: (FormFieldState<String> state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...widget.labels.asMap().entries.map((entry) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 0),
                  child: Row(children: [
                    Transform.scale(
                      scale: 1.85,
                      child: Radio(
                        fillColor: MaterialStateProperty.resolveWith(
                            (states) => themeColor),
                        focusColor: themeColor,
                        hoverColor: themeColor,
                        overlayColor: MaterialStateProperty.resolveWith(
                            (states) => themeColor),
                        value: entry.value.id,
                        groupValue: _groupValue,
                        onChanged: (String? value) {
                          state.didChange(value);
                          state.validate();
                          _valueChangedHandler(value);
                        },
                        activeColor: themeColor,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(entry.value.label,
                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: themeColor
                          )),
                    )
                  ]),
                );
              }),
              state.hasError && state.errorText!.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.only(left: 5, top: 12),
                      child: Text(
                        state.errorText!,
                        style: TextStyle(
                            fontFamily: 'Oswald',
                            fontSize: 14.sp,
                            overflow: TextOverflow.visible,
                            color: Theme.of(context).errorColor),
                      ),
                    )
                  : Container(),
            ],
          );
        });
  }
}
