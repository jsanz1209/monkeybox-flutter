import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/common/item_dropdown.dart';

class CustomDropdown extends StatefulWidget {
  final String label;
  final List<ItemDropdown> options;
  final String fieldName;
  final GlobalKey<FormFieldState>? formKey;
  final String? Function(int?)? validator;
  final Function(int?)? onSave;
  final Function(int?)? onChange;
  final int? initialValue;
  final bool? isBlackTheme;
  const CustomDropdown(
      {Key? key,
      required this.label,
      required this.fieldName,
      required this.options,
      this.initialValue,
      this.onSave,
      this.onChange,
      this.validator,
      this.formKey,
      this.isBlackTheme})
      : super(key: key);

  @override
  State<CustomDropdown> createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  int? dropdownValue;

  Color get themeColor {
    return widget.isBlackTheme != null ? Colors.black : Colors.white;
  }

  @override
  void initState() {
    super.initState();
    if (widget.initialValue != null) {
      dropdownValue = widget.initialValue;
    }
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<int>(
      key: widget.formKey,
      validator: widget.validator,
      isDense: true,
      isExpanded: true,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onSaved: (value) {
        if (widget.onSave != null) {
          widget.onSave!(value);
        }
      },
      value: dropdownValue,
      icon: Transform.translate(
        offset: const Offset(6, -6),
        child: Icon(
          Icons.arrow_drop_down,
          color: themeColor,
        ),
      ),
      dropdownColor: widget.isBlackTheme != null
          ? Colors.white
          : Theme.of(context).primaryColor,
      iconSize: 32.sp,
      elevation: 12,
      style: Theme.of(context).textTheme.bodyText1!.copyWith(color: themeColor),
      decoration: InputDecoration(
          errorStyle: TextStyle(
              fontFamily: 'Oswald',
              fontSize: 14.sp,
              overflow: TextOverflow.visible,
              color: Theme.of(context).errorColor),
          errorMaxLines: 2,
          isDense: true,
          labelText: widget.label,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: themeColor, width: 2.sp),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: themeColor, width: 2.sp),
          ),
          errorBorder: UnderlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).errorColor, width: 2.sp),
          ),
          hintStyle: TextStyle(color: themeColor),
          labelStyle:
              Theme.of(context).textTheme.bodyText2!.copyWith(height: 1.75),
          contentPadding: EdgeInsets.symmetric(
              vertical: dropdownValue != null ? -2.sp : -6.sp)),
      onChanged: (int? newValue) {
        if (widget.onChange != null) {
          widget.onChange!(newValue);
        }
        setState(() {
          dropdownValue = newValue;
        });
      },
      items: widget.options.map<DropdownMenuItem<int>>((ItemDropdown item) {
        return DropdownMenuItem<int>(
          value: item.id,
          child: FittedBox(
            child: Text(item.value,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: themeColor, fontFamily: 'Work Sans')),
          ),
        );
      }).toList(),
    );
  }
}
