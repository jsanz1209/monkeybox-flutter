import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/hex_color.dart';

class CustomSwitch extends StatefulWidget {
  final GlobalKey<FormFieldState>? formKey;
  final String label;
  final Function(bool?)? onSave;
  final String? Function(bool?)? validator;
  final bool? isBlackTheme;
  final bool? initialValue;

  const CustomSwitch(
      {Key? key,
      required this.formKey,
      required this.label,
      this.onSave,
      this.validator,
      this.isBlackTheme,
      this.initialValue})
      : super(key: key);

  @override
  State<CustomSwitch> createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch> {
  bool _value = true;

  Color get themeColor {
    return widget.isBlackTheme != null ? Colors.black : Colors.white;
  }

  @override
  void initState() {
    super.initState();
    if (widget.initialValue != null) {
      _value = widget.initialValue!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 2.sp, color: themeColor))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(widget.label,
              style:
                  Theme.of(context).textTheme.bodyText2!.copyWith(height: 4)),
          FormField<bool>(
              key: widget.formKey,
              validator: widget.validator,
              onSaved: (value) =>
                  widget.onSave != null ? widget.onSave!(_value) : null,
              initialValue: _value,
              builder: (FormFieldState<bool> state) {
                return Transform.translate(
                  offset: const Offset(6, 0),
                  child: Transform.scale(
                    scale: 0.85,
                    child: CupertinoSwitch(
                      value: _value,
                      onChanged: (_) {
                        state.didChange(true);
                        setState(() {
                          _value = !_value;
                        });
                      },
                      activeColor: HexColor('#00ffc2'),
                      thumbColor: Colors.white,
                      trackColor: Colors.grey,
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
