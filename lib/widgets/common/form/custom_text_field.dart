import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:flutter_cupertino_datetime_picker/flutter_cupertino_datetime_picker.dart'
    as cupertinoDatePicker;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:monkeybox/pages/app.dart';


class CustomTextFormField extends StatefulWidget {
  final String label;
  final String fieldName;
  final TextInputType keyboardType;
  final GlobalKey<FormFieldState>? formKey;
  final bool? isPassword;
  final bool? isTypeDate;
  final bool? isTypeMonthOnlyDate;
  final Widget? suffixWidget;
  final Widget? suffixWidgetPlaceHolder;
  final List<TextInputFormatter>? inputFormatters;
  final String? suffixText;
  final String? Function(String?)? validator;
  final bool? isBlackTheme;
  final String? initialValue;
  final Function(String?)? onSave;
  final TextInputAction? textInputAction;
  final Function(String)? onFieldSubmitted;
  final FocusScopeNode? focusNode;
  final int? maxLines;
  final int? maxLength;
  final Function? onBlur;
  final bool? disabled;
  final bool? hasToReloadValue;
  final Function(String)? onChanged;

  const CustomTextFormField(
      {Key? key,
      required this.label,
      required this.fieldName,
      required this.keyboardType,
      this.initialValue,
      this.formKey,
      this.validator,
      this.onSave,
      this.isPassword = false,
      this.isTypeDate = false,
      this.isTypeMonthOnlyDate = false,
      this.inputFormatters,
      this.suffixWidget,
      this.suffixWidgetPlaceHolder,
      this.suffixText,
      this.isBlackTheme,
      this.textInputAction,
      this.onFieldSubmitted,
      this.focusNode,
      this.maxLines,
      this.maxLength,
      this.onBlur,
      this.onChanged,
      this.hasToReloadValue,
      this.disabled = false})
      : super(key: key);

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  TextEditingController _fieldCtrl = TextEditingController();
  final _fieldFocusNode = FocusNode();
  late bool _togglePassword;
  DateTime? _selectedDate;
  String languageCode = Localizations.localeOf(appNavigatorKey.currentContext!).languageCode;

  @override
  void initState() {
    super.initState();
    _togglePassword = widget.isPassword!;
    _fieldFocusNode.addListener(() {
      if (widget.formKey != null) {
        if (_fieldFocusNode.hasFocus) {
          widget.formKey!.currentState!.reset();
        } else {
          widget.formKey!.currentState!.validate();
        }
        if (widget.onBlur != null && !_fieldFocusNode.hasFocus) {
          widget.onBlur!();
        }
      }
      setState(() {});
    });

    _fieldCtrl.addListener(() {
      if (_fieldCtrl.text.isNotEmpty) {
        setState(() {});
      }
    });

    if (widget.initialValue != null) {
      if (widget.isTypeDate! && !widget.isTypeMonthOnlyDate!) {
        _selectedDate = DateTime.parse(widget.initialValue!);
        _fieldCtrl.text = DateFormat('dd MMMM yyyy', languageCode)
            .format(DateTime.parse(widget.initialValue!));
      } else if (widget.isTypeMonthOnlyDate!) {
        _selectedDate = DateTime.parse(widget.initialValue!);
        _fieldCtrl.text = DateFormat('MMMM yyyy', languageCode)
            .format(DateTime.parse(widget.initialValue!));
      } else {
        _fieldCtrl.text = widget.initialValue!;
      }
    }
  }

  @override
  void didUpdateWidget (CustomTextFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.hasToReloadValue != null && widget.initialValue != null) {
      Future.delayed(const Duration(milliseconds: 100), () {
      _fieldCtrl.text = widget.initialValue!;
      });
    }  
  }

  @override
  void dispose() {
    _fieldFocusNode.removeListener(() {});
    _fieldCtrl.removeListener(() {});
    _fieldCtrl.dispose();
    _fieldFocusNode.dispose();
    super.dispose();
  }

  Color get themeColor {
    return widget.isBlackTheme != null ? Colors.black : Colors.white;
  }

  get _getContentPadding {
    return widget.isTypeMonthOnlyDate!
        ? EdgeInsets.symmetric(vertical: 4.sp)
        : _fieldFocusNode.hasFocus || _fieldCtrl.text.isNotEmpty
            ? EdgeInsets.symmetric(vertical: 8.sp)
            : EdgeInsets.symmetric(vertical: widget.isPassword! ? 2.sp : 0);
  }

  Widget? get _getSuffixIcon {
    return widget.isPassword!
        ? IconButton(
            color: Colors.red,
            padding: EdgeInsets.zero,
            onPressed: widget.disabled! ? null : () {
              setState(() {
                _togglePassword = !_togglePassword;
              });
            },
            icon: Container(
              margin: EdgeInsets.only(
                  left: 0,
                  top: _fieldFocusNode.hasFocus
                      ? 12.sp
                      : _fieldCtrl.text.isEmpty
                          ? 0.sp
                          : 12.sp),
              padding: EdgeInsets.zero,
              child: Icon(
                  _togglePassword ? Icons.visibility : Icons.visibility_off,
                  color: themeColor,
                  size: 20.sp),
            ))
        : _fieldCtrl.text.isNotEmpty
            ? Container(
                padding: EdgeInsets.zero,
                margin: const EdgeInsets.only(top: 0, left: 0),
                child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Transform.translate(
                        offset: const Offset(0, 8),
                        child: Icon(
                          Icons.clear_rounded,
                          color: themeColor,
                          size: 21.sp,
                        )),
                    onPressed: widget.disabled! ? null : () {
                      setState(() {
                        _fieldCtrl.clear();
                        _fieldCtrl = TextEditingController();
                        _fieldFocusNode.requestFocus();
                      });
                    }),
              )
            : widget.isTypeDate!
                ? Icon(
                    Icons.event_note_outlined,
                    color: Colors.grey,
                    size: 20.sp,
                  )
                : widget.suffixWidget != null && !_fieldFocusNode.hasFocus
                    ? Transform.translate(
                        offset: const Offset(0, -2),
                        child: Transform.scale(
                            scale: 1,
                            child: Container(
                                child: widget.suffixWidgetPlaceHolder)),
                      )
                    : null;
  }

  Widget? get _getSuffixWidget {
    return widget.suffixWidget != null
        ? Transform.translate(
            offset: const Offset(0, 2),
            child: SizedBox(
                width: 24.sp, height: 24.sp, child: widget.suffixWidget))
        : null;
  }

  void Function()? _onTap() {
    if (!widget.disabled!) {
      return widget.isTypeDate!
          ? () async {
              FocusScope.of(context).requestFocus(FocusNode());
              DateTime? dateTime = await _selectDate(context);
              _setDate(dateTime);
            }
          : null;
    }
    return null;
  }

  Future<DateTime?> _selectDate(BuildContext context) async {
    DateTime? picked;
    if (widget.isTypeMonthOnlyDate!) {
      cupertinoDatePicker.DatePicker.showDatePicker(context,
          minDateTime: DateTime(2012),
          maxDateTime: DateTime.now(),
          initialDateTime: _selectedDate ?? DateTime.now(),
          dateFormat: 'MM-yyyy',
          locale: cupertinoDatePicker.DATETIME_PICKER_LOCALE_DEFAULT,
          pickerMode: cupertinoDatePicker.DateTimePickerMode.date,
          onCancel: () {},
          onClose: () {}, onConfirm: (dateTime, __) {
        _setDate(dateTime);
      });
    } else {
      picked = await showDatePicker(
          context: context,
          helpText: AppLocalizations.of(context).selectADate,
          cancelText: AppLocalizations.of(context).cancel,
          confirmText: AppLocalizations.of(context).ok,
          locale: Localizations.localeOf(context),
          initialDate: _selectedDate ?? DateTime.now(),
          firstDate: DateTime(1950, 1),
          lastDate: DateTime.now());

      return picked;
    }
    return null;
  }

  _setDate(DateTime? dateTime) {
    if (dateTime != null && dateTime != _selectedDate) {
      setState(() {
        _selectedDate = dateTime;
      });
    }
    if (_selectedDate != null) {
      var dateFormat =
          widget.isTypeMonthOnlyDate! ? 'MMMM yyyy' : 'dd MMMM yyyy';
      _fieldCtrl.text = DateFormat(dateFormat, languageCode).format(_selectedDate!);
      widget.formKey!.currentState!.validate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        key: widget.formKey,
        focusNode: _fieldFocusNode,
        controller: _fieldCtrl,
        inputFormatters: widget.inputFormatters,
        enabled: widget.disabled == false,
        textInputAction: widget.textInputAction,
        maxLines: widget.maxLines ?? 1,
        minLines: widget.maxLines != null ? 1 : null,
        maxLength: widget.maxLength,
        onEditingComplete: () {
          if (widget.textInputAction == TextInputAction.next) {
            if (widget.focusNode != null) {
              widget.focusNode!.nextFocus();
            }
            if (_fieldCtrl.value.text.isNotEmpty || widget.isPassword!) {
              _fieldFocusNode.nextFocus();
            }
          }
        },
        decoration: InputDecoration(
            errorStyle: TextStyle(
                fontFamily: 'Oswald',
                fontSize: 14.sp,
                overflow: TextOverflow.clip,
                color: Theme.of(context).errorColor),
            errorMaxLines: 2,
            isDense: true,
            labelText: widget.label,
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: themeColor, width: 2.sp),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: themeColor, width: 2.sp),
            ),
            errorBorder: UnderlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).errorColor, width: 2.sp),
            ),
            suffixIcon: _getSuffixIcon,
            suffixIconConstraints: widget.maxLines == null
                ? BoxConstraints(maxWidth: 32.sp, maxHeight: 32.sp)
                : null,
            suffixText: widget.suffixText,
            suffix: _getSuffixWidget,
            hintStyle: TextStyle(color: themeColor),
            labelStyle: Theme.of(context).textTheme.bodyText2!.copyWith(
                height: widget.suffixWidget != null ? 2.5.sp : 1.1875.sp),
            contentPadding:
                _getContentPadding //Change this value to custom as you like
            ),
        cursorColor: themeColor,
        obscureText: _togglePassword,
        style:
            Theme.of(context).textTheme.bodyText1!.copyWith(color: themeColor),
        keyboardType: widget.keyboardType,
        validator: widget.validator,
        onChanged: (value) {
          if (widget.onChanged != null) {
            widget.onChanged!(value);
          }
        },
        onFieldSubmitted: (e) {
          if (widget.textInputAction == TextInputAction.done) {
            _fieldFocusNode.unfocus();
          }
          if (widget.onFieldSubmitted != null) {
            widget.onFieldSubmitted!(e);
          }
        },
        onSaved: (value) {
          if (widget.onSave != null) {
            widget.onSave!(widget.isTypeDate!
                ? DateFormat('yyyy-MM-dd').format(_selectedDate!)
                : value);
          }
        },
        onTap: _onTap());
  }
}

/**  WORKAROUND CUSTOM ICON ERROR!
if (((_fieldCtrl.text.isNotEmpty &&
            widget.validator!(_fieldCtrl.text) != null) ||
        (widget.formKey!.currentState != null &&
            widget.formKey!.currentState!.validate() == false)) &&
    !_fieldFocusNode.hasFocus)
  Container(
      padding: const EdgeInsets.only(left: 20.0, top: 5.0),
      // height: errorContainerHeight,
      child: Row(
        children: <Widget>[
          Icon(Icons.info_outline_rounded,
              size: 20.0, color: Colors.red[700]),
          Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                  widget.formKey!.currentState != null &&
                          widget.formKey!.currentState!.errorText !=
                              null
                      ? widget.formKey!.currentState!.errorText!
                      : '',
                  style: TextStyle(
                      fontSize: 16.0, color: Colors.red[700])))
        ],
      )), */
