import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/common/item_autocomplete.dart';

class CustomAutocomplete extends StatefulWidget {
  final List<ItemAutocomplete> options;
  final GlobalKey<FormFieldState>? formKey;
  final String label;
  final Function(ItemAutocomplete?)? onSave;
  final String? Function(ItemAutocomplete?)? validator;
  final bool? isBlackTheme;
  final ItemAutocomplete? initialValue;

  const CustomAutocomplete(
      {Key? key,
      required this.label,
      required this.options,
      this.formKey,
      this.onSave,
      this.validator,
      this.isBlackTheme,
      this.initialValue})
      : super(key: key);

  static String _displayStringForOption(ItemAutocomplete option) => option.name;

  @override
  State<CustomAutocomplete> createState() => _CustomAutocompleteState();
}

class _CustomAutocompleteState extends State<CustomAutocomplete> {
  ItemAutocomplete? _value;
  String? _textSearch;

  Color get themeColor {
    return widget.isBlackTheme != null ? Colors.black : Colors.white;
  }

  void initState() {
    super.initState();
    if (widget.initialValue != null) {
      _value = widget.initialValue!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FormField<ItemAutocomplete>(
        key: widget.formKey,
        validator: widget.validator,
        onSaved: (value) =>
            widget.onSave != null ? widget.onSave!(_value) : null,
        initialValue: _value,
        builder: (FormFieldState<ItemAutocomplete> state) {
          return Transform.translate(
            offset: const Offset(0, -20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin: EdgeInsets.only(top: 0),
                    width: MediaQuery.of(context).size.width,
                    height: 70,
                    padding: EdgeInsets.zero,
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 2.sp,
                                color: state.hasError
                                    ? Theme.of(context).errorColor
                                    : themeColor))),
                    child: Stack(
                      alignment: Alignment.topLeft,
                      children: [
                        if ((_textSearch == null || _textSearch!.isEmpty) &&
                            _value == null)
                          Positioned(
                            bottom: 8.sp,
                            child: Text(widget.label,
                                style: Theme.of(context).textTheme.bodyText2!),
                          ),
                        if (_textSearch != null && _textSearch!.isNotEmpty ||
                            _value != null)
                          Positioned(
                            bottom: 28,
                            child: Text(widget.label,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(fontSize: 12.sp)),
                          ),
                        Positioned(
                            left: 0,
                            right: 0,
                            bottom: -11.sp,
                            child: Autocomplete<ItemAutocomplete>(
                                displayStringForOption:
                                    CustomAutocomplete._displayStringForOption,
                                initialValue: _value != null
                                    ? TextEditingValue(text: _value!.name)
                                    : null,
                                optionsBuilder:
                                    (TextEditingValue textEditingValue) {
                                  setState(() {
                                    _textSearch = textEditingValue.text;
                                  });
                                  if (textEditingValue.text == '') {
                                    return widget.options;
                                  }
                                  var options = widget.options
                                      .where((ItemAutocomplete option) {
                                    return option.name.toLowerCase().contains(
                                        textEditingValue.text.toLowerCase());
                                  });
                                  if (options.isEmpty) {
                                    return List.generate(
                                        1,
                                        (index) => ItemAutocomplete(
                                            id: -1,
                                            name: 'No se encuentran resultados'));
                                  } else {
                                    return options;
                                  }
                                },
                                fieldViewBuilder: (BuildContext context,
                                    TextEditingController
                                        fieldTextEditingController,
                                    FocusNode fieldFocusNode,
                                    VoidCallback onFieldSubmitted) {
                                  return TextField(
                                    controller: fieldTextEditingController,
                                    focusNode: fieldFocusNode,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(color: Colors.black),
                                  );
                                },
                                optionsViewBuilder:
                                    (context, onSelected, options) {
                                  bool noneResults =
                                      options.elementAt(0).id == -1;
                                  return Align(
                                      alignment: Alignment.topLeft,
                                      child: Material(
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  32.sp,
                                              height: noneResults ? 50 : 200,
                                              color: Colors.grey.shade200,
                                              child: ListView.builder(
                                                  padding: EdgeInsets.zero,
                                                  itemBuilder: (ctx, index) {
                                                    var option =
                                                        options.elementAt(index);
                                                    return InkWell(
                                                      onTap: () {
                                                        if (option.id != -1) {
                                                          onSelected(option);
                                                        }
                                                      },
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets.all(
                                                                8.0),
                                                        child: Text(
                                                          option.name,
                                                          style: Theme.of(context)
                                                              .textTheme
                                                              .bodyText2!
                                                              .copyWith(
                                                                  color: option.id ==
                                                                          -1
                                                                      ? Colors
                                                                          .grey
                                                                      : Colors
                                                                          .black),
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                  itemCount: options.length))));
                                },
                                onSelected: (ItemAutocomplete selection) {
                                  if (selection.id != -1) {
                                    debugPrint(
                                        'You just selected ${CustomAutocomplete._displayStringForOption(selection)}');
                                    state.didChange(selection);
                                    state.validate();
                                    setState(() {
                                      _value = selection;
                                    });
                                  }
                                })),
                      ],
                    )),
                state.hasError
                    ? Padding(
                        padding: const EdgeInsets.only(left: 5, top: 12),
                        child: Text(
                          state.errorText!,
                          style: TextStyle(
                              fontFamily: 'Oswald',
                              fontSize: 14.sp,
                              overflow: TextOverflow.visible,
                              color: Theme.of(context).errorColor),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
          );
        });
  }
}
