import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomCheckbox extends StatefulWidget {
  final Widget? textWiget;
  final String? text;
  final Function(bool?)? onSave;
  final String? Function(bool?)? validator;
  final GlobalKey<FormFieldState>? formKey;
  final bool? isBlackTheme;
  final bool? initialValue;
  final Function(bool)? onChange;

  const CustomCheckbox(
      {Key? key,
      this.text,
      this.textWiget,
      this.onSave,
      this.validator,
      this.formKey,
      this.isBlackTheme,
      this.initialValue,
      this.onChange})
      : super(key: key);

  @override
  _CustomCheckboxState createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  bool isChecked = false;

  Color get themeColor {
    return widget.isBlackTheme != null ? Colors.black : Colors.white;
  }

  @override
  void initState() {
    super.initState();
    if (widget.initialValue != null) {      
      isChecked = widget.initialValue!;
    }
  }



  @override
  Widget build(BuildContext context) {
    return FormField<bool>(
      key: widget.formKey,
      validator: widget.validator,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onSaved: (value) => widget.onSave != null ? widget.onSave!(value) : null,
      initialValue: widget.initialValue,
      builder: (FormFieldState<bool> state) => Column(
        children: [
          Row(
            children: [
              Transform.scale(
                scale: 1.3.sp,
                child: Column(
                  children: [
                    SizedBox(
                      width: 24.sp,
                      height: 24.sp,
                      child: Checkbox(
                        checkColor: widget.isBlackTheme != null ? Colors.white :  Theme.of(context).primaryColor,
                        // overlayColor: MaterialStateProperty.resolveWith((state) => Colors.white),
                        activeColor: Theme.of(context).primaryColor,
                        overlayColor: MaterialStateProperty.resolveWith(
                            (state) => !isChecked
                                ? themeColor
                                : Theme.of(context).primaryColor),
                        fillColor: MaterialStateProperty.resolveWith((state) =>
                            isChecked
                                ? themeColor
                                : Theme.of(context).primaryColor),
                        value: isChecked,
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        side: BorderSide(color: themeColor, width: 2.sp),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4)),
                        onChanged: (bool? value) {
                          state.didChange(value);
                          setState(() {
                            isChecked = value!;
                          });
                          if (widget.onChange != null) {
                            widget.onChange!(value!);
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.only(left: 10.4),
                child: widget.textWiget ?? Text(widget.text!),
              ))
            ],
          ),
          state.hasError
              ? Padding(
                  padding: const EdgeInsets.only(left: 5, top: 12),
                  child: Text(
                    state.errorText!,
                    style: TextStyle(
                        fontFamily: 'Oswald',
                        fontSize: 14.sp,
                        overflow: TextOverflow.visible,
                        color: Theme.of(context).errorColor),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
