import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants/settings.dart';
import '../../helpers/hex_color.dart';
import '../../pages/app.dart';
import '../../providers/auth/auth_provider.dart';

class CustomExtraThemeData {
  Color? primary;
  Color? secondary;
  Color? tertiary;

  Color? buttonBackground;
  Color? buttonTextColor;
  Color? buttonBorderColor;

  Color? navigationBarWodIconColor;
  Color? navigationBarOpenBoxIconColor;
  Color? navigationBarCompetitorIconColor;
  Color? navigationBarWodLabelColor;
  Color? navigationBarOpenBoxLabelColor;
  Color? navigationBarCompetitorLabelColor;

  Color? wodsColor;
  Color? openBoxColor;
  Color? wodsSecondaryColor;
  Color? openBoxSecondaryColor;

  Color? competitorsColor;
  Color? competitorsSecondaryColor;

  Color? wodsLabelColor;
  Color? openBoxLabelColor;
  Color? wodsSecondaryLabelColor;
  Color? openBoxSecondaryLabelColor;
  Color? competitorsLabelColor;
  Color? competitorsSecondaryLabelColor;

  Color? rankingGenderLabelPrimary;
  Color? rankingGenderLabelSecondary;

  Color? fabBackground;
  Color? fabIcon;

  Color? headerReservationPrimaryColorWod;
  Color? headerReservationSecondaryColorWod;

  Color? headerReservationPrimaryColorOpenBox;
  Color? headerReservationSecondaryColorOpenBox;

  Color? headerReservationPrimaryColorCompetitor;
  Color? headerReservationSecondaryColorCompetitor;

  Color? navigationBarWodBackgroundColor;
  Color? navigationBarOpenBoxBackgroundColor;
  Color? navigationBarCompetitorBackgroundColor;

  Color? userImageInputBackground;

  Color? textSideNavColor;

  Color? fieldSetColor;

  CustomExtraThemeData(
      {this.primary,
      this.secondary,
      this.tertiary,
      this.buttonBackground,
      this.buttonTextColor,
      this.buttonBorderColor,
      this.navigationBarWodIconColor,
      this.navigationBarOpenBoxIconColor,
      this.navigationBarCompetitorIconColor,
      this.navigationBarWodLabelColor,
      this.navigationBarOpenBoxLabelColor,
      this.navigationBarCompetitorLabelColor,
      this.wodsColor,
      this.openBoxColor,
      this.wodsSecondaryColor,
      this.openBoxSecondaryColor,
      this.competitorsColor,
      this.competitorsSecondaryColor,
      this.wodsLabelColor,
      this.openBoxLabelColor,
      this.wodsSecondaryLabelColor,
      this.openBoxSecondaryLabelColor,
      this.competitorsLabelColor,
      this.competitorsSecondaryLabelColor,
      this.rankingGenderLabelPrimary,
      this.rankingGenderLabelSecondary,
      this.fabBackground,
      this.fabIcon,
      this.headerReservationPrimaryColorWod,
      this.headerReservationSecondaryColorWod,
      this.headerReservationPrimaryColorOpenBox,
      this.headerReservationSecondaryColorOpenBox,
      this.headerReservationPrimaryColorCompetitor,
      this.headerReservationSecondaryColorCompetitor,
      this.navigationBarWodBackgroundColor,
      this.navigationBarOpenBoxBackgroundColor,
      this.navigationBarCompetitorBackgroundColor,
      this.userImageInputBackground,
      this.textSideNavColor,
      this.fieldSetColor});

  static ThemeData getThemeData(context) {
    var boxId = Provider.of<AuthProvider>(context, listen: false).boxID;
    return (boxId == null || boxId == 1)
        ? getChinchillaTheme()
        : getAlbaceteTheme();
  }

  static ThemeData getChinchillaTheme() {
    return ThemeData(
        appBarTheme:
            AppBarTheme(actionsIconTheme: IconThemeData(color: Colors.white)),
        iconTheme: IconThemeData(color: Colors.white),
        navigationBarTheme: NavigationBarThemeData(
            backgroundColor: HexColor('#233645'), indicatorColor: Colors.white),
        primaryColor: HexColor('#233645'),
        colorScheme: ColorScheme.fromSwatch().copyWith(
            primary: HexColor('#233645'),
            secondary: HexColor('#141D24'),
            primaryVariant: Color.fromRGBO(52, 100, 139, 1)),
        fontFamily: 'Oswald',
        textSelectionTheme: TextSelectionThemeData(
            selectionHandleColor: Colors.green, selectionColor: Colors.green),
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                  color: Colors.white,
                  fontSize: 36,
                  fontFamily: 'Oswald',
                  letterSpacing: 0.72,
                  height: 1.472),
              bodyText1: TextStyle(
                  color: Colors.white,
                  fontFamily: 'WorkSans',
                  fontSize: 16,
                  letterSpacing: 0.32,
                  fontWeight: FontWeight.normal,
                  height: 1.1875),
              headline1: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontFamily: 'Oswald',
                  letterSpacing: 0.72,
                  height: 1.472),
              headline2: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontFamily: 'Oswald',
                  letterSpacing: 0.25,
                  height: 1.472),
              headline3: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontFamily: 'Oswald',
                  letterSpacing: 0.25,
                  height: 1.472),
              bodyText2: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'WorkSans',
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  letterSpacing: 0.32,
                  height: 1.1875),
            ),
        switchTheme: SwitchThemeData(
          thumbColor:
              MaterialStateProperty.resolveWith((state) => HexColor('#00ffc2')),
          trackColor:
              MaterialStateProperty.resolveWith((state) => HexColor('#00ffc2')),
          overlayColor:
              MaterialStateProperty.resolveWith((state) => HexColor('#00ffc2')),
        ),
        textButtonTheme: TextButtonThemeData(
            style: ButtonStyle(
                padding: MaterialStateProperty.resolveWith(
                    (states) => EdgeInsets.zero),
                textStyle: MaterialStateProperty.resolveWith((_) => TextStyle(
                    // color: Colors.white,
                    fontFamily: 'WorkSans',
                    letterSpacing: 0.32,
                    height: 1.1875,
                    fontSize: 16)))),
        errorColor: HexColor('#F4344F'));
  }

  static ThemeData getAlbaceteTheme() {
    return ThemeData(
        appBarTheme: AppBarTheme(
            actionsIconTheme: IconThemeData(color: Settings.DEEP_LILAC)),
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        navigationBarTheme: NavigationBarThemeData(
            backgroundColor: Settings.DEEP_LILAC, indicatorColor: Colors.white),
        primaryColor: Settings.DEEP_LILAC,
        colorScheme: ColorScheme.fromSwatch().copyWith(
            primary: Settings.DEEP_LILAC,
            primaryVariant: Settings.LILAC,
            secondary: Colors.white,
            secondaryVariant: Settings.ORANGE),
        fontFamily: 'Oswald',
        textSelectionTheme: TextSelectionThemeData(
            selectionHandleColor: Colors.green, selectionColor: Colors.green),
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                  color: Colors.white,
                  fontSize: 36,
                  fontFamily: 'Oswald',
                  letterSpacing: 0.72,
                  height: 1.472),
              bodyText1: TextStyle(
                  color: Settings.DEEP_LILAC,
                  fontFamily: 'WorkSans',
                  fontSize: 16,
                  letterSpacing: 0.32,
                  fontWeight: FontWeight.normal,
                  height: 1.1875),
              headline1: TextStyle(
                  color: Settings.DEEP_LILAC,
                  fontSize: 24,
                  fontFamily: 'Oswald',
                  letterSpacing: 0.72,
                  height: 1.472),
              headline2: TextStyle(
                  color: Settings.DEEP_LILAC,
                  fontSize: 20,
                  fontFamily: 'Oswald',
                  letterSpacing: 0.25,
                  height: 1.472),
              headline3: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontFamily: 'Oswald',
                  letterSpacing: 0.25,
                  height: 1.472),
              bodyText2: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'WorkSans',
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  letterSpacing: 0.32,
                  height: 1.1875),
            ),
        switchTheme: SwitchThemeData(
          thumbColor:
              MaterialStateProperty.resolveWith((state) => HexColor('#00ffc2')),
          trackColor:
              MaterialStateProperty.resolveWith((state) => HexColor('#00ffc2')),
          overlayColor:
              MaterialStateProperty.resolveWith((state) => HexColor('#00ffc2')),
        ),
        textButtonTheme: TextButtonThemeData(
            style: ButtonStyle(
                padding: MaterialStateProperty.resolveWith(
                    (states) => EdgeInsets.zero),
                textStyle: MaterialStateProperty.resolveWith((_) => TextStyle(
                    // color: Colors.white,
                    fontFamily: 'WorkSans',
                    letterSpacing: 0.32,
                    height: 1.1875,
                    fontSize: 16)))),
        errorColor: HexColor('#F4344F'));
  }

  static CustomExtraThemeData getExtraThemeData() {
    var boxId = Provider.of<AuthProvider>(appNavigatorKey.currentContext!,
            listen: false)
        .boxID;
    return (boxId == null || boxId == 1)
        ? CustomExtraThemeData(
            primary: HexColor('#233645'),
            secondary: HexColor('#141D24'),
            tertiary: Settings.ORANGE,
            userImageInputBackground: Colors.white,
            buttonBackground: HexColor("#00FFC2"),
            buttonTextColor: Colors.black,
            buttonBorderColor: HexColor("#00FFC2"),
            navigationBarWodIconColor: Colors.white,
            navigationBarOpenBoxIconColor: Colors.white,
            navigationBarCompetitorIconColor: Colors.white,
            navigationBarWodLabelColor:
                Theme.of(appNavigatorKey.currentContext!)
                    .textTheme
                    .bodyText2!
                    .color,
            navigationBarOpenBoxLabelColor:
                Theme.of(appNavigatorKey.currentContext!)
                    .textTheme
                    .bodyText2!
                    .color,
            navigationBarCompetitorLabelColor:
                Theme.of(appNavigatorKey.currentContext!)
                    .textTheme
                    .bodyText2!
                    .color,
            navigationBarWodBackgroundColor: HexColor('#233645'),
            navigationBarOpenBoxBackgroundColor: HexColor('#ff4e00'),
            navigationBarCompetitorBackgroundColor: HexColor('#3dca44'),
            wodsColor: HexColor('#233645'),
            openBoxColor: HexColor('#ff4e00'),
            wodsSecondaryColor: HexColor('#0024db'),
            openBoxSecondaryColor: HexColor('#bc0000'),
            competitorsColor: HexColor('#3dca44'),
            competitorsSecondaryColor: HexColor('#006400'),
            wodsLabelColor: Colors.white,
            openBoxLabelColor: Colors.white,
            wodsSecondaryLabelColor: Colors.white,
            openBoxSecondaryLabelColor: Colors.white,
            competitorsLabelColor: Colors.white,
            competitorsSecondaryLabelColor: Colors.white,
            rankingGenderLabelPrimary: HexColor('#233645'),
            rankingGenderLabelSecondary: HexColor("#00FFC2"),
            fabBackground: HexColor('#233645'),
            fabIcon: Colors.white,
            headerReservationPrimaryColorWod: HexColor('#006fff'),
            headerReservationSecondaryColorWod: HexColor('#0024db'),
            headerReservationPrimaryColorOpenBox: HexColor('#ff4e00'),
            headerReservationSecondaryColorOpenBox: HexColor('#bc0000'),
            headerReservationPrimaryColorCompetitor: HexColor('#3dca44'),
            headerReservationSecondaryColorCompetitor: HexColor('#006400'),
            textSideNavColor: Colors.white,
            fieldSetColor: Colors.lightBlue[100])
        : CustomExtraThemeData(
            primary: Settings.DEEP_LILAC,
            secondary: Colors.white,
            tertiary: Settings.ORANGE,
            userImageInputBackground: Colors.white,
            buttonBackground: Colors.white,
            buttonTextColor: Colors.black,
            buttonBorderColor: Colors.black,
            navigationBarWodIconColor: Colors.white,
            navigationBarOpenBoxIconColor: Colors.white,
            navigationBarCompetitorIconColor: Colors.white,
            navigationBarWodLabelColor: Colors.white,
            navigationBarOpenBoxLabelColor: Colors.white,
            navigationBarCompetitorLabelColor: Colors.white,
            navigationBarWodBackgroundColor: Settings.LILAC,
            navigationBarOpenBoxBackgroundColor: Settings.ORANGE,
            navigationBarCompetitorBackgroundColor: HexColor('#3dca44'),
            wodsColor: Settings.LILAC,
            openBoxColor: Settings.ORANGE,
            wodsSecondaryColor: Settings.DEEP_LILAC,
            openBoxSecondaryColor: Settings.DEEP_ORANGE,
            competitorsColor: HexColor('#3dca44'),
            competitorsSecondaryColor: HexColor('#006400'),
            wodsLabelColor: Colors.white,
            openBoxLabelColor: Colors.white,
            wodsSecondaryLabelColor: Colors.white,
            openBoxSecondaryLabelColor: Colors.white,
            competitorsLabelColor: Colors.white,
            competitorsSecondaryLabelColor: Colors.white,
            rankingGenderLabelPrimary: Settings.DEEP_LILAC,
            rankingGenderLabelSecondary: Settings.ORANGE,
            fabBackground: Settings.ORANGE,
            fabIcon: Colors.white,
            headerReservationPrimaryColorWod: Settings.LILAC,
            headerReservationSecondaryColorWod: Settings.DEEP_LILAC,
            headerReservationPrimaryColorOpenBox: Settings.ORANGE,
            headerReservationSecondaryColorOpenBox: Settings.DEEP_ORANGE,
            headerReservationPrimaryColorCompetitor: HexColor('#3dca44'),
            headerReservationSecondaryColorCompetitor: HexColor('#006400'),
            textSideNavColor: Settings.DEEP_LILAC,
            fieldSetColor: Colors.white);
  }
}
