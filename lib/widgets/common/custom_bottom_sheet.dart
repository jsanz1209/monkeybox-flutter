import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/common/bottom_sheet.dart' as bottom_model;

class CustomBottomSheet extends StatelessWidget {
  final Function selectEvent;
  final List<bottom_model.BottomSheet> model;
  const CustomBottomSheet({
    Key? key,
    required this.selectEvent,
    required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Wrap(children: [
        ...model.map((entry) {
          return ListTile(
            title: Text(entry.title),
            subtitle: Text(
              entry.subtitle,
              style:
                  Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 12.sp),
            ),
            leading: Icon(entry.icon, size: 32.sp),
            onTap: () {
              Navigator.of(context).pop(entry.value);
              selectEvent(entry.value);
            },
          );
        }).toList()
      ]),
    );
  }
}
