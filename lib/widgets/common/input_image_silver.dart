import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:monkeybox/models/common/bottom_sheet.dart' as bottom_model;
import 'package:monkeybox/pages/side-menu/photoview_image/photoview_user_image.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/custom_bottom_sheet.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:provider/provider.dart';

class InputImageSilver extends StatefulWidget {
  final bool? isBlackTheme;
  final double? fontSize;
  final String? initValue;
  final double expandedHeight;
  final Function(File?)? onChange;

  const InputImageSilver(
      {Key? key,
      required this.expandedHeight,
      this.isBlackTheme,
      this.fontSize,
      this.initValue,
      this.onChange})
      : super(key: key);

  @override
  _InputImageSilverState createState() => _InputImageSilverState();
}

class _InputImageSilverState extends State<InputImageSilver> {
  File? _storedImage;
  final ImagePicker _picker = ImagePicker();

  Color get themeColor {
    return widget.isBlackTheme != null
        ? CustomExtraThemeData.getThemeData(context).colorScheme.primary
        : Colors.white;
  }

  Future<void> _takePicture(ImageSource option) async {
    try {
      final pickedFile =
          await _picker.pickImage(source: option, imageQuality: 10);
      if (pickedFile != null) {
        setState(() {
          _storedImage = File(pickedFile.path);
        });
      } else {
        debugPrint('no image selected');
      }
    } catch (error) {
      debugPrint(error.toString());
    } finally {}
  }

  Future showChoicesImage(BuildContext context) async {
    var result = await showModalBottomSheet(
        context: context,
        builder: (context) => CustomBottomSheet(
                selectEvent: (ImageSource option) async {},
                model: [
                  bottom_model.BottomSheet(
                      title: 'Cámara',
                      subtitle: 'Hazte una foto',
                      icon: Icons.camera_alt,
                      value: ImageSource.camera),
                  bottom_model.BottomSheet(
                      title: 'Galería',
                      subtitle: 'Escoge una foto de tu galería',
                      icon: Icons.photo_album,
                      value: ImageSource.gallery)
                ]));

    if (result != null) {
      return _takePicture(result);
    } else {
      return Future<bool>.value(false);
    }
  }

  Padding get _buildImagePlaceHolderError {
    return Padding(
      padding: const EdgeInsets.all(32),
      child: SvgPicture.asset(
        'assets/images/icons/person.svg',
        color: widget.isBlackTheme != null
            ? Colors.white
            : CustomExtraThemeData.getThemeData(context).colorScheme.secondary,
        width: double.infinity,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    var menuProvider = authProvider.isLoggedIn
        ? Provider.of<MenuProvider>(context, listen: false)
        : null;
    return Container(
      child: Stack(
        children: [
          InkWell(
            onTap: () {
              if (authProvider.isLoggedIn) {
                Navigator.of(menuProvider!.navigatorKey.currentContext!)
                    .pushNamed(PhotoViewUserImage.routeName, arguments: {
                  'tag': menuProvider.tagProfileAnimationHero,
                  'imageUrl': menuProvider.user!.profile!.pictureUrl
                });
              }
            },
            child: Hero(
              tag: authProvider.isLoggedIn
                  ? menuProvider!.tagProfileAnimationHero
                  : '',
              transitionOnUserGestures: true,
              child: Container(
                  height: widget.expandedHeight * 0.75,
                  width: widget.expandedHeight * 0.75,
                  decoration: BoxDecoration(
                      color: CustomExtraThemeData.getExtraThemeData()
                          .userImageInputBackground,
                      //Here goes the same radius, u can put into a var or function
                      borderRadius: BorderRadius.all(Radius.circular(100)),
                      // border: Border.all(
                      //     color: Theme.of(context).colorScheme.secondary,
                      //     width: 2.sp),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.white24,
                            // spreadRadius: 2,
                            blurRadius: 10,
                            offset: Offset.zero),
                      ]),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: _storedImage != null
                        ? Image.file(_storedImage!, fit: BoxFit.cover)
                        : widget.initValue != null
                            ? CachedNetworkImage(
                                imageUrl: widget.initValue!,
                                fit: BoxFit.cover,
                              )
                            : _buildImagePlaceHolderError,
                  )),
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            width: 32.sp,
            height: 32.sp,
            child: Container(
              width: 32.sp,
              height: 32.sp,
              decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(
                    color: Theme.of(context).colorScheme.secondary,
                    width: 2.sp,
                  )),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: IconButton(
                    onPressed: () {
                      showChoicesImage(context).then((value) {
                        if (widget.onChange != null) {
                          widget.onChange!(_storedImage);
                        }
                      });
                    },
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.camera_alt_rounded,
                      size: 14.sp,
                      color: Theme.of(context).colorScheme.secondary,
                    )),
              ),
            ),
          )
        ],
      ),
    );
  }
}
