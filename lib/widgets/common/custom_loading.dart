import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/app/app_provider.dart';
import 'package:provider/provider.dart';

class CustomLoading extends StatefulWidget {
  const CustomLoading({Key? key}) : super(key: key);

  @override
  _CustomLoadingState createState() => _CustomLoadingState();
}

class _CustomLoadingState extends State<CustomLoading>
    with TickerProviderStateMixin {
  final Tween<double> _tween = Tween(begin: 0.75, end: 1.5);
  late final AnimationController _scaleController;
  late final AnimationController _fadeController;
  late Timer _timer;
  bool _reverse = true;

  @override
  initState() {
    super.initState();
    _initAnimations();
    _setIntervalScaleAnimation();
  }

  @override
  void dispose() {
    _scaleController.dispose();
    _fadeController.dispose();
    _timer.cancel();
    super.dispose();
  }

  _initAnimations() {
    _fadeController = AnimationController(
      duration: const Duration(seconds: 900),
      vsync: this,
    )..repeat(reverse: true);
    _scaleController = AnimationController(
      duration: const Duration(milliseconds: 900),
      vsync: this,
    )..repeat(reverse: true);
  }

  _setIntervalScaleAnimation() {
    _timer = Timer.periodic(const Duration(milliseconds: 800), (timer) {
      if (_reverse) {
        _scaleController.reverse();
      } else {
        _scaleController.forward();
      }
      setState(() {
        _reverse = !_reverse;
      });
    });
  }

  _getImageLoading(infoBOX) {
    return infoBOX != null && infoBOX is! String
        ? NetworkImage('${Settings.apiURLUploads}/${infoBOX.logoLoading!}')
        : const AssetImage('assets/images/logo_loading.png');
  }

  @override
  Widget build(BuildContext context) {
    dynamic infoBOX;
    try {
      infoBOX = Provider.of<AppProvider>(appNavigatorKey.currentContext!, listen: false).infoBOX;
    } catch (error) {
      debugPrint('Error fetching loading logo, user is not logged in: ' + error.toString());
      infoBOX = 'assets/images/logo_loading.png';
    }
    return Center(
      child: FadeTransition(
        opacity: CurvedAnimation(
          parent: _scaleController,
          curve: Curves.linear,
        ),
        child: ScaleTransition(
          scale: _tween.animate(
              CurvedAnimation(parent: _scaleController, curve: Curves.linear)),
          child: Container(
              width: 175.sp,
              height: 175.sp,
              padding: EdgeInsets.zero,
              decoration: const BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 10,
                      color: Colors.black45,
                      offset: Offset(0, 0),
                      spreadRadius: 10)
                ],
              ),
              child: FittedBox(
                  child: CircleAvatar(
                      radius: 30.0,
                      backgroundColor: Theme.of(context).primaryColor,
                      backgroundImage: _getImageLoading(infoBOX)))),
        ),
      ),
    );
  }
}
