import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/hex_color.dart';

class CustomElevatedButton extends StatelessWidget {
  final String text;
  final Color backgroundColor;
  final Color textColor;
  final Function pressEvent;
  final Color? borderColor;
  final double? borderWidth;
  final bool? withoutMarginTop;
  final double? fontSize;
  final double? width;
  final double? height;
  final bool? isDisabled;
  final bool? adjustoFit;
  final EdgeInsets? contentPadding;

  const CustomElevatedButton(
      {Key? key,
      required this.text,
      required this.backgroundColor,
      required this.textColor,
      required this.pressEvent,
      this.borderColor,
      this.withoutMarginTop,
      this.borderWidth,
      this.fontSize,
      this.width,
      this.height,
      this.adjustoFit,
      this.contentPadding,
      this.isDisabled = false})
      : super(key: key);

  double? get _width {
    return this.adjustoFit != null
        ? null
        : this.width != null
            ? ScreenUtil().setSp(this.width!)
            : double.infinity;
  }

  double? get _height {
    return this.adjustoFit != null
        ? null
        : this.height != null
            ? ScreenUtil().setSp(this.height!)
            : 50.sp;
  }

  EdgeInsets get _margin {
    return EdgeInsets.only(top: withoutMarginTop != null ? 0 : 14.sp);
  }

  EdgeInsets get _padding {
    return this.contentPadding ?? EdgeInsets.zero;
  }

  double get _fontSize {
    return fontSize != null ? ScreenUtil().setSp(fontSize!) : 24.sp;
  }

  BorderSide get _border {
    return borderColor != null
        ? BorderSide(
            color: borderColor!, width: ScreenUtil().setSp(borderWidth!))
        : BorderSide.none;
  }

  VisualDensity get _density {
    return adjustoFit != null ? VisualDensity.compact : VisualDensity.standard;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _width,
      height: _height,
      margin: _margin,
      padding: _padding,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: HexColor('#00000033'),
            blurRadius: 4,
            offset: const Offset(4, 4),
            spreadRadius: 4,
          ),
        ],
      ),
      child: ElevatedButton(
          child: Text(
            text,
            style: TextStyle(
                fontSize: _fontSize, fontFamily: 'Oswald', color: textColor, height: 1.3),
          ),
          style: ElevatedButton.styleFrom(
            visualDensity: _density,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
              side: _border,
            ),
            padding:
                EdgeInsets.symmetric(vertical: 4.sp, horizontal: 8.sp),
            primary: backgroundColor,
          ),
          onPressed: isDisabled!
              ? null
              : () {
                  pressEvent();
                }),
    );
  }
}
