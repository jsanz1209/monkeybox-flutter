import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:monkeybox/models/common/bottom_sheet.dart' as bottom_model;
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/custom_bottom_sheet.dart';
import 'package:provider/provider.dart';

import 'buttons/custom_elevated_button.dart';

class InputImage extends StatefulWidget {
  final GlobalKey<FormFieldState>? formKey;
  final Function(File?)? onSave;
  final String? Function(String?)? validator;
  final String textButton;
  final bool? isBlackTheme;
  final double? fontSize;
  final String? initValue;
  final Function(File?)? onChange;

  const InputImage(
      {Key? key,
      required this.formKey,
      required this.onSave,
      required this.textButton,
      required this.validator,
      this.isBlackTheme,
      this.fontSize,
      this.initValue,
      this.onChange})
      : super(key: key);

  @override
  _InputImageState createState() => _InputImageState();
}

class _InputImageState extends State<InputImage> {
  File? _storedImage;
  final ImagePicker _picker = ImagePicker();

  Color get themeColor {
    return widget.isBlackTheme != null
        ? Theme.of(context).primaryColor
        : Colors.white;
  }

  Future<void> _takePicture(ImageSource option) async {
    try {
      final pickedFile =
          await _picker.pickImage(source: option, imageQuality: 10);
      if (pickedFile != null) {
        setState(() {
          _storedImage = File(pickedFile.path);
        });
        // final appDir = await getApplicationDocumentsDirectory();
        // final fileName = basename(pickedFile.path);
        // final savedImage = _storedImage!.copySync('${appDir.path}/$fileName');
      } else {
        debugPrint('no image selected');
      }
    } catch (error) {
      debugPrint(error.toString());
    } finally {}
  }

  Future showChoicesImage(BuildContext context) async {
    var result = await showModalBottomSheet(
        context: context,
        builder: (context) => CustomBottomSheet(
                selectEvent: (ImageSource option) async {},
                model: [
                  bottom_model.BottomSheet(
                      title: 'Cámara',
                      subtitle: 'Hazte una foto',
                      icon: Icons.camera_alt,
                      value: ImageSource.camera),
                  bottom_model.BottomSheet(
                      title: 'Galería',
                      subtitle: 'Escoge una foto de tu galería',
                      icon: Icons.photo_album,
                      value: ImageSource.gallery)
                ]));

    if (result != null) {
      return _takePicture(result);
    } else {
      return Future<bool>.value(false);
    }
  }

  Padding get _buildImagePlaceHolderError {
    return Padding(
      padding: const EdgeInsets.all(32),
      child: SvgPicture.asset(
        'assets/images/icons/person.svg',
        color: Colors.white,
        width: double.infinity,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FormField<String>(
      key: widget.formKey,
      validator: widget.validator,
      initialValue: widget.initValue,
      onSaved: (value) =>
          widget.onSave != null ? widget.onSave!(_storedImage) : null,
      builder: (FormFieldState<String> state) => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.all(_storedImage != null ? 8 : 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                    flex: 1,
                    child: _storedImage == null
                        ? widget.initValue != null
                            ? Hero(
                                tag: Provider.of<MenuProvider>(context,
                                        listen: false)
                                    .tagProfileAnimationHero,
                                transitionOnUserGestures: true,
                                child: Container(
                                    height: 100.sp,
                                    width: 100.sp,
                                    decoration: BoxDecoration(
                                        //Here goes the same radius, u can put into a var or function
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(100)),
                                        border: Border.all(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 1),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              spreadRadius: 3,
                                              blurRadius: 4,
                                              offset: Offset.zero),
                                        ]),
                                    child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        child: CachedNetworkImage(
                                          imageUrl: widget.initValue!,
                                          fit: BoxFit.cover,
                                          placeholder: (ctx, _) =>
                                              _buildImagePlaceHolderError,
                                          alignment: Alignment.center,
                                          errorWidget: (ctx, error, _) {
                                            return _buildImagePlaceHolderError;
                                          },
                                        ))),
                              )
                            : IconButton(
                                padding: EdgeInsets.zero,
                                // icon: const Icon(Icons.person, color: Colors.white, size: 64),
                                icon: SvgPicture.asset(
                                  'assets/images/icons/person.svg',
                                  color: themeColor,
                                ),
                                iconSize: 64.sp,
                                onPressed: null)
                        : Container(
                            height: 100.sp,
                            width: 100.sp,
                            decoration: BoxDecoration(
                                //Here goes the same radius, u can put into a var or function
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100)),
                                border: Border.all(
                                    color:
                                        Theme.of(context).colorScheme.secondary,
                                    width: 2.sp),
                                boxShadow: [
                                  BoxShadow(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                      spreadRadius: 4,
                                      blurRadius: 5,
                                      offset: Offset.zero),
                                ]),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: Image.file(_storedImage!,
                                    fit: BoxFit.cover)))),
                Flexible(
                  flex: 2,
                  child: Container(
                    margin: widget.initValue != null || _storedImage != null
                        ? const EdgeInsets.only(left: 18)
                        : EdgeInsets.zero,
                    child: CustomElevatedButton(
                        withoutMarginTop: true,
                        text: widget.textButton,
                        backgroundColor: themeColor,
                        fontSize: widget.fontSize != null
                            ? ScreenUtil().setSp(widget.fontSize!)
                            : null,
                        textColor: widget.isBlackTheme != null
                            ? Colors.white
                            : Theme.of(context).primaryColor,
                        pressEvent: () {
                          showChoicesImage(context).then((value) {
                            if (widget.initValue != null) {
                              state.didChange(widget.initValue);
                            } else if (_storedImage == null) {
                              state.didChange(null);
                            } else {
                              List<int> imageBytes =
                                  _storedImage!.readAsBytesSync();
                              String base64Image = base64Encode(imageBytes);
                              state.didChange(base64Image);
                            }
                            if (widget.onChange != null) {
                              widget.onChange!(_storedImage);
                            }
                            state.validate();
                          });
                        }),
                  ),
                )
              ],
            ),
          ),
          state.hasError
              ? SizedBox(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5, top: 12, bottom: 0),
                    child: Text(
                      state.errorText!,
                      style: TextStyle(
                          fontFamily: 'Oswald',
                          fontSize: 14.sp,
                          overflow: TextOverflow.visible,
                          color: Theme.of(context).errorColor),
                      textAlign: TextAlign.left,
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
