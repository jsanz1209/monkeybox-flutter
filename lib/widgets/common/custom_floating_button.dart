import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';

class CustomFloatingButton extends StatefulWidget {
  final ScrollController scrollControllerMain;
  final bool? isWhiteTheme;
  final Color? background;
  CustomFloatingButton(
      {Key? key,
      required this.scrollControllerMain,
      this.isWhiteTheme,
      this.background})
      : super(key: key);

  @override
  _CustomFloatingButtonState createState() => _CustomFloatingButtonState();
}

class _CustomFloatingButtonState extends State<CustomFloatingButton> {
  bool _showBackToTopButton = false;

  @override
  void initState() {
    super.initState();
    widget.scrollControllerMain.addListener(() {
      setState(() {
        if (widget.scrollControllerMain.offset >= 400) {
          _showBackToTopButton = true; // show the back-to-top button
        } else {
          _showBackToTopButton = false; // hide the back-to-top button
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedScale(
        duration: const Duration(milliseconds: 300),
        scale: _showBackToTopButton ? 0.75 : 0,
        child: FloatingActionButton(
          backgroundColor: widget.isWhiteTheme != null
              ? Colors.white
              : widget.background ??
                  CustomExtraThemeData.getExtraThemeData().fabBackground,
          onPressed: _scrollToTop,
          child: Icon(Icons.arrow_upward_rounded,
              color: widget.isWhiteTheme != null
                  ? CustomExtraThemeData.getExtraThemeData().fabBackground
                  : Colors.white),
        ));
  }

  // This function is triggered when the user presses the back-to-top button
  void _scrollToTop() {
    widget.scrollControllerMain.animateTo(0,
        duration: Duration(milliseconds: 700), curve: Curves.ease);
  }
}
