import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'dart:ui' as ui;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:monkeybox/pages/app.dart';

class DatePickerWidget extends StatefulWidget {
  final Function onSelect;
  final DateTime currentDateTime;
  final Color? buttonsColor;
  final Color? labelColor;
  DatePickerWidget(this.currentDateTime, this.onSelect, this.buttonsColor, this.labelColor,{Key? key})
      : super(key: key);

  @override
  _DatePickerWidgetState createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('${getPrefix()} ${getFormattedDate()}'.toUpperCase(),
                style: Theme.of(context)
                    .textTheme
                    .headline3!
                    .copyWith(color: Colors.black)),
            SizedBox(
              height: 10.sp,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton.icon(
                  onPressed: () {
                    widget.onSelect(
                        widget.currentDateTime.subtract(Duration(days: 1)));
                  },
                  icon: Icon(Icons.arrow_left, color: widget.labelColor,),
                  label: Text(
                    getPrevDate(),
                    style: Theme.of(context)
                        .textTheme
                        .headline3!.copyWith(color: widget.labelColor),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: widget.buttonsColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          bottomLeft: Radius.circular(25),
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                      ),
                      padding: EdgeInsets.only(right: 16.sp, left: 4.sp),
                      minimumSize: Size(80.sp, 45.sp)),
                ),
                ElevatedButton(
                  onPressed: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,                        
                        onChanged: (date) {}, onConfirm: (date) {
                      widget.onSelect(date);
                    }, currentTime: widget.currentDateTime, locale: LocaleType.es);
                  },
                  child: Icon(Icons.event_rounded, size: 24.sp, color: widget.labelColor),
                  style: ElevatedButton.styleFrom(
                      primary: widget.buttonsColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                      ),
                      minimumSize: Size(25.sp, 45.sp)),
                ),
                Directionality(
                  textDirection: ui.TextDirection.rtl,
                  child: ElevatedButton.icon(
                    onPressed: () {
                      widget.onSelect(
                          widget.currentDateTime.add(Duration(days: 1)));
                    },
                    icon: Icon(Icons.arrow_left, color: widget.labelColor),
                    label: Text(
                      getNextDate(),
                      style: Theme.of(context)
                          .textTheme
                          .headline3!.copyWith(color: widget.labelColor),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: widget.buttonsColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            topRight: Radius.circular(25),
                            bottomRight: Radius.circular(25),
                          ),
                        ),
                        padding: EdgeInsets.only(left: 16.sp, right: 4.sp),
                        minimumSize: Size(80.sp, 45.sp)),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  getFormattedDate() {
    Locale myLocale = Localizations.localeOf(context);
    return DateFormat('EEEE d MMMM y', myLocale.languageCode)
        .format(widget.currentDateTime);
  }

  getPrefix() {
    DateTime today = DateTime.now();
    DateTime yesterday = today.subtract(Duration(days: 1));
    DateTime tomorrow = today.add(Duration(days: 1));
    if (DateFormat('dd/MM/yyyy').format(today) ==
        DateFormat('dd/MM/yyyy').format(widget.currentDateTime)) {
      return AppLocalizations.of(appNavigatorKey.currentContext!).today + ' -';
    } else if (DateFormat('dd/MM/yyyy').format(yesterday) ==
        DateFormat('dd/MM/yyyy').format(widget.currentDateTime)) {
      return AppLocalizations.of(appNavigatorKey.currentContext!).yesterday +
          ' -';
    } else if (DateFormat('dd/MM/yyyy').format(tomorrow) ==
        DateFormat('dd/MM/yyyy').format(widget.currentDateTime)) {
      return AppLocalizations.of(appNavigatorKey.currentContext!).tomorrow +
          ' -';
    }

    return '';
  }

  getPrevDate() {
    return DateFormat('dd/MM')
        .format(widget.currentDateTime.subtract(Duration(days: 1)));
  }

  getNextDate() {
    return DateFormat('dd/MM')
        .format(widget.currentDateTime.add(Duration(days: 1)));
  }
}
