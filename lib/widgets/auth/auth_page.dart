import 'package:flutter/material.dart';
import 'package:monkeybox/providers/auth/reset_password_provider.dart';
import 'package:monkeybox/providers/auth/signup_provider.dart';
import 'package:monkeybox/widgets/auth/auth_nested_routes.dart';
import 'package:provider/provider.dart';

class AuthPage extends StatelessWidget {
  static const routeName = 'auth';
  final String setupPageRoute;
  const AuthPage({Key? key, required this.setupPageRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('Auth build');
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => SignUpProvider()),
          ChangeNotifierProvider(create: (_) => ResetPasswordProvider()),
        ],
        child: Scaffold(
            body: AuthNestedRoutes(setupPageRoute: setupPageRoute)));
  }
}
