import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/auth/signup.dart';
import 'package:monkeybox/models/box/api/box.dart';
import 'package:monkeybox/models/common/item_dropdown.dart';
import 'package:monkeybox/models/common/item_radio_button.dart';
import 'package:monkeybox/models/wods/api/intensity_category.dart';
import 'package:monkeybox/providers/auth/signup_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:monkeybox/widgets/common/form/custom_checkbox.dart';
import 'package:monkeybox/widgets/common/form/custom_dropdown.dart';
import 'package:monkeybox/widgets/common/form/custom_radio_button.dart';
import 'package:monkeybox/widgets/common/form/custom_switch.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FormSignup extends StatefulWidget {
  final Function(Signup?) submitEvent;
  const FormSignup({Key? key, required this.submitEvent}) : super(key: key);

  @override
  State<FormSignup> createState() => _FormSignupState();
}

class _FormSignupState extends State<FormSignup> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields([
    'firstName',
    'firstSurname',
    'secondSurname',
    'email',
    'confirmEmail',
    'birthdate',
    'gender',
    'height',
    'weight',
    'category',
    'box',
    'instagram',
    'facebook',
    'twitter',
    'whatsapp',
    'password',
    'confirmPassword',
    'showFirstSurname',
    'showSecondSurname',
    'showBirthdate',
    'showGender',
    'showAge',
    'showWeight',
    'showHeight',
    'showCategory',
    'showInstagram',
    'showFacebook',
    'showTwitter',
    'showWhatsapp',
    'policy'
  ]);

  Signup _data = Signup();

  List<ItemDropdown>? _intensityData;
  List<ItemDropdown>? _boxData;

  late Future<void> _futureFetchData;

  int _boxSelected = -1;

  void showInfoBOXModal(String title, String? html) {
    if (_boxSelected != -1) {
      showDialog(
          useSafeArea: false,
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.zero,
              child: Scaffold(
                appBar: CustomHeader(title: title, withBackButton: true),
                body: SingleChildScrollView(
                  child: Container(child: Html(data: html)),
                ),
              ),
            );
          });
    } else {
      SnackBarHelper.showSnackBar(
          AppLocalizations.of(context).box,
          AppLocalizations.of(context).youMustSelectABox,
          TypeSnackBarEnum.info);
    }
  }

  Widget get _widgetPolicyMonkey {
    return Text.rich(
        TextSpan(
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(color: Colors.white, letterSpacing: 0, height: 1.45.sp),
          text: AppLocalizations.of(context).accept,
          children: [
            TextSpan(
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    var infoBOX =
                        Provider.of<SignUpProvider>(context, listen: false)
                            .infoBOX;
                    showInfoBOXModal(AppLocalizations.of(context).privacyPolicy,
                        infoBOX != null ? infoBOX.privacyPolicy : '');
                  },
                text: AppLocalizations.of(context).thePrivacyPolicy,
                style: const TextStyle(
                  decoration: TextDecoration.underline,
                )),
             TextSpan(text: AppLocalizations.of(context).and),
            TextSpan(
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    var infoBOX =
                        Provider.of<SignUpProvider>(context, listen: false)
                            .infoBOX;
                    showInfoBOXModal(AppLocalizations.of(context).boxRules,
                        infoBOX != null ? infoBOX.normative : '');
                  },
                text: AppLocalizations.of(context).theBoxRules,
                style: const TextStyle(
                  decoration: TextDecoration.underline,
                )),
          ],
        ),
        style: Theme.of(context)
            .textTheme
            .bodyText2!
            .copyWith(color: Colors.white));
  }

  void _onSubmit() {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      debugPrint('is valid');
      _formKey.currentState!.save();
      widget.submitEvent(_data);
    } else {
      debugPrint('is invalid');
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            curve: Curves.easeInOut);
      }
      widget.submitEvent(null);
    }
  }

  Widget get _buildFormWidget {
    var focusNode = FocusScopeNode();
    return SingleChildScrollView(
      padding: const EdgeInsets.only(bottom: 40),
      child: FocusScope(
        node: focusNode,
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CustomTextFormField(
                  formKey: _fields['firstName'],
                  label: AppLocalizations.of(context).firstName,
                  fieldName: 'firstName',
                  focusNode: focusNode,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.text,
                  onSave: (value) {
                    _data = _data.copyWith(name: value);
                  },
                  validator: (value) => FormValidators.validatorRequired(
                      value, AppLocalizations.of(context).firstNameMandatory)),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                  formKey: _fields['firstSurname'],
                  label: AppLocalizations.of(context).firstSurname,
                  textInputAction: TextInputAction.next,
                  fieldName: 'firstSurname',
                  keyboardType: TextInputType.text,
                  focusNode: focusNode,
                  onSave: (value) {
                    _data = _data.copyWith(firstSurname: value);
                  },
                  validator: null),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                  formKey: _fields['secondSurname'],
                  label:AppLocalizations.of(context).secondSurname,
                  textInputAction: TextInputAction.next,
                  fieldName: 'secondSurname',
                  focusNode: focusNode,
                  keyboardType: TextInputType.text,
                  onSave: (value) {
                    _data = _data.copyWith(secondSurname: value);
                  },
                  validator: null),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                formKey: _fields['email'],
                label: AppLocalizations.of(context).email,
                fieldName: 'email',
                textInputAction: TextInputAction.next,
                focusNode: focusNode,
                keyboardType: TextInputType.emailAddress,
                onSave: (value) {
                  _data = _data.copyWith(email: value);
                },
                validator: (value) {
                  _fields['confirmEmail']!.currentState!.validate();
                  return FormValidators.validatorEmail(value);
                },
              ),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                  formKey: _fields['confirmEmail'],
                  label: AppLocalizations.of(context).confirmEmail,
                  fieldName: 'confirmEmail',
                  textInputAction: TextInputAction.done,
                  focusNode: focusNode,
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    final isValidEmail = FormValidators.validatorEmail(value);
                    if (isValidEmail == null) {
                      return FormValidators.validatorMatch(
                          value,
                          _fields['email']!.currentState!.value,
                          AppLocalizations.of(context).emailFieldsNoMatch);
                    }
                    return isValidEmail;
                  }),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                formKey: _fields['birthdate'],
                isTypeDate: true,
                label: AppLocalizations.of(context).birthdate,
                fieldName: 'birthdate',
                focusNode: focusNode,
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.datetime,
                onSave: (value) {
                  _data = _data.copyWith(birthdate: value);
                },
                validator: (value) => FormValidators.validatorRequired(
                    value, AppLocalizations.of(context).birthdateMandatory),
              ),
              SizedBox(height: 30.sp),
              SizedBox(
                height: 20.sp,
                child: Text(
                  AppLocalizations.of(context).gender,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(color: Colors.white),
                  textAlign: TextAlign.left,
                ),
              ),
              Transform.translate(
                  offset: const Offset(-5, 0),
                  child: CustomRadioButton(
                      formKey: _fields['gender'],
                      fieldName: 'gender',
                      labels: [
                        ItemRadioButton('M', AppLocalizations.of(context).man),
                        ItemRadioButton('F', AppLocalizations.of(context).woman)
                      ],
                      onSave: (value) {
                        _data = _data.copyWith(gender: value);
                      },
                      validator: (value) => FormValidators.validatorRequired(
                          value, AppLocalizations.of(context).genderMandatory))),
              SizedBox(height: 15.sp),
              CustomTextFormField(
                formKey: _fields['height'],
                label: AppLocalizations.of(context).height,
                fieldName: 'height',
                suffixText: 'm',
                textInputAction: TextInputAction.next,
                focusNode: focusNode,
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                inputFormatters: [
                  MaskTextInputFormatter(
                      mask: 'S,##',
                      filter: {'S': RegExp(r'[1-2]'), '#': RegExp(r'[0-9]')})
                ],
                onSave: (value) {
                  _data = _data.copyWith(height: value);
                },
                validator: (value) => FormValidators.validatorRequired(
                    value, AppLocalizations.of(context).heightMandatory),
              ),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                  formKey: _fields['weight'],
                  label: AppLocalizations.of(context).bodyWeight,
                  fieldName: 'weight',
                  suffixText: 'kg',
                  textInputAction: TextInputAction.next,
                  focusNode: focusNode,
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  onSave: (value) {
                    _data = _data.copyWith(weight: value);
                  }),
              SizedBox(height: 30.sp),
              CustomDropdown(
                formKey: _fields['category'],
                label: AppLocalizations.of(context).favoriteCategory,
                fieldName: 'category',
                options: _intensityData != null ? _intensityData! : [],
                onSave: (value) {
                  _data = _data.copyWith(category: value);
                },
                validator: (value) {
                  return FormValidators.validatorRequired(
                      value, AppLocalizations.of(context).favoriteCategoryMandatory);
                },
              ),
              SizedBox(height: 30.sp),
              CustomDropdown(
                formKey: _fields['box'],
                label: AppLocalizations.of(context).box,
                fieldName: 'box',
                options: _boxData != null ? _boxData! : [],
                onChange: (value) async {
                  _boxSelected = value!;
                  await Provider.of<SignUpProvider>(context, listen: false)
                      .getInfoBox(_boxSelected);
                },
                onSave: (value) {
                  _data = _data.copyWith(box: value);
                },
                validator: (value) => FormValidators.validatorRequired(
                    value, AppLocalizations.of(context).boxMandatory),
              ),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                  formKey: _fields['instagram'],
                  label: 'Instagram',
                  fieldName: 'instagram',
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.text,
                  focusNode: focusNode,
                  suffixWidget: SvgPicture.asset(
                    'assets/images/icons/instagram.svg',
                    color: Colors.white,
                    height: 24.sp,
                    width: 24.sp,
                  ),
                  suffixWidgetPlaceHolder: SvgPicture.asset(
                    'assets/images/icons/instagram.svg',
                    color: Colors.grey,
                    height: 24.sp,
                    width: 24.sp,
                  ),
                  onSave: (value) {
                    _data = _data.copyWith(instagram: value);
                  }),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                formKey: _fields['facebook'],
                label: 'Facebook',
                fieldName: 'facebook',
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.text,
                focusNode: focusNode,
                suffixWidget: SvgPicture.asset(
                  'assets/images/icons/facebook.svg',
                  color: Colors.white,
                  height: 24.sp,
                  width: 24.sp,
                ),
                suffixWidgetPlaceHolder: SvgPicture.asset(
                  'assets/images/icons/facebook.svg',
                  color: Colors.grey,
                  height: 24.sp,
                  width: 24.sp,
                ),
                onSave: (value) {
                  _data = _data.copyWith(facebook: value);
                },
              ),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                formKey: _fields['twitter'],
                label: 'Twitter',
                fieldName: 'twitter',
                textInputAction: TextInputAction.next,
                focusNode: focusNode,
                keyboardType: TextInputType.text,
                suffixWidget: SvgPicture.asset(
                  'assets/images/icons/twitter.svg',
                  color: Colors.white,
                  height: 24.sp,
                  width: 24.sp,
                ),
                suffixWidgetPlaceHolder: SvgPicture.asset(
                  'assets/images/icons/twitter.svg',
                  color: Colors.grey,
                  height: 24.sp,
                  width: 24.sp,
                ),
                onSave: (value) {
                  _data = _data.copyWith(twitter: value);
                },
              ),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                formKey: _fields['whatsapp'],
                label: 'Whatsapp',
                fieldName: 'whatsapp',
                textInputAction: TextInputAction.next,
                focusNode: focusNode,
                keyboardType: TextInputType.text,
                suffixWidget: SvgPicture.asset(
                  'assets/images/icons/whatsapp.svg',
                  color: Colors.white,
                  height: 24.sp,
                  width: 24.sp,
                ),
                suffixWidgetPlaceHolder: SvgPicture.asset(
                  'assets/images/icons/whatsapp.svg',
                  color: Colors.grey,
                  height: 24.sp,
                  width: 24.sp,
                ),
                onSave: (value) {
                  _data = _data.copyWith(whatsapp: value);
                },
              ),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                formKey: _fields['password'],
                label: AppLocalizations.of(context).definePassword,
                fieldName: 'password',
                focusNode: focusNode,
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.text,
                isPassword: true,
                validator: (value) {
                  _fields['confirmPassword']!.currentState!.validate();
                  return FormValidators.validatorPassword(value);
                },
                onSave: (value) {
                  _data = _data.copyWith(password: value);
                },
              ),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                formKey: _fields['confirmPassword'],
                label: AppLocalizations.of(context).confirmPassword,
                fieldName: 'confirmPassword',
                isPassword: true,
                focusNode: focusNode,
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.text,
                validator: (value) {
                  final isValidPassword =
                      FormValidators.validatorPassword(value);
                  if (isValidPassword == null) {
                    return FormValidators.validatorMatch(
                        value,
                        _fields['password']!.currentState!.value,
                        AppLocalizations.of(context).passwordFieldsNoMatch);
                  }
                  return isValidPassword;
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 44, bottom: 0),
                child: Text(AppLocalizations.of(context).athelteProfilePrivacy,
                    style: Theme.of(context).textTheme.headline1!.copyWith(color: Colors.white)),
              ),
              CustomSwitch(
                formKey: _fields['showFirstSurname'],
                label: AppLocalizations.of(context).displayFirstSurname,
                onSave: (value) {
                  _data = _data.copyWith(showFirstSurname: value);
                },
              ),
              CustomSwitch(
                formKey: _fields['showSecondSurname'],
                label: AppLocalizations.of(context).displaySecondSurname,
                onSave: (value) {
                  _data = _data.copyWith(showSecondSurname: value);
                },
              ),
              CustomSwitch(
                formKey: _fields['showBirthdate'],
                label: AppLocalizations.of(context).displayBirthdate,
                onSave: (value) {
                  _data = _data.copyWith(showBirthdate: value);
                },
              ),
              CustomSwitch(
                  formKey: _fields['showGender'],
                  label: AppLocalizations.of(context).displayGender,
                  onSave: (value) {
                    _data = _data.copyWith(showGender: value);
                  }),
              CustomSwitch(
                  formKey: _fields['showAge'],
                  label: AppLocalizations.of(context).displayAge,
                  onSave: (value) {
                    _data = _data.copyWith(showAge: value);
                  }),
              CustomSwitch(
                  formKey: _fields['showWeight'],
                  label: AppLocalizations.of(context).displayWeight,
                  onSave: (value) {
                    _data = _data.copyWith(showWeight: value);
                  }),
              CustomSwitch(
                  formKey: _fields['showHeight'],
                  label: AppLocalizations.of(context).displayHeight,
                  onSave: (value) {
                    _data = _data.copyWith(showHeight: value);
                  }),
              CustomSwitch(
                formKey: _fields['showCategory'],
                label: AppLocalizations.of(context).displayFavoriteCategory,
                onSave: (value) {
                  _data = _data.copyWith(showCategory: value);
                },
              ),
              CustomSwitch(
                  formKey: _fields['showInstagram'],
                  label: AppLocalizations.of(context).displayInstagram,
                  onSave: (value) {
                    _data = _data.copyWith(showInstagram: value);
                  }),
              CustomSwitch(
                  formKey: _fields['showFacebook'],
                  label: AppLocalizations.of(context).displayFacebook,
                  onSave: (value) {
                    _data = _data.copyWith(showFacebook: value);
                  }),
              CustomSwitch(
                  formKey: _fields['showTwitter'],
                  label: AppLocalizations.of(context).displayTwitter,
                  onSave: (value) {
                    _data = _data.copyWith(showTwitter: value);
                  }),
              CustomSwitch(
                  formKey: _fields['showWhatsapp'],
                  label: AppLocalizations.of(context).displayWhatsapp,
                  onSave: (value) {
                    _data = _data.copyWith(showWhatsapp: value);
                  }),
              SizedBox(height: 30.sp),
              CustomCheckbox(
                textWiget: _widgetPolicyMonkey,
                formKey: _fields['policy'],
                validator: (value) => FormValidators.validatorTrueRequired(
                    value,
                    AppLocalizations.of(context).acceptPrivacyPolicyAndBoxRules),
              ),
              SizedBox(height: 19.5.sp),
              CustomElevatedButton(
                  text: AppLocalizations.of(context).register,
                  backgroundColor: Colors.white,
                  textColor: CustomExtraThemeData.getChinchillaTheme().colorScheme.primary,
                  pressEvent: _onSubmit)
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _futureFetchData =
        Provider.of<SignUpProvider>(context, listen: false).fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _futureFetchData,
      builder: (ctx, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var listData = snapshot.data as List<List<dynamic>>;
          _intensityData = (listData[0] as List<IntensityCategory>).map((item) {
            return ItemDropdown(item.id, item.category);
          }).toList();
          _boxData = (listData[1] as List<Box>).map((item) {
            return ItemDropdown(item.id, item.name);
          }).toList();
        }
        return _buildFormWidget;
      },
    );
  }
}
