import 'dart:io';

import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/common/user_image_sliver_app_bar.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HeaderSignup extends StatefulWidget {
    final Function(File?) onChange;

  const HeaderSignup({Key? key, required this.onChange}) : super(key: key);

  @override
  State<HeaderSignup> createState() => _HeaderSignupState();
}

class _HeaderSignupState extends State<HeaderSignup> {

  File? _imageFile;

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      delegate: UserImageSliverAppBar(
          title: AppLocalizations.of(context).signUpAt,
          imageUrl: null,
          imageFile: _imageFile,
          withBackButton: true,
          onChange: (value) {
            setState(() {
              _imageFile = value;
            });
            widget.onChange(value);
          }),
      pinned: true,
      // floating: true,
    );
  }
}
