import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/pages/auth/reset_password_page.dart';
import 'package:monkeybox/pages/auth/signup_page.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FormLogin extends StatelessWidget {
  final Function(String, String) submitEvent;
  FormLogin({Key? key, required this.submitEvent}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields(['email', 'password']);

  final _data = <String, String>{};

  void _onSubmit() {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      _formKey.currentState!.save();
      debugPrint('is valid');
      submitEvent(_data['email']!, _data['password']!);
    } else {
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            curve: Curves.easeInOut);
      }
      debugPrint('is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    var focusNode = FocusScopeNode();
    return FocusScope(
      node: focusNode,
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            CustomTextFormField(
                formKey: _fields['email'],
                label: AppLocalizations.of(context).email,
                fieldName: 'email',
                focusNode: focusNode,
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                onSave: (value) {
                  _data['email'] = value!;
                },
                // data: _data,
                validator: FormValidators.validatorEmail),
            SizedBox(height: 19.5.sp),
            CustomTextFormField(
                formKey: _fields['password'],
                label: AppLocalizations.of(context).password,
                fieldName: 'password',
                isPassword: true,
                focusNode: focusNode,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.done,
                onFieldSubmitted: (_) {
                  _onSubmit();
                },
                onSave: (value) {
                  _data['password'] = value!;
                },
                validator: FormValidators.validatorPassword),
            TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(ResetPasswordPage.routeName);
                },
                child: Align(
                  alignment: Alignment.centerLeft,
                  child:
                      Text(AppLocalizations.of(context).forgotYourPassword, textAlign: TextAlign.left),
                ),
                style: Theme.of(context).textButtonTheme.style!.copyWith(
                    foregroundColor: MaterialStateProperty.resolveWith(
                        (_) => Colors.white))),
            CustomElevatedButton(
                text: AppLocalizations.of(context).login,
                backgroundColor: Colors.white,
                textColor: CustomExtraThemeData.getChinchillaTheme().colorScheme.primary,
                pressEvent: _onSubmit),
            CustomElevatedButton(
                text: AppLocalizations.of(context).signup,
                backgroundColor: CustomExtraThemeData.getChinchillaTheme().colorScheme.primary,
                textColor: Colors.white,
                borderColor: Colors.white,
                borderWidth: 2.sp,
                pressEvent: () {
                  var navigatorKey =
                      Provider.of<AuthProvider>(context, listen: false)
                          .navigatorKey;
                  Navigator.of(navigatorKey.currentContext!)
                      .pushNamed(SignUpPage.routeName);
                }),
          ],
        ),
      ),
    );
  }
}
