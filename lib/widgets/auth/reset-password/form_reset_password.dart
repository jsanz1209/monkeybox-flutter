import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';

class FormResetPassword extends StatelessWidget {
  
  final Function(String) submitEvent;
  FormResetPassword({Key? key, required this.submitEvent}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields(['email']);

  final _data = <String, String>{};

  void _onSubmit() {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      _formKey.currentState!.save();
      debugPrint('is valid');
      submitEvent(_data['email']!);
    } else {
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            curve: Curves.easeInOut);
      }
      debugPrint('is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
              formKey: _fields['email'],
              label: 'Email',
              fieldName: 'email',
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              isBlackTheme: true,
              onSave: (value) {
                _data['email'] = value!;
              },
              validator: FormValidators.validatorEmail),
          SizedBox(
            height: 12.sp,
          ),
          CustomElevatedButton(
              text: 'ENVIAR',
              backgroundColor: Theme.of(context).primaryColor,
              textColor: Colors.white,
              pressEvent: _onSubmit),
        ],
      ),
    );
  }
}
