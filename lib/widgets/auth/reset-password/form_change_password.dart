import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';

class FormChangePassword extends StatelessWidget {
  final Function(String) submitEvent;
  FormChangePassword({Key? key, required this.submitEvent}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields(['newPassword', 'confirmPassword']);

  final _data = <String, String>{};

  void _onSubmit() {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      _formKey.currentState!.save();
      debugPrint('is valid');
      submitEvent(_data['newPassword']!);
    } else {
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            curve: Curves.easeInOut);
      }
      debugPrint('is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    var focusNode = FocusScopeNode();
    return FocusScope(
        node: focusNode,
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              CustomTextFormField(
                formKey: _fields['newPassword'],
                label: 'Nueva contraseña',
                fieldName: 'newPassword',
                isPassword: true,
                isBlackTheme: true,
                focusNode: focusNode,
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.text,
                onSave: (value) {
                  _data['newPassword'] = value!;
                },
                validator: (value) {
                  _fields['confirmPassword']!.currentState!.validate();
                  return FormValidators.validatorPassword(value);
                },
              ),
              SizedBox(height: 30.sp),
              CustomTextFormField(
                formKey: _fields['confirmPassword'],
                label: 'Confirmar contraseña',
                fieldName: 'confirmPassword',
                isPassword: true,
                isBlackTheme: true,
                focusNode: focusNode,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.text,
                validator: (value) {
                  final isValidPassword =
                      FormValidators.validatorPassword(value);
                  if (isValidPassword == null) {
                    return FormValidators.validatorMatch(
                        value,
                        _fields['newPassword']!.currentState!.value,
                        'Los campos contraseña no coincide');
                  }
                  return isValidPassword;
                },
              ),
              SizedBox(
                height: 12.sp,
              ),
              CustomElevatedButton(
                  text: 'CAMBIAR',
                  backgroundColor: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  pressEvent: _onSubmit),
            ],
          ),
        ));
  }
}
