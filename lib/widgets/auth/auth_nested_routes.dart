import 'package:flutter/material.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/pages/auth/change_password_page.dart';
import 'package:monkeybox/pages/auth/login_page.dart';
import 'package:monkeybox/pages/auth/reset_password_page.dart';
import 'package:monkeybox/pages/auth/signup_page.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:provider/provider.dart';

class AuthNestedRoutes extends StatefulWidget {
  final String setupPageRoute;

  const AuthNestedRoutes({Key? key, required this.setupPageRoute})
      : super(key: key);

  @override
  _AuthNestedRoutesState createState() => _AuthNestedRoutesState();
}

class _AuthNestedRoutesState extends State<AuthNestedRoutes> {
  @override
  Widget build(BuildContext context) {
    var navigatorAuthKey = Provider.of<AuthProvider>(
            appNavigatorKey.currentContext!,
            listen: false)
        .navigatorKey;
    return WillPopScope(
      child: Navigator(
        key:
            navigatorAuthKey, //(*) Uncomment if we will want to navigate from this navigator key
        initialRoute: widget.setupPageRoute.isEmpty
            ? LoginPage.routeName
            : widget.setupPageRoute,
        onGenerateRoute: _onGenerateRoute,
      ),
      onWillPop: () {
        if (navigatorAuthKey.currentState!.canPop()) {
          navigatorAuthKey.currentState!.pop();
          return Future<bool>.value(false);
        }
        return Future<bool>.value(true);
      },
    );
  }

  Route _onGenerateRoute(RouteSettings settings) {
    late Widget page; // = const DashboardScreen();
    switch (settings.name) {
      case LoginPage.routeName:
        page = const LoginPage();
        break;
      case SignUpPage.routeName:
        page = const SignUpPage();
        break;
      case ResetPasswordPage.routeName:
        page = ResetPasswordPage();
        break;

      case ChangePasswordPage.routeName:
        page = ChangePasswordPage();
        break;
    }
    return MaterialPageRoute<dynamic>(
      builder: (context) {
        return page;
      },
      settings: settings,
    );
  }
}
