
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/common/charts/chat_data.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/common/charts/bars_chart_widget.dart';
import 'package:monkeybox/widgets/common/charts/lines_chart_widget.dart';
import 'package:monkeybox/widgets/side_menu/sport-data/select_type_chart_widget.dart';
import 'package:provider/provider.dart';

class ChartsSportDataWidget extends StatefulWidget {
  final List<ChartData>? listChartData;
  final String label;
  const ChartsSportDataWidget({Key? key, required this.label, required this.listChartData})
      : super(key: key);

  @override
  State<ChartsSportDataWidget> createState() => _ChartsSportDataWidgetState();
}

class _ChartsSportDataWidgetState extends State<ChartsSportDataWidget> {
  List<bool> _otherBtnPressed = [false, true];

  @override
  Widget build(BuildContext context) {
    var sportDataProvider =
        Provider.of<SportDataProvider>(context, listen: false);
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        width: double.infinity,
        height: widget.listChartData == null || widget.listChartData!.isEmpty ? (MediaQuery.of(context).size.height / 2) : null,
        padding: EdgeInsets.only(bottom: 10.sp),
        child: widget.listChartData == null ? const SizedBox() :
        widget.listChartData != null && widget.listChartData!.isEmpty ? 
        Center(child: Text('No se encontraron resultados'))
        :
         Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 16.sp, left: 16.sp),
              child: Text.rich(TextSpan(
                  text: '${widget.label} ACTUAL ',
                  children: [
                    TextSpan(
                        text: '${widget.listChartData!.last.y} KG',
                        style: Theme.of(context).textTheme.headline2!.copyWith(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                  style: Theme.of(context)
                      .textTheme
                      .headline2!
                      .copyWith(color: Colors.grey))),
            ),
            Padding(
              padding: EdgeInsets.only(right: 16.sp),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SelectTypeChartWidget(
                      id: 0,
                      text: 'Barras',
                      onPressOtherBtn: () {
                        setState(() {
                          _otherBtnPressed = [false, true];
                        });
                      },
                      isOtherBtnPressed: _otherBtnPressed[0]),
                  SelectTypeChartWidget(
                      id: 1,
                      text: 'Líneas',
                      onPressOtherBtn: () {
                        setState(() {
                          _otherBtnPressed = [true, false];
                        });
                      },
                      isOtherBtnPressed: _otherBtnPressed[1]),
                ],
              ),
            ),
            if (_otherBtnPressed[1])
              BarsChartWidget(
                series: sportDataProvider
                    .getDefaultColumnSeries(widget.listChartData!),
                title: 'GRÁFICA de Barras ${widget.label}',
                labelAxisX: 'Fecha',
                labelAxisY: 'Kilogramos'
              ),
            if (_otherBtnPressed[0])
              LinesChartWidget(
                  title: 'GRÁFICA de Líneas ${widget.label}',
                  series: sportDataProvider
                      .getDefaultSplineSeries(widget.listChartData!),
                  labelAxisX: 'Fecha',
                  labelAxisY: 'Kilogramos'),

          ],
        ),
      ),
    );
  }
}
