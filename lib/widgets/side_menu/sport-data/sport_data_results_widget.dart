import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/widgets/side_menu/rm/rm_result_widget.dart';
import 'package:monkeybox/widgets/side_menu/sport-data/sport_data_result_widget.dart';
import 'package:monkeybox/widgets/side_menu/weight/weight_result_widget.dart';

class SportDataResultsWidget<T> extends StatelessWidget {
  final List<T> listData;
  final Function(int) onDelete;
  final Function(T) onUpdate;
  final String typeResultWidget;
  final String titleDelete;
  final String descriptionDelete;

  const SportDataResultsWidget(
      {Key? key,
      required this.typeResultWidget,
      required this.listData,
      required this.onUpdate,
      required this.onDelete,
      required this.titleDelete,
      required this.descriptionDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 560,
        margin: EdgeInsets.only(top: 0),
        child: DraggableScrollableSheet(
          initialChildSize: 0.3,
          minChildSize: 0.3,
          maxChildSize: 1,
          builder: (BuildContext context, ScrollController scrollController) =>
              Container(
            width: double.infinity,
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 3,
                  blurRadius: 7,
                  offset: Offset(0, 0), // changes position of shadow
                ),
              ],
            ),
            child: SingleChildScrollView(
              controller: scrollController,
              child: Padding(
                padding: EdgeInsets.all(16.sp),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          height: 3,
                          width: 25,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      Text('RESULTADOS',
                          style: Theme.of(context)
                              .textTheme
                              .headline2!
                              .copyWith(color: Colors.grey)),
                      SizedBox(
                        height: 8.sp,
                      ),
                      ...listData.map((data) {
                        return SportDataResultWidget<T>(
                          resultWidget: _generateResultWidget(data)!,
                          data: data,
                          titleDelete: titleDelete,
                          descriptionDelete: descriptionDelete,
                          onDelete: (int id) {
                            onDelete(id);
                          },
                          onUpdate: (T data) {
                            onUpdate(data);
                          },
                        );
                      })
                    ]),
              ),
            ),
          ),
        ));
  }

  Widget? _generateResultWidget(data) {
    if (typeResultWidget == 'RMResultWidget') {
      return RMResultWidget(data: data);
    } else if (typeResultWidget == 'WeightResultWidget') {
      return WeightResultWidget(data: data);
    }
    return null;
  }
}
