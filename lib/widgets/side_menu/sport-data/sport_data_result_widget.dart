import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/helpers/dialogs.dart';

class SportDataResultWidget<T> extends StatelessWidget {
  final dynamic data;
  final Function(int) onDelete;
  final Function(T) onUpdate;
  final Widget resultWidget;
  final String titleDelete;
  final String descriptionDelete;

  const SportDataResultWidget(
      {Key? key,
      required this.data,
      required this.onUpdate,
      required this.onDelete,
      required this.resultWidget,
      required this.titleDelete,
      required this.descriptionDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          DateFormat('dd/MM/yyy').format(DateTime.parse(data.date)),
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: Colors.grey, fontSize: 16.sp),
        ),
        resultWidget,
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            IconButton(
                onPressed: () async {
                  var result = await _showDialogDeleting();
                  if (result) {
                    onDelete(data.id);
                  }
                },
                icon: Icon(Icons.delete_rounded),
                iconSize: 20.sp,
                visualDensity: VisualDensity.compact,
                padding: EdgeInsets.zero,
                constraints: BoxConstraints(),
                color: Colors.black),
            IconButton(
                onPressed: () {
                  onUpdate(data);
                },
                icon: Icon(Icons.edit_rounded),
                iconSize: 20.sp,
                visualDensity: VisualDensity.compact,
                padding: EdgeInsets.zero,
                color: Colors.black),
          ],
        )
      ],
    );
  }

  Future<bool> _showDialogDeleting() async {
    return await Dialogs.showConfirmDialogMaterial(
        titleDelete, descriptionDelete);
  }
}
