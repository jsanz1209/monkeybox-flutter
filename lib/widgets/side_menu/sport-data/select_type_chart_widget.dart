import 'package:flutter/material.dart';

class SelectTypeChartWidget extends StatefulWidget {
  final String text;
  final int id;
  final Function onPressOtherBtn;
  final bool isOtherBtnPressed;

  const SelectTypeChartWidget(
      {Key? key,
      required this.id,
      required this.text,
      required this.onPressOtherBtn,
      required this.isOtherBtnPressed})
      : super(key: key);

  @override
  State<SelectTypeChartWidget> createState() => _SelectTypeChartWidgetState();
}

class _SelectTypeChartWidgetState extends State<SelectTypeChartWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      reverseDuration: const Duration(milliseconds: 500),
      vsync: this,
    );
    if (!widget.isOtherBtnPressed) {
      _controller.forward();
    }
  }

  @override
  void dispose() {
    // Dispose controller to release resources.
    this._controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(SelectTypeChartWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isOtherBtnPressed) {
      _controller.reverse();
    } else {
      _controller.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    final sizeAnimationBarsChart = Tween<double>(begin: 1, end: 1.3).animate(
        CurvedAnimation(
            parent: _controller,
            curve: Curves.linear,
            reverseCurve: Curves.linear));
    return AnimatedBuilder(
        animation: sizeAnimationBarsChart,
        builder: (context, child) {
          return Transform.scale(
            scale: sizeAnimationBarsChart.value,
            child: TextButton(
                onPressed: () {
                  setState(() {
                    _controller.forward();
                    widget.onPressOtherBtn();
                  });
                },
                child: Text(
                  widget.text,
                  style: Theme.of(context).textTheme.headline3!.copyWith(
                      color: Colors.black,
                      fontWeight: widget.isOtherBtnPressed
                          ? FontWeight.normal
                          : FontWeight.bold),
                )),
          );
        });
  }
}
