import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';

class CustomMenuHeader extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool? isTransparent;
  final AppBar appBar = AppBar();
  final Function? onClick;
  final List<Widget>? actions;

  CustomMenuHeader(
      {Key? key,
      required this.title,
      this.onClick,
      this.actions,
      this.isTransparent = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomHeader(
        title: title,
        withMenuButton: true,
        isTransparent: isTransparent,
        actions: actions,
        onPress: () {
          Scaffold.of(context).openDrawer();
          if (onClick != null) onClick!(context);
        });
  }

  @override
  Size get preferredSize => Size.fromHeight(64.h);
}
