import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/sport-data/api/body_weights_data.dart';

class WeightResultWidget extends StatelessWidget {
  final BodyWeightsData? data;

  const WeightResultWidget({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          '${data!.weight}KG',
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: Colors.black,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold),
        ),
        Icon(
          Icons.accessibility_new_rounded,
          size: 20.sp,
          color: Colors.black,
        ),
        Text(
          ' - ${data!.imc}',
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: Colors.black,
              fontSize: 16.sp,
              fontWeight: FontWeight.normal),
        ),
      ],
    );
  }
}
