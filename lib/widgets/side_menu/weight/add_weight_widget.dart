import 'package:flutter/material.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/weight/weight.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/side_menu/weight/form_add_weight_widget.dart';
import 'package:provider/provider.dart';

class AddWeightWidget extends StatelessWidget {
  final Function onBack;

  const AddWeightWidget({Key? key, required this.onBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: FormAddWeightWidget(
          onSubmit: (Weight data, int? idWeight) =>
              _submit(context, data, idWeight),
          onBack: () => onBack()),
    );
  }

  void _submit(BuildContext context, Weight data, int? idWeight) async {
    try {
      if (idWeight != null) {
        await Provider.of<SportDataProvider>(context, listen: false)
            .updateWeightData(data, idWeight);
        SnackBarHelper.showSnackBar('Peso modificado',
            'El Peso se ha modificado correctamente', TypeSnackBarEnum.success);
      } else {
        await Provider.of<SportDataProvider>(context, listen: false)
            .addWeightData(data);
        SnackBarHelper.showSnackBar('Peso añadido',
            'El Peso se ha añadido correctamente', TypeSnackBarEnum.success);
      }
      Provider.of<MenuProvider>(context, listen: false).refreshProfile();
      onBack();
    } catch (error) {
      rethrow;
    }
  }
}
