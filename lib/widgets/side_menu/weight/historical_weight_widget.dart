import 'package:flutter/material.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/common/charts/chat_data.dart';
import 'package:monkeybox/models/sport-data/api/body_weights_data.dart';
import 'package:monkeybox/models/weight/weight.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/side_menu/sport-data/charts_sport_data_widget.dart';
import 'package:monkeybox/widgets/side_menu/sport-data/sport_data_results_widget.dart';
import 'package:provider/provider.dart';

import 'form_weight_widget.dart';

class HistoricalWeightWidget extends StatefulWidget {
  final Function(BodyWeightsData) onUpdateWeight;
  HistoricalWeightWidget({Key? key, required this.onUpdateWeight})
      : super(key: key);

  @override
  State<HistoricalWeightWidget> createState() => _HistoricalWeightWidgetState();
}

class _HistoricalWeightWidgetState extends State<HistoricalWeightWidget> {
  List<ChartData>? _listChartData;
  List<BodyWeightsData>? _listData;
  Weight? _formWeight;
  int? _idWeight;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              FormWeightWidget(
                  idWeight: _idWeight,
                  onSubmit:
                      (List<BodyWeightsData> listData, Weight formWeight) {
                    _idWeight = null;
                    _setDataUI(listData, formWeight);
                  }),
              if (_listData == null)
                SizedBox(height: MediaQuery.of(context).size.height / 2),
              Stack(
                children: [
                  ChartsSportDataWidget(
                    listChartData: _listChartData,
                    label: 'PESO',
                  ),
                  if (_listData != null && _listData!.isNotEmpty)
                    SportDataResultsWidget<BodyWeightsData>(
                      typeResultWidget: 'WeightResultWidget',
                      titleDelete: 'Eliminar Peso',
                      descriptionDelete:
                          '¿Estás seguro de que quieres eliminar este Peso?',
                      listData: _listData!,
                      onDelete: (int idWeight) {
                        _deleteWeight(idWeight);
                      },
                      onUpdate: (BodyWeightsData data) {
                        widget.onUpdateWeight(data);
                      },
                    ),
                ],
              )
            ],
          ),
        ));
  }

  void _setDataUI(List<BodyWeightsData> listData, Weight formWeight) {
    setState(() {
      _formWeight = formWeight;
      if (listData.isEmpty) {
        _listChartData = [];
        _listData = listData;
      } else {
        listData.sort((a, b) => a.date!.compareTo(b.date!));
        _listData = listData.toList();
        _listChartData = listData
            .map((item) =>
                ChartData(x: DateTime.parse(item.date!), y: item.weight, label: '📟 ${item.imc}'))
            .toList();
      }
    });
  }

  void _deleteWeight(int idWeight) async {
    try {
      _idWeight = idWeight;
      var sportProvider =
          Provider.of<SportDataProvider>(context, listen: false);
      await sportProvider.deleteBodyWeightsData(idWeight);
      SnackBarHelper.showSnackBar('Peso borrado',
          'El Peso se ha borrado correctamente', TypeSnackBarEnum.success);
      Future.delayed(const Duration(milliseconds: 200), () async {
        var listData = await sportProvider.getBodyWeights(
            _formWeight!.startDate!, _formWeight!.endDate);
        _setDataUI(listData, _formWeight!);
      });
    } catch (error) {
      rethrow;
    }
  }
}
