import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/models/sport-data/api/body_weights_data.dart';
import 'package:monkeybox/models/weight/weight.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';
import 'package:provider/provider.dart';

class FormWeightWidget extends StatefulWidget {
  final int? idWeight;
  final Function(List<BodyWeightsData>, Weight) onSubmit;
  FormWeightWidget(
      {Key? key, required this.idWeight, required this.onSubmit})
      : super(key: key);

  @override
  State<FormWeightWidget> createState() => _FormWeightWidgetState();
}

class _FormWeightWidgetState extends State<FormWeightWidget> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields(['imc', 'startDate', 'endDate']);
  late Future<List<BodyWeightsData>> _futureBodyWeight;
  Weight _data = Weight();
  double _imc = -1;
  bool _isFirstLoad = true;

  void _init() {
    var startDate = DateFormat('yyyy-MM-dd')
        .format(DateTime.now().subtract(const Duration(days: 30 * 3)));
    var endDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
    _futureBodyWeight = Provider.of<SportDataProvider>(context, listen: false)
        .getBodyWeights(startDate, endDate);
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void didUpdateWidget(FormWeightWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.idWeight != null && widget.idWeight != oldWidget.idWeight) {
      _init();
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<BodyWeightsData>>(
      future: _futureBodyWeight,
      builder: (ctx, asyncSnapShot) {
        if (asyncSnapShot.connectionState == ConnectionState.waiting) {
          return SizedBox();
        } else {
          var listBodyWeightData = asyncSnapShot.data!;
          _imc =
              listBodyWeightData.length > 0 ? listBodyWeightData.last.imc! : -1;
          if (_isFirstLoad) {
            _isFirstLoad = false;
            Future.delayed(const Duration(milliseconds: 200), () {
              _formKey.currentState!.save();
              widget.onSubmit(listBodyWeightData, _data);
            });
          }
          return Container(
            color: Colors.grey.shade300,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(
                      height: 8.sp,
                    ),
                    if (_imc != -1)
                      CustomTextFormField(
                          label: 'IMC actual',
                          fieldName: 'imc',
                          isBlackTheme: true,
                          disabled: true,
                          initialValue: _imc.toString(),
                          keyboardType:
                              TextInputType.numberWithOptions(decimal: true)),
                    SizedBox(
                      height: 8.sp,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: CustomTextFormField(
                            formKey: _fields['startDate'],
                            label: 'Desde',
                            fieldName: 'startDate',
                            initialValue: DateFormat('yyyy-MM-dd').format(
                                DateTime.now()
                                    .subtract(const Duration(days: 30 * 3))),
                            isBlackTheme: true,
                            isTypeDate: true,
                            isTypeMonthOnlyDate: true,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            onSave: (value) {
                              _data = _data.copyWith(startDate: value);
                            },
                            validator: (value) {
                              return FormValidators.validatorRequired(
                                  value, 'La fecha de inicio es obligatoria');
                            },
                          ),
                        ),
                        SizedBox(
                          width: 4.sp,
                        ),
                        Expanded(
                          child: CustomTextFormField(
                              formKey: _fields['endDate'],
                              label: 'Hasta',
                              fieldName: 'endDate',
                              isTypeMonthOnlyDate: true,
                              isBlackTheme: true,
                              isTypeDate: true,
                              initialValue: DateFormat('yyyy-MM-dd')
                                  .format(DateTime.now()),
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              onSave: (value) {
                                _data = _data.copyWith(endDate: value);
                              },
                              validator: (value) {
                                return FormValidators.validatorRequired(
                                    value, 'La fecha de fin es obligatoria');
                              }),
                        ),
                      ],
                    ),
                    SizedBox(height: 9.sp),
                    CustomElevatedButton(
                        text: 'BUSCAR HISTÓRICO',
                        backgroundColor: Theme.of(context).primaryColor,
                        textColor: Colors.white,
                        pressEvent: _onSubmit),
                    SizedBox(height: 19.5.sp),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  void _onSubmit() async {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      debugPrint('is valid');
      _formKey.currentState!.save();
      setState(() {
        _isFirstLoad = true;
        _futureBodyWeight =
            Provider.of<SportDataProvider>(context, listen: false)
                .getBodyWeights(_data.startDate, _data.endDate);
      _formKey.currentState!.reset();                
      });
    } else {
      debugPrint('is invalid');
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            alignmentPolicy: ScrollPositionAlignmentPolicy.keepVisibleAtStart,
            alignment: 0,
            curve: Curves.easeInOut);
      }
    }
  }
}
