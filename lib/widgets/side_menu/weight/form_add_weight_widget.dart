import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/models/weight/weight.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/weight/weight_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';
import 'package:provider/provider.dart';

class FormAddWeightWidget extends StatefulWidget {
  final Function(Weight, int?) onSubmit;
  final Function onBack;
  const FormAddWeightWidget(
      {Key? key, required this.onSubmit, required this.onBack})
      : super(key: key);

  @override
  State<FormAddWeightWidget> createState() => _FormAddWeightWidgetState();
}

class _FormAddWeightWidgetState extends State<FormAddWeightWidget> {
  final _formKey = GlobalKey<FormState>();

  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields(['date', 'weight', 'imc']);

  Weight _data = Weight();
  int? _idWeight;

  @override
  void initState() {
    super.initState();
    var dataUpdate =
        Provider.of<WeightProvider>(context, listen: false).dataUpdate;
    if (dataUpdate != null) {
      _idWeight = dataUpdate.id;
      _data = _data.copyWith(
          weight: dataUpdate.weight,
          startDate: dataUpdate.date,
          imc: dataUpdate.imc);
    }
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<MenuProvider>(context, listen: false).user;
    return Container(
      child: Container(
        margin: EdgeInsets.all(
          16.sp,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              CustomTextFormField(
                formKey: _fields['date'],
                label: 'Fecha',
                fieldName: 'date',
                initialValue: _data.startDate!.isNotEmpty
                    ? DateFormat('yyyy-MM-dd')
                        .format(DateTime.parse(_data.startDate!))
                    : DateFormat('yyyy-MM-dd').format(DateTime.now()),
                isBlackTheme: true,
                isTypeDate: true,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                onSave: (value) {
                  _data = _data.copyWith(startDate: value);
                },
                validator: (value) {
                  return FormValidators.validatorRequired(
                      value, 'La fecha es obligatoria');
                },
              ),
              SizedBox(height: 19.5.sp),
              CustomTextFormField(
                formKey: _fields['weight'],
                label: 'Peso',
                fieldName: 'weight',
                isBlackTheme: true,
                suffixText: 'kg',
                initialValue: _data.weight != null && _data.weight != 0
                    ? _data.weight.toString()
                    : null,
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                textInputAction: TextInputAction.next,
                onSave: (value) {
                  _data = _data.copyWith(
                      weight:
                          double.parse(value!.replaceFirst(RegExp(','), '.')));
                },
                onChanged: (value) {
                  setState(() {
                    if (value.isNotEmpty) {
                      _data = _data.copyWith(
                          imc: double.parse(
                                  value.replaceFirst(RegExp(','), '.')) /
                              pow(user!.profile!.height, 2));
                    } else {
                      _data = _data.copyWith(imc: -1);
                    }
                  });
                },
                validator: (value) {
                  return FormValidators.validatorRequired(
                      value, 'El peso es obligatorio');
                },
              ),
              SizedBox(height: 19.5.sp),
              CustomTextFormField(
                  formKey: _fields['imc'],
                  label: 'IMC (${user!.profile!.height}m)',
                  fieldName: 'imc',
                  isBlackTheme: true,
                  initialValue: _data.imc != null && _data.imc != -1
                      ? _data.imc!.toStringAsFixed(2)
                      : '',
                  disabled: true,
                  hasToReloadValue: true,
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  textInputAction: TextInputAction.done),
              SizedBox(height: 9.sp),
              CustomElevatedButton(
                  text: 'GUARDAR',
                  backgroundColor: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  pressEvent: _onSubmit),
              SizedBox(height: 9.sp),
              CustomElevatedButton(
                  text: 'CANCELAR',
                  backgroundColor: Colors.white,
                  textColor: Theme.of(context).primaryColor,
                  borderColor: Theme.of(context).primaryColor,
                  borderWidth: 2.sp,
                  pressEvent: () {
                    widget.onBack();
                  })
            ],
          ),
        ),
      ),
    );
  }

  void _onSubmit() async {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      debugPrint('is valid');
      _formKey.currentState!.save();
      widget.onSubmit(_data, _idWeight);
    } else {
      debugPrint('is invalid');
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            alignmentPolicy: ScrollPositionAlignmentPolicy.keepVisibleAtStart,
            alignment: 0,
            curve: Curves.easeInOut);
      }
    }
  }
}
