import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/side-menu/card_dashboard.dart' as card_item;
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:provider/provider.dart';

class CardDashboard extends StatelessWidget {
  final card_item.CardDashboard item;
  const CardDashboard({Key? key, required this.item}) : super(key: key);

  get imageWidget {
    return item.backgroundName == 'dash1' || item.backgroundName == 'dash2' ? AssetImage('assets/images/${item.backgroundName}.jpg'): CachedNetworkImageProvider('${Settings.apiURLBase}${item.backgroundName}');
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: InkWell(
      onTap: () {
        var navigatorKey =
            Provider.of<MenuProvider>(context, listen: false).navigatorKey;
        Navigator.of(navigatorKey.currentContext!)
            .pushNamed(item.routeName);
      },
      child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: imageWidget,
                  fit: BoxFit.cover),
              color: Theme.of(context)
                  .primaryColor // Specifies the background color and the opacity

              ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(
                'assets/images/icons/${item.foregroundName}.svg',
                alignment: Alignment.centerRight,
                color: Colors.white,
                width: 42.39.sp,
              ),
              SizedBox(
                height: 8.sp,
              ),
              Text(item.title, style: Theme.of(context).textTheme.headline6)
            ],
          )),
    ));
  }
}
