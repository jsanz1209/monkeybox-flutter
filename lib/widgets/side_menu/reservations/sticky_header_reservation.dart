import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/providers/reservations/reservations_provider.dart';
import 'package:provider/provider.dart';

class StickHeaderReservation extends StatefulWidget {
  final Function(int) onPress;
  final List<String> timeChips;
  final Color color;
  const StickHeaderReservation(
      {Key? key,
      required this.onPress,
      required this.timeChips,
      required this.color})
      : super(key: key);

  @override
  State<StickHeaderReservation> createState() => _StickHeaderReservationState();
}

class _StickHeaderReservationState extends State<StickHeaderReservation> {
  final ScrollController _scrollController = ScrollController();

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var chipSelectedIndex =
        Provider.of<ReservationsProvider>(context, listen: true)
            .chipIndexSelected;
    if (_scrollController.hasClients) {
      _scrollController.animateTo((chipSelectedIndex > widget.timeChips.length / 2)
          ? _scrollController.position.maxScrollExtent
          : _scrollController.position.minScrollExtent, duration: const Duration(milliseconds: 300), curve: Curves.ease);
    }
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
        height: 50,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          controller: _scrollController,
          shrinkWrap: true,
          itemBuilder: (ctx, index) {
            var item = widget.timeChips[index];
            return ActionChip(
              onPressed: () {
                Provider.of<ReservationsProvider>(context, listen: false)
                    .chipIndexSelected = index;
                widget.onPress(index);
              },
              backgroundColor:
                  chipSelectedIndex == index ? widget.color : Colors.white,
              visualDensity: VisualDensity.compact,
              label: Text(item,
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                      color: chipSelectedIndex == index
                          ? Colors.white
                          : Colors.black,
                      fontSize: 14.sp,
                      fontFamily: 'Oswald')),
            );
          },
          itemCount: widget.timeChips.length,
        ));
  }
}
