import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/enums/reservation_type_enum.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/dialogs.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/reservations/api/training.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/reservations/reservations_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/custom_floating_button.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:monkeybox/widgets/common/date_picker.dart';
import 'package:monkeybox/widgets/side_menu/reservations/item_reservation.dart';
import 'package:monkeybox/widgets/side_menu/reservations/sticky_header_reservation.dart';
import 'package:provider/provider.dart';

class ContentReservation extends StatefulWidget {
  final String titleHeader;
  final ReservationTypeEnum typeReservation;
  final Color color;
  final Color colorSecondary;

  const ContentReservation(
      {Key? key,
      required this.titleHeader,
      required this.typeReservation,
      required this.color,
      required this.colorSecondary})
      : super(key: key);

  @override
  _ContentReservationState createState() => _ContentReservationState();
}

class _ContentReservationState extends State<ContentReservation> {
  late List<GlobalKey> _keyGrid;
  final ScrollController _scrollController = ScrollController();
  final ScrollController _scrollControllerMain = ScrollController();

  late DateTime _currentDate;
  int _sensitivity = 8;
  List<String> _timeChips = [];
  List<Training> _data = [];

  @override
  void initState() {
    super.initState();
    _currentDate =
        Provider.of<ReservationsProvider>(context, listen: false).selectedDate;
    if (mounted) {
      _getTrainings();
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _scrollControllerMain.dispose();
    super.dispose();
  }

  ListView get _buildNoneResults {
    return ListView(children: [
      Container(
        height: MediaQuery.of(context).size.height * 0.75,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            DatePickerWidget(
                _currentDate,
                _changeDateTime,
                widget.typeReservation == ReservationTypeEnum.wod
                    ? CustomExtraThemeData.getExtraThemeData().wodsColor
                    : (widget.typeReservation == ReservationTypeEnum.openBOX)
                        ? CustomExtraThemeData.getExtraThemeData().openBoxColor
                        : CustomExtraThemeData.getExtraThemeData()
                            .competitorsColor,
                widget.typeReservation == ReservationTypeEnum.wod
                    ? CustomExtraThemeData.getExtraThemeData().wodsLabelColor
                    : (widget.typeReservation == ReservationTypeEnum.openBOX)
                        ? CustomExtraThemeData.getExtraThemeData()
                            .openBoxLabelColor
                        : CustomExtraThemeData.getExtraThemeData()
                            .competitorsLabelColor),
            Expanded(
              child: Center(
                child: Text(
                  'No se encuentran resultados para el día seleccionado',
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .headline2!
                      .copyWith(color: Colors.black),
                ),
              ),
            )
          ],
        ),
      ),
    ]);
  }

  CustomScrollView get _buildTrainings {
    return CustomScrollView(
      shrinkWrap: true,
      controller: _scrollControllerMain,
      slivers: <Widget>[
        SliverStickyHeader(
          header: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              alignment: Alignment.centerLeft,
              child: DatePickerWidget(
                  _currentDate,
                  _changeDateTime,
                  widget.typeReservation == ReservationTypeEnum.wod
                      ? CustomExtraThemeData.getExtraThemeData().wodsColor
                      : (widget.typeReservation == ReservationTypeEnum.openBOX)
                          ? CustomExtraThemeData.getExtraThemeData()
                              .openBoxColor
                          : CustomExtraThemeData.getExtraThemeData()
                              .competitorsColor,
                  widget.typeReservation == ReservationTypeEnum.wod
                      ? CustomExtraThemeData.getExtraThemeData().wodsLabelColor
                      : (widget.typeReservation == ReservationTypeEnum.openBOX)
                          ? CustomExtraThemeData.getExtraThemeData()
                              .openBoxLabelColor
                          : CustomExtraThemeData.getExtraThemeData()
                              .competitorsLabelColor)),
        ),
        SliverAppBar(
          titleSpacing: 0,
          stretch: true,
          floating: true,
          snap: true,
          leadingWidth: 0,
          toolbarHeight: 42.5,
          title: SizedBox(width: double.infinity, child: _getStickyWidget()),
          pinned: true,
          forceElevated: true,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return ListView.builder(
                controller: _scrollController,
                padding: EdgeInsets.only(top: 0, bottom: 84),
                shrinkWrap: true,
                itemCount: _data.length,
                itemBuilder: (BuildContext context, int index) {
                  return ItemReservation(
                      date: _currentDate,
                      globalKey: _keyGrid[index],
                      item: _data[index],
                      titleHeader: widget.titleHeader,
                      color: widget.color,
                      typeReservation: widget.typeReservation,
                      colorSecondary: widget.colorSecondary,
                      onReservation: (int id, [bool onQueue = false]) {
                        _onReservation(id, _timeChips[index], onQueue);
                      },
                      onCancel: (int id, [bool onQueue = false]) {
                        _onCancel(id, _timeChips[index], onQueue);
                      },
                      onTimeSelected: () {
                        Provider.of<ReservationsProvider>(context,
                                listen: false)
                            .chipIndexSelected = index;
                      },
                      timeChip: _timeChips[index]);
                });
          }, childCount: 1),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    _keyGrid = List.generate(_data.length, (index) => GlobalKey());
    _timeChips = _data
        .map((item) => item.startTime.substring(0, item.startTime.length - 3))
        .toList();

    return Scaffold(
      backgroundColor: HexColor('#e5e5e5'),
      body: GestureDetector(
          onHorizontalDragUpdate: (details) {
            if (details.delta.dx > _sensitivity) {
              _changeDateTime(_currentDate.subtract(const Duration(days: 1)));
              // Right Swipe
            } else if (details.delta.dx < -_sensitivity) {
              _changeDateTime(_currentDate.add(const Duration(days: 1)));
            }
          },
          child: Container(
              color: HexColor('#e5e5e5'),
              child: _data.isEmpty ? _buildNoneResults : _buildTrainings)),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Padding(
        padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom + 70),
        child: CustomFloatingButton(
          scrollControllerMain: _scrollControllerMain,
          background: widget.typeReservation == ReservationTypeEnum.wod
              ? CustomExtraThemeData.getExtraThemeData().wodsColor
              : widget.typeReservation == ReservationTypeEnum.openBOX
                  ? CustomExtraThemeData.getExtraThemeData().openBoxColor
                  : CustomExtraThemeData.getExtraThemeData().competitorsColor,
        ),
      ),
    );
  }

  void _init() {
    Provider.of<ReservationsProvider>(context, listen: false)
        .chipIndexSelected = 0;
  }

  String _getSelectedDate() {
    return DateFormat('yyyy-MM-dd').format(_currentDate);
  }

  String _getDateTimeLarge(DateTime date) {
    final String languageCode = Localizations.localeOf(context).languageCode;
    return DateFormat('EEEE d MMMM y', languageCode).format(date);
  }

  void _changeDateTime(DateTime selectedDate) {
    _currentDate = selectedDate;
    Provider.of<ReservationsProvider>(context, listen: false).selectedDate =
        selectedDate;
    _init();
    _getTrainings();
  }

  void _getTrainings() {
    var boxID = Provider.of<AuthProvider>(context, listen: false).boxID;
    Provider.of<ReservationsProvider>(context, listen: false)
        .getTrainingsReservations(
            _getSelectedDate(), boxID!, widget.typeReservation.index)
        .then((value) {
      if (mounted) {
        setState(() {
          _data = value;
        });
      }
    });
  }

  void _onReservation(int trainingID, String time,
      [bool onQueue = false]) async {
    if (Provider.of<MenuProvider>(context, listen: false)
        .user!
        .userBoxes[0]
        .status
        .contains('AUTOMATICALLY_DISABLED')) {
      var reservation = await _showDialogUserOff();
      if (reservation) {
        _setReservation(trainingID, time, onQueue);
      }
    } else {
      _setReservation(trainingID, time, onQueue);
    }
  }

  void _setReservation(int trainingID, time, [bool onQueue = false]) async {
    try {
      var menuProvider = Provider.of<MenuProvider>(context, listen: false);
      var response =
          await Provider.of<ReservationsProvider>(context, listen: false)
              .setReservation(trainingID, _getSelectedDate());
      _getTrainings();
      var date = _getDateTimeLarge(DateTime.parse(response.date));
      if (onQueue) {
        SnackBarHelper.showSnackBar(
            'Reserva en cola confirmada',
            'Se ha encolado con éxito la reserva para el día $date a las $time',
            TypeSnackBarEnum.success);
      } else if (response.lessThanMin) {
        SnackBarHelper.showSnackBar(
            'Reserva confirmada',
            'Se ha confirmado con éxito la reserva para el día $date a las $time. Si la clase continua con menos de ${response.minAthletes} personas, tu reserva será cancelada ${response.cancelTime} minutos antes de la realización de la clase',
            TypeSnackBarEnum.success);
        menuProvider.refreshProfile();
      } else {
        SnackBarHelper.showSnackBar(
            'Reserva confirmada',
            'Se ha confirmado con éxito la reserva para el día $date a las $time.',
            TypeSnackBarEnum.success);
        menuProvider.refreshProfile();
      }
    } catch (error) {
      //TO-DO: Go to fees if error response code is 422
      rethrow;
    }
  }

  void _onCancel(int reservationID, String time, [bool onQueue = false]) async {
    var cancel = await _showDialogCancelReservation();
    if (cancel) {
      _cancelReservation(reservationID, time, onQueue);
    }
  }

  void _cancelReservation(int reservationID, String time,
      [bool onQueue = false]) async {
    try {
      var menuProvider = Provider.of<MenuProvider>(context, listen: false);
      var response =
          await Provider.of<ReservationsProvider>(context, listen: false)
              .cancelReservation(reservationID);
      var date = _getDateTimeLarge(DateTime.parse(_getSelectedDate()));
      if (response.status == 200) {
        _getTrainings();
        if (onQueue) {
          SnackBarHelper.showSnackBar(
              'Reserva en cola cancelada',
              'Se ha cancelado con éxito la reserva en cola para el día $date a las $time',
              TypeSnackBarEnum.success);
        } else {
          SnackBarHelper.showSnackBar(
              'Reserva cancelada',
              'Se ha cancelado con éxito la reserva para el día $date a las $time',
              TypeSnackBarEnum.success);
          menuProvider.refreshProfile();
        }
      }
    } catch (error) {
      rethrow;
    }
  }

  Future<bool> _showDialogUserOff() async {
    return await Dialogs.showConfirmDialogMaterial('USUARIO DADO DE BAJA',
        'Tu usuario se encuentra dado de baja, si aceptas la reserva tu usuario será automáticamente dado de alta y se creará la correspondiente como “pendiente de pago”.');
  }

  Future<bool> _showDialogCancelReservation() async {
    return await Dialogs.showConfirmDialogMaterial(
        'CANCELAR RESERVA', '¿Estás seguro de que deseas cancelar la reserva?');
  }

  StickHeaderReservation _getStickyWidget() {
    return StickHeaderReservation(
        timeChips: _timeChips,
        color: widget.color,
        onPress: (index) {
          Scrollable.ensureVisible(_keyGrid[index].currentContext!,
              duration: const Duration(milliseconds: 700),
              curve: Curves.easeInOut);
        });
  }
}
