import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/enums/reservation_type_enum.dart';
import 'package:monkeybox/models/reservations/api/training.dart';
import 'package:monkeybox/models/reservations/api/training_reservation.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/reservations/reservations_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/side_menu/reservations/grid_reservation.dart';
import 'package:monkeybox/widgets/side_menu/reservations/header_reservation.dart';
import 'package:provider/provider.dart';
import 'package:visibility_detector/visibility_detector.dart';

class ItemReservation extends StatefulWidget {
  final Training item;
  final GlobalKey globalKey;
  final String timeChip;
  final DateTime date;
  final Color color;
  final Color colorSecondary;
  final String titleHeader;
  final Function onTimeSelected;
  final ReservationTypeEnum typeReservation;
  final Function(int, [bool]) onReservation;
  final Function(int, [bool]) onCancel;

  const ItemReservation(
      {Key? key,
      required this.item,
      required this.date,
      required this.typeReservation,
      required this.globalKey,
      required this.timeChip,
      required this.titleHeader,
      required this.color,
      required this.colorSecondary,
      required this.onTimeSelected,
      required this.onReservation,
      required this.onCancel})
      : super(key: key);

  @override
  State<ItemReservation> createState() => _ItemReservationState();
}

class _ItemReservationState extends State<ItemReservation> {
  bool _toggleParticipants = false;

  Widget _buildButtonUserReserved(DateTime dateTimeTraining) {
    return Row(
      mainAxisAlignment: !_isPassedDate(dateTimeTraining) &&
            _isThereMoreThanCancelTime(widget.item)
          ? MainAxisAlignment.spaceBetween
          : MainAxisAlignment.end,
      children: [
        if (!_isPassedDate(dateTimeTraining) &&
            _isThereMoreThanCancelTime(widget.item))
          CustomElevatedButton(
              adjustoFit: true,
              fontSize: 14.sp,
              text: 'CANCELAR',
              backgroundColor: Colors.red,
              textColor: Colors.white,
              pressEvent: () {
                var userID =
                    Provider.of<AuthProvider>(context, listen: false).userID;
                var userReservation = widget.item.reservations
                    .firstWhere((element) => userID == element.user!.id);
                widget.onCancel(userReservation.id);
              }),
        CustomElevatedButton(
            adjustoFit: true,
            fontSize: 14.sp,
            borderColor: widget.color,
            text: 'YA HAS RESERVADO',
            backgroundColor: Colors.white,
            borderWidth: 2.sp,
            textColor: widget.color,
            pressEvent: () {}),
      ],
    );
  }

  Widget _buildButtonsUserOnQueue(
    List<TrainingReservation> reservationsOnQueue,
  ) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        CustomElevatedButton(
            fontSize: 14.sp,
            text: 'MOSTRAR COLA',
            adjustoFit: true,
            backgroundColor: widget.color,
            textColor: Colors.white,
            pressEvent: () {
              setState(() {
                _toggleParticipants = !_toggleParticipants;
              });
            }),
        CustomElevatedButton(
            fontSize: 14.sp,
            text: 'CANCELAR COLA (${reservationsOnQueue.length})',
            backgroundColor: Colors.red,
            adjustoFit: true,
            textColor: Colors.white,
            pressEvent: () {
              var userID =
                  Provider.of<AuthProvider>(context, listen: false).userID;
              var userReservation = widget.item.reservations
                  .firstWhere((element) => userID == element.user!.id);
              widget.onCancel(userReservation.id, true);
            }),
      ],
    );
  }

  Widget _buildButtonsWithoutReservation(
    DateTime dateTimeTraining,
    int availablePlaces,
    List<TrainingReservation> reservationsOnQueue,
  ) {
    return Row(
        mainAxisAlignment: reservationsOnQueue.isNotEmpty && !_isPassedDate(dateTimeTraining)
            ? MainAxisAlignment.spaceBetween
            : MainAxisAlignment.end,
        children: [
          if (reservationsOnQueue.isNotEmpty && !_isPassedDate(dateTimeTraining))
            CustomElevatedButton(
                fontSize: 14.sp,
                text: 'MOSTRAR COLA',
                adjustoFit: true,
                backgroundColor: widget.color,
                textColor: Colors.white,
                pressEvent: () {
                  setState(() {
                    _toggleParticipants = !_toggleParticipants;
                  });
                }),
          availablePlaces <= 0 && !_isPassedDate(dateTimeTraining)
              ? CustomElevatedButton(
                  adjustoFit: true,
                  fontSize: 14.sp,
                  text: 'EN COLA (${reservationsOnQueue.length})',
                  isDisabled: _isPassedDate(dateTimeTraining),
                  backgroundColor: widget.color,
                  textColor: Colors.white,
                  pressEvent: () => widget.onReservation(widget.item.id, true))
              : CustomElevatedButton(
                  adjustoFit: true,
                  fontSize: 14.sp,
                  text: 'RESERVAR',
                  isDisabled: _isPassedDate(dateTimeTraining),
                  backgroundColor: widget.color,
                  textColor: Colors.white,
                  pressEvent: () => widget.onReservation(widget.item.id))
        ]);
  }

  Widget _buildButtons(
    List<TrainingReservation> reservations,
    List<TrainingReservation> reservationsOnQueue,
    DateTime dateTimeTraining,
    int availablePlaces,
  ) {
    if (_isUserReserverd(reservations) &&
        !_isUserReserverd(reservationsOnQueue)) {
      return _buildButtonUserReserved(dateTimeTraining);
    } else if (_isUserReserverd(reservationsOnQueue)) {
      return _buildButtonsUserOnQueue(reservationsOnQueue);
    } else {
      return _buildButtonsWithoutReservation(
          dateTimeTraining, availablePlaces, reservationsOnQueue);
    }
  }

  List<Padding> _buildParticipants(
      List<TrainingReservation> reservationsOnQueue) {
    return reservationsOnQueue.asMap().entries.map((element) {
      var index = element.key + 1;
      var participant = element.value.user;
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 4),
        child: Text(
              '$index.- ${participant!.profile!.name} ${participant.profile!.firstSurname}',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(color: Colors.black)),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    var occupiedPlaces =
        widget.item.reservations.where((element) => element.queue != 1).length;
    var capacity = widget.item.capacity;
    var reservations = widget.item.reservations;
    var reservationsWithoutQueue = widget.item.reservations.where((element) => element.queue == 0).toList();
    var reservationsWithType = widget.item.reservations.where((element) => element.queue == 0 && element.trainingType == widget.typeReservation.index).toList();
    var reservationsWithoutType = widget.item.reservations.where((element) => element.queue == 0 && element.trainingType != widget.typeReservation.index).toList();
    var availablePlaces = capacity - reservationsWithType.length;
    var startTime = widget.item.startTime.split(':');
    var dateTimeTraining = DateTime(widget.date.year, widget.date.month,
        widget.date.day, int.parse(startTime[0]), int.parse(startTime[1]));
    var reservationsOnQueue =
        reservations.where((element) => element.queue == 1).toList();
    reservations.sort((a, b) => a.trainingType.compareTo(b.trainingType));     
    reservationsOnQueue.sort((a, b) => a.updatedAt.compareTo(b.updatedAt));   
  
    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        padding: const EdgeInsets.only(bottom: 16, top: 0),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 3,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: VisibilityDetector(
          key: widget.globalKey,
          onVisibilityChanged: (visibilityInfo) {
            var visiblePercentage = visibilityInfo.visibleFraction * 100;
            if (visiblePercentage >= 75) {
              widget.onTimeSelected();
            }
          },
          child: Padding(
            padding: EdgeInsets.zero,
            child: Column(
              children: [
                HeaderReservation(
                  titleHeader: widget.titleHeader,
                  availablePlaces: availablePlaces,
                  time: widget.timeChip,
                  color: widget.color,
                  colorSecondary: widget.colorSecondary,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.6, left: 12, right: 12),
                  child: Column(
                    children: [
                      GridReservation(
                        capacity: capacity,
                        occupiedPlaces: occupiedPlaces,
                        reservations: reservationsWithoutQueue,
                        reservationsWithoutType: reservationsWithoutType,
                        color: widget.color,
                        time: widget.timeChip,
                        colorSecondary: widget.colorSecondary,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              width: double.infinity,
                              child: _buildButtons(
                                  reservations,
                                  reservationsOnQueue,
                                  dateTimeTraining,
                                  availablePlaces)),
                          if (_toggleParticipants)
                            ..._buildParticipants(reservationsOnQueue)
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }

  bool _isUserReserverd(
    List<TrainingReservation> reservations,
  ) {
    return Provider.of<ReservationsProvider>(context, listen: false)
        .isUserReserverd(reservations, context);
  }

  bool _isPassedDate(DateTime expirationDate) {
    return Provider.of<ReservationsProvider>(context, listen: false)
        .isPassedDate(expirationDate);
  }

  bool _isThereMoreThanCancelTime(Training training) {
    return Provider.of<ReservationsProvider>(context, listen: false)
        .isThereMoreThanCancelTime(training, context, widget.date);
  }
}
