import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/enums/reservation_type_enum.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/reservations/api/training_reservation.dart';
import 'package:monkeybox/pages/side-menu/athlete_profile/athlete_profile_page.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:visibility_detector/visibility_detector.dart';

class GridReservationItem extends StatefulWidget {
  final String tag;
  final TrainingReservation? item;
  final Color color;
  final int index;
  final int occupiedPlaces;
  final Color colorSecondary;

  const GridReservationItem({
    Key? key,
    required this.tag,
    required this.item,
    required this.occupiedPlaces,
    required this.index,
    required this.color,
    required this.colorSecondary,
  }) : super(key: key);

  @override
  _GridReservationItemState createState() => _GridReservationItemState();
}

class _GridReservationItemState extends State<GridReservationItem> {
  bool _imageLoad = false;

  SizedBox get _placeholderErrorImageWidget {
    return SizedBox(
        height: 75.w,
        width: 75.w,
        child: Icon(
          Icons.image,
          color: Colors.white,
          size: 48.sp,
        ));
  }

  Widget get _imageEmpty {
    return FittedBox(
      fit: BoxFit.cover,
      child: Icon(
        Icons.person_outline_rounded,
        color: widget.color,
        size: 100.sp,
      ),
    );
  }

  Color get _getColorTrainingType {
    // return widget.item!.trainingType == ReservationTypeEnum.wod.index
    //     ? HexColor('#0024db')
    //     : widget.item!.trainingType == ReservationTypeEnum.openBOX.index
    //         ? HexColor('#bc0000')
    //         : HexColor('#006400');
    return widget.item!.trainingType == ReservationTypeEnum.wod.index
        ? CustomExtraThemeData.getExtraThemeData().wodsColor!
        : widget.item!.trainingType == ReservationTypeEnum.openBOX.index
            ? CustomExtraThemeData.getExtraThemeData().openBoxColor!
            : HexColor('#006400');

  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (widget.item != null) {
          Navigator.of(context).pushNamed(AthleteProfilePage.routeName,
              arguments: {
                'profile': widget.item!.user!.profile,
                'tag': widget.tag
              });
        }
      },
      child: Container(
          color: widget.colorSecondary,
          child: widget.index < widget.occupiedPlaces
              ? Stack(children: [
                  Hero(
                    tag: widget.tag,
                    transitionOnUserGestures: true,
                    child: VisibilityDetector(
                      key: GlobalKey(),
                      onVisibilityChanged: (visibilityInfo) {
                        var visiblePercentage =
                            visibilityInfo.visibleFraction * 100;
                        if (visiblePercentage == 100) {
                          setState(() {
                            _imageLoad = true;
                          });
                        }
                      },
                      child: _imageLoad
                          ? CachedNetworkImage(
                              imageUrl: widget
                                  .item!.user!.profile!.resizedPictureUrl!,
                              fit: BoxFit.cover,
                              fadeInDuration: const Duration(milliseconds: 300),
                              fadeOutDuration:
                                  const Duration(milliseconds: 600),
                              placeholder: (ctx, url) =>
                                  _placeholderErrorImageWidget,
                              errorWidget: (ctx, url, _) =>
                                  _placeholderErrorImageWidget,
                            )
                          : _imageEmpty,
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      color: _getColorTrainingType,
                      alignment: Alignment.centerLeft,
                      height: 16.sp,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 4, right: 4, top: 4, bottom: 4),
                        child: FittedBox(
                          child: Text(
                            '${widget.item!.user!.profile!.name} ${widget.item!.user!.profile!.firstSurname}',
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context)
                                .textTheme
                                .headline3!
                                .copyWith(
                                    color: Colors.white,
                                    fontSize: 8.sp,
                                    height: 1),
                          ),
                        ),
                      ),
                    ),
                  )
                ])
              : _imageEmpty),
    );
  }
}
