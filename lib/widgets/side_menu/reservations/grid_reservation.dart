
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/reservations/api/training_reservation.dart';
import 'package:monkeybox/widgets/side_menu/reservations/grid_reservation_item.dart';
class GridReservation extends StatelessWidget {
  final int capacity;
  final int occupiedPlaces;
  final Color color;
  final Color colorSecondary;
  final String time;
  final List<TrainingReservation> reservations;
  final List<TrainingReservation> reservationsWithoutType;

  const GridReservation(
      {Key? key,
      required this.color,
      required this.colorSecondary,
      required this.capacity,
      required this.occupiedPlaces,
      required this.time,
      required this.reservations,
      required this.reservationsWithoutType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.only(top: 16),
        children: capacity > 0
            ? List.generate((reservationsWithoutType.length + capacity), (index) {
                var tag = 'athlete-profile-$time-$index';
                return GridReservationItem(
                    tag: tag,
                    item: index < reservations.length
                        ? reservations[index]
                        : null,
                    occupiedPlaces: occupiedPlaces,
                    index: index,
                    color: color,
                    colorSecondary: colorSecondary);
              }).toList()
            : [],
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 70.w,
            mainAxisExtent: 70.w,
            childAspectRatio: 1,
            crossAxisSpacing: 15.w,
            mainAxisSpacing: 15.w));
  }
}
