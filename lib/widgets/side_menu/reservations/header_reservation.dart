import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HeaderReservation extends StatelessWidget {
  final int availablePlaces;
  final String time;
  final Color color;
  final Color colorSecondary;
  final String titleHeader;

  const HeaderReservation(
      {Key? key,
      required this.availablePlaces,
      required this.time,
      required this.color,
      required this.colorSecondary,
      required this.titleHeader})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 40.sp,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: Container(
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.centerLeft,
            color: color,
            child: Padding(
              padding: const EdgeInsets.only(left: 14),
              child: Text(titleHeader,
                  style: Theme.of(context)
                      .textTheme
                      .headline2!
                      .copyWith(color: Colors.white)),
            ),
          )),
          Container(
            height: double.infinity,
            // width: double.infinity,
            alignment: Alignment.centerLeft,
            color: colorSecondary,
            child: Padding(
              padding: const EdgeInsets.only(left: 14, right: 14),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(time,
                      style: Theme.of(context)
                          .textTheme
                          .headline2!
                          .copyWith(color: Colors.white)),
                  SizedBox(
                    width: 8.sp,
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.person_rounded,
                        size: 27.5.sp,
                        color: Colors.white,
                      ),
                      Text(
                          availablePlaces <= 0
                              ? '0'
                              : availablePlaces.toString(),
                          style: Theme.of(context)
                              .textTheme
                              .headline2!
                              .copyWith(color: Colors.white)),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
