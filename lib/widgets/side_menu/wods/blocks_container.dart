import 'package:flutter/material.dart';
import 'package:monkeybox/models/wods/api/block.dart';
import 'package:monkeybox/widgets/side_menu/wods/block.dart';

class BlocksContainerWidget extends StatelessWidget {
  final List<Block> blocks;
  final bool isAddingRating;
  final List<FocusScope>? childrenBlocks;

  const BlocksContainerWidget(
      this.blocks, this.isAddingRating, this.childrenBlocks,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: blocks
          .asMap()
          .entries
          .map((e) => BlockWidget(e.value, isAddingRating,
              isAddingRating ? childrenBlocks![e.key] : null))
          .toList(),
    );
  }
}
