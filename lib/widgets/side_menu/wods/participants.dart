import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/reservations/api/reservation.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/models/wods/api/wod.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/pages/side-menu/athlete_profile/athlete_profile_page.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:provider/provider.dart';
import 'package:responsive_grid/responsive_grid.dart';

class ParticipantsWidget extends StatefulWidget {
  final Wod wod;
  ParticipantsWidget(this.wod, {Key? key}) : super(key: key);

  @override
  _ParticipantsWidgetState createState() => _ParticipantsWidgetState();
}

class _ParticipantsWidgetState extends State<ParticipantsWidget> {
  List<User> participants = [];
  bool loadingParticipants = false;
  int countParticipants = 0;

  @override
  void initState() {
    super.initState();
    loadParticipants();
  }

  @override
  void didUpdateWidget(ParticipantsWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!Provider.of<WodsProvider>(context, listen: false).isAddingRatingMode) {
      loadParticipants();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 16.0),
        width: double.infinity,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(color: Colors.grey.shade500, blurRadius: 10)
        ]),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Participantes ($countParticipants)'
                    .toUpperCase(),
                style: TextStyle(
                    fontSize: 20.sp, fontFamily: 'Oswald', color: Colors.black),
              ),
              loadingParticipants
                  ? CircularProgressIndicator()
                  : Container(
                      margin: EdgeInsets.only(top: 10.sp, bottom: 0),
                      child: participants.length == 0
                          ? Text(
                              'No hay participantes inscritos',
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  fontFamily: 'Oswald',
                                  color: Colors.black))
                          : ResponsiveGridList(
                              scroll: false,
                              desiredItemWidth: 50.sp,
                              minSpacing: 10.sp,
                              children: participants
                                  .map((user) => buildParticipant(user))
                                  .toList(),
                            ),
                    ),
            ],
          ),
        ));
  }

  Widget buildParticipant(User user) {
    return Container(
      width: 50.sp,
      child: Hero(
        tag: 'participant-wod-${user.id}',
        transitionOnUserGestures: true,
        child: InkWell(
          onTap: () {
            var menuProvider =
                Provider.of<MenuProvider>(context, listen: false);
            Navigator.of(menuProvider.navigatorKey.currentContext!)
                .pushNamed(AthleteProfilePage.routeName, arguments: {
              'profile': user.profile,
              'tag': 'participant-wod-${user.id}'
            });
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 50.sp,
                width: 50.sp,
                decoration: BoxDecoration(
                  color: Theme.of(appNavigatorKey.currentContext!).primaryColor,
                  image: DecorationImage(
                      image: NetworkImage(user.profile!.resizedPictureUrl!),
                      fit: BoxFit.cover),
                ),
              ),
              SizedBox(
                height: 5.sp,
              ),
              Text(
                user.profile!.name,
                textAlign: TextAlign.left,
                softWrap: true,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 10.sp, color: Colors.black),
              ),
            ],
          ),
        ),
      ),
    );
  }

  loadParticipants() {
    setState(() {
      loadingParticipants = true;
    });
    Provider.of<WodsProvider>(context, listen: false)
        .getReservationAndTraining(widget.wod.boxId, widget.wod.date)
        .then((response) {
      if (mounted) {
        setState(() {
          loadingParticipants = false;
        });
        if (response.success! && response.status == 200) {
          List<Reservation> reservations = [];
          for (var reservation in response.data) {
            reservations.add(Reservation.fromJson(reservation));
          }
          setState(() {
            participants = reservations
                .takeWhile((reservation) =>
                    reservation.queue != null && reservation.queue == 0)
                .map((reservation) => reservation.user!)
                .toList();
            countParticipants = participants.length;
          });
        }
      }
    });
  }
}
