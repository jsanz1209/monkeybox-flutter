import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class WarmWidget extends StatefulWidget {
  final String warm;
  WarmWidget(this.warm, {Key? key}) : super(key: key);

  @override
  State<WarmWidget> createState() => _WarmWidgetState();
}

class _WarmWidgetState extends State<WarmWidget> {
  YoutubePlayerController? _controller;

  @override
  void dispose() {
    if (_controller != null) {
      _controller!.close();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0.sp),
      child: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(color: Colors.grey.shade500, blurRadius: 10)
        ]),
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(top: kToolbarHeight),
              padding: EdgeInsets.only(
                  left: 8.sp, right: 8.sp, bottom: 8.sp, top: 0.sp),
              child: Html(
                data: widget.warm,
                customRender: {
                  'img': (final renderContext, final widget) {
                    return Container(
                      margin: EdgeInsets.only(top: 8.sp),
                      child: Center(
                        child: CachedNetworkImage(
                          imageUrl:
                              renderContext.tree.attributes['src'].toString(),
                        ),
                      ),
                    );
                  },
                  'iframe': (final renderContext, final widget) {
                    final src = renderContext.tree.attributes['src'].toString();
                    var id = src.split('embed/')[1];
                    if (_controller != null) {
                      _controller!.close();
                    }
                    _controller = YoutubePlayerController(
                        initialVideoId: id,
                        params: YoutubePlayerParams(
                          showControls: true,
                          showFullscreenButton: true,
                          desktopMode: false,
                          autoPlay: false,
                        ));
                    var youtube = YoutubePlayerIFrame(
                      controller: _controller,
                      aspectRatio: 16 / 9,
                      gestureRecognizers: {},
                    );
                    return youtube;
                  }
                },
                style: {
                  'img': Style(
                    display: Display.BLOCK,
                    margin: EdgeInsets.only(bottom: 14.sp, top: 14.sp),
                  ),
                  'span': Style(
                    fontSize: FontSize.rem(0.9.sp),
                    margin: EdgeInsets.zero,
                  ),
                  'p': Style(
                      // lineHeight: LineHeight.normal,
                      fontSize: FontSize.rem(0.9.sp),
                      margin: EdgeInsets.zero,
                      padding: EdgeInsets.zero),
                  '.youtube-embed-wrapper':
                      Style(padding: EdgeInsets.only(top: 12.sp))
                },
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.primaryVariant),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: 8.0.sp, vertical: 14.sp),
                child: Text(
                  'Calentamiento',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Oswald',
                      fontSize: 16.sp),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
