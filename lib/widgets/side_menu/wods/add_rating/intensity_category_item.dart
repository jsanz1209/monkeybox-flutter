import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/wods/api/intensity_category.dart';

class IntensityCategoryItem extends StatefulWidget {
  final IntensityCategory item;
  final int selectedID;
  final Function(int) onSelectedIntensityCategory;
  const IntensityCategoryItem(
      {Key? key,
      required this.item,
      required this.selectedID,
      required this.onSelectedIntensityCategory})
      : super(key: key);

  @override
  _IntensityCategoryItemState createState() => _IntensityCategoryItemState();
}

class _IntensityCategoryItemState extends State<IntensityCategoryItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 8.sp),
      child: ActionChip(
        label: Text(
          widget.item.category,
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
              color: widget.selectedID == widget.item.id
                  ? Colors.white
                  : Colors.grey),
        ),
        onPressed: () {
          setState(() {
            widget.onSelectedIntensityCategory(widget.item.id);
          });
        },
        backgroundColor: widget.selectedID == widget.item.id
            ? Theme.of(context).colorScheme.primary
            : Colors.white,
        visualDensity: VisualDensity.compact,
        labelPadding: EdgeInsets.symmetric(vertical: 0.sp, horizontal: 8.sp),
      ),
    );
  }
}
