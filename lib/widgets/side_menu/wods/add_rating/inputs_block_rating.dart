import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/wods/api/block.dart';
import 'package:monkeybox/models/wods/api/rating_type.dart';
import 'package:monkeybox/widgets/common/form/custom_checkbox.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';

class InputsBlockRating extends StatefulWidget {
  final Block block;
  final bool isCompleted;
  final String? observations;
  final int indexBlock;
  final List<GlobalKey<FormState>> formKeys;
  final Map<String, GlobalKey<FormFieldState<dynamic>>> fields;
  final List<Map> datas;
  final List<bool?> onNotCompleteBlocks;
  final List<Widget> widgets;
  final List<RatingType> ratingTypes;

  InputsBlockRating({
    Key? key,
    required this.block,
    required this.isCompleted,
    required this.observations,
    required this.indexBlock,
    required this.widgets,
    required this.formKeys,
    required this.fields,
    required this.datas,
    required this.onNotCompleteBlocks,
    required this.ratingTypes
  }) : super(key: key);

  @override
  _InputsBlockRatingState createState() => _InputsBlockRatingState();
}

class _InputsBlockRatingState extends State<InputsBlockRating> {
  bool? _toggleIsComplete;

  @override
  void initState() {
    super.initState();
    _toggleIsComplete = widget.isCompleted;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> displayedWidgets = [];
    if (_toggleIsComplete!) {
      displayedWidgets = [...widget.widgets ];
    }
      displayedWidgets.add(
        CustomCheckbox(
            isBlackTheme: true,
            initialValue: !widget.isCompleted,
            onChange: (value) {
              setState(() {
                widget.onNotCompleteBlocks[widget.indexBlock] = !value;
                _toggleIsComplete = !_toggleIsComplete!;
              });
            },
            onSave: (value) {
              widget.datas[widget.indexBlock]['not_completed'] = value!;
            },
            textWiget: Text('No completado / No realizado',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.black))),
      );

      displayedWidgets.add(SizedBox(
        height: 24.sp,
      ));

      displayedWidgets.add(CustomTextFormField(
          key: widget.fields['observations'],
          label: 'Observaciones',
          fieldName: 'observations',
          isBlackTheme: true,
          maxLines: 8,
          initialValue: widget.observations,
          onSave: (value) {
            widget.datas[widget.indexBlock]['observations'] = value!;
          },
          onBlur: () {
            widget.formKeys[widget.indexBlock].currentState!.validate();
          },
          keyboardType: TextInputType.multiline));
      displayedWidgets.add(SizedBox(
        height: 24.sp,
      ));
    

    widget.datas[widget.indexBlock]['blockID'] = widget.block.id;
    widget.datas[widget.indexBlock]['ratingTypes'] = widget.ratingTypes;

    return Column(children: displayedWidgets);
  }
}
