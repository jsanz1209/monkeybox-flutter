import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class AnimatedMoodButton extends StatefulWidget {
  final Function onPressOtherBtn;
  final Color color;
  final int id;
  final String icon;
  final String text;
  final bool isOtherBtnPressed;

  const AnimatedMoodButton(
      {Key? key,
      required this.id,
      required this.onPressOtherBtn,
      required this.color,
      required this.icon,
      required this.text,
      required this.isOtherBtnPressed})
      : super(key: key);

  @override
  State<AnimatedMoodButton> createState() => _AnimatedMoodButtonState();
}

class _AnimatedMoodButtonState extends State<AnimatedMoodButton>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    this._controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      reverseDuration: const Duration(milliseconds: 500),
      vsync: this,
    );
  }

  @override
  void dispose() {
    // Dispose controller to release resources.
    this._controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(AnimatedMoodButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isOtherBtnPressed) {
      _controller.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    final sizeAnimation = Tween<double>(begin: 1, end: 1.25).animate(
        CurvedAnimation(
            parent: _controller,
            curve: Curves.linear,
            reverseCurve: Curves.linear));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AnimatedBuilder(
          animation: sizeAnimation,
          builder: (context, child) {
            return Container(
                margin:
                    EdgeInsets.only(top: widget.isOtherBtnPressed ? 0 : 16.sp),
                child:
                    Transform.scale(scale: sizeAnimation.value, child: child));
          },
          child: IconButton(
            onPressed: () {
              _controller.forward();
              widget.onPressOtherBtn();
            },
            iconSize: 64.sp,
            color: Colors.black,
            icon: SvgPicture.asset(
              widget.icon,
              color: widget.isOtherBtnPressed ? Colors.black : widget.color,
              height: 64.sp,
              width: 64.sp,
            ),
          ),
        ),
        if (!widget.isOtherBtnPressed)
          Padding(
            padding: EdgeInsets.only(top: 8.sp),
            child: Text(
              widget.text,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: widget.color, fontWeight: FontWeight.bold),
            ),
          )
      ],
    );
  }
}
