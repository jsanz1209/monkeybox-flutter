import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AnimatedEffortButton extends StatefulWidget {
  final String text;
  final int id;
  final Function onPressOtherBtn;
  final Color color;
  final bool isOtherBtnPressed;

  const AnimatedEffortButton(
      {Key? key,
      required this.id,
      required this.text,
      required this.onPressOtherBtn,
      required this.color,
      required this.isOtherBtnPressed})
      : super(key: key);

  @override
  State<AnimatedEffortButton> createState() => _AnimatedEffortButtonState();
}

class _AnimatedEffortButtonState extends State<AnimatedEffortButton>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    this._controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      reverseDuration: const Duration(milliseconds: 500),
      vsync: this,
    );
  }

  @override
  void dispose() {
    // Dispose controller to release resources.
    this._controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(AnimatedEffortButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isOtherBtnPressed) {
      _controller.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    final sizeAnimation = Tween<double>(begin: 1, end: 1.1).animate(
        CurvedAnimation(
            parent: _controller,
            curve: Curves.linear,
            reverseCurve: Curves.linear));
    return AnimatedBuilder(
      animation: sizeAnimation,
      builder: (context, child) {
        return Transform.scale(scale: sizeAnimation.value, child: child);
      },
      child: ElevatedButton(
          onPressed: () {
            _controller.forward();
            widget.onPressOtherBtn();
          },
          child: FittedBox(
            child: Text(
              widget.text,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: Colors.black, fontSize: 14.sp, fontWeight: widget.isOtherBtnPressed ? FontWeight.normal : FontWeight.bold),
            ),
          ),
          style: ElevatedButton.styleFrom(
            primary: widget.color,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(
                    width: 2.sp,
                      color: widget.isOtherBtnPressed
                          ? Colors.transparent
                          : Colors.black)))),
    );
  }
}
