import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/wods/api/wod.dart';
import 'package:monkeybox/providers/app/app_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:monkeybox/widgets/side_menu/wods/add_rating/intensity_category_item.dart';
import 'package:provider/provider.dart';

class DateIntensityCategory extends StatefulWidget {
  final Wod wod;
  final Function(int) onSelectedIntensityCategory;
  const DateIntensityCategory(
      {Key? key, required this.wod, required this.onSelectedIntensityCategory})
      : super(key: key);

  @override
  State<DateIntensityCategory> createState() => _DateIntensityCategoryState();
}

class _DateIntensityCategoryState extends State<DateIntensityCategory> {
  int? _selectedID;

  @override
  void initState() {
    super.initState();
    _getSelectedCategory();
  }

  _getSelectedCategory() {
    var user = Provider.of<MenuProvider>(context, listen: false).user;
    setState(() {
      _selectedID = Provider.of<WodsProvider>(context, listen: false)
          .getSelectedIntesityCategory(user);
    });
    var intensityCategories =
        Provider.of<AppProvider>(context, listen: false).intensitiesCategory;
    var categoryDefault = user!.profile!.intensityCategory;
    setState(() {
      if (_selectedID == null) {
        if (categoryDefault != null) {
          _selectedID = categoryDefault.id;
        } else {
          _selectedID = intensityCategories
              .firstWhere((element) => element.category == 'RX')
              .id;
        }
      }
      widget.onSelectedIntensityCategory(_selectedID!);
    });
  }

  @override
  Widget build(BuildContext context) {
    var intensityCategories =
        Provider.of<AppProvider>(context, listen: false).intensitiesCategory;
    final String languageCode = Localizations.localeOf(context).languageCode;
    return Container(
      width: double.infinity,
      color: HexColor('#e5e5e5'),
      padding: EdgeInsets.symmetric(vertical: 16.sp, horizontal: 25.6.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 8.sp,
          ),
          Text('Fecha',
              style: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(fontSize: 13.sp)),
          SizedBox(
            height: 3.sp,
          ),
          Text(
              DateFormat('EEEE dd MMMM yyyy', languageCode)
                  .format(DateTime.parse(widget.wod.date)),
              style: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(color: Colors.black)),
          SizedBox(
            height: 16.sp,
          ),
          Text('Categoría',
              style: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(fontSize: 13.sp)),
          Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: intensityCategories.map((item) {
                return IntensityCategoryItem(
                  item: item,
                  onSelectedIntensityCategory: (value) {
                    setState(() {
                      _selectedID = value;
                    });
                    widget.onSelectedIntensityCategory(value);
                  },
                  selectedID: _selectedID!,
                );
              }).toList())
        ],
      ),
    );
  }
}
