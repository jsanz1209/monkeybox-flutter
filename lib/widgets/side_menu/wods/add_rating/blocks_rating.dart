import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/common/item_dropdown_multiple.dart';
import 'package:monkeybox/models/reservations/api/reservation.dart';
import 'package:monkeybox/models/wods/api/block.dart';
import 'package:monkeybox/models/wods/api/rating.dart';
import 'package:monkeybox/models/wods/api/rating_type.dart';
import 'package:monkeybox/models/wods/api/wod.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/side_menu/wods/add_rating/date_intensity_category.dart';
import 'package:monkeybox/widgets/side_menu/wods/add_rating/inputs_block_rating.dart';
import 'package:monkeybox/widgets/side_menu/wods/blocks_container.dart';
import 'package:provider/provider.dart';

class BlocksRatings extends StatefulWidget {
  final Wod wod;
  final Function(List<Map>, int) onSubmit;
  final Function(List<Rating>) onDeleteRatings;
  const BlocksRatings(
      {Key? key,
      required this.wod,
      required this.onSubmit,
      required this.onDeleteRatings})
      : super(key: key);

  @override
  State<BlocksRatings> createState() => _BlocksRatingsState();
}

class _BlocksRatingsState extends State<BlocksRatings> {
  late List<bool?> _onNotCompleteBlocks;
  List<GlobalKey<FormState>> _formKeys = [];
  List<Map> _datas = [];
  List<Reservation> _participants = [];
  late Future _futureFetchParticipantsData;
  int? _intensityCategory;

  @override
  void initState() {
    super.initState();
    var wodsProvider = Provider.of<WodsProvider>(context, listen: false);
    _futureFetchParticipantsData = isThereAnyTeamBlock
        ? wodsProvider.getParticipants(widget.wod.date)
        : Future.value([]);
    var blocksWithRatingTypes = wodsProvider.blocksWithRatingTypes;
    _onNotCompleteBlocks = List.generate(blocksWithRatingTypes.length, (index) {
      return null;
    });
  }

  @override
  Widget build(BuildContext context) {
    var blocksWithRatingTypes =
        Provider.of<WodsProvider>(context, listen: false).blocksWithRatingTypes;
    var user = Provider.of<MenuProvider>(context, listen: false).user;
    var ratingsUser =
        Provider.of<WodsProvider>(context, listen: false).getRatingUser(user);
    _generateDataForms(blocksWithRatingTypes.length);
    return FutureBuilder(
        future: _futureFetchParticipantsData,
        builder: (ctx, asyncSnapshot) {
          if (asyncSnapshot.connectionState == ConnectionState.done) {
            _participants = asyncSnapshot.data != null &&
                    (asyncSnapshot.data as List).isNotEmpty
                ? asyncSnapshot.data as List<Reservation>
                : [];
            return Column(
              children: [
                if (blocksWithRatingTypes.isNotEmpty)
                  DateIntensityCategory(
                    wod: widget.wod,
                    onSelectedIntensityCategory: (value) {
                      _intensityCategory = value;
                    },
                  ),
                Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 16.sp, vertical: 8.sp),
                  padding: EdgeInsets.only(bottom: 16.sp),
                  child: Column(
                    children: [
                      blocksWithRatingTypes.isNotEmpty
                          ? BlocksContainerWidget(
                              blocksWithRatingTypes,
                              true,
                              List.generate(blocksWithRatingTypes.length,
                                  (index) {
                                return _getInputsBlock(
                                    context,
                                    index,
                                    blocksWithRatingTypes[index],
                                    ratingsUser
                                        .where((element) =>
                                            element.blockID ==
                                            blocksWithRatingTypes[index].id)
                                        .toList());
                              }).toList())
                          : const SizedBox(),
                      Consumer<WodsProvider>(
                        builder: (context, model, child) {
                          return CustomElevatedButton(
                              text: 'GUARDAR',
                              backgroundColor: Theme.of(context).primaryColor,
                              textColor: Colors.white,
                              isDisabled: !model.isFeedbackFilled,
                              pressEvent: _onSubmit);
                        }
                      ),
                      if (ratingsUser.isNotEmpty)
                        CustomElevatedButton(
                            text: 'ELIMINAR',
                            backgroundColor: Colors.white,
                            textColor: Colors.red,
                            borderColor: Colors.red,
                            borderWidth: 2.sp,
                            pressEvent: () {
                              widget.onDeleteRatings(ratingsUser);
                            }),
                      CustomElevatedButton(
                          text: 'CANCELAR',
                          backgroundColor: Colors.white,
                          textColor: Theme.of(context).primaryColor,
                          borderColor: Theme.of(context).primaryColor,
                          borderWidth: 2.sp,
                          pressEvent: () {
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                ),
              ],
            );
          } else {
            return const SizedBox();
          }
        });
  }

  bool get isThereAnyTeamBlock {
    return widget.wod.blocks
        .where((element) => element.isTeam == 1)
        .toList()
        .isNotEmpty;
  }

  void _generateDataForms(int count) {
    if (count != 0) {
      _formKeys = List.generate(count, (index) {
        return GlobalKey<FormState>();
      }).toList();

      _datas = List.generate(count, (index) {
        return Map();
      }).toList();
    }
  }

  FocusScope _getInputsBlock(BuildContext context, int indexBlock, Block block,
      List<Rating> blockRatingsUser) {
    List<Widget> widgets = [];
    bool isCompleted = true;
    bool isTeam = block.isTeam == 1 ? true : false;
    String? observations;
    List<ItemDropdownMultiple> users = [];
    List<RatingType> ratingTypes = [];
    FocusScopeNode focusNode = FocusScopeNode();

    users = _getParticipants(blockRatingsUser, null);
    final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields = {};
    _fields['observations'] = GlobalKey<FormFieldState>();

    for (var index = 0; index < block.ratingTypes.length; index++) {
      var ratingType = block.ratingTypes[index];
      String? ratingFormat;
      ratingTypes.add((ratingType));
      if (blockRatingsUser.isNotEmpty && index < blockRatingsUser.length) {
        if (_datas[indexBlock]['idRatings'] == null) {
          _datas[indexBlock]['idRatings'] = [];
        }
        _datas[indexBlock]['idRatings'].add(blockRatingsUser[index].id);
        _datas[indexBlock]['isUpdateRating'] = true;
        isCompleted = _onNotCompleteBlocks[indexBlock] ??
            (blockRatingsUser[index].isComplete == 1 ? true : false);
        observations = blockRatingsUser[index].observations;
        ratingFormat = blockRatingsUser[index].rating.toString();
        users = _getParticipants(blockRatingsUser, index);
      } else {
        isCompleted = _onNotCompleteBlocks[indexBlock] ?? true;
      }

      Provider.of<WodsProvider>(context, listen: false)
          .setupInputsBasedOnRating(
              ratingType,
              ratingFormat,
              isTeam,
              _fields,
              widgets,
              _datas[indexBlock],
              users,
              _formKeys[indexBlock],
              focusNode);
    }
    return FocusScope(
      node: focusNode,
      child: Form(
          key: _formKeys[indexBlock],
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                InputsBlockRating(
                  key: UniqueKey(),
                  widgets: widgets,
                  block: block,
                  datas: _datas,
                  fields: _fields,
                  formKeys: _formKeys,
                  indexBlock: indexBlock,
                  isCompleted: isCompleted,
                  observations: observations,
                  onNotCompleteBlocks: _onNotCompleteBlocks,
                  ratingTypes: ratingTypes,
                )
              ],
            ),
          )),
    );
  }

  _getParticipants(List<Rating> blockRatingsUser, int? index) {
    var userID = Provider.of<MenuProvider>(context, listen: false).user!.id;
    List<ItemDropdownMultiple> users = _participants.map((element) {
      var id = element.user!.id;
      var fullName = element.user!.profile!.getFullName();
      var imageUrl = element.user!.profile!.resizedPictureUrl;
      var isChecked = index != null
          ? (blockRatingsUser[index].team ?? [])
              .where((item) {
                return item.userID == userID || id == item.userID;
              })
              .toList()
              .isNotEmpty
          : false;
      return ItemDropdownMultiple(id, fullName, imageUrl, isChecked);
    }).toList();
    users.removeWhere((element) => element.id == userID);
    return users;
  }

  void _onSubmit() {
    bool allIsValid = true;
    if (_formKeys.isNotEmpty) {
      for (var i = 0; i < _formKeys.length; i++) {
        var formKey = _formKeys[i];
        final isValid = formKey.currentState!.validate();
        debugPrint(isValid.toString());
        if (isValid) {
          formKey.currentState!.save();
        } else {
          allIsValid = false;
        }
      }
    }
    if (allIsValid) {
      widget.onSubmit(_datas, _intensityCategory!);
    }
  }
}
