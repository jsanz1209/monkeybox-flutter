import 'package:flutter/material.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/wods/mood_button.dart';
import 'package:monkeybox/widgets/side_menu/wods/add_rating/animated_mood_button.dart';

class MoodButtons extends StatefulWidget {
  final int? mood;
  final Function(int) onMoodSelected;
  const MoodButtons(
      {Key? key, required this.mood, required this.onMoodSelected})
      : super(key: key);

  @override
  _MoodButtonsState createState() => _MoodButtonsState();
}

class _MoodButtonsState extends State<MoodButtons> {
  List<bool> _otherBtnPressed = List.generate(3, (index) => true);
  List<MoodButton> _buttons = [
    MoodButton(
        id: 3,
        text: 'Feliz',
        color: HexColor('#1de11d'),
        icon: 'assets/images/icons/happy-outline.svg'),
    MoodButton(
        id: 2,
        text: 'Neutral',
        color: Colors.orange,
        icon: 'assets/images/icons/neutral-outline.svg'),
    MoodButton(
        id: 1,
        text: 'Triste',
        color: Colors.red,
        icon: 'assets/images/icons/sad-outline.svg'),
  ];

  @override
  void initState() {
    super.initState();
    if (widget.mood != null) {
      var newListOtherButtonPressed =
          _otherBtnPressed.asMap().entries.map((entry) {
        if (_buttons[entry.key].id == widget.mood) {
          return false;
        } else {
          return true;
        }
      }).toList();
      _otherBtnPressed = newListOtherButtonPressed;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buttons.asMap().entries.map((entryBtn) {
            var index = entryBtn.key;
            return AnimatedMoodButton(
              id: index,
              text: entryBtn.value.text,
              icon: entryBtn.value.icon,
              color: entryBtn.value.color,
              isOtherBtnPressed: _otherBtnPressed[index],
              onPressOtherBtn: () {
                var newListOtherButtonPressed =
                    _otherBtnPressed.asMap().entries.map((entry) {
                  if (entry.key == index) {
                    return false;
                  } else {
                    return true;
                  }
                }).toList();
                setState(() {
                  _otherBtnPressed = newListOtherButtonPressed;
                });
                widget.onMoodSelected(_buttons[index].id);
              },
            );
          }).toList()),
    );
  }
}
