import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/wods/effort_button.dart';
import 'package:monkeybox/widgets/side_menu/wods/add_rating/animated_effort_button.dart';

class EffortButtons extends StatefulWidget {
  final int? effort;
  final Function(int) onEffortSelected;
  const EffortButtons({Key? key, required this.effort, required this.onEffortSelected}) : super(key: key);

  @override
  _EffortButtonsState createState() => _EffortButtonsState();
}

class _EffortButtonsState extends State<EffortButtons> {
  List<bool> _otherBtnPressed = List.generate(10, (index) => true);
  List<EffortButton> _buttons = [
    EffortButton(id: 1, text: 'Muy, muy fácil', color: HexColor('#77e3e1')),
    EffortButton(id: 2,  text: 'Fácil', color: HexColor('#8ee5ba')),
    EffortButton(id: 3, text: 'Moderada', color: HexColor('#77d7d6')),
    EffortButton(id: 4, text: 'Un poco intensa', color: HexColor('#93d148')),
    EffortButton(id: 5, text: 'Intensa', color: HexColor('#f6cd51')),
    EffortButton(id: 6, text: 'Bastante intensa', color: HexColor('#f6cd51')),
    EffortButton(id: 7, text: 'Muy intensa', color: HexColor('#ed9c44')),
    EffortButton(id: 8, text: 'Extremadamente intensa', color: HexColor('#ed9c44')),
    EffortButton(id: 9, text: 'Casi al máximo', color: HexColor('#e9633a')),
    EffortButton(id: 10, text: 'Máximo', color: HexColor('#c02830'))
  ];

  @override
  void initState() {
    super.initState();
    if (widget.effort != null) {
      var newListOtherButtonPressed =
          _otherBtnPressed.asMap().entries.map((entry) {
        if (_buttons[entry.key].id == widget.effort) {
          return false;
        } else {
          return true;
        }
      }).toList();
      _otherBtnPressed = newListOtherButtonPressed;
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width * 0.43;

    return SizedBox(
      width: double.infinity,
      child: Wrap(
          alignment: WrapAlignment.spaceAround,
          direction: Axis.horizontal,
          runSpacing: 10,
          children: _buttons.asMap().entries.map((entryBtn) {
            var index = entryBtn.key;
            return Container(
              width: width,
              height: 44.sp,
              child: AnimatedEffortButton(
                id: index,
                text: entryBtn.value.text,
                color: entryBtn.value.color,
                isOtherBtnPressed: _otherBtnPressed[index],
                onPressOtherBtn: () {
                  var newListOtherButtonPressed =
                      _otherBtnPressed.asMap().entries.map((entry) {
                    if (entry.key == index) {
                      return false;
                    } else {
                      return true;
                    }
                  }).toList();
                  setState(() {
                    _otherBtnPressed = newListOtherButtonPressed;
                  });
                  widget.onEffortSelected(_buttons[index].id);
                },
              ),
            );
          }).toList()),
    );
  }
}
