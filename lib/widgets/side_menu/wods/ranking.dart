import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:monkeybox/models/user/api/profile.dart';
import 'package:monkeybox/models/wods/api/block.dart';
import 'package:monkeybox/models/wods/api/intensity_category.dart';
import 'package:monkeybox/models/wods/api/rating.dart';
import 'package:monkeybox/models/wods/api/rating_type.dart';
import 'package:monkeybox/models/wods/api/wod.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/pages/side-menu/athlete_profile/athlete_profile_page.dart';
import 'package:monkeybox/providers/app/app_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:provider/provider.dart';

class RankingWidget extends StatefulWidget {
  final Wod wod;
  RankingWidget(this.wod, {Key? key}) : super(key: key);

  @override
  _RankingWidgetState createState() => _RankingWidgetState();
}

class _RankingWidgetState extends State<RankingWidget> {
  bool loadingRanking = false;
  Map<String, dynamic> ranking = Map<String, dynamic>();

  @override
  void initState() {
    super.initState();
    loadRanking();
  }

  @override
  void didUpdateWidget(RankingWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!Provider.of<WodsProvider>(context, listen: false).isAddingRatingMode) {
      loadRanking();
    }
  }

  @override
  Widget build(BuildContext context) {
    return _isThereAnyRanking
        ? Container(
            alignment: Alignment.centerLeft,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
            child: Padding(
              padding: EdgeInsets.only(
                  top: 24.0.sp, left: 16.sp, right: 16.sp, bottom: 64.sp),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: buildRanking(),
              ),
            ),
          )
        : const SizedBox();
  }

  buildRanking() {
    List<Widget> rankingElements = [
      Text(
        'Ranking diario',
        textAlign: TextAlign.left,
        style: TextStyle(
            color: Colors.white, fontSize: 20.sp, fontFamily: 'Oswald'),
      ),
      SizedBox(
        height: 16.sp,
      ),
    ];
    List<IntensityCategory> categoriesToShow =
        Provider.of<AppProvider>(context, listen: false)
            .intensitiesCategory
            .where((category) {
      return ranking[category.category] != null &&
          ranking[category.category].length != 0;
    }).toList();
    rankingElements.addAll(categoriesToShow
        .map((category) => buildRankingItemContainer(category))
        .toList());

    return rankingElements;
  }

  Widget buildRankingItemContainer(IntensityCategory category) {
    List<Widget> containerElements = [
      Container(
          padding: EdgeInsets.all(8.sp),
          decoration: BoxDecoration(color: Colors.grey.shade600),
          child: Center(
              child: Text(category.category.toUpperCase(),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.sp,
                      fontFamily: 'Oswald')))),
    ];
    containerElements.addAll(widget.wod.blocks
        .where((e) {
          Map rankingCategory = ranking[category.category] as Map;
          var infoCategoryBlock = rankingCategory[e.title];
          return e.ratingTypes.length > 0 &&
              infoCategoryBlock != null &&
              infoCategoryBlock.isNotEmpty;
        })
        .map((e) => buildRankingItemBlockContainer(e, category))
        .toList());
    return Padding(
      padding: const EdgeInsets.only(bottom: 12.0),
      child: Column(
        children: containerElements,
      ),
    );
  }

  Widget buildRankingItemBlockContainer(
      Block block, IntensityCategory category) {
    var userItemsAndLength = _getUserItemsAndLength(block, category);
    var userLists = userItemsAndLength[0];
    var numItems = userItemsAndLength[1];
    return Container(
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(5.sp),
              decoration: BoxDecoration(color: Colors.grey.shade300),
              child: Column(
                children: [
                  Center(
                      child: Text(
                    block.title.toUpperCase(),
                    style: TextStyle(
                        color: Colors.grey.shade600,
                        fontFamily: 'Oswald',
                        fontSize: 12.sp),
                  )),
                  SizedBox(
                    height: 4.sp,
                  ),
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: block.ratingTypes.map((ratingType) {
                        return Text(ratingType.name.toUpperCase(),
                            style: TextStyle(
                                fontSize: 16.sp,
                                fontFamily: 'Oswald',
                                color: Colors.black));
                      }).toList(),
                    ),
                  ),
                ],
              ),
            ),
            _getBuildUserItemWidgets(block, userLists, numItems)
          ],
        ));
  }

  _getUserItemsAndLength(Block block, IntensityCategory category) {
    var numItems = 0;
    List<List<Widget>> userLists = [];
    block.ratingTypes.forEach((ratingType) {
      List<Widget> userList = buildUserList(block, category, ratingType);
      userLists.add(userList);
      var length = userList.length;
      numItems = length > numItems ? length : numItems;
    });
    return [userLists, numItems];
  }

  Widget _getBuildUserItemWidgets(
      Block block, List<List<Widget>> userLists, int numItems) {
    if (block.isTeam == 1) {
      return Container(
        decoration: BoxDecoration(color: Colors.white),
        height: numItems * 70,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: block.ratingTypes.asMap().entries.map((entry) {
            return SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: userLists.elementAt(entry.key)),
            );
          }).toList(),
        ),
      );
    } else {
      return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: block.ratingTypes.length > 1
              ? MainAxisAlignment.spaceAround
              : MainAxisAlignment.start,
          children: block.ratingTypes.asMap().entries.map((entry) {
            return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: userLists.elementAt(entry.key));
          }).toList());
    }
  }

  List<Widget> buildUserList(
      Block block, IntensityCategory category, RatingType ratingType) {
    List<Widget> elements = [];
    try {
      var data = ranking[category.category][block.title.toUpperCase()]
          [ratingType.name];
      int position = 1;
      if (data != null) {
        for (var item in data) {
          elements.add(
              buildUserItem(Rating.fromJson(item), position++, ratingType));
        }
      }
    } catch (_) {}
    return elements;
  }

  double getOpacityPosition(int position) {
    if (position >= 1 && position <= 3) {
      return (1 - (position / 10) > 0 ? 1 - (position / 10) : 0.1);
    }
    return 0.5;
  }

  Widget buildUserItem(Rating data, int position, RatingType ratingType) {
    Profile profile = data.userInfo;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Wrap(
        children: [
          Container(
            width: 17.sp,
            height: 35.sp,
            decoration: BoxDecoration(
                color: Theme.of(appNavigatorKey.currentContext!)
                    .primaryColor
                    .withOpacity(getOpacityPosition(position)),
                borderRadius: BorderRadius.all(Radius.elliptical(10, 10))),
            child: Center(
                child: FittedBox(
              child: Text(position.toString() + 'º',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12.sp,
                      fontFamily: 'Oswald')),
            )),
          ),
          SizedBox(
            width: 4.sp,
          ),
          Row(
            children: [
              _buildItemUserRaking(profile, position, ratingType),
              if (data.team!.isNotEmpty)
                ...data.team!.map((team) {
                  return Row(children: [
                    SizedBox(width: 8.sp),
                    _buildItemUserRaking(team, position, ratingType),
                  ]);
                }).toList(),
              Transform.translate(
                  offset: Offset(-10, 0),
                  child: Text(
                      formatValueFromRanking(
                          ratingType.id, data.rating.toString()),
                      style: TextStyle(
                          fontWeight: FontWeight.w800,
                          color: Colors.black,
                          fontSize: 12.sp))),
            ],
          )
        ],
      ),
    );
  }

  Container _buildItemUserRaking(
      Profile profile, int position, RatingType ratingType) {
    var tag = 'ranking-wod-${profile.userID}-$position-${ratingType.id}';
    return Container(
        width: 48.sp,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Hero(
                  tag: tag,
                  child: InkWell(
                    onTap: () {
                      var menuProvider =
                          Provider.of<MenuProvider>(context, listen: false);
                      Navigator.of(menuProvider.navigatorKey.currentContext!)
                          .pushNamed(AthleteProfilePage.routeName,
                              arguments: {'profile': profile, 'tag': tag});
                    },
                    child: Container(
                      height: 35.sp,
                      width: 35.sp,
                      decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.secondary,
                          image: DecorationImage(
                            image: CachedNetworkImageProvider(
                                profile.resizedPictureUrl!),
                            fit: BoxFit.cover,
                          )),
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 16.sp,
                      height: 16.sp,
                      margin: EdgeInsets.only(left: 35.sp),
                      decoration: BoxDecoration(
                          color: profile.gender == 'M'
                              ? CustomExtraThemeData.getExtraThemeData().rankingGenderLabelPrimary
                              : CustomExtraThemeData.getExtraThemeData().rankingGenderLabelSecondary,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(25),
                              bottomRight: Radius.circular(25))),
                      child: Center(
                        child: Text(
                          profile.gender,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 8.sp,
                              color: profile.gender == 'F'
                                  ? CustomExtraThemeData.getExtraThemeData().rankingGenderLabelPrimary
                                  : CustomExtraThemeData.getExtraThemeData().rankingGenderLabelSecondary),
                        ),
                      ),
                    )),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 4.0.sp),
              child: Text(
                profile.name.capitalizeFirst.toString(),
                softWrap: true,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: Colors.black, fontSize: 8.sp, height: 1.5.sp),
              ),
            )
          ],
        ));
  }

  formatValueFromRanking(int ratingTypeId, dynamic value) {
    if (ratingTypeId == RatingType.TIME ||
        ratingTypeId == RatingType.TIME_ALBACETE) {
      int valueNumber = int.parse(value);
      int minutes = (valueNumber / 60).floor();
      int segs = valueNumber % 60;
      return '${minutes < 10 ? '0$minutes' : minutes}:${segs < 10 ? '0$segs' : segs}';
    }

    if (ratingTypeId == RatingType.REPS ||
        ratingTypeId == RatingType.ROUNDS ||
        ratingTypeId == RatingType.CALORIES ||
        ratingTypeId == RatingType.REPS_ALBACETE ||
        ratingTypeId == RatingType.ROUNDS_ALBACETE ||
        ratingTypeId == RatingType.CALORIES_ALBACETE) {
      return value.toString();
    }

    if (ratingTypeId == RatingType.KG ||
        ratingTypeId == RatingType.KG_ALBACETE) {
      return '${value}kg';
    }

    if (ratingTypeId == RatingType.KM ||
        ratingTypeId == RatingType.KM_ALBACETE) {
      return '${value}km';
    }

    if (ratingTypeId == RatingType.M || ratingTypeId == RatingType.M_ALBACETE) {
      return '${value}m';
    }
  }

  loadRanking() {
    Provider.of<WodsProvider>(context, listen: false)
        .getRankings(widget.wod.id)
        .then((response) {
      if (response.success! && response.status == 200) {
        if (mounted) {
          setState(() {
            var data = response.data;
            final Map clonedMap = json.decode(json.encode(data));
            ranking = Provider.of<WodsProvider>(context, listen: false)
                .filterRakingsNoCompleted(data, clonedMap);
          });
        }
      }
    });
  }

  bool get _isThereAnyRanking {
    return (ranking.isNotEmpty &&
        (ranking['RX'].isNotEmpty ||
            ranking['SCALED'].isNotEmpty ||
            ranking['ROOKIE'].isNotEmpty ||
            ranking['TEENAGER'].isNotEmpty));
  }
}
