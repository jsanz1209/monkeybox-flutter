import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/sport-data/api/exercise_data.dart';
import 'package:monkeybox/models/wods/api/block.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

class BlockWidget extends StatefulWidget {
  final Block block;
  final bool isAddingRating;
  final FocusScope? form;
  const BlockWidget(this.block, this.isAddingRating, this.form, {Key? key})
      : super(key: key);

  @override
  State<BlockWidget> createState() => _BlockWidgetState();
}

class _BlockWidgetState extends State<BlockWidget> {
  VideoPlayerController? _videoController;
  ChewieController? _chewieController;
  @override
  void dispose() {
    if (_videoController != null && _chewieController != null) {
      _videoController!.dispose();
      _chewieController!.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: widget.isAddingRating ? 0 : 8.sp, vertical: 8.sp),
      child: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(color: Colors.grey.shade500, blurRadius: 10)
        ]),
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(top: kToolbarHeight),
              padding: EdgeInsets.only(
                  left: 8.sp, right: 8.sp, bottom: 8.sp, top: 0.sp),
              child: widget.isAddingRating
                  ? widget.form
                  : Html(
                      data: widget.block.work,
                      customRender: {
                        'a': (final renderContext, final widget) {
                          ExerciseData? exerciseObject;
                          if (renderContext.tree.attributes['exercise-id'] !=
                              null) {
                            String exerciseId = renderContext
                                .tree.attributes['exercise-id']
                                .toString();
                            var exercises = Provider.of<SportDataProvider>(
                                    context,
                                    listen: false)
                                .exercises;
                            exerciseObject = exercises.firstWhereOrNull(
                                (item) => item.id == int.parse(exerciseId));
                          }
                          return InkWell(
                              onTap: () {
                                if (exerciseObject != null) {
                                  showInfoExerciseModal(
                                      context,
                                      exerciseObject.description.name,
                                      exerciseObject.description.explanation);
                                }
                              },
                              child: widget);
                        },
                        'img': (final renderContext, final widget) {
                          var imageUrl =
                              renderContext.tree.attributes['src'].toString();
                          if (imageUrl.contains('.mp4')) {
                            if (this._videoController != null &&
                                _chewieController != null) {
                              _videoController!.removeListener(() {});
                              _chewieController!.removeListener(() {});
                            }
                            _videoController =
                                VideoPlayerController.network(imageUrl);
                            _chewieController = ChewieController(
                                videoPlayerController: _videoController!,
                                autoInitialize: true,
                                autoPlay: false);
                            return Container(
                              margin: EdgeInsets.symmetric(vertical: 16.sp),
                              child: AspectRatio(
                                  aspectRatio:
                                      _videoController!.value.aspectRatio,
                                  child:
                                      Chewie(controller: _chewieController!)),
                            );
                          } else {
                            return Container(
                              margin: EdgeInsets.only(top: 8.sp),
                              child: Center(
                                child: CachedNetworkImage(
                                  imageUrl: imageUrl,
                                ),
                              ),
                            );
                          }
                        }
                      },
                      style: {
                        'span': Style(
                          fontSize: FontSize.rem(0.9.sp),
                          margin: EdgeInsets.zero,
                          padding: EdgeInsets.zero,
                        ),
                        'p': Style(
                            fontSize: FontSize.rem(0.9.sp),
                            margin: EdgeInsets.zero,
                            padding: EdgeInsets.zero,
                            lineHeight: LineHeight.normal),
                        'ul': Style(
                          color: Colors.black,
                          margin: EdgeInsets.zero,
                          padding: EdgeInsets.zero,
                        ),
                        'li': Style(
                          color: Colors.black,
                          margin: EdgeInsets.zero,
                        ),
                        'img': Style(
                            display: Display.BLOCK,
                            margin: EdgeInsets.only(bottom: 14.sp, top: 14.sp))
                      },
                    ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              width: double.infinity,
              height: 47.1.sp,
              decoration: BoxDecoration(color: Theme.of(context).colorScheme.primaryVariant),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: 8.0.sp, vertical: 14.sp),
                child: Text(
                  widget.block.title,
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Oswald',
                  ),
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              height: 47.1.sp,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  widget.block.ratingTypes.length > 0
                      ? Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(21, 29, 36, 1)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              widget.block.getParsedRatingTypes(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Oswald',
                                  fontSize: 16.sp),
                            ),
                          ),
                        )
                      : const SizedBox(),
                  widget.block.restText.isNotEmpty
                      ? Container(
                          alignment: Alignment.center,
                          height: kToolbarHeight,
                          decoration: BoxDecoration(color: HexColor('#00b4ff')),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.av_timer_rounded,
                                  size: 24.sp,
                                  color: Colors.black,
                                ),
                                Text(
                                  widget.block.restText,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 18.sp),
                                ),
                              ],
                            ),
                          ),
                        )
                      : const SizedBox(),
                  widget.block.capTime.isNotEmpty
                      ? Container(
                          alignment: Alignment.center,
                          height: kToolbarHeight,
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(3, 255, 194, 1)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.timer,
                                  size: 24.sp,
                                  color: Colors.black,
                                ),
                                Text(
                                  widget.block.capTime,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18.sp,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : const SizedBox(),
                  widget.block.goal.isNotEmpty
                      ? Container(
                          alignment: Alignment.center,
                          height: kToolbarHeight,
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 214, 2, 1)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Image(
                                  image: AssetImage('assets/images/medal.png'),
                                  height: 24.sp,
                                  width: 24.sp,
                                ),
                                Text(
                                  widget.block.goal,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 18.sp),
                                ),
                              ],
                            ),
                          ),
                        )
                      : const SizedBox(),
                  widget.block.isTeam == 1
                      ? Container(
                          alignment: Alignment.center,
                          height: kToolbarHeight,
                          decoration: BoxDecoration(color: HexColor('#2e8b57')),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.people_outline_rounded,
                              size: 24.sp,
                              color: Colors.black,
                            ),
                          ),
                        )
                      : const SizedBox(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void showInfoExerciseModal(
      BuildContext context, String name, String explanation) {
    showDialog(
        useSafeArea: false,
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.zero,
            child: Scaffold(
              appBar: CustomHeader(title: name, withBackButton: true),
              body: Container(
                  child: Html(
                data: explanation,
                style: {
                  'p': Style(
                      fontSize: FontSize.rem(1.sp),
                      padding: EdgeInsets.zero,
                      color: Colors.black,
                      textAlign: TextAlign.center),
                },
              )),
            ),
          );
        });
  }
}
