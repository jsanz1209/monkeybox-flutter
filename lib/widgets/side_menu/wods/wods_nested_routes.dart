import 'package:flutter/material.dart';
import 'package:monkeybox/pages/side-menu/wods/add_rating_page.dart';
import 'package:monkeybox/pages/side-menu/wods/wods_page.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:provider/provider.dart';

class WodsNestedRoutes extends StatelessWidget {
  final String setupPageRoute;
  final bool? isCompetitor;

  const WodsNestedRoutes(
      {Key? key, required this.setupPageRoute, this.isCompetitor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var navigatorWodsKey =
        Provider.of<WodsProvider>(context, listen: false).navigatorKey;
    return WillPopScope(
      child: Navigator(
        key:
            navigatorWodsKey, //(*) Uncomment if we will want to navigate from this navigator key
        initialRoute:
            setupPageRoute.isEmpty ? WodsPage.routeName : setupPageRoute,
        onGenerateRoute: _onGenerateRoute,
      ),
      onWillPop: () {
        if (navigatorWodsKey.currentState!.canPop()) {
          navigatorWodsKey.currentState!.pop();
          return Future<bool>.value(false);
        }
        var menuProvider = Provider.of<MenuProvider>(context, listen: false);
        menuProvider.navigatorKey.currentState!.pop();
        return Future<bool>.value(false);
      },
    );
  }

  Route _onGenerateRoute(RouteSettings settings) {
    late Widget page;
    switch (settings.name) {
      case WodsPage.routeName:
        page = WodsPage(isCompetitor: isCompetitor);
        break;
      case AddRatingPage.routeName:
        page = const AddRatingPage();
        break;
    }
    return MaterialPageRoute<dynamic>(
      builder: (context) {
        return page;
      },
      settings: settings,
    );
  }
}
