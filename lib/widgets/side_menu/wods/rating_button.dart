import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/wods/api/wod.dart';
import 'package:monkeybox/pages/side-menu/wods/add_rating_page.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:provider/provider.dart';

class RatingButton extends StatelessWidget {
  final Wod wod;
  final Function onBackFromAddingRating;
  const RatingButton(
      {Key? key, required this.wod, required this.onBackFromAddingRating})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<MenuProvider>(context, listen: false).user;
    var blocksRatingsUser =
        Provider.of<WodsProvider>(context, listen: true).getRatingUser(user);
    return ElevatedButton.icon(
      onPressed: () {
        Provider.of<WodsProvider>(context, listen: false).isAddingRatingMode =
            true;
        Navigator.of(context)
            .pushNamed(
          AddRatingPage.routeName,
          arguments: wod,
        )
            .then((value) {
          Provider.of<WodsProvider>(context, listen: false).isAddingRatingMode =
              false;
          if (value != null) {
            onBackFromAddingRating();
          }
        });
      },
      icon: blocksRatingsUser.isNotEmpty
          ? Icon(Icons.edit_rounded, color: Colors.black)
          : Icon(Icons.add_rounded, color: Colors.black),
      label: Text(
        'Resultado'.toUpperCase(),
        style: TextStyle(
            color: Colors.black, fontFamily: 'Oswald', fontSize: 14.sp),
      ),
      style: OutlinedButton.styleFrom(
          backgroundColor: CustomExtraThemeData.getExtraThemeData().buttonBackground,
          elevation: 0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
          minimumSize: Size(100.sp, 42.sp)),
    );
  }
}
