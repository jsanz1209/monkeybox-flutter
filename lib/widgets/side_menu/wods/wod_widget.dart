import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/wods/api/wod.dart';
import 'package:monkeybox/providers/wods/wods_provider.dart';
import 'package:monkeybox/widgets/side_menu/wods/blocks_container.dart';
import 'package:monkeybox/widgets/side_menu/wods/participants.dart';
import 'package:monkeybox/widgets/side_menu/wods/ranking.dart';
import 'package:monkeybox/widgets/side_menu/wods/rating_button.dart';
import 'package:monkeybox/widgets/side_menu/wods/warm.dart';
import 'package:provider/provider.dart';

class WodWidget extends StatefulWidget {
  final Wod wod;
  final Function onBackFromAddingRating;
  const WodWidget(this.wod, this.onBackFromAddingRating, {Key? key})
      : super(key: key);

  @override
  State<WodWidget> createState() => _WodWidgetState();
}

class _WodWidgetState extends State<WodWidget> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var rankingWidgetKey =
        Provider.of<WodsProvider>(context, listen: false).rankingWidgetKey;
    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: EdgeInsets.zero,
          decoration: BoxDecoration(color: Theme.of(context).primaryColor),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.sp, vertical: 14.sp),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'WOD',
                        style: TextStyle(
                            color: Colors.white70,
                            fontSize: 10.sp,
                            fontFamily: 'Oswald'),
                      ),
                      SizedBox(
                        height: 5.sp,
                      ),
                      ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: width / 2),
                        child: Container(
                          width: width / 2,
                          child: Text(
                            widget.wod.title,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.sp,
                                fontFamily: 'Oswald'),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 8.sp),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    RatingButton(
                        wod: widget.wod,
                        onBackFromAddingRating: () {
                          setState(() {
                            Future.delayed(const Duration(milliseconds: 800),
                                () {
                              widget.onBackFromAddingRating();
                            });
                          });
                        })
                  ],
                ),
              )
            ],
          ),
        ),
        widget.wod.warmUp.isNotEmpty
            ? WarmWidget(widget.wod.warmUp)
            : SizedBox(),
        widget.wod.blocks.length > 0
            ? BlocksContainerWidget(widget.wod.blocks, false, null)
            : const SizedBox(),
        ParticipantsWidget(widget.wod),
        RankingWidget(widget.wod, key: rankingWidgetKey)
      ],
    );
  }
}
