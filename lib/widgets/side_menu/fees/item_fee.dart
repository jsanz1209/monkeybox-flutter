import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/fees/api/fee_user.dart';
import 'package:monkeybox/pages/side-menu/fees/payment_gateway_stripe_page.dart';
import 'package:monkeybox/providers/fees/fees_bonuses_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:provider/provider.dart';

class ItemFee extends StatelessWidget {
  final FeeUser itemFee;
  const ItemFee({Key? key, required this.itemFee}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var menuProvider = Provider.of<MenuProvider>(context, listen: false);
    var infoBox = menuProvider.infoBOX;
    var name = itemFee.name;
    if (name == null) {
      var bonuses = Provider.of<FeesBonusesProvider>(context, listen: false)
          .listBonusesUser;
      name =
          bonuses.firstWhere((element) => element.id == itemFee.serviceID).name;
    }
    return Container(
      margin: EdgeInsets.only(top: 16.sp, bottom: 0.sp),
      child: Card(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.sp),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '$name | ${itemFee.quantity}€',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.black),
              ),
              if (itemFee.administrationFees)
                Text(
                  '* Pago online conlleva ${infoBox!.administrationFees}€ por gastos de gestión',
                  style: Theme.of(context)
                      .textTheme
                      .headline3!
                      .copyWith(color: Colors.grey, height: 1.5),
                ),
              Text(
                  itemFee.sessionsPerWeek != null
                      ? '${itemFee.sessionsPerWeek} sesiones por semana'
                      : 'Sesiones ilimitadas por semana',
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                      color: Colors.grey, height: 1.5, fontSize: 14.sp)),
              Text(
                  'Periodo ${getFormatDate(itemFee.startDate)} al ${getFormatDate(itemFee.endDate)}',
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                      color: Colors.grey, height: 1.5, fontSize: 14.sp)),
              ActionChip(
                label: FittedBox(
                  fit: BoxFit.contain,
                  child: Text(textActionChip,
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          )),
                ),
                onPressed: () {
                  if (itemFee.status == Settings.CREATED_PAYMENT) {
                    Navigator.of(context).pushNamed(
                        PaymentGatewayStripePage.routeName,
                        arguments: {'fee': itemFee, 'name': name});
                  }
                },
                backgroundColor: colorBasedOnTypeFee,
                visualDensity: VisualDensity.compact,
                labelPadding:
                    EdgeInsets.symmetric(vertical: 0.sp, horizontal: 16.sp),
              ),
            ],
          ),
        ),
        elevation: 0,
        shape:
            Border(left: BorderSide(width: 3.sp, color: colorBasedOnTypeFee)),
      ),
    );
  }

  String get textActionChip {
    switch (itemFee.status) {
      case Settings.COMPLETED_PAYMENT:
        return 'Pagado el ${getFormatDate(itemFee.issueDate!)}';
      case Settings.CREATED_PAYMENT:
        return 'Pulsa aquí para pagar';
      case Settings.PROCESSING_PAYMENT:
        return 'Pago en proceso';
      default:
        return 'Estado del pago desconocido';
    }
  }

  getFormatDate(String date) {
    return DateFormat('dd/MM/yyyy').format(DateTime.parse(date));
  }

  Color get colorBasedOnTypeFee {
    switch (itemFee.status) {
      case Settings.COMPLETED_PAYMENT:
        return HexColor('#8fcb00');
      case Settings.CREATED_PAYMENT:
        return Colors.red;
      case Settings.PROCESSING_PAYMENT:
        return Colors.orange;
      default:
        return Colors.grey;
    }
  }
}
