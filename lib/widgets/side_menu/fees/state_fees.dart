import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/fees/api/fee_user.dart';
import 'package:monkeybox/widgets/side_menu/fees/state_fee_item.dart';

class StateFees extends StatefulWidget {
  final bool? isBold;
  final Function(String?) onStateFeeSelected;
  final List<FeeUser> listFeesUser;
  const StateFees(
      {Key? key, required this.onStateFeeSelected, required this.listFeesUser, this.isBold = false})
      : super(key: key);

  @override
  State<StateFees> createState() => _StateFeesState();
}

class _StateFeesState extends State<StateFees> {
  List<bool> _otherBtnPressed = [true, false, true, true];

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      if (widget.isBold!)
        Column(
          children: [
            Text(
              'Estado de cuotas/bonos',
              style: Theme.of(context).textTheme.bodyText2!.copyWith(
                  fontWeight:
                      widget.isBold! ? FontWeight.bold : FontWeight.normal,
                  color: Colors.grey),
            ),
            SizedBox(
              height: 4.sp,
            ),
          ],
        ),
      Row(
        children: [
          Expanded(
              child: StateFeeItem(
                  label: 'PAGADAS (${getNumberFeesTypePayment(Settings.COMPLETED_PAYMENT)})',
                  colorActivated: HexColor('#8fcb00'),
                  isOtherBtnPressed: _otherBtnPressed[0],
                  onPressOtherBtn: () {
                    _setOtherButtonPressed(0);
                    widget.onStateFeeSelected(Settings.COMPLETED_PAYMENT);
                  })),
          SizedBox(
            width: 8.sp,
          ),
          Expanded(
            child: StateFeeItem(
                label: 'NO PAGADAS (${getNumberFeesTypePayment(Settings.CREATED_PAYMENT)})',
                colorActivated: Colors.red,
                isOtherBtnPressed: _otherBtnPressed[1],
                onPressOtherBtn: () {
                  _setOtherButtonPressed(1);
                  widget.onStateFeeSelected(Settings.CREATED_PAYMENT);
                }),
          ),
          SizedBox(
            width: 8.sp,
          ),
          Expanded(
              child: StateFeeItem(
                  label: 'EN PROCESO (${getNumberFeesTypePayment(Settings.PROCESSING_PAYMENT)})',
                  colorActivated: Colors.orange,
                  isOtherBtnPressed: _otherBtnPressed[2],
                  onPressOtherBtn: () {
                    _setOtherButtonPressed(2);
                    widget.onStateFeeSelected(Settings.PROCESSING_PAYMENT);
                  })),
          SizedBox(
            width: 8.sp,
          ),
          Expanded(
            child: StateFeeItem(
                label: 'TODAS (${getNumberFeesTypePayment(null)})',
                isOtherBtnPressed: _otherBtnPressed[3],
                onPressOtherBtn: () {
                  _setOtherButtonPressed(3);
                  widget.onStateFeeSelected(null);
                }),
          )
        ],
      ),
    ]);
  }

  int getNumberFeesTypePayment(String? typePayment) {
    if (typePayment == null) {
      return widget.listFeesUser.length;
    }
    return widget.listFeesUser.where((element) => element.status == typePayment).toList().length;
  }

  void _setOtherButtonPressed(index) {
    var newListOtherButtonPressed =
        _otherBtnPressed.asMap().entries.map((entry) {
      if (entry.key == index) {
        return false;
      } else {
        return true;
      }
    }).toList();
    setState(() {
      _otherBtnPressed = newListOtherButtonPressed;
    });
  }
}
