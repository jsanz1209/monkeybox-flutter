import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/common/item_dropdown.dart';
import 'package:monkeybox/widgets/common/form/custom_dropdown.dart';

class SelectFee extends StatefulWidget {
  final Function onChange;
  final List<ItemDropdown> itemsFeesBonuses;
  final int? feeSelected;
  final String? nameFeeBonusSelected;
  final int? priceFeeBonusSelected;

  const SelectFee(
      {Key? key,
      required this.onChange,
      required this.itemsFeesBonuses,
      required this.feeSelected,
      required this.nameFeeBonusSelected,
      required this.priceFeeBonusSelected})
      : super(key: key);

  @override
  State<SelectFee> createState() => _SelectFeeState();
}

class _SelectFeeState extends State<SelectFee> {
  int? _selectedFeeBonus;

  @override
  void initState() {
    super.initState();
    _selectedFeeBonus = widget.feeSelected;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomDropdown(
            isBlackTheme: true,
            label: 'Selecciona una cuota/bono',
            fieldName: 'feeBonus',
            initialValue: widget.feeSelected,
            onChange: (value) {
              widget.onChange(value);
              setState(() {
                _selectedFeeBonus = value;
              });
            },
            options: widget.itemsFeesBonuses),
        SizedBox(
          height: 8.sp,
        ),
        if (_selectedFeeBonus == null)
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
                primary: HexColor('#e5e5e5'),
                elevation: 0,
                alignment: Alignment.center,
                visualDensity: VisualDensity.compact,
                padding: EdgeInsets.zero),
            onPressed: () {},
            icon: Icon(
              Icons.warning_rounded,
              color: HexColor('#cfcf0f'),
              size: 32.sp,
            ),
            label: Text(
              'Seleccione una cuota/bono para realizar reservas.',
              softWrap: true,
              maxLines: 3,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.black,
                  fontSize: 12.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
                primary: HexColor('#e5e5e5'),
                elevation: 0,
                alignment: Alignment.center,
                visualDensity: VisualDensity.compact,
                padding: EdgeInsets.zero),
            onPressed: () {
              _showDialogNoneFeeBonusSelected();
            },
            icon: Icon(
              Icons.info_rounded,
              color: HexColor('#4169e1'),
              size: 32.sp,
            ),
            label: Text(
              'Para cambiar su cuota/bono use el selector superior.',
              softWrap: true,
              maxLines: 2,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: HexColor('#4169e1'),
                  fontSize: 12.sp,
                  fontWeight: FontWeight.normal),
            ))
      ],
    );
  }

  void _showDialogNoneFeeBonusSelected() {
    showDialog(
        useSafeArea: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              'Cuota/Bono actual seleccionado',
              style: Theme.of(context)
                  .textTheme
                  .headline2!
                  .copyWith(color: HexColor('#4169e1'), fontWeight: FontWeight.bold),
            ),
            content: Text(_selectedFeeBonus == null
                ? 'Ninguna cuota/bono seleccionado'
                : '${widget.nameFeeBonusSelected!} | ${widget.priceFeeBonusSelected}€'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: Text('Aceptar',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText2!
                          .copyWith(color: Colors.blue))),
            ],
          );
        });
  }
}
