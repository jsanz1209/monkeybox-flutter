import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/common/item_dropdown.dart';
import 'package:monkeybox/models/fees/api/bonus_user.dart';
import 'package:monkeybox/models/fees/api/fee_box.dart';
import 'package:monkeybox/models/fees/api/fee_user.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/providers/fees/fees_bonuses_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/side_menu/fees/select_fee.dart';
import 'package:monkeybox/widgets/side_menu/fees/state_fees.dart';
import 'package:provider/provider.dart';
import 'package:collection/collection.dart';

class FeesHeader extends StatefulWidget {
  final Function(bool isBonus, dynamic feeBonus) onChangeFeeBonus;
  final Function(String?) onStateFeeSelected;

  FeesHeader(
      {Key? key,
      required this.onChangeFeeBonus,
      required this.onStateFeeSelected})
      : super(key: key);

  @override
  _FeesHeaderState createState() => _FeesHeaderState();
}

class _FeesHeaderState extends State<FeesHeader> {
  @override
  Widget build(BuildContext context) {
    var feesProvider = Provider.of<FeesBonusesProvider>(context, listen: false);
    var menuProvider = Provider.of<MenuProvider>(context, listen: false);
    return SliverPersistentHeader(
      delegate: FeesSliverAppBar(
          title: 'CUOTAS Y BONOS',
          listFeesBox: feesProvider.listFeesBox,
          listBonusesUser: feesProvider.listBonusesUser,
          listFeesUser: feesProvider.listFeesUser,
          feeSelected: feesProvider.infoFeeUserBox!.feeID != null
              ? feesProvider.infoFeeUserBox!.feeID
              : feesProvider.infoFeeUserBox!.bonus != null
                  ? feesProvider.infoFeeUserBox!.bonus!.serviceID
                  : null,
          isAutomaticallyDisabled: menuProvider.user!.userBoxes[0].status ==
              '5-AUTOMATICALLY_DISABLED',
          onStateFeeSelected: (value) {
            widget.onStateFeeSelected(value);
          },
          onChangeFeeBonus: (isBonus, feeBonusOBject) {
            widget.onChangeFeeBonus(isBonus, feeBonusOBject);
          }),
      pinned: true,
      // floating: true,
    );
  }
}

class FeesSliverAppBar extends SliverPersistentHeaderDelegate {
  double expandedHeight = 190.h;
  final String title;
  final Function(bool isBonus, dynamic feeBonus) onChangeFeeBonus;
  final Function(String?) onStateFeeSelected;
  final List<FeeUser> listFeesUser;
  final bool isAutomaticallyDisabled;

  List<BonusUser> listBonusesUser = [];
  List<FeesBox> listFeesBox = [];
  int? feeSelected;
  List<ItemDropdown> _itemsFeesBonuses = [];

  FeesSliverAppBar(
      {required this.title,
      required this.onChangeFeeBonus,
      required this.onStateFeeSelected,
      required this.listFeesUser,
      required this.feeSelected,
      required this.listBonusesUser,
      required this.listFeesBox,
      required this.isAutomaticallyDisabled}) {
    expandedHeight = feeSelected != null ? 190.h : 220.h;
    _itemsFeesBonuses = [
      ...listFeesBox
          .map((item) => ItemDropdown(item.id,
              'Cuota - ${item.name} | ${item.quantity}${item.currencyCode == 'EUR' ? '\€' : '\$'}'))
          .toList(),
      ...listBonusesUser
          .map((item) => ItemDropdown(item.id,
              'Bono - ${item.name} | ${item.quantity}${item.currencyCode == 'EUR' ? '\€' : '\$'}'))
          .toList()
    ];
  }

  double getOpacity(double shrinkOffset) {
    var fixOpacity = (shrinkOffset / expandedHeight);
    return 1 - fixOpacity < 0
        ? 0
        : 1 - fixOpacity > 1
            ? 1
            : 1 - fixOpacity;
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: HexColor('#e5e5e5'),
      child: Column(
        children: [
          Expanded(
            child: shrinkOffset > 0
                ? Opacity(
                    opacity: (shrinkOffset / expandedHeight),
                    child: _getContentHeaderCollapsed())
                : Opacity(
                    opacity: 1 - (shrinkOffset / expandedHeight),
                    child: _getContentHeaderExpanded()),
          ),
        ],
      ),
    );
  }

  @override
  double get maxExtent => feeSelected != null ? 190.h : 220.h;

  @override
  double get minExtent => 90.sp;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;

  String? get nameFeeBonusSelected {
    var name = listFeesBox
        .firstWhereOrNull((element) => element.id == feeSelected)
        ?.name;
    if (name == null) {
      name = listBonusesUser
          .firstWhereOrNull((element) => element.id == feeSelected)
          ?.name;
    }
    return name;
  }

  int? get priceFeeBonusSelected {
    var quantity = listFeesBox
        .firstWhereOrNull((element) => element.id == feeSelected)
        ?.quantity;
    if (quantity == null) {
      quantity = listBonusesUser
          .firstWhereOrNull((element) => element.id == feeSelected)
          ?.quantity;
    }
    return quantity;
  }

  _getContentHeaderExpanded() {
    return Wrap(children: [
      Container(
        color: HexColor('#e5e5e5'),
        width: double.infinity,
        padding: EdgeInsets.only(
            top: 14.sp, left: 24.sp, right: 24.sp, bottom: 8.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SelectFee(
              itemsFeesBonuses: _itemsFeesBonuses,
              nameFeeBonusSelected: nameFeeBonusSelected,
              priceFeeBonusSelected: priceFeeBonusSelected,
              feeSelected: feeSelected,
              onChange: (value) {
                feeSelected = value;
                var feeObject = listFeesBox
                    .firstWhereOrNull((element) => element.id == feeSelected);
                if (feeObject == null) {
                  var bonusOboject = listBonusesUser
                      .firstWhereOrNull((element) => element.id == feeSelected);
                  onChangeFeeBonus(true, bonusOboject);
                } else {
                  onChangeFeeBonus(false, feeObject);
                }
              },
            ),
            SizedBox(
              height: 8.sp,
            ),
            StateFees(
                isBold: true,
                listFeesUser: listFeesUser,
                onStateFeeSelected: (value) {
                  onStateFeeSelected(value);
                }),
            if (isAutomaticallyDisabled)
              Padding(
                padding: EdgeInsets.only(top: 8.sp),
                child: Text(
                  '(*) Tu usuario se encuentra dado de baja, realiza una reserva de clase para generar la cuota pendiente de pago.',
                  style: Theme.of(appNavigatorKey.currentContext!)
                      .textTheme
                      .bodyText2!
                      .copyWith(
                          color: Theme.of(appNavigatorKey.currentContext!)
                              .errorColor,
                          fontSize: 15.sp, fontWeight: FontWeight.bold),
                ),
              ),
          ],
        ),
      ),
    ]);
  }

  _getContentHeaderCollapsed() {
    return Wrap(children: [
      Container(
        color: Colors.cyan[100],
        width: double.infinity,
        padding: EdgeInsets.only(
            top: 14.sp, left: 24.sp, right: 24.sp, bottom: 8.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              feeSelected != null
                  ? '${nameFeeBonusSelected!} | ${priceFeeBonusSelected!}€'
                  : 'No tienes seleccionada ninguna cuota/bono',
              style: Theme.of(appNavigatorKey.currentContext!)
                  .textTheme
                  .headline3!
                  .copyWith(
                      color: Colors.black,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 8.sp,
            ),
            StateFees(
              listFeesUser: listFeesUser,
              onStateFeeSelected: (value) {
                onStateFeeSelected(value);
              },
            )
          ],
        ),
      ),
    ]);
  }
}
