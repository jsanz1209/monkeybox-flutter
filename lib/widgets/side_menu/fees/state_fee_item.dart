import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StateFeeItem extends StatefulWidget {
  final String label;
  final Function onPressOtherBtn;
  final bool isOtherBtnPressed;
  final Color? colorActivated;

  const StateFeeItem(
      {Key? key,
      required this.label,
      required this.onPressOtherBtn,
      required this.isOtherBtnPressed,
      this.colorActivated})
      : super(key: key);

  @override
  State<StateFeeItem> createState() => _StateFeeItemState();
}

class _StateFeeItemState extends State<StateFeeItem> {
  @override
  Widget build(BuildContext context) {
    return ActionChip(
      label: FittedBox(
        fit: BoxFit.contain,
        child: Text(widget.label,
            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                  fontSize: 10.sp,
                  fontWeight: FontWeight.bold,
                  color: widget.isOtherBtnPressed ? Colors.grey : Colors.white,
                )),
      ),
      onPressed: () {
        setState(() {
          widget.onPressOtherBtn();
        });
      },
      backgroundColor: widget.isOtherBtnPressed
          ? Colors.white
          : widget.colorActivated ?? Theme.of(context).primaryColor,
      visualDensity: VisualDensity.compact,
      labelPadding: EdgeInsets.symmetric(vertical: 0.sp, horizontal: 8.sp),
    );
  }
}
