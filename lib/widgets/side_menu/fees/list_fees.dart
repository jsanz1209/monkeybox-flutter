import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/providers/fees/fees_bonuses_provider.dart';
import 'package:monkeybox/widgets/side_menu/fees/item_fee.dart';
import 'package:provider/provider.dart';

class ListFees extends StatelessWidget {
  final String? filter;
  const ListFees({Key? key, required this.filter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var listFees = Provider.of<FeesBonusesProvider>(context, listen: false)
        .listFeesUser
        .where((item) {
      if (filter == null) {
        return true;
      }
      return item.status == filter;
    }).toList();
    return listFees.isNotEmpty
        ? Container(
            padding: EdgeInsets.symmetric(vertical: 16.sp, horizontal: 24.sp),
            color: Colors.white,
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ...listFees.map((itemFee) => ItemFee(
                      itemFee: itemFee,
                    ))
              ],
            ),
          )
        : Container(
            height: MediaQuery.of(context).size.height -
                84.sp -
                220.sp -
                8.sp -
                MediaQuery.of(context).padding.top -
                -MediaQuery.of(context).padding.bottom,
            child: Center(
              child: Text(
                'No se encuentran resultados para los criterios de búsquedas seleccionados',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .headline2!
                    .copyWith(color: Theme.of(context).primaryColor),
              ),
            ),
          );
  }
}
