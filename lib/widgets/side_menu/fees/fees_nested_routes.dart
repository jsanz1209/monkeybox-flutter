import 'package:flutter/material.dart';
import 'package:monkeybox/pages/side-menu/fees/fees_bonuses_page.dart';
import 'package:monkeybox/pages/side-menu/fees/payment_gateway_stripe_page.dart';
import 'package:monkeybox/providers/fees/fees_bonuses_provider.dart';
import 'package:provider/provider.dart';

class FeesNestedRoutes extends StatelessWidget {
  final String setupPageRoute;

  const FeesNestedRoutes(
      {Key? key, required this.setupPageRoute})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var navigatorFeesKey =
        Provider.of<FeesBonusesProvider>(context, listen: false).navigatorKey;
    return WillPopScope(
      child: Navigator(
        key:
            navigatorFeesKey, //(*) Uncomment if we will want to navigate from this navigator key
        initialRoute:
            setupPageRoute.isEmpty ? FeesBonusesPage.routeName : setupPageRoute,
        onGenerateRoute: _onGenerateRoute,
      ),
      onWillPop: () {
        return navigatorFeesKey.currentState!.maybePop();
      },
    );
  }

  Route _onGenerateRoute(RouteSettings settings) {
    late Widget page;
    switch (settings.name) {
      case FeesBonusesPage.routeName:
        page = FeesBonusesPage();
        break;
      case PaymentGatewayStripePage.routeName:
        page =  PaymentGatewayStripePage();
        break;
    }
    return MaterialPageRoute<dynamic>(
      builder: (context) {
        return page;
      },
      settings: settings,
    );
  }
}
