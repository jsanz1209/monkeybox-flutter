import 'dart:async';

import 'package:flutter/material.dart';
import 'package:monkeybox/pages/side-menu/athlete_profile/athlete_profile_page.dart';
import 'package:monkeybox/pages/side-menu/box/box_conditions_page.dart';
import 'package:monkeybox/pages/side-menu/box/box_contact_page.dart';
import 'package:monkeybox/pages/side-menu/chronometer/chronometer_page.dart';
import 'package:monkeybox/pages/side-menu/dashboard/dashboard_page.dart';
import 'package:monkeybox/pages/side-menu/fees/fees_bonuses_page.dart';
import 'package:monkeybox/pages/side-menu/fees/main_fees_bonuses.dart';
import 'package:monkeybox/pages/side-menu/my_stats/my_stats_page.dart';
import 'package:monkeybox/pages/side-menu/notifications/notifications_page.dart';
import 'package:monkeybox/pages/side-menu/payment_management/payment_managetment_page.dart';
import 'package:monkeybox/pages/side-menu/photoview_image/photoview_user_image.dart';
import 'package:monkeybox/pages/side-menu/reservations/reservations_page.dart';
import 'package:monkeybox/pages/side-menu/rm/rm_page.dart';
import 'package:monkeybox/pages/side-menu/tutorial/tutorial_page.dart';
import 'package:monkeybox/pages/side-menu/user/user_page.dart';
import 'package:monkeybox/pages/side-menu/weight/weight_page.dart';
import 'package:monkeybox/pages/side-menu/wods/main_wods_page.dart';
import 'package:monkeybox/pages/side-menu/wods/wods_page.dart';
import 'package:monkeybox/providers/chrono/chrono_bloc.dart';
import 'package:monkeybox/providers/notifications/push_notifications_manager.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:provider/provider.dart';

class SideMenuNestedRoutes extends StatefulWidget {
  final String setupPageRoute;

  const SideMenuNestedRoutes({Key? key, required this.setupPageRoute})
      : super(key: key);

  @override
  _SideMenuNestedRoutesState createState() => _SideMenuNestedRoutesState();
}

class _SideMenuNestedRoutesState extends State<SideMenuNestedRoutes> {
  late HeroController _heroController;
  late Timer _timer;
  final _pushNotificationManager = PushNotificationManager();

  @override
  void initState() {    
    super.initState();
    _heroController = HeroController(createRectTween: _createRectTween);
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await Provider.of<MenuProvider>(context, listen: false).fetchUserData();
      _pushNotificationManager.setFCMToken();
      _refreshNotifications();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    _pushNotificationManager.cancelSubscription();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var navigatorKey =
        Provider.of<MenuProvider>(context, listen: false).navigatorKey;
    return WillPopScope(
      child: Navigator(
        key: navigatorKey,
        initialRoute: widget.setupPageRoute.isEmpty
            ? DashboardPage.routeName
            : widget.setupPageRoute,
        onGenerateRoute: _onGenerateRoute,
        observers: [_heroController],
      ),
      onWillPop: () {
        var _chronoBloc = ChronoBloc();
        if (_chronoBloc.isInChronoSection) {
          _chronoBloc.chronoGenBackEventSink.add(VoidCallback);
          return Future<bool>.value(false);
        } else if (navigatorKey.currentState!.canPop()) {
          navigatorKey.currentState!.maybePop();
          return Future<bool>.value(false);
        }
        return Future<bool>.value(true);
      },
    );
  }

  Route _onGenerateRoute(RouteSettings settings) {
    late Widget page;
    switch (settings.name) {
      case DashboardPage.routeName:
        page = const DashboardPage();
        break;
      case PaymentManagementPage.routeName:
        page = const PaymentManagementPage();
        break;
      case ReservationsPage.routeName:
        page = const ReservationsPage();
        break;
      case MainWodsPage.routeName:
        page = const MainWodsPage(setupPageRoute: WodsPage.routeName);
        break;
      case MainWodsPage.routeNameCompetitors:
        page = const MainWodsPage(
          setupPageRoute: WodsPage.routeName,
          isCompetitor: true,
        );
        break;
      case MainFeesPage.routeName:
        page = const MainFeesPage(setupPageRoute: FeesBonusesPage.routeName);
        break;
      case ChronometerPage.routeName:
        page = const ChronometerPage();
        break;
      case MyStatsPage.routeName:
        page = const MyStatsPage();
        break;
      case RMPage.routeName:
        page = const RMPage();
        break;
      case WeightPage.routeName:
        page = const WeightPage();
        break;
      case NotificationsPage.routeName:
        page = const NotificationsPage();
        break;
      case UserPage.routeName:
        page = const UserPage();
        break;
      case BoxContactPage.routeName:
        page = const BoxContactPage();
        break;
      case BoxConditionsPage.routeName:
        page = const BoxConditionsPage();
        break;
      case TutorialPage.routeName:
        page = TutorialPage();
        break;
      case AthleteProfilePage.routeName:
        page = const AthleteProfilePage();
        break;
      case PhotoViewUserImage.routeName:
        page = const PhotoViewUserImage();
        break;
    }
    return MaterialPageRoute<dynamic>(
      builder: (context) {
        return page;
      },
      settings: settings,
    );
  }

  Tween<Rect?> _createRectTween(Rect? begin, Rect? end) {
    return MaterialRectArcTween(begin: begin, end: end);
  }

  void _refreshNotifications() {
    _timer = Timer.periodic(new Duration(minutes: 1), (timer) async {
      await Provider.of<MenuProvider>(context, listen: false).fetchMessages();
    });
  }
}
