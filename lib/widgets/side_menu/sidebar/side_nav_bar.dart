import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/enums/category_intensity_enum.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/models/wods/api/intensity_category.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/pages/side-menu/user/user_page.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/custom_theme.dart';
import 'package:provider/provider.dart';

class SideNavBar extends StatelessWidget {
  const SideNavBar({Key? key}) : super(key: key);

  Padding get _buildImagePlaceHolderError {
    return Padding(
      padding: const EdgeInsets.all(32),
      child: SvgPicture.asset(
        'assets/images/icons/person.svg',
        color: CustomExtraThemeData.getExtraThemeData().textSideNavColor,
        width: double.infinity,
      ),
    );
  }

  void _goToUserProfile(BuildContext context) {
    Navigator.pop(context);
    Navigator.of(context).push(PageRouteBuilder(
        transitionDuration: Duration(milliseconds: 1000),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          const begin = Offset(0.0, 1.0);
          const end = Offset.zero;
          final tween = Tween(begin: begin, end: end);
          animation.drive(tween);
          return child;
        },
        pageBuilder: (_, __, ___) => UserPage()));
  }

  SvgPicture _getIconIntensityCategory(IntensityCategory intensityCategory) {
    if ((intensityCategory.id - 1) == CategoryIntensityEnum.rx.index) {
      return SvgPicture.asset(
        'assets/images/icons/trophy.svg',
        color: CustomExtraThemeData.getExtraThemeData().textSideNavColor,
        height: 18.5.sp,
        width: 18.5.sp,
      );
    } else if ((intensityCategory.id - 1) ==
        CategoryIntensityEnum.scaled.index) {
      return SvgPicture.asset(
        'assets/images/icons/medal.svg',
        color: CustomExtraThemeData.getExtraThemeData().textSideNavColor,
        height: 18.5.sp,
        width: 18.5.sp,
      );
    } else if (intensityCategory.id - 1 == CategoryIntensityEnum.rookie.index) {
      return SvgPicture.asset(
        'assets/images/icons/ribbon.svg',
        color: CustomExtraThemeData.getExtraThemeData().textSideNavColor,
        height: 18.5.sp,
        width: 18.5.sp,
      );
    } else {
      return SvgPicture.asset(
        'assets/images/icons/happy.svg',
        color: CustomExtraThemeData.getExtraThemeData().textSideNavColor,
        height: 18.5.sp,
        width: 18.5.sp,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var random = Random().nextInt(1000).toString();
    var tag = 'user-profile-tag-$random';
    Provider.of<MenuProvider>(context, listen: false).tagProfileAnimationHero =
        tag;
    User? user = Provider.of<MenuProvider>(context).user;
    return user != null
        ? Container(
            color:
                Theme.of(appNavigatorKey.currentContext!).colorScheme.secondary,
            padding: EdgeInsets.zero,
            constraints: BoxConstraints(
                maxHeight: user.userBoxes[0].bonus != null
                    ? 194.h
                    : 168.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  constraints: BoxConstraints(
                      minWidth: 130.sp, maxWidth: 130.sp, maxHeight: 130.sp),
                  padding: EdgeInsets.zero,
                  margin: const EdgeInsets.only(left: 16, right: 8),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    //Here goes the same radius, u can put into a var or function
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                  ),
                  child: InkWell(
                    onTap: () {
                      _goToUserProfile(context);
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Hero(
                        tag: tag,
                        transitionOnUserGestures: true,
                        child: user.profile!.pictureUrl == null
                            ? _buildImagePlaceHolderError
                            : CachedNetworkImage(
                                imageUrl: user.profile!.pictureUrl!,
                                fit: BoxFit.cover,
                                width: 130.sp,
                                height: 130.sp,
                                placeholder: (ctx, _) =>
                                    _buildImagePlaceHolderError,
                                alignment: Alignment.center,
                                errorWidget: (ctx, error, _) {
                                  return _buildImagePlaceHolderError;
                                },
                              ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(left: 16, top: 4, right: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FittedBox(
                          child: Text('${user.profile!.getFullName()}',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1!
                                  .copyWith(
                                      fontSize: 20.sp,
                                      decoration: TextDecoration.underline)),
                        ),
                        SizedBox(height: 12.sp),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 6.0, left: 2),
                                  child: SizedBox(
                                    height: 16.sp,
                                    width: 16.sp,
                                    child: SvgPicture.asset(
                                      'assets/images/icons/wods.svg',
                                      color: CustomExtraThemeData
                                              .getExtraThemeData()
                                          .textSideNavColor,
                                      height: 16.sp,
                                      width: 16.sp,
                                    ),
                                  ),
                                ),
                                Text.rich(
                                    TextSpan(
                                      text: 'WODS: ',
                                      children: [
                                        TextSpan(
                                            text: '${user.profile!.wodCount}',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                          text: ' / Victorias: ',
                                        ),
                                        TextSpan(
                                            text:
                                                '${user.profile!.wodsVictories}',
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold))
                                      ],
                                    ),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(
                                            color: CustomExtraThemeData
                                                    .getExtraThemeData()
                                                .textSideNavColor,
                                            fontSize: 12.sp,
                                            height: 1.5)),
                              ],
                            ),
                            SizedBox(height: 8.sp),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 9.0, left: 1),
                                  child: Transform.scale(
                                    scale: 1.5,
                                    child: SizedBox(
                                      height: 16.sp,
                                      width: 16.sp,
                                      child: SvgPicture.asset(
                                        'assets/images/icons/height.svg',
                                        color: CustomExtraThemeData
                                                .getExtraThemeData()
                                            .textSideNavColor,
                                        height: 16.sp,
                                        width: 16.sp,
                                      ),
                                    ),
                                  ),
                                ),
                                Text.rich(
                                    TextSpan(
                                      text: 'Estatura: ',
                                      children: [
                                        TextSpan(
                                            text: '${user.profile!.height}m',
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(
                                            color: CustomExtraThemeData
                                                    .getExtraThemeData()
                                                .textSideNavColor,
                                            fontSize: 12.sp,
                                            height: 1.8)),
                              ],
                            ),
                            SizedBox(height: 12.sp),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 2, right: 8.0),
                                  child: SizedBox(
                                    height: 16.sp,
                                    width: 16.sp,
                                    child: SvgPicture.asset(
                                      'assets/images/icons/weight.svg',
                                      color: CustomExtraThemeData
                                              .getExtraThemeData()
                                          .textSideNavColor,
                                      height: 16.sp,
                                      width: 16.sp,
                                    ),
                                  ),
                                ),
                                Text.rich(
                                    TextSpan(
                                      text: 'Peso actual: ',
                                      children: [
                                        TextSpan(
                                            text: '${user.profile!.weight}kg',
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(
                                            color: CustomExtraThemeData
                                                    .getExtraThemeData()
                                                .textSideNavColor,
                                            fontSize: 12.sp,
                                            height: 1.25)),
                              ],
                            ),
                            SizedBox(height: 12.sp),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                if (user.profile!.intensityCategory != null)
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          left: 2, right: 7.0),
                                      child: SizedBox(
                                        height: 18.5.sp,
                                        width: 18.5.sp,
                                        child: _getIconIntensityCategory(
                                            user.profile!.intensityCategory!),
                                      )),
                                if (user.profile!.intensityCategory != null)
                                  Text.rich(
                                      TextSpan(
                                        text: 'Categoría favorita: ',
                                        children: [
                                          TextSpan(
                                              text: user.profile!
                                                  .intensityCategory!.category,
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                              color: CustomExtraThemeData
                                                      .getExtraThemeData()
                                                  .textSideNavColor,
                                              fontSize: 12.sp,
                                              height: 1.2)),
                              ],
                            ),
                            SizedBox(height: 12.sp),
                            if (user.userBoxes[0].bonus != null)
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          left: 2, right: 7.0),
                                      child: SizedBox(
                                          height: 18.5.sp,
                                          width: 18.5.sp,
                                          child: SvgPicture.asset(
                                            'assets/images/icons/card.svg',
                                            color: CustomExtraThemeData
                                                    .getExtraThemeData()
                                                .textSideNavColor,
                                            height: 16.sp,
                                            width: 16.sp,
                                          ))),
                                  Text.rich(
                                      TextSpan(
                                        text: 'Bono: ',
                                        children: [
                                          TextSpan(
                                              text: user.userBoxes[0].bonus!
                                                  .consumedSessions
                                                  .toString(),
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                          TextSpan(
                                            text: ' de ',
                                          ),
                                          TextSpan(
                                              text: user
                                                  .userBoxes[0].bonus!.sessionsNumber
                                                  .toString()),
                                          TextSpan(
                                            text: ' sesiones.',
                                          ),
                                        ],
                                      ),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                              color: CustomExtraThemeData
                                                      .getExtraThemeData()
                                                  .textSideNavColor,
                                              fontSize: 12.sp,
                                              height: 1.2)),
                                ],
                              ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        : SizedBox();
  }
}
