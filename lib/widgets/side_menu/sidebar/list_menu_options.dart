import 'package:flutter/material.dart';
import 'package:monkeybox/enums/menu_options_enum.dart';
import 'package:monkeybox/models/side-menu/item_menu.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/pages/side-menu/box/box_conditions_page.dart';
import 'package:monkeybox/pages/side-menu/box/box_contact_page.dart';
import 'package:monkeybox/pages/side-menu/chronometer/chronometer_page.dart';
import 'package:monkeybox/pages/side-menu/dashboard/dashboard_page.dart';
import 'package:monkeybox/pages/side-menu/fees/main_fees_bonuses.dart';
import 'package:monkeybox/pages/side-menu/my_stats/my_stats_page.dart';
import 'package:monkeybox/pages/side-menu/notifications/notifications_page.dart';
import 'package:monkeybox/pages/side-menu/payment_management/payment_managetment_page.dart';
import 'package:monkeybox/pages/side-menu/reservations/reservations_page.dart';
import 'package:monkeybox/pages/side-menu/rm/rm_page.dart';
import 'package:monkeybox/pages/side-menu/tutorial/tutorial_page.dart';
import 'package:monkeybox/pages/side-menu/user/user_page.dart';
import 'package:monkeybox/pages/side-menu/weight/weight_page.dart';
import 'package:monkeybox/pages/side-menu/wods/main_wods_page.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/auth/auth_page.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/item_menu_option.dart';
import 'package:provider/provider.dart';

import '../../../models/themes/theme_model.dart';

class ListMenuOptions extends StatefulWidget {
  const ListMenuOptions({Key? key}) : super(key: key);

  @override
  State<ListMenuOptions> createState() => _ListMenuOptionsState();
}

class _ListMenuOptionsState extends State<ListMenuOptions> {
  List<ItemMenu> _menuItems = [];

  @override
  initState() {
    super.initState();
    var menuProvider = Provider.of<MenuProvider>(context, listen: false);
    var profile = menuProvider.user;
    var infoBox = menuProvider.infoBOX;
    _menuItems = [
      ItemMenu(
          id: MenuOptionsMenuEnum.home,
          title: 'Home',
          svgName: 'home',
          routeName: DashboardPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.paymentManagement,
          title: 'Gestión de pagos',
          svgName: 'wallet',
          routeName: PaymentManagementPage.routeName,
          isVisible:
              false // profile!.roles.indexWhere((element) => element.roleName == 'admin') !=-1
          ),
      ItemMenu(
          id: MenuOptionsMenuEnum.reservations,
          title: 'Reservas',
          svgName: 'bookmark',
          routeName: ReservationsPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.wods,
          title: 'WODs',
          svgName: 'wods',
          routeName: MainWodsPage.routeName,
          contentPadding: const EdgeInsets.only(top: 0, bottom: 0, left: 18)),
      ItemMenu(
          id: MenuOptionsMenuEnum.wodsCompetitors,
          title: 'WODs Competidores',
          svgName: 'wods',
          routeName: MainWodsPage.routeNameCompetitors,
          contentPadding: const EdgeInsets.only(top: 0, bottom: 0, left: 18),
          isVisible: profile!.userBoxes
                  .indexWhere((element) => element.isCompetitor) !=
              -1),
      ItemMenu(
          id: MenuOptionsMenuEnum.fees,
          title: 'Cuotas y bonos',
          svgName: 'card',
          routeName: MainFeesPage.routeName,
          contentPadding: const EdgeInsets.only(top: 0, bottom: 0, left: 18),
          isVisible: infoBox!.onlinePayment == 1),
      ItemMenu(
          id: MenuOptionsMenuEnum.timer,
          title: 'Cronómetro',
          iconData: Icons.timer,
          routeName: ChronometerPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.rm,
          title: 'RM',
          svgName: 'barcode-outline',
          routeName: RMPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.weight,
          title: 'Peso',
          svgName: 'body',
          routeName: WeightPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.myStats,
          title: 'Mis estadísticas',
          svgName: 'chart',
          routeName: MyStatsPage.routeName,
          isVisible: false),
      ItemMenu(
          id: MenuOptionsMenuEnum.notifications,
          title: 'Notificaciones',
          svgName: 'bell',
          routeName: NotificationsPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.profile,
          title: 'Perfil',
          iconData: Icons.account_circle,
          routeName: UserPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.boxContact,
          title: 'Contacto BOX',
          iconData: Icons.map,
          routeName: BoxContactPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.boxConditions,
          title: 'Condiciones del BOX',
          svgName: 'skull',
          routeName: BoxConditionsPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.tutorial,
          title: 'Tutorial',
          iconData: Icons.school,
          routeName: TutorialPage.routeName),
      ItemMenu(
          id: MenuOptionsMenuEnum.logout,
          title: 'Cerrar Sesión',
          iconData: Icons.exit_to_app,
          routeName: AuthPage.routeName)
    ];
  }

  _onTapItem(ItemMenu item, BuildContext context) {
    var navigatorSideMenuKey =
        Provider.of<MenuProvider>(context, listen: false).navigatorKey;
    if (item.id == MenuOptionsMenuEnum.logout) {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      authProvider.logout();
      Future.delayed(Duration(milliseconds: 700), () {
        Provider.of<ThemeModel>(appNavigatorKey.currentContext!, listen: false)
            .toggleTheme(appNavigatorKey.currentContext!);
      });
      Navigator.of(appNavigatorKey.currentContext!)
          .pushReplacementNamed(item.routeName);
    } else if (item.id == MenuOptionsMenuEnum.profile) {
      Navigator.pop(context);
      Navigator.of(context).push(PageRouteBuilder(
          transitionDuration: Duration(milliseconds: 1000),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            const begin = Offset(0.0, 1.0);
            const end = Offset.zero;
            final tween = Tween(begin: begin, end: end);
            animation.drive(tween);
            return child;
          },
          pageBuilder: (_, __, ___) => UserPage()));
    } else {
      Navigator.of(context).pop();
      Navigator.of(navigatorSideMenuKey.currentContext!)
          .pushNamed(item.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Container(
            color: Theme.of(context).colorScheme.secondary,
            child: ListView(
                padding: const EdgeInsets.only(top: 4, bottom: 9),
                children: [
                  ..._menuItems
                      .where((element) => element.isVisible!)
                      .toList()
                      .map((item) => ItemMenuOption(
                          item: item,
                          onTap: () {
                            _onTapItem(item, context);
                          }))
                      .toList()
                ])));
  }
}
