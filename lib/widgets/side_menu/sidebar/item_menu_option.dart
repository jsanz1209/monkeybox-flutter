import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/enums/menu_options_enum.dart';
import 'package:monkeybox/models/messages/api/message.dart';
import 'package:monkeybox/models/side-menu/item_menu.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:provider/provider.dart';

import '../../../pages/app.dart';

class ItemMenuOption extends StatelessWidget {
  final ItemMenu item;
  final VoidCallback onTap;

  const ItemMenuOption({Key? key, required this.item, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Message> messages =
        Provider.of<MenuProvider>(context, listen: true).notReadMessages;

    return ListTile(
        onTap: onTap,
        contentPadding: item.contentPadding ??
            const EdgeInsets.only(top: 0, bottom: 0, left: 16),
        visualDensity: VisualDensity.comfortable,
        horizontalTitleGap: 0,
        dense: true,
        leading: item.svgName != null
            ? SvgPicture.asset(
                'assets/images/icons/${item.svgName}.svg',
                color: Theme.of(appNavigatorKey.currentContext!).textTheme.bodyText1!.color,
                width: 24.sp,
              )
            : Icon(
                item.iconData,
                color: Theme.of(appNavigatorKey.currentContext!).textTheme.bodyText1!.color,
                size: 24.sp,
              ),
        title: Row(
          children: [
            Text(
              item.title,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(fontSize: 18.sp),
            ),
            if (item.id == MenuOptionsMenuEnum.notifications &&
                messages.isNotEmpty)
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: 16,
                  child: Text(messages.length.toString(),
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold)),
                ),
              )
          ],
        ));
  }
}
