import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/list_menu_options.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/side_nav_bar.dart';

class ContentDrawer extends StatefulWidget {
  const ContentDrawer({Key? key}) : super(key: key);

  @override
  State<ContentDrawer> createState() => _ContentDrawerState();
}

class _ContentDrawerState extends State<ContentDrawer> {

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).primaryColor,
      child: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Container(
            color: Theme.of(context).primaryColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [const SideNavBar(), const ListMenuOptions()],
            )),
      ),
    );
  }
}
