import 'package:flutter/material.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/content_drawer.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        color: Theme.of(context).primaryColor,
        child: Drawer(
          child: ContentDrawer(),
        ));
  }
}
