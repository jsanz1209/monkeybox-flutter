import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/models/common/item_autocomplete.dart';
import 'package:monkeybox/models/common/item_dropdown.dart';
import 'package:monkeybox/models/rm/rm.dart';
import 'package:monkeybox/models/sport-data/api/body_weights_data.dart';
import 'package:monkeybox/models/sport-data/api/exercise_data.dart';
import 'package:monkeybox/providers/rm/rm_provider.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/form/custom_autocomplete.dart';
import 'package:monkeybox/widgets/common/form/custom_dropdown.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';
import 'package:provider/provider.dart';

class FormAddRMWidget extends StatefulWidget {
  final Function(RM, int?) onSubmit;
  final Function onBack;
  const FormAddRMWidget(
      {Key? key, required this.onSubmit, required this.onBack})
      : super(key: key);

  @override
  State<FormAddRMWidget> createState() => _FormAddRMWidgetState();
}

class _FormAddRMWidgetState extends State<FormAddRMWidget> {
  final _formKey = GlobalKey<FormState>();

  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields(
          ['date', 'exercise', 'weight', 'reps', 'bodyWeight']);

  final List<ItemDropdown> _repsData = [
    ItemDropdown(1, '1'),
    ItemDropdown(2, '2'),
    ItemDropdown(3, '3'),
    ItemDropdown(4, '4'),
    ItemDropdown(5, '5'),
  ];

  late List<ItemAutocomplete> _exerciseOptions;
  late Future<List<List<dynamic>>> _futureExercisesBodyWeights;

  RM _data = RM();
  int? _idRM;

  @override
  void initState() {
    super.initState();
    var dataUpdate = Provider.of<RmProvider>(context, listen: false).dataUpdate;
    if (dataUpdate != null) {
      _idRM = dataUpdate.id;
      _data = _data.copyWith(
          startDate: dataUpdate.date,
          bodyWeight: dataUpdate.weight,
          exercise: dataUpdate.exerciseID,
          reps: dataUpdate.repetitions,
          weight: dataUpdate.kgLb);
    }
    _futureExercisesBodyWeights =
        Provider.of<SportDataProvider>(context, listen: false)
            .getExerciseBodyWeights();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<List<dynamic>>>(
      future: _futureExercisesBodyWeights,
      builder: (ctx, asyncSnapShot) {
        if (asyncSnapShot.data == null) {
          return SizedBox();
        }
        var listData = asyncSnapShot.data as List<List<dynamic>>;
        var exerciseData = listData[0] as List<ExerciseData>;
        var bodyWeightsData = listData[1].reversed.toList() as  List<BodyWeightsData>;

        _exerciseOptions = exerciseData
            .asMap()
            .entries
            .map((e) => ItemAutocomplete(
                id: e.value.id, name: e.value.description.name))
            .toList();
        _exerciseOptions.sort((a, b) => a.name.trim().compareTo(b.name.trim()));
        return Container(
          child: Container(
            margin: EdgeInsets.all(
              16.sp,
            ),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  CustomTextFormField(
                    formKey: _fields['date'],
                    label: 'Fecha',
                    fieldName: 'date',
                    initialValue: _data.startDate!.isNotEmpty
                        ? DateFormat('yyyy-MM-dd')
                            .format(DateTime.parse(_data.startDate!))
                        : DateFormat('yyyy-MM-dd').format(DateTime.now()),
                    isBlackTheme: true,
                    isTypeDate: true,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    onSave: (value) {
                      _data = _data.copyWith(startDate: value);
                    },
                    validator: (value) {
                      return FormValidators.validatorRequired(
                          value, 'La fecha es obligatoria');
                    },
                  ),
                  CustomAutocomplete(
                    options: _exerciseOptions,
                    formKey: _fields['exercise'],
                    label: 'Ejercicio',
                    initialValue: _data.exercise != null ? _exerciseOptions.firstWhere((element) => element.id == _data.exercise) : null,
                    isBlackTheme: true,
                    onSave: (value) {
                      _data = _data.copyWith(exercise: value!.id);
                    },
                    validator: (value) {
                      return FormValidators.validatorRequired(
                          value, 'El ejercicio es obligatorio');
                    },
                  ),
                  CustomTextFormField(
                    formKey: _fields['weight'],
                    label: 'Peso',
                    fieldName: 'weight',
                    isBlackTheme: true,
                    suffixText: 'kg',
                    initialValue: _data.weight != null && _data.weight != 0 ? _data.weight.toString() : null,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    textInputAction: TextInputAction.next,
                    onSave: (value) {
                      _data = _data.copyWith(weight: double.parse(
                              value!.replaceFirst(RegExp(','), '.')));
                    },
                    validator: (value) {
                      return FormValidators.validatorRequired(
                          value, 'El peso es obligatorio');
                    },
                  ),
                  SizedBox(height: 19.5.sp),
                  CustomDropdown(
                    formKey: _fields['reps'],
                    label: 'Repeticiones',
                    fieldName: 'reps',
                    initialValue: _data.reps != null && _data.reps != 0 ? _data.reps : 1,
                    options: _repsData,
                    isBlackTheme: true,
                    onSave: (value) {
                      _data = _data.copyWith(reps: value);
                    },
                    validator: (value) {
                      return FormValidators.validatorRequired(
                          value, 'Las repeticiones son obligatorias');
                    },
                  ),
                  SizedBox(height: 19.5.sp),
                  CustomTextFormField(
                    formKey: _fields['bodyWeight'],
                    label: 'Peso corporal',
                    fieldName: 'bodyWeight',
                    isBlackTheme: true,
                    suffixText: 'kg',
                    initialValue: _data.bodyWeight != null && _data.bodyWeight != 0 ? _data.bodyWeight.toString() : bodyWeightsData[0].weight.toString(),
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    textInputAction: TextInputAction.next,
                    onSave: (value) {
                      _data = _data.copyWith(bodyWeight: double.parse(
                              value!.replaceFirst(RegExp(','), '.')));
                    },
                    validator: (value) {
                      return FormValidators.validatorRequired(
                          value, 'El peso corporal obligatorio');
                    },
                  ),
                  SizedBox(height: 9.sp),
                  CustomElevatedButton(
                      text: 'GUARDAR',
                      backgroundColor: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      pressEvent: _onSubmit),
                  SizedBox(height: 9.sp),
                  CustomElevatedButton(
                      text: 'CANCELAR',
                      backgroundColor: Colors.white,
                      textColor: Theme.of(context).primaryColor,
                      borderColor: Theme.of(context).primaryColor,
                      borderWidth: 2.sp,
                      pressEvent: () {
                        widget.onBack();
                      })
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _onSubmit() async {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      debugPrint('is valid');
      _formKey.currentState!.save();
      widget.onSubmit(_data, _idRM);
      Provider.of<RmProvider>(context, listen: false).itemAutocomplete = _exerciseOptions.firstWhere((element) => element.id == _data.exercise);
      _formKey.currentState!.reset();
    } else {
      debugPrint('is invalid');
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            alignmentPolicy: ScrollPositionAlignmentPolicy.keepVisibleAtStart,
            alignment: 0,
            curve: Curves.easeInOut);
      }
    }
  }
}
