import 'package:flutter/material.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/rm/rm.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/side_menu/rm/form_add_rm_widget.dart';
import 'package:provider/provider.dart';

class AddRMWidget extends StatelessWidget {
  final Function onBack;

  const AddRMWidget({Key? key, required this.onBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: FormAddRMWidget(
          onSubmit: (RM data, int? idRM) => _submit(context, data, idRM),
          onBack: () => onBack()),
    );
  }

  void _submit(BuildContext context, RM data, int? idRM) async {
    try {
      if (idRM != null) {
        await Provider.of<SportDataProvider>(context, listen: false)
            .updateAthleticData(data, idRM);
        SnackBarHelper.showSnackBar('RM modificado',
            'El RM se ha modificado correctamente', TypeSnackBarEnum.success);
      } else {
        await Provider.of<SportDataProvider>(context, listen: false)
            .addAthleticData(data);
        SnackBarHelper.showSnackBar('RM añadido',
            'El RM se ha añadido correctamente', TypeSnackBarEnum.success);
      }
      onBack();
    } catch (error) {
      rethrow;
    }
  }
}
