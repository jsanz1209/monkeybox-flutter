import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/models/common/item_autocomplete.dart';
import 'package:monkeybox/models/common/item_dropdown.dart';
import 'package:monkeybox/models/rm/rm.dart';
import 'package:monkeybox/models/sport-data/api/exercise_data.dart';
import 'package:monkeybox/models/sport-data/api/rm_data.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/rm/rm_provider.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/form/custom_autocomplete.dart';
import 'package:monkeybox/widgets/common/form/custom_dropdown.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';
import 'package:provider/provider.dart';

class FormRMWidget extends StatefulWidget {
  final Function(List<RMData>, RM) onSubmit;
  const FormRMWidget({Key? key, required this.onSubmit}) : super(key: key);

  @override
  State<FormRMWidget> createState() => _FormRMWidgetState();
}

class _FormRMWidgetState extends State<FormRMWidget> {
  final _formKey = GlobalKey<FormState>();

  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields(['exercise', 'reps', 'startDate', 'endDate']);

  final List<ItemDropdown> _repsData = [
    ItemDropdown(1, '1'),
    ItemDropdown(2, '2'),
    ItemDropdown(3, '3'),
    ItemDropdown(4, '4'),
    ItemDropdown(5, '5'),
  ];

  late List<ItemAutocomplete> _exerciseOptions;
  late Future<List<ExerciseData>> _futureExercises;

  RM _data = RM();

  @override
  void initState() {
    super.initState();
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    _futureExercises = Provider.of<SportDataProvider>(context, listen: false)
        .getExercises(authProvider.boxID!);
    Future.delayed(const Duration(milliseconds: 400), () {
      if (Provider.of<RmProvider>(context, listen: false).itemAutocomplete !=
          null) {
        _onSubmit();
      }
    });    
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<ExerciseData>>(
      future: _futureExercises,
      builder: (ctx, asyncSnapShot) {
        _exerciseOptions = asyncSnapShot.data == null
            ? []
            : asyncSnapShot.data!
                .asMap()
                .entries
                .map((e) => ItemAutocomplete(
                    id: e.value.id, name: e.value.description.name))
                .toList();
        _exerciseOptions.sort((a, b) => a.name.trim().compareTo(b.name.trim()));
        return Container(
          color: Colors.grey.shade300,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 16.sp),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  CustomAutocomplete(
                    options: _exerciseOptions,
                    formKey: _fields['exercise'],
                    label: 'Ejercicio',
                    isBlackTheme: true,
                    initialValue:
                        Provider.of<RmProvider>(context, listen: false)
                            .itemAutocomplete,
                    onSave: (value) {
                      _data = _data.copyWith(exercise: value!.id);
                    },
                    validator: (value) {
                      return FormValidators.validatorRequired(
                          value, 'El ejercicio es obligatorio');
                    },
                  ),
                  CustomDropdown(
                    formKey: _fields['reps'],
                    label: 'Repeticiones',
                    fieldName: 'reps',
                    initialValue: 1,
                    options: _repsData,
                    isBlackTheme: true,
                    onSave: (value) {
                      _data = _data.copyWith(reps: value);
                    },
                    validator: (value) {
                      return FormValidators.validatorRequired(
                          value, 'Las repeticiones son obligatorias');
                    },
                  ),
                  SizedBox(height: 19.5.sp),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: CustomTextFormField(
                          formKey: _fields['startDate'],
                          label: 'Desde',
                          fieldName: 'startDate',
                          initialValue: DateFormat('yyyy-MM-dd').format(
                              DateTime.now()
                                  .subtract(const Duration(days: 30 * 3))),
                          isBlackTheme: true,
                          isTypeDate: true,
                          isTypeMonthOnlyDate: true,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          onSave: (value) {
                            _data = _data.copyWith(startDate: value);
                          },
                          validator: (value) {
                            return FormValidators.validatorRequired(
                                value, 'La fecha de inicio es obligatoria');
                          },
                        ),
                      ),
                      SizedBox(
                        width: 4.sp,
                      ),
                      Expanded(
                        child: CustomTextFormField(
                            formKey: _fields['endDate'],
                            label: 'Hasta',
                            fieldName: 'endDate',
                            isTypeMonthOnlyDate: true,
                            isBlackTheme: true,
                            isTypeDate: true,
                            initialValue:
                                DateFormat('yyyy-MM-dd').format(DateTime.now()),
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            onSave: (value) {
                              _data = _data.copyWith(endDate: value);
                            },
                            validator: (value) {
                              return FormValidators.validatorRequired(
                                  value, 'La fecha de fin es obligatoria');
                            }),
                      ),
                    ],
                  ),
                  SizedBox(height: 9.sp),
                  CustomElevatedButton(
                      text: 'BUSCAR HISTÓRICO',
                      backgroundColor: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      pressEvent: _onSubmit),
                  SizedBox(height: 19.5.sp),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _onSubmit() async {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      debugPrint('is valid');
      _formKey.currentState!.save();
      var sportProvider =
          Provider.of<SportDataProvider>(context, listen: false);
      try {
        var data = await sportProvider.getRMData(
            _data.exercise!, _data.startDate!, _data.endDate!, _data.reps);
        widget.onSubmit(data, _data);
      } catch (error) {
        rethrow;
      }
    } else {
      debugPrint('is invalid');
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            alignmentPolicy: ScrollPositionAlignmentPolicy.keepVisibleAtStart,
            alignment: 0,
            curve: Curves.easeInOut);
      }
    }
  }
}
