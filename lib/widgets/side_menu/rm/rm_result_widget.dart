import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/models/sport-data/api/rm_data.dart';

class RMResultWidget extends StatelessWidget {
  final RMData? data;

  const RMResultWidget({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Row(
          children: [
            Text.rich(TextSpan(
              text: '${data!.repetitions} - ${data!.kgLb}KG',
              children: [
                TextSpan(
                  text: ' - ${data!.weight}KG',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.normal),
                )
              ],
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.black,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.bold),
            )),
            Icon(
              Icons.accessibility_new_rounded,
              size: 20.sp,
              color: Colors.black,
            )
          ],
        );
  }
  
}
