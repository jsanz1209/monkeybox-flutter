import 'package:flutter/material.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/common/charts/chat_data.dart';
import 'package:monkeybox/models/rm/rm.dart';
import 'package:monkeybox/models/sport-data/api/rm_data.dart';
import 'package:monkeybox/providers/sport-data/sport_data_provider.dart';
import 'package:monkeybox/widgets/side_menu/rm/form_rm_widget.dart';
import 'package:monkeybox/widgets/side_menu/sport-data/charts_sport_data_widget.dart';
import 'package:monkeybox/widgets/side_menu/sport-data/sport_data_results_widget.dart';
import 'package:provider/provider.dart';

class HistoricalRMWidget extends StatefulWidget {
  final Function(RMData) onUpdateRM;
  HistoricalRMWidget({Key? key, required this.onUpdateRM}) : super(key: key);

  @override
  State<HistoricalRMWidget> createState() => _HistoricalRMWidgetState();
}

class _HistoricalRMWidgetState extends State<HistoricalRMWidget> {
  List<ChartData>? _listChartData;
  List<RMData>? _listData;
  RM? _formRM;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            FormRMWidget(onSubmit: (List<RMData> listData, RM formRM) {
              _setDataUI(listData, formRM);
            }),
            if (_listData == null)
              SizedBox(height: MediaQuery.of(context).size.height / 2),
            Stack(
              children: [
                ChartsSportDataWidget(
                  listChartData: _listChartData,
                  label: 'RM',
                ),
                if (_listData != null && _listData!.isNotEmpty)
                  SportDataResultsWidget<RMData>(
                    typeResultWidget: 'RMResultWidget',
                    titleDelete: 'Eliminar RM',
                    descriptionDelete:
                        '¿Estás seguro de que quieres eliminar este RM?',
                    listData: _listData!,
                    onDelete: (int idAthleticData) {
                      _deleteRM(idAthleticData);
                    },
                    onUpdate: (RMData data) {
                      widget.onUpdateRM(data);
                    },
                  ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void _setDataUI(List<RMData> listData, RM formRM) {
    setState(() {
      _formRM = formRM;
      if (listData.isEmpty) {
        _listChartData = [];
        _listData = listData;
      } else {
        listData.sort((a, b) => a.date.compareTo(b.date));
        _listData = listData.toList();
        _listChartData = listData
            .map(
                (item) => ChartData(x: DateTime.parse(item.date), y: item.kgLb, label: '${item.weight}kg 🧍🏻‍♂️'))
            .toList();
      }
    });
  }

  void _deleteRM(int idAthleticData) async {
    try {
      var sportProvider =
          Provider.of<SportDataProvider>(context, listen: false);
      await sportProvider.deleteAthleticData(idAthleticData);
      SnackBarHelper.showSnackBar('RM borrado',
          'El RM se ha borrado correctamente', TypeSnackBarEnum.success);
      var listData = await sportProvider.getRMData(_formRM!.exercise!,
          _formRM!.startDate!, _formRM!.endDate!, _formRM!.reps);
      _setDataUI(listData, _formRM!);
    } catch (error) {
      rethrow;
    }
  }
}
