import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class HeaderBoxContact extends StatelessWidget {
  const HeaderBoxContact({Key? key}) : super(key: key);

  void _launchURL(String url) async {
    if (!await launchUrl(Uri.parse(url))) throw 'Could not launch $url';
  }

  @override
  Widget build(BuildContext context) {   
    var infoBOX = Provider.of<MenuProvider>(context, listen: false).infoBOX;
    return SizedBox(
      height: 245.h,
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: double.infinity,
              color: Colors.black,
              child: CachedNetworkImage(
                imageUrl: '${Settings.apiURLUploads}/${infoBOX!.logoContact}',
                height: 169.sp,
                width: 169.sp,
              ),
            ),
          ),
          Expanded(
              child: Container(
            height: double.infinity,
            color: Theme.of(context).primaryColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(infoBOX.name!,
                      style: Theme.of(context).textTheme.headline2!.copyWith(color: Colors.white)),
                  SizedBox(
                    height: 6.sp,
                  ),
                  Text(infoBOX.address!,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(fontSize: 12.sp, color: Colors.white)),
                  SizedBox(
                    height: 6.sp,
                  ),
                  ActionChip(
                      padding: EdgeInsets.zero,
                      labelPadding: const EdgeInsets.only(
                          left: 0, right: 0, top: 1, bottom: 1),
                      visualDensity: VisualDensity.compact,
                      backgroundColor: Theme.of(context).colorScheme.secondary,
                      avatar: Icon(
                        Icons.phone,
                        color: Theme.of(context)
                            .textTheme
                            .bodyText1!.color,
                        size: 16.sp,
                      ),
                      onPressed: () {
                        _launchURL('tel:${infoBOX.phoneNumber!}');
                      },
                      label: SizedBox(
                        width: double.infinity,
                        child: Text(infoBOX.phoneNumber!,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.bold)),
                      )),
                  Padding(
                    padding: const EdgeInsets.only(top: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (infoBOX.instagramAccount!.isNotEmpty)
                          IconButton(
                            padding: EdgeInsets.zero,
                            visualDensity: VisualDensity.compact,
                            onPressed: () {
                              _launchURL(infoBOX.instagramAccount!);
                            },
                            icon: SvgPicture.asset(
                              'assets/images/icons/instagram.svg',
                              color: Colors.white,
                              width: 32.sp,
                              height: 32.sp,
                              alignment: Alignment.centerLeft,
                            ),
                          ),
                        if (infoBOX.facebookAccount!.isNotEmpty)
                          IconButton(
                            padding: EdgeInsets.zero,
                            visualDensity: VisualDensity.compact,
                            onPressed: () {
                              _launchURL(infoBOX.facebookAccount!);
                            },
                            icon: SvgPicture.asset(
                                'assets/images/icons/facebook.svg',
                                color: Colors.white,
                                width: 32.sp,
                                height: 32.sp),
                          ),
                        if (infoBOX.twitterAccount!.isNotEmpty)
                          IconButton(
                            padding: EdgeInsets.zero,
                            visualDensity: VisualDensity.compact,
                            onPressed: () {
                              _launchURL(infoBOX.twitterAccount!);
                            },
                            icon: SvgPicture.asset(
                              'assets/images/icons/twitter.svg',
                              color: Colors.white,
                              width: 32.sp,
                              height: 32.sp,
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }
}
