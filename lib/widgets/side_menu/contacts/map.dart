import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:monkeybox/helpers/location_helper.dart';
import 'package:monkeybox/models/side-menu/place_location.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/custom_loading.dart';
import 'package:provider/provider.dart';

class Map extends StatefulWidget {
  static const routeName = '/map';
  final bool? isSelecting;
  const Map({Key? key, this.isSelecting = false}) : super(key: key);

  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> {
  final Completer<GoogleMapController> _controller = Completer();
  late CameraPosition _kGooglePlex;
  LatLng? _selectLocation;
  Future<PlaceLocation>? _future;
  PlaceLocation? initialLocation;

  @override
  void initState() {
    super.initState();
    var infoBOX = Provider.of<MenuProvider>(context, listen: false).infoBOX;
    var address = infoBOX!.address!;
    var name = infoBOX.name;
    _future = LocationHelper.getPlaceFromAddress('$name $address');
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _future,
        builder: (ctx, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CustomLoading();
          } else {
            initialLocation = snapshot.data as PlaceLocation;
            _kGooglePlex = CameraPosition(
              target:
                  LatLng(initialLocation!.latitude, initialLocation!.longitude),
              zoom: 16,
            );
            return GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: _kGooglePlex,
              zoomControlsEnabled: true,
              zoomGesturesEnabled: true,
              mapToolbarEnabled: true,
              myLocationButtonEnabled: true,
              scrollGesturesEnabled: true,
              onTap: widget.isSelecting!
                  ? (LatLng coords) {
                      _selectPosition(coords);
                    }
                  : null,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              markers: _selectLocation == null
                  ? {
                      Marker(
                          markerId: const MarkerId('m1'),
                          position: LatLng(initialLocation!.latitude,
                              initialLocation!.longitude))
                    }
                  : {
                      Marker(
                          markerId: const MarkerId('m1'),
                          position: _selectLocation!)
                    },
            );
          }
        });
  }

  void _selectPosition(LatLng coords) {
    setState(() {
      _selectLocation = coords;
    });
  }
}
