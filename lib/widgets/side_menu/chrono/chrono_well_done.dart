import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/models/chrono/time_item.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';

class ChronoWellDone extends StatelessWidget {
  final int typeChrono;
  final int rounds;
  final int restTime;
  final int time;
  final Function onFinish;

  const ChronoWellDone({
    Key? key,
    required this.typeChrono,
    required this.time,
    required this.onFinish,
    required this.rounds,
    required this.restTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.thumb_up_rounded, size: 140.sp, color: Colors.black),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 16.sp),
          child: Text('!Bien hecho!',
              style: Theme.of(context).textTheme.headline3!.copyWith(
                  color: Colors.black,
                  fontSize: 36.sp,
                  fontWeight: FontWeight.bold)),
        ),
        _getDescription(),
        CustomElevatedButton(
            text: 'FINALIZAR',
            backgroundColor: Theme.of(context).primaryColor,
            textColor: Colors.white,
            pressEvent: () {
              onFinish();
            }),
      ],
    );
  }

  Widget _getDescription() {
    var formattedTime = TimeItem(time: time).getTime();
    var formattedRestTime = TimeItem(time: restTime).getTime();

    switch (typeChrono) {
      case Settings.CHRONO_TYPE_AMRAP:
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('AMRAP: '),
            Icon(Icons.fitness_center_rounded),
            Text(' $formattedTime')
          ],
        );

      case Settings.CHRONO_TYPE_FOR_TIME:
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('FOR TIME: '),
            Icon(Icons.fitness_center_rounded),
            Text(' $formattedTime')
          ],
        );

      case Settings.CHRONO_TYPE_EMOM:
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('EMOM: '),
            Icon(Icons.refresh),
            Text(' $rounds '),
            Icon(Icons.fitness_center_rounded),
            Text(' $formattedTime')
          ],
        );

      default:
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('TABATA: '),
            Icon(Icons.refresh),
            Text(' $rounds '),
            Icon(Icons.fitness_center_rounded),
            Expanded(child: FittedBox(child: Text(' $formattedTime '))),
            Icon(Icons.pause_circle_filled_rounded),
            Expanded(child: FittedBox(child: Text(' $formattedRestTime'))),
          ],
        );
    }
  }
}
