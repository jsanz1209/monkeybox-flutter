import 'package:bottom_picker/bottom_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/chrono/time_item.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';

class ChronoSettingsWidget extends StatefulWidget {
  final int type;
  final Function onCancel;
  final Function onStart;
  ChronoSettingsWidget(this.type, this.onCancel, this.onStart,
      {Key? key})
      : super(key: key);

  @override
  _ChronoSettingsWidgetState createState() => _ChronoSettingsWidgetState();
}

class _ChronoSettingsWidgetState extends State<ChronoSettingsWidget> {
  var theme = Theme.of(appNavigatorKey.currentContext!);
  Size screenSize = MediaQuery.of(appNavigatorKey.currentContext!).size;
  TextStyle descriptionStyle = TextStyle(
      fontSize: 19.sp,
      color: Theme.of(appNavigatorKey.currentContext!).colorScheme.secondary);
  int timeWork = 20 * 60;
  int rounds = 8;
  int timeRest = 10;
  int timeRepeat = -1;

  @override
  void initState() {
    super.initState();
    _initExerciseParams();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomHeader(
        title: 'CONFIGURACIÓN',
        withBackButton: true,
        onPress: () {
          widget.onCancel();
        },
      ),
      body: Container(
          width: screenSize.width,
          height: screenSize.height,
          child: Padding(
            padding: EdgeInsets.all(32.sp),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      _getTypeName(widget.type),
                      style: TextStyle(
                          fontSize: 28.sp,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.secondary),
                    ),
                  ),
                  SizedBox(
                    height: 16.sp,
                  ),
                  _getTypeDescription(widget.type),
                  SizedBox(
                    height: 8.sp,
                  ),
                  if (widget.type == Settings.CHRONO_TYPE_EMOM ||
                      widget.type == Settings.CHRONO_TYPE_TABATA)
                    CustomElevatedButton(
                      text: 'RONDAS ($rounds)',
                      backgroundColor: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      pressEvent: () {
                        _showRoundsPicker();
                      },
                    ),
                  CustomElevatedButton(
                    text:
                        '${widget.type == Settings.CHRONO_TYPE_TABATA ? 'TIEMPO DE TRABAJO' : 'TIEMPO'} (${TimeItem(time: timeWork).geTimeMinutes()})',
                    backgroundColor: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    pressEvent: () {
                      if (widget.type == Settings.CHRONO_TYPE_TABATA) {
                        _showTimeTabataWorkPicker();
                      } else {
                        _showTimeWorkPicker();
                      }
                    },
                  ),
                  if (widget.type == Settings.CHRONO_TYPE_TABATA)
                    CustomElevatedButton(
                      text:
                          'TIEMPO DE DESCANSO (${TimeItem(time: timeRest).geTimeMinutes()})',
                      backgroundColor: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      pressEvent: () {
                        _showTimeTabataRestPicker();
                      },
                    ),
                  if (widget.type == Settings.CHRONO_TYPE_AMRAP ||
                      widget.type == Settings.CHRONO_TYPE_FOR_TIME)
                    CustomElevatedButton(
                      text:
                          'AVISAR CADA (${TimeItem(time: timeRepeat).geTimeMinutes()})',
                      backgroundColor: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      pressEvent: () {
                        _showTimeRepeatPicker();
                      },
                    ),
                  CustomElevatedButton(
                    text:
                        'EMPEZAR${widget.type == Settings.CHRONO_TYPE_EMOM || widget.type == Settings.CHRONO_TYPE_TABATA ? ' (${_getTotalTime()})' : ''}',
                    backgroundColor: HexColor('#00ffc2'),
                    textColor: Theme.of(context).primaryColor,
                    borderColor: Theme.of(context).primaryColor,
                    borderWidth: 2.sp,
                    pressEvent: () {
                      widget.onStart(_getTypeName(widget.type), widget.type,
                          timeWork, rounds, timeRepeat, timeRest);
                    },
                  ),
                  CustomElevatedButton(
                      text: 'CANCELAR',
                      backgroundColor: Colors.white,
                      textColor: Theme.of(context).primaryColor,
                      borderColor: Theme.of(context).primaryColor,
                      borderWidth: 2.sp,
                      pressEvent: () {
                        widget.onCancel();
                      })
                ],
              ),
            ),
          )),
    );
  }

  void _initExerciseParams() {
    if (widget.type == Settings.CHRONO_TYPE_EMOM) {
      timeWork = 60;
    }

    if (widget.type == Settings.CHRONO_TYPE_TABATA) {
      timeWork = 20;
    }
  }

  String _getTypeName(int type) {
    switch (type) {
      case Settings.CHRONO_TYPE_AMRAP:
        return 'AMRAP';
      case Settings.CHRONO_TYPE_FOR_TIME:
        return 'FOR TIME';
      case Settings.CHRONO_TYPE_EMOM:
        return 'EMOM';
      default:
        return 'TABATA';
    }
  }

  Text _getTypeDescription(int type) {
    switch (type) {
      case Settings.CHRONO_TYPE_AMRAP:
        return Text.rich(
            TextSpan(
                text: 'Máximas repeticiones/rondas posibles en ',
                children: [
                  TextSpan(
                      text: TimeItem(time: timeWork).getTime(),
                      style: descriptionStyle.copyWith(
                          fontWeight: FontWeight.bold))
                ]),
            style: descriptionStyle);
      case Settings.CHRONO_TYPE_FOR_TIME:
        return Text.rich(
            TextSpan(
                text:
                    'Acaba tan rápido como puedas dentro un tiempo límite de ',
                children: [
                  TextSpan(
                      text: TimeItem(time: timeWork).getTime(),
                      style: descriptionStyle.copyWith(
                          fontWeight: FontWeight.bold))
                ]),
            style: descriptionStyle);
      case Settings.CHRONO_TYPE_EMOM:
        return Text.rich(
            TextSpan(children: [
              TextSpan(
                text: '$rounds rondas ',
                style: descriptionStyle.copyWith(fontWeight: FontWeight.bold),
              ),
              TextSpan(text: 'de ', style: descriptionStyle),
              TextSpan(
                  text: '${TimeItem(time: timeWork).getTime()} ',
                  style:
                      descriptionStyle.copyWith(fontWeight: FontWeight.bold)),
              TextSpan(text: 'de ', style: descriptionStyle),
              TextSpan(
                  text: 'trabajo - ',
                  style:
                      descriptionStyle.copyWith(fontWeight: FontWeight.bold)),
              TextSpan(
                  text: 'Total: ${_getTotalTime()} min.',
                  style:
                      descriptionStyle.copyWith(fontWeight: FontWeight.bold)),
            ]),
            style: descriptionStyle);
      default:
        return Text.rich(
            TextSpan(children: [
              TextSpan(
                text: '$rounds rondas ',
                style: descriptionStyle.copyWith(fontWeight: FontWeight.bold),
              ),
              TextSpan(text: 'de ', style: descriptionStyle),
              TextSpan(
                  text: '${TimeItem(time: timeWork).getTime()} ',
                  style:
                      descriptionStyle.copyWith(fontWeight: FontWeight.bold)),
              TextSpan(text: 'de ', style: descriptionStyle),
              TextSpan(
                  text: 'trabajo - ',
                  style:
                      descriptionStyle.copyWith(fontWeight: FontWeight.bold)),
              TextSpan(text: ' + ', style: descriptionStyle),
              TextSpan(
                  text: '${TimeItem(time: timeRest).getTime()} ',
                  style:
                      descriptionStyle.copyWith(fontWeight: FontWeight.bold)),
              TextSpan(text: 'de ', style: descriptionStyle),
              TextSpan(
                  text: 'descanso - ',
                  style:
                      descriptionStyle.copyWith(fontWeight: FontWeight.bold)),
              TextSpan(
                  text: 'Total: ${_getTotalTime()} min.',
                  style:
                      descriptionStyle.copyWith(fontWeight: FontWeight.bold)),
            ]),
            style: descriptionStyle);
    }
  }

  String _getTotalTime() {
    int total = 0;
    if (widget.type == Settings.CHRONO_TYPE_EMOM) {
      total = timeWork * rounds;
    } else {
      total = (timeWork * rounds) + (timeRest * (rounds - 1));
    }
    return '${TimeItem(time: total).geTimeMinutes().toString()}';
  }

  void _showRoundsPicker() {
    var items = List.generate(100, (index) => index + 1);
    BottomPicker(
      buttonAlignement: MainAxisAlignment.end,
      items: items
          .map((e) => Text(e.toString(),
              style: Theme.of(context).textTheme.headline3!.copyWith(
                  fontSize: 20.sp,
                  color: Theme.of(context).colorScheme.secondary)))
          .toList(),
      title: 'Elige un número de rondas',
      titleStyle: Theme.of(context)
          .textTheme
          .headline2!
          .copyWith(color: Theme.of(context).primaryColor, fontSize: 18.sp),
      dismissable: true,
      buttonSingleColor: Theme.of(context).primaryColor,
      buttonText: 'Aceptar',
      buttonTextStyle:
          Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 13.sp),
      selectedItemIndex: items.indexWhere((element) => element == rounds),
      onSubmit: (value) {
        setState(() {
          rounds = items[value];
        });
      },
    ).show(appNavigatorKey.currentContext!);
  }

  void _showTimeWorkPicker() {
    var items = _generateTimeWorkOptions();
    BottomPicker(
      buttonAlignement: MainAxisAlignment.end,
      items: items.map((e) => _buildTimeItem(e)).toList(),
      title: 'Elige un tiempo de trabajo',
      titleStyle: Theme.of(context)
          .textTheme
          .headline2!
          .copyWith(color: Theme.of(context).primaryColor, fontSize: 18.sp),
      dismissable: true,
      buttonSingleColor: Theme.of(context).primaryColor,
      buttonText: 'Aceptar',
      buttonTextStyle:
          Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 13.sp),
      selectedItemIndex:
          items.indexWhere((element) => element.time == timeWork),
      onSubmit: (value) {
        setState(() {
          timeWork = items[value].time;
        });
      },
    ).show(appNavigatorKey.currentContext!);
  }

  void _showTimeTabataWorkPicker() {
    var items = _generateTimeWorTabatakOptions();
    BottomPicker(
      buttonAlignement: MainAxisAlignment.end,
      items: items.map((e) => _buildTimeItem(e)).toList(),
      title: 'Elige un tiempo de trabajo',
      titleStyle: Theme.of(context)
          .textTheme
          .headline2!
          .copyWith(color: Theme.of(context).primaryColor, fontSize: 18.sp),
      dismissable: true,
      buttonSingleColor: Theme.of(context).primaryColor,
      buttonText: 'Aceptar',
      buttonTextStyle:
          Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 13.sp),
      selectedItemIndex:
          items.indexWhere((element) => element.time == timeWork),
      onSubmit: (value) {
        setState(() {
          timeWork = items[value].time;
        });
      },
    ).show(appNavigatorKey.currentContext!);
  }

  void _showTimeTabataRestPicker() {
    var items = _generateTimeWorTabatakOptions();
    BottomPicker(
      buttonAlignement: MainAxisAlignment.end,
      items: items.map((e) => _buildTimeItem(e)).toList(),
      title: 'Elige un tiempo de descanso',
      titleStyle: Theme.of(context)
          .textTheme
          .headline2!
          .copyWith(color: Theme.of(context).primaryColor, fontSize: 18.sp),
      dismissable: true,
      buttonSingleColor: Theme.of(context).primaryColor,
      buttonText: 'Aceptar',
      buttonTextStyle:
          Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 13.sp),
      selectedItemIndex:
          items.indexWhere((element) => element.time == timeRest),
      onSubmit: (value) {
        setState(() {
          timeRest = items[value].time;
        });
      },
    ).show(appNavigatorKey.currentContext!);
  }

  void _showTimeRepeatPicker() {
    var items = _generateTimeRepeatOptions();
    BottomPicker(
      buttonAlignement: MainAxisAlignment.end,
      items: items.map((e) => _buildTimeItem(e)).toList(),
      title: 'Elige un tiempo de repetición',
      titleStyle: Theme.of(context)
          .textTheme
          .headline2!
          .copyWith(color: Theme.of(context).primaryColor, fontSize: 18.sp),
      dismissable: true,
      buttonSingleColor: Theme.of(context).primaryColor,
      buttonText: 'Aceptar',
      buttonTextStyle:
          Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 13.sp),
      selectedItemIndex:
          items.indexWhere((element) => element.time == timeRepeat),
      onSubmit: (value) {
        setState(() {
          timeRepeat = items[value].time;
        });
      },
    ).show(appNavigatorKey.currentContext!);
  }

  Text _buildTimeItem(TimeItem timeItem) {
    return Text(
      timeItem.getTime(),
      style: Theme.of(context).textTheme.headline3!.copyWith(
          fontSize: 20.sp, color: Theme.of(context).colorScheme.secondary),
    );
  }

  List<TimeItem> _generateTimeWorTabatakOptions() {
    List<TimeItem> list =
        List.generate(25, (index) => TimeItem(time: index + 5));
    int currentTime = 30;

    while (currentTime <= 55) {
      list.add(TimeItem(time: currentTime));
      currentTime += 5;
    }

    currentTime = 60;

    while (currentTime < 180) {
      list.add(TimeItem(time: currentTime));
      currentTime += 15;
    }

    while (currentTime < 300) {
      list.add(TimeItem(time: currentTime));
      currentTime += 30;
    }

    while (currentTime <= (120 * 60)) {
      list.add(TimeItem(time: currentTime));
      currentTime += 60;
    }

    return list;
  }

  List<TimeItem> _generateTimeWorkOptions() {
    List<TimeItem> list = [
      TimeItem(time: 20),
      TimeItem(time: 30),
      TimeItem(time: 45),
      TimeItem(time: 60),
    ];

    int currentTime = 75;

    while (currentTime < 180) {
      list.add(TimeItem(time: currentTime));
      currentTime += 15;
    }

    while (currentTime < 300) {
      list.add(TimeItem(time: currentTime));
      currentTime += 30;
    }

    while (currentTime <= (120 * 60)) {
      list.add(TimeItem(time: currentTime));
      currentTime += 60;
    }

    return list;
  }

  List<TimeItem> _generateTimeRepeatOptions() {
    List<TimeItem> list = [TimeItem(time: -1)];
    int currentTime = 15;

    while (currentTime < 60) {
      list.add(TimeItem(time: currentTime));
      currentTime += 5;
    }
    while (currentTime <= 1200) {
      list.add(TimeItem(time: currentTime));
      currentTime += 30;
    }

    return list;
  }
}
