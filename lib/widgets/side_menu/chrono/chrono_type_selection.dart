import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/pages/app.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:monkeybox/widgets/side_menu/sidebar/navigation_drawer.dart';

import '../custom_menu_header.dart';

class ChronoTypeSelectionWidget extends StatefulWidget {
  final Function onSelect;
  final Function onCancel;
  final bool fromWODS;

  ChronoTypeSelectionWidget(this.onSelect, this.onCancel, this.fromWODS,
      {Key? key})
      : super(key: key);

  @override
  _ChronoTypeSelectionWidgetState createState() =>
      _ChronoTypeSelectionWidgetState();
}

class _ChronoTypeSelectionWidgetState extends State<ChronoTypeSelectionWidget> {
  var theme = Theme.of(appNavigatorKey.currentContext!);
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(appNavigatorKey.currentContext!).size;
    return Scaffold(
      appBar: (widget.fromWODS
          ? CustomHeader(
              title: 'CRONÓMETRO',
              withBackButton: true,
              onPress: widget.onCancel,
            )
          : CustomMenuHeader(title: 'CRONÓMETRO')) as PreferredSizeWidget,
      drawer: widget.fromWODS ? null : NavigationDrawer(),
      body: Container(
        width: double.infinity,
        height: screenSize.height,
        margin: EdgeInsets.symmetric(vertical: 32.sp, horizontal: 24.sp),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Elige una modalidad:',
                  style: TextStyle(
                      fontSize: 20.sp,
                      color: Theme.of(context).colorScheme.secondary),
                ),
                SizedBox(
                  height: 8.sp,
                ),
                CustomElevatedButton(
                  text: 'AMRAP',
                  backgroundColor: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  pressEvent: () {
                    widget.onSelect(Settings.CHRONO_TYPE_AMRAP);
                  },
                ),
                CustomElevatedButton(
                  text: 'FOR TIME',
                  backgroundColor: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  pressEvent: () {
                    widget.onSelect(Settings.CHRONO_TYPE_FOR_TIME);
                  },
                ),
                CustomElevatedButton(
                  text: 'EMOM',
                  backgroundColor: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  pressEvent: () {
                    widget.onSelect(Settings.CHRONO_TYPE_EMOM);
                  },
                ),
                CustomElevatedButton(
                  text: 'TABATA',
                  backgroundColor: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  pressEvent: () {
                    widget.onSelect(Settings.CHRONO_TYPE_TABATA);
                  },
                ),
                if (widget.fromWODS)
                  CustomElevatedButton(
                      text: 'ATRÁS',
                      backgroundColor: Colors.white,
                      textColor: Theme.of(context).primaryColor,
                      borderColor: Theme.of(context).primaryColor,
                      borderWidth: 2.sp,
                      pressEvent: () {
                        widget.onCancel();
                      })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
