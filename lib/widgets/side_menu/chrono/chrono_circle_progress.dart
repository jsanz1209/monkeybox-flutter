import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class ChronoCircleProgress extends StatelessWidget {
  final int typeChrono;
  final int currentRound;
  final int totalRounds;
  final String formattedTotalTime;
  final String formattedCurrentTime;
  final double percentageProgress;
  final StreamSubscription<int> timerSubscription;
  final Function togglePlay;

  const ChronoCircleProgress(
      {Key? key,
      required this.typeChrono,
      required this.currentRound,
      required this.totalRounds,
      required this.formattedCurrentTime,
      required this.formattedTotalTime,
      required this.percentageProgress,
      required this.timerSubscription,
      required this.togglePlay})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (typeChrono == Settings.CHRONO_TYPE_EMOM || typeChrono == Settings.CHRONO_TYPE_TABATA)
          Text(
            '$currentRound/$totalRounds ($formattedTotalTime)',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 36.sp, color: Theme.of(context).primaryColor),
          ),
        SizedBox(
          height: 24.sp,
        ),
        CircularPercentIndicator(
          radius: 145.0,
          lineWidth: 15,
          percent: percentageProgress,
          animation: true,
          animationDuration: 200,
          addAutomaticKeepAlive: true,
          reverse: typeChrono == Settings.CHRONO_TYPE_AMRAP,
          animateFromLastPercent: true,
          backgroundWidth: 13,
          backgroundColor: typeChrono == Settings.CHRONO_TYPE_AMRAP
              ? Theme.of(context).primaryColor
              : Colors.grey[50]!,
          progressColor: typeChrono == Settings.CHRONO_TYPE_AMRAP
              ? Colors.grey[50]!
              : Theme.of(context).primaryColor,
          center: GestureDetector(
            onTap: () {
              togglePlay();
            },
            child: CircularPercentIndicator(
              radius: 125.0,
              lineWidth: 2,
              progressColor: HexColor('#03FFC2'),
              percent: 1,
              center: timerSubscription.isPaused
                  ? Container(
                      height: double.infinity,
                      width: double.infinity,
                      color: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 32.sp,
                          ),
                          Transform.scale(
                            scale: 5,
                            child: Icon(
                              Icons.play_arrow_rounded,
                              size: 32.sp,
                            ),
                          ),
                          SizedBox(
                            height: 42.sp,
                          ),
                          Text(
                            formattedCurrentTime,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2!
                                .copyWith(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 24.sp),
                          ),
                          Text(
                            'Pulsa para continuar',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2!
                                .copyWith(
                                    color: Theme.of(context).primaryColor),
                          ),
                        ],
                      ),
                    )
                  : Text(
                      formattedCurrentTime,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontSize: 70.sp,
                          color: Theme.of(context).primaryColor),
                    ),
            ),
          ),
        ),
      ],
    );
  }
}
