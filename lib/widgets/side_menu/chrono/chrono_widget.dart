import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:monkeybox/constants/settings.dart';
import 'package:monkeybox/helpers/dialogs.dart';
import 'package:monkeybox/providers/chrono/chrono_bloc.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/custom_header.dart';
import 'package:monkeybox/widgets/side_menu/chrono/chrono_circle_progress.dart';
import 'package:monkeybox/widgets/side_menu/chrono/chrono_well_done.dart';
import 'package:wakelock/wakelock.dart';

class ChronoWidget extends StatefulWidget {
  final int type;
  final int timeWork;
  final int rounds;
  final int timeRepeat;
  final int timeRest;
  final String name;
  final Function onCancel;
  final Function? test;

  const ChronoWidget(
      {Key? key,
      required this.type,
      required this.rounds,
      required this.timeWork,
      required this.timeRepeat,
      required this.timeRest,
      required this.name,
      required this.onCancel,
      this.test})
      : super(key: key);

  @override
  State<ChronoWidget> createState() => _ChronoWidgetState();
}

class _ChronoWidgetState extends State<ChronoWidget>
    with SingleTickerProviderStateMixin {
  // Time countdown
  late Stream<int>? _timerStream;
  late StreamSubscription<int> _timerSubscription;
  late StreamController<int> _streamController;
  bool _isRunningTime = true;
  double _percentageProgress = 0;

  int _totalTime = 0;
  String _formattedTotalTime = '00:00';
  int _currenTime = 20;
  String _formattedCurrentTime = '';
  bool _timeIsInitialized = false;

  // Initial countdown
  late Timer _timerCountDown;
  int _startCountDown = 10;

  // Repeat countdown
  int _timeRepeatCountDown = 10;

  //Animations
  late AnimationController _animationController;
  late Animation<Offset> _offset;
  late ConfettiController _controllerCenter;

  // Audio
  AudioCache _audioCache = AudioCache();

  // Type
  int _typeChrono = Settings.CHRONO_TYPE_AMRAP;

  // Rounds
  int _currentRound = 1;
  int _totalRounds = 3;

  //TABATA
  bool _isRestTime = false;
  int _restTime = 10;

  final _chronoBloc = ChronoBloc();
  late StreamSubscription<void> _streamSubscription;

  @override
  void initState() {
    super.initState();
    Wakelock.enable();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 700));
    _audioCache.load('sounds/start_end.mp3');
    _audioCache.load('sounds/midway.mp3');
    _audioCache.load('sounds/final-round.mp3');
    _audioCache.load('sounds/lets-go.mp3');
    _audioCache.load('sounds/rest-time.mp3');
    _offset = Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset.zero)
        .animate(_animationController);
    _controllerCenter =
        ConfettiController(duration: const Duration(seconds: 2));
    _initTime();
    _startInitialCountDown();
    _chronoBloc.initControllersChronoWidgetGenBack();
    _streamSubscription = _chronoBloc.chronoWidgetBackEventStream.listen((event) {
      _onCancel();
    });
  }

  @override
  void dispose() {
    if (_timeIsInitialized) {
      _streamController.close();
      _timerSubscription.cancel();
      _timerStream = null;
    }

    _timerCountDown.cancel();
    _animationController.dispose();
    _chronoBloc.disposeChronoWidgetBackEvent();

    _streamSubscription.cancel();

    Wakelock.disable();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        _onCancel();
        return Future<bool>.value(true);
      },
      child: Scaffold(
        appBar: CustomHeader(
          title: widget.name,
          withBackButton: true,
          onPress: () {
            _onCancel();
          },
        ),
        body: Padding(
          padding: EdgeInsets.all(32.sp),
          child: Center(
            child: !_timeIsInitialized
                ? Text(
                    _startCountDown == 0 ? 'GO' : _startCountDown.toString(),
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontSize: 50.sp,
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold),
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ConfettiWidget(
                        confettiController: _controllerCenter,
                        blastDirectionality: BlastDirectionality.explosive,
                        particleDrag: 0.05,
                        emissionFrequency: 0.05,
                        numberOfParticles: 50,
                        gravity: 0.05,
                        maximumSize: const Size(20, 10),
                        colors: [
                          Colors.green.withOpacity(0.75),
                          Colors.blue.withOpacity(0.75),
                          Colors.pink.withOpacity(0.75),
                          Colors.orange.withOpacity(0.75),
                          Colors.purple.withOpacity(0.75),
                        ], // manually specify the colors to be used
                      ),
                      _isRunningTime
                          ? ChronoCircleProgress(
                              typeChrono: _typeChrono,
                              currentRound: _currentRound,
                              totalRounds: _totalRounds,
                              formattedCurrentTime: _formattedCurrentTime,
                              formattedTotalTime: _formattedTotalTime,
                              percentageProgress: _percentageProgress,
                              timerSubscription: _timerSubscription,
                              togglePlay: _togglePlay,
                            )
                          : SlideTransition(
                              position: _offset,
                              child: ChronoWellDone(
                                restTime: _restTime,
                                rounds: _totalRounds,
                                time: _currenTime,
                                typeChrono: _typeChrono,
                                onFinish: widget.onCancel,
                              )),
                      if (_timeIsInitialized && _isRunningTime)
                        Container(
                          margin: EdgeInsets.only(top: 16.sp),
                          child: CustomElevatedButton(
                              text: 'CANCELAR',
                              backgroundColor: Colors.white,
                              textColor: Colors.red,
                              borderColor: Colors.red,
                              borderWidth: 2.sp,
                              pressEvent: () {
                                _onCancel();
                              }),
                        )
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  void _togglePlay() {
    setState(() {
      if (_timeIsInitialized) {
        if (_timerSubscription.isPaused) {
          _timerSubscription.resume();
        } else {
          _timerSubscription.pause();
        }
      }
    });
  }

  void _startInitialCountDown() {
    _timerCountDown = new Timer.periodic(
      const Duration(seconds: 1),
      (Timer timer) {
        if (_startCountDown == 0) {
          _startTimeCountDown();
          _timerCountDown.cancel();
        } else {
          if (_startCountDown == 4) {
            _audioCache.play('sounds/start_end.mp3',
                stayAwake: true, mode: PlayerMode.LOW_LATENCY);
          }
          setState(() {
            _startCountDown--;
          });
        }
      },
    );
  }

  Stream<int> _stopWatchStream() {
    Timer? timer;
    Duration timerInterval = Duration(seconds: 1);
    int counter = _typeChrono == Settings.CHRONO_TYPE_AMRAP ? _currenTime : 0;

    void stopTimer() {
      if (timer != null) {
        timer!.cancel();
        timer = null;
        counter = 0;
        _streamController.close();
      }
    }

    void pauseTimer() {
      timer!.cancel();
    }

    void tick(_) {
      // AMRAP, chron is greater than 0
      if (_typeChrono == Settings.CHRONO_TYPE_AMRAP && counter > 0) {
        counter--;
        _streamController.add(counter);
        // when the remaining time is 3 seconds
        if (counter == 3) {
          _audioCache.play('sounds/start_end.mp3',
              stayAwake: true, mode: PlayerMode.LOW_LATENCY);
        }
        // NO AMRAP Chrono, if the type is TABATA, time to process must be less than current time if we'arent in rest time and less than rest time otherwise
      } else if (_typeChrono != Settings.CHRONO_TYPE_AMRAP &&
          ((counter < _currenTime && !_isRestTime) ||
              (counter < _restTime && _isRestTime))) {
        counter++;
        _streamController.add(counter);
        if (_typeChrono == Settings.CHRONO_TYPE_TABATA) {
          if (_isRestTime) {
            if (counter == _restTime - 3) {
              _audioCache.play('sounds/start_end.mp3',
                  stayAwake: true, mode: PlayerMode.LOW_LATENCY);
            }
          } else if (counter == _currenTime - 3) {
            _audioCache.play('sounds/start_end.mp3',
                stayAwake: true, mode: PlayerMode.LOW_LATENCY);
          }
        } else {
          // when the remaining time is 3 seconds
          if (counter == _currenTime - 3) {
            _audioCache.play('sounds/start_end.mp3',
                stayAwake: true, mode: PlayerMode.LOW_LATENCY);
          }
        }
        // Type EMOM
      } else if (_typeChrono == Settings.CHRONO_TYPE_EMOM &&
          _currentRound < _totalRounds) {
        setState(() {
          _currentRound++;
          // if we're in the last round
          if (_currentRound == _totalRounds) {
            _audioCache.play('sounds/final-round.mp3',
                stayAwake: true, mode: PlayerMode.LOW_LATENCY);
          }
        });
        counter = 0;
        _streamController.add(counter);
        // Type TABATA
      } else if (_typeChrono == Settings.CHRONO_TYPE_TABATA &&
          _currentRound < _totalRounds) {
        _isRestTime = !_isRestTime;
        // we're in rest time mode
        if (_isRestTime) {
          _audioCache.play('sounds/rest-time.mp3',
              stayAwake: true, mode: PlayerMode.LOW_LATENCY);
          // we're in work mode
        } else {
          setState(() {
            _currentRound++;
            // if we're in the last round
            if (_currentRound == _totalRounds) {
              _audioCache.play('sounds/final-round.mp3',
                  stayAwake: true, mode: PlayerMode.LOW_LATENCY);
              // otherwise
            } else {
              _audioCache.play('sounds/lets-go.mp3',
                  stayAwake: true, mode: PlayerMode.LOW_LATENCY);
            }
          });
        }
        counter = 0;
        _streamController.add(counter);
      } else {
        _animationController.forward();
        _controllerCenter.play();
        setState(() {
          _isRunningTime = false;
        });
      }
      if (!_isRunningTime) {
        stopTimer();
      }
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    _streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: pauseTimer,
    );

    return _streamController.stream;
  }

  void _startTimeCountDown() {
    setState(() {
      _timeIsInitialized = true;
    });

    _timerStream = _stopWatchStream();
    _timerSubscription = _timerStream!.listen((int newTick) {
      if (newTick != 0) {
        _totalTime++;
        var formattedTotalTime = _formatTime(_totalTime);
        var minutesStr = formattedTotalTime['minutesStr'].toString();
        var secondsStr = formattedTotalTime['secondsStr'].toString();
        _formattedTotalTime = '$minutesStr:$secondsStr';
      }
      if (_timeRepeatCountDown != -1 &&
          newTick % _timeRepeatCountDown == 0 &&
          newTick != 0 &&
          !_isRestTime) {
        _audioCache.play('sounds/midway.mp3',
            stayAwake: true, mode: PlayerMode.LOW_LATENCY);
      }

      setState(() {
        var formattedTime = _formatTime(newTick);
        var minutesStr = formattedTime['minutesStr'].toString();
        var secondsStr = formattedTime['secondsStr'].toString();
        _formattedCurrentTime = '$minutesStr:$secondsStr';
        var timeToProcess =
            _isRestTime && _typeChrono == Settings.CHRONO_TYPE_TABATA
                ? _restTime
                : _currenTime;
        _percentageProgress = _typeChrono == Settings.CHRONO_TYPE_AMRAP
            ? 1 - (newTick / _currenTime)
            : (newTick / timeToProcess);
      });
    });
  }

  void _initTime() {
    _currenTime = widget.timeWork;
    _totalRounds = widget.rounds;
    _timeRepeatCountDown = widget.timeRepeat;
    _restTime = widget.timeRest;
    _typeChrono = widget.type;
    var minutesStr = '';
    var secondsStr = '';
    if (_typeChrono == Settings.CHRONO_TYPE_AMRAP) {
      var formattedTime = _formatTime(_currenTime);
      minutesStr = formattedTime['minutesStr'].toString();
      secondsStr = formattedTime['secondsStr'].toString();
    } else {
      minutesStr = '00';
      secondsStr = '00';
    }
    _formattedCurrentTime = '$minutesStr:$secondsStr';
    if (_typeChrono == Settings.CHRONO_TYPE_EMOM ||
        _typeChrono == Settings.CHRONO_TYPE_TABATA) {
      _timeRepeatCountDown = (_currenTime / 2).ceil();
    }
  }

  Map<String, String> _formatTime(int time) {
    var minutesStr = ((time / 60) % 60).floor().toString().padLeft(2, '0');
    var secondsStr = (time % 60).floor().toString().padLeft(2, '0');
    var map = Map<String, String>();
    map['minutesStr'] = minutesStr;
    map['secondsStr'] = secondsStr;
    return map;
  }

  void _onCancel() async {
    _togglePlay();
    var isCanceled = await Dialogs.showConfirmDialogMaterial(
        'CANCELAR CRONO', '¿Estás seguro de que quieres cancelar el crono?');
    if (isCanceled) {
      widget.onCancel();
    } else {
      _togglePlay();
    }
  }
}
