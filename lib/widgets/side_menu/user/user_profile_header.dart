import 'dart:io';

import 'package:flutter/material.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/user_image_sliver_app_bar.dart';
import 'package:provider/provider.dart';

class UserProfileHeader extends StatefulWidget {
  final Function(File?) onChange;
  final GlobalKey<ScaffoldState> scaffoldKey;

  UserProfileHeader({Key? key, required this.onChange, required this.scaffoldKey}) : super(key: key);

  @override
  _UserProfileHeaderState createState() => _UserProfileHeaderState();
}

class _UserProfileHeaderState extends State<UserProfileHeader> {
  File? _imageFile;

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<MenuProvider>(context, listen: false).user;

    return SliverPersistentHeader(
      delegate: UserImageSliverAppBar(
          title: 'MI CUENTA',
          imageUrl: user!.profile!.pictureUrl,
          imageFile: _imageFile,
          isBlackTheme: true,
          minExtentCustom: 135,
          scaffoldKey: widget.scaffoldKey,
          onChange: (value) {
            setState(() {
              _imageFile = value;
            });
            widget.onChange(value);
          }),
      pinned: true,
      // floating: true,
    );
  }
}
