import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/models/user/api/history.dart';

class HistoryItem extends StatelessWidget {
  final History element;
  const HistoryItem({Key? key, required this.element}) : super(key: key);

  String _getDateFormat(DateTime? formatteDate) {
    if (formatteDate != null) {
      return DateFormat('dd/MM/yyyy').format(formatteDate);
    }
    return 'Actual';
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
        minVerticalPadding: 0,
        dense: true,
        title: Text(
          '${_getDateFormat(element.startDate)} - ${_getDateFormat(element.endDate)}',
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(color: Colors.grey, fontSize: 16.sp),
        ),
        trailing: Chip(
          padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 4),
          label: Text(element.status,
              style: Theme.of(context).textTheme.bodyText2!.copyWith(
                  color: Colors.white,
                  fontSize: 12.sp,
                  fontWeight: FontWeight.bold)),
          backgroundColor:
              element.status == 'ALTA' ? HexColor('#8fcb00') : Colors.red,
        ));
  }
}
