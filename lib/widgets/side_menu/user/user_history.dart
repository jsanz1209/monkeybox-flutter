import 'package:flutter/material.dart';
import 'package:monkeybox/models/user/api/history.dart';
import 'package:monkeybox/providers/auth/auth_provider.dart';
import 'package:monkeybox/providers/user/user_provider.dart';
import 'package:monkeybox/widgets/side_menu/user/history_item.dart';
import 'package:provider/provider.dart';

class UserHistory extends StatefulWidget {
  const UserHistory({Key? key}) : super(key: key);

  @override
  State<UserHistory> createState() => _UserHistoryState();
}

class _UserHistoryState extends State<UserHistory> {
  Future<List<History>>? _future;

  @override
  void initState() {
    super.initState();
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    var boxID = authProvider.boxID;
    var userID = authProvider.userID;
    _future = Provider.of<UserProvider>(context, listen: false)
        .getHistoryByBoxId(userID!, boxID!);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _future,
        builder: (ctx, snapShot) {
          if (snapShot.connectionState == ConnectionState.done) {
            var historyData = snapShot.data as List<History>;
            return ListView.builder(
                padding: const EdgeInsets.only(
                    top: 24, right: 16, left: 16, bottom: 0),
                itemBuilder: (ctx, index) {
                  var element = historyData[index];
                  return HistoryItem(element: element);
                },
                itemCount: historyData.length);
          }
          return SizedBox();
        });
  }
}
