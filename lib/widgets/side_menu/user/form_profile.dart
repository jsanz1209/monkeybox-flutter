import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:monkeybox/helpers/form_helper.dart';
import 'package:monkeybox/helpers/form_validators.dart';
import 'package:monkeybox/models/common/item_dropdown.dart';
import 'package:monkeybox/models/common/item_radio_button.dart';
import 'package:monkeybox/models/user/api/profile.dart';
import 'package:monkeybox/providers/app/app_provider.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/widgets/common/buttons/custom_elevated_button.dart';
import 'package:monkeybox/widgets/common/fieldset.dart';
import 'package:monkeybox/widgets/common/form/custom_dropdown.dart';
import 'package:monkeybox/widgets/common/form/custom_radio_button.dart';
import 'package:monkeybox/widgets/common/form/custom_switch.dart';
import 'package:monkeybox/widgets/common/form/custom_text_field.dart';
import 'package:provider/provider.dart';

class FormProfile extends StatefulWidget {
  final Function(Profile, String?, String?) submitEvent;
  final Function onCancel;
  final Function onDelete;

  const FormProfile(
      {Key? key,
      required this.submitEvent,
      required this.onCancel,
      required this.onDelete})
      : super(key: key);

  @override
  _FormProfileState createState() => _FormProfileState();
}

class _FormProfileState extends State<FormProfile> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, GlobalKey<FormFieldState<dynamic>>> _fields =
      FormHelper.getFormFields([
    'image',
    'firstName',
    'firstSurname',
    'secondSurname',
    'email',
    'birthdate',
    'gender',
    'height',
    'weight',
    'category',
    'instagram',
    'facebook',
    'twitter',
    'whatsapp',
    'password',
    'newPassword',
    'confirmPassword',
    'showFirstSurname',
    'showSecondSurname',
    'showBirthdate',
    'showGender',
    'showAge',
    'showWeight',
    'showHeight',
    'showCategory',
    'showInstagram',
    'showFacebook',
    'showTwitter',
    'showWhatsapp',
  ]);

  List<ItemDropdown>? _intensityData;
  late Profile _data;
  late String? _password;
  late String? _newPassword;

  @override
  void initState() {
    super.initState();
    _data = Provider.of<MenuProvider>(context, listen: false).user!.profile!;
  }

  void _onSubmit() {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      debugPrint('is valid');
      _formKey.currentState!.save();
      widget.submitEvent(_data, _password, _newPassword);
    } else {
      debugPrint('is invalid');
      var firstErrorField = FormHelper.getFirstKeyError(_fields);
      if (firstErrorField != null) {
        Scrollable.ensureVisible(firstErrorField!.currentContext!,
            duration: const Duration(milliseconds: 700),
            curve: Curves.easeInOut);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var focusNode = FocusScopeNode();
    var _listIntensity =
        Provider.of<AppProvider>(context, listen: false).intensitiesCategory;
    _intensityData = _listIntensity.map((item) {
      return ItemDropdown(item.id, item.category);
    }).toList();
    return FocusScope(
      node: focusNode,
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 16.sp, bottom: 16.sp),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                FieldSet(title: 'Datos básicos', widgets: [
                  CustomTextFormField(
                      formKey: _fields['firstName'],
                      label: 'Nombre',
                      fieldName: 'firstName',
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      focusNode: focusNode,
                      initialValue: _data.name,
                      isBlackTheme: true,
                      onSave: (value) {
                        _data = _data.copyWith(name: value);
                      },
                      validator: (value) => FormValidators.validatorRequired(
                          value, 'El nombre es obligatorio')),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                      formKey: _fields['firstSurname'],
                      label: 'Primer apellido',
                      fieldName: 'firstSurname',
                      focusNode: focusNode,
                      initialValue: _data.firstSurname,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      isBlackTheme: true,
                      onSave: (value) {
                        _data = _data.copyWith(firstSurname: value);
                      },
                      validator: null),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                      formKey: _fields['secondSurname'],
                      label: 'Segundo apellido',
                      fieldName: 'secondSurname',
                      initialValue: _data.secondSurname,
                      isBlackTheme: true,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      focusNode: focusNode,
                      onSave: (value) {
                        _data = _data.copyWith(secondSurname: value);
                      },
                      validator: null),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                    formKey: _fields['email'],
                    label: 'Email',
                    fieldName: 'email',
                    initialValue: _data.email,
                    isBlackTheme: true,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.done,
                    focusNode: focusNode,
                    onSave: (value) {
                      _data = _data.copyWith(email: value);
                    },
                    validator: (value) {
                      return FormValidators.validatorEmail(value);
                    },
                  ),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                    formKey: _fields['birthdate'],
                    isTypeDate: true,
                    label: 'Fecha de nacimiento',
                    fieldName: 'birthdate',
                    initialValue: _data.birthdate,
                    isBlackTheme: true,
                    keyboardType: TextInputType.datetime,
                    focusNode: focusNode,
                    onSave: (value) {
                      _data = _data.copyWith(birthdate: value);
                    },
                    validator: (value) => FormValidators.validatorRequired(
                        value, 'La fecha de nacimiento es obligatoria'),
                  ),
                  SizedBox(height: 24.sp),
                  SizedBox(
                    height: 30.sp,
                    child: Text(
                      'Sexo',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.black),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Transform.translate(
                      offset: const Offset(-5, 0),
                      child: CustomRadioButton(
                          isBlackTheme: true,
                          formKey: _fields['gender'],
                          initValue: _data.gender,
                          fieldName: 'gender',
                          labels: [
                            ItemRadioButton('M', 'Hombre'),
                            ItemRadioButton('F', 'Mujer')
                          ],
                          onSave: (value) {
                            _data = _data.copyWith(gender: value);
                          },
                          validator: (value) =>
                              FormValidators.validatorRequired(
                                  value, 'El sexo es obligatorio'))),
                ]),
                SizedBox(
                  height: 16.sp,
                ),
                FieldSet(title: 'Datos atléticos', widgets: [
                  CustomTextFormField(
                    formKey: _fields['height'],
                    label: 'Estatura',
                    fieldName: 'height',
                    suffixText: 'm',
                    initialValue: _data.height.toString(),
                    focusNode: focusNode,
                    isBlackTheme: true,
                    inputFormatters: [
                      MaskTextInputFormatter(mask: 'S,##', filter: {
                        'S': RegExp(r'[1-2]'),
                        '#': RegExp(r'[0-9]')
                      })
                    ],
                    textInputAction: TextInputAction.next,
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    onSave: (value) {
                      _data = _data.copyWith(
                          height: double.parse(
                              value!.replaceFirst(RegExp(','), '.')));
                    },
                    validator: (value) => FormValidators.validatorRequired(
                        value, 'La estatura es obligatoria'),
                  ),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                    formKey: _fields['weight'],
                    label: 'Peso corporal',
                    fieldName: 'weight',
                    suffixText: 'kg',
                    initialValue: _data.weight.toString(),
                    isBlackTheme: true,
                    focusNode: focusNode,
                    textInputAction: TextInputAction.next,
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    onSave: (value) {
                      _data = _data.copyWith(
                          weight: double.parse(
                              value!.replaceFirst(RegExp(','), '.')));
                    },
                  ),
                  SizedBox(
                    height: 30.sp,
                  ),
                  CustomDropdown(
                    formKey: _fields['category'],
                    label: 'Categoría favorita',
                    fieldName: 'category',
                    initialValue: _data.intensityCategory != null ? _data.intensityCategory!.id : null,
                    isBlackTheme: true,
                    options: _intensityData != null ? _intensityData! : [],
                    onSave: (value) {
                      var intentisyCategory = _listIntensity
                          .firstWhere((element) => element.id == value);
                      _data =
                          _data.copyWith(intensityCategory: intentisyCategory);
                    },
                    validator: (value) {
                      return FormValidators.validatorRequired(
                          value, 'La categoría es obligatoria');
                    },
                  ),
                ]),
                SizedBox(height: 16.sp),
                FieldSet(title: 'Redes sociales', widgets: [
                  CustomTextFormField(
                      formKey: _fields['instagram'],
                      label: 'Instagram',
                      fieldName: 'instagram',
                      initialValue: _data.privacy != null ? _data.privacy!.instagram : null,
                      isBlackTheme: true,
                      focusNode: focusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      suffixWidget: SvgPicture.asset(
                        'assets/images/icons/instagram.svg',
                        color: Colors.black,
                        height: 24.sp,
                        width: 24.sp,
                      ),
                      suffixWidgetPlaceHolder: SvgPicture.asset(
                        'assets/images/icons/instagram.svg',
                        color: Colors.grey,
                        height: 24.sp,
                        width: 24.sp,
                      ),
                      onSave: (value) {
                        _data.privacy!.instagram = value;
                        _data = _data.copyWith(privacy: _data.privacy);
                      }),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                      formKey: _fields['facebook'],
                      label: 'Facebook',
                      fieldName: 'facebook',
                      initialValue: _data.privacy != null ? _data.privacy!.facebook : null,
                      isBlackTheme: true,
                      focusNode: focusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      suffixWidget: SvgPicture.asset(
                        'assets/images/icons/facebook.svg',
                        color: Colors.black,
                        height: 24.sp,
                        width: 24.sp,
                      ),
                      suffixWidgetPlaceHolder: SvgPicture.asset(
                        'assets/images/icons/facebook.svg',
                        color: Colors.grey,
                        height: 24.sp,
                        width: 24.sp,
                      ),
                      onSave: (value) {
                        _data.privacy!.facebook = value;
                        _data = _data.copyWith(privacy: _data.privacy);
                      }),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                      formKey: _fields['twitter'],
                      label: 'Twitter',
                      fieldName: 'twitter',
                      isBlackTheme: true,
                      initialValue: _data.privacy != null ? _data.privacy!.twitter : null,
                      focusNode: focusNode,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.text,
                      suffixWidget: SvgPicture.asset(
                        'assets/images/icons/twitter.svg',
                        color: Colors.black,
                        height: 24.sp,
                        width: 24.sp,
                      ),
                      suffixWidgetPlaceHolder: SvgPicture.asset(
                        'assets/images/icons/twitter.svg',
                        color: Colors.grey,
                        height: 24.sp,
                        width: 24.sp,
                      ),
                      onSave: (value) {
                        _data.privacy!.twitter = value;
                        _data = _data.copyWith(privacy: _data.privacy);
                      }),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                    formKey: _fields['whatsapp'],
                    label: 'Whatsapp',
                    fieldName: 'whatsapp',
                    initialValue: _data.privacy != null ? _data.privacy!.whatsapp : null,
                    isBlackTheme: true,
                    keyboardType: TextInputType.text,
                    focusNode: focusNode,
                    textInputAction: TextInputAction.next,
                    suffixWidget: SvgPicture.asset(
                      'assets/images/icons/whatsapp.svg',
                      color: Colors.black,
                      height: 24.sp,
                      width: 24.sp,
                    ),
                    suffixWidgetPlaceHolder: SvgPicture.asset(
                      'assets/images/icons/whatsapp.svg',
                      color: Colors.grey,
                      height: 24.sp,
                      width: 24.sp,
                    ),
                    onSave: (value) {
                      _data.privacy!.whatsapp = value;
                      _data = _data.copyWith(privacy: _data.privacy);
                    },
                  ),
                ]),
                SizedBox(
                  height: 16.sp,
                ),
                FieldSet(
                    title: 'Privacidad',
                    withoutMarginChild: true,
                    widgets: [
                      CustomSwitch(
                        isBlackTheme: true,
                        label: 'Mostrar primer apellido',
                        formKey: _fields['showFirstSurname'],
                        initialValue: _data.privacy != null ? _data.privacy!.firstSurname : null,
                        onSave: (value) {
                          _data.privacy!.firstSurname = value!;
                          _data = _data.copyWith(privacy: _data.privacy);
                        },
                      ),
                      CustomSwitch(
                        isBlackTheme: true,
                        formKey: _fields['showSecondSurname'],
                        label: 'Mostrar segundo apellido',
                        initialValue: _data.privacy != null ? _data.privacy!.secondSurname : null,
                        onSave: (value) {
                          _data.privacy!.secondSurname = value!;
                          _data = _data.copyWith(privacy: _data.privacy);
                        },
                      ),
                      CustomSwitch(
                        isBlackTheme: true,
                        formKey: _fields['showBirthdate'],
                        label: 'Mostrar fecha de nacimiento',
                        initialValue: _data.privacy != null ? _data.privacy!.birthdate : null,
                        onSave: (value) {
                          _data.privacy!.birthdate = value!;
                          _data = _data.copyWith(privacy: _data.privacy);
                        },
                      ),
                      CustomSwitch(
                          isBlackTheme: true,
                          formKey: _fields['showGender'],
                          initialValue: _data.privacy != null ? _data.privacy!.gender : null,
                          label: 'Mostrar sexo',
                          onSave: (value) {
                            _data.privacy!.gender = value!;
                            _data = _data.copyWith(privacy: _data.privacy);
                          }),
                      CustomSwitch(
                          isBlackTheme: true,
                          formKey: _fields['showAge'],
                          initialValue: _data.privacy != null ? _data.privacy!.age : null,
                          label: 'Mostrar edad',
                          onSave: (value) {
                            _data.privacy!.age = value!;
                            _data = _data.copyWith(privacy: _data.privacy);
                          }),
                      CustomSwitch(
                          isBlackTheme: true,
                          formKey: _fields['showWeight'],
                          initialValue: _data.privacy != null ? _data.privacy!.weight : null,
                          label: 'Mostrar peso',
                          onSave: (value) {
                            _data.privacy!.weight = value!;
                            _data = _data.copyWith(privacy: _data.privacy);
                          }),
                      CustomSwitch(
                          isBlackTheme: true,
                          formKey: _fields['showHeight'],
                          initialValue: _data.privacy != null ? _data.privacy!.height : null,
                          label: 'Mostrar estatura',
                          onSave: (value) {
                            _data.privacy!.height = value!;
                            _data = _data.copyWith(privacy: _data.privacy);
                          }),
                      CustomSwitch(
                        isBlackTheme: true,
                        formKey: _fields['showCategory'],
                        initialValue: _data.privacy != null ? _data.privacy!.favoriteCategory : null,
                        label: 'Mostrar categoría favorita',
                        onSave: (value) {
                          _data.privacy!.favoriteCategory = value!;
                          _data = _data.copyWith(privacy: _data.privacy);
                        },
                      ),
                      CustomSwitch(
                          isBlackTheme: true,
                          formKey: _fields['showInstagram'],
                          initialValue: _data.privacy != null ? _data.privacy!.showInstagram : null,
                          label: 'Mostrar Instagram',
                          onSave: (value) {
                            _data.privacy!.showInstagram = value!;
                            _data = _data.copyWith(privacy: _data.privacy);
                          }),
                      CustomSwitch(
                          isBlackTheme: true,
                          formKey: _fields['showFacebook'],
                          initialValue: _data.privacy != null ? _data.privacy!.showFacebook : null,
                          label: 'Mostrar Facebook',
                          onSave: (value) {
                            _data.privacy!.showFacebook = value!;
                            _data = _data.copyWith(privacy: _data.privacy);
                          }),
                      CustomSwitch(
                          isBlackTheme: true,
                          formKey: _fields['showTwitter'],
                          initialValue: _data.privacy != null ? _data.privacy!.showTwitter : null,
                          label: 'Mostrar Twitter',
                          onSave: (value) {
                            _data.privacy!.showTwitter = value!;
                            _data = _data.copyWith(privacy: _data.privacy);
                          }),
                      CustomSwitch(
                          isBlackTheme: true,
                          formKey: _fields['showWhatsapp'],
                          initialValue: _data.privacy != null ?_data.privacy!.showWhatsapp : null,
                          label: 'Mostrar Whatsapp',
                          onSave: (value) {
                            _data.privacy!.showWhatsapp = value!;
                            _data = _data.copyWith(privacy: _data.privacy);
                          }),
                    ]),
                SizedBox(
                  height: 16.sp,
                ),
                FieldSet(title: 'Modificar contraseña', widgets: [
                  CustomTextFormField(
                    formKey: _fields['password'],
                    label: 'Contraseña actual',
                    fieldName: 'password',
                    isPassword: true,
                    isBlackTheme: true,
                    focusNode: focusNode,
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.text,
                    onSave: (value) {
                      _password = value!;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return null;
                      }
                      return FormValidators.validatorPassword(value);
                    },
                  ),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                    formKey: _fields['newPassword'],
                    label: 'Definir contraseña',
                    fieldName: 'newPassword',
                    isPassword: true,
                    isBlackTheme: true,
                    focusNode: focusNode,
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.text,
                    onSave: (value) {
                      _newPassword = value!;
                    },
                    validator: (value) {
                      if (value!.isEmpty &&
                          _fields['password']!.currentState!.value.isEmpty) {
                        return null;
                      }
                      _fields['confirmPassword']!.currentState!.validate();
                      return FormValidators.validatorPassword(value);
                    },
                  ),
                  SizedBox(height: 30.sp),
                  CustomTextFormField(
                    formKey: _fields['confirmPassword'],
                    label: 'Confirmar contraseña',
                    fieldName: 'confirmPassword',
                    isPassword: true,
                    isBlackTheme: true,
                    focusNode: focusNode,
                    textInputAction: TextInputAction.done,
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value!.isEmpty &&
                          _fields['password']!.currentState!.value.isEmpty) {
                        return null;
                      }
                      final isValidPassword =
                          FormValidators.validatorPassword(value);
                      if (isValidPassword == null) {
                        return FormValidators.validatorMatch(
                            value,
                            _fields['newPassword']!.currentState!.value,
                            'Los campos contraseña no coincide');
                      }
                      return isValidPassword;
                    },
                  ),
                ]),
                CustomElevatedButton(
                    text: 'GUARDAR',
                    backgroundColor: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    pressEvent: _onSubmit),
                CustomElevatedButton(
                    text: 'ELIMINAR CUENTA',
                    backgroundColor: Colors.white,
                    textColor: Colors.red,
                    borderColor: Colors.red,
                    borderWidth: 2.sp,
                    pressEvent: () {
                      widget.onDelete();
                    }),
                CustomElevatedButton(
                    text: 'CANCELAR',
                    backgroundColor: Colors.white,
                    textColor: Theme.of(context).primaryColor,
                    borderColor: Theme.of(context).primaryColor,
                    borderWidth: 2.sp,
                    pressEvent: () {
                      widget.onCancel();
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
