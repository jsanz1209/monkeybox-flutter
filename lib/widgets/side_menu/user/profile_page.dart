import 'dart:io';

import 'package:flutter/material.dart';
import 'package:monkeybox/enums/type_snackbar_enum.dart';
import 'package:monkeybox/helpers/dialogs.dart';
import 'package:monkeybox/helpers/hex_color.dart';
import 'package:monkeybox/helpers/snackbar_helper.dart';
import 'package:monkeybox/models/user/api/account_request.dart';
import 'package:monkeybox/models/user/api/change_password_request.dart';
import 'package:monkeybox/models/user/api/privacy_data_request.dart';
import 'package:monkeybox/models/user/api/profile.dart';
import 'package:monkeybox/models/user/api/profile_data_request.dart';
import 'package:monkeybox/models/user/api/social_network_data_request.dart';
import 'package:monkeybox/models/user/api/user.dart';
import 'package:monkeybox/models/user/api/user_request.dart';
import 'package:monkeybox/pages/side-menu/dashboard/dashboard_page.dart';
import 'package:monkeybox/providers/side_menu/menu_provider.dart';
import 'package:monkeybox/providers/user/user_provider.dart';
import 'package:monkeybox/widgets/common/custom_floating_button.dart';
import 'package:monkeybox/widgets/common/user_image_sliver_app_bar.dart';
import 'package:monkeybox/widgets/side_menu/user/form_profile.dart';
import 'package:monkeybox/widgets/side_menu/user/user_profile_header.dart';
import 'package:provider/provider.dart';

class UserProfile extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  const UserProfile({Key? key, required this.scaffoldKey}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    isSelectedImage = true;
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: HexColor('#e5e5e5'),
        body: Container(
          color: Theme.of(context).colorScheme.secondary,
          child: SafeArea(
            bottom: false,
            child: Container(
              color: HexColor('#e5e5e5'),
              child: CustomScrollView(controller: _scrollController, slivers: <Widget>[
                UserProfileHeader(
                  onChange: _changeImageEvent,
                  scaffoldKey: widget.scaffoldKey,
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                      (_, index) => Padding(
                          padding: const EdgeInsets.only(
                              top: 0, right: 16, left: 16, bottom: 84),
                          child: FormProfile(
                            onCancel: () {
                              var navigationKey =
                                  Provider.of<MenuProvider>(context, listen: false)
                                      .navigatorKey;
                              Navigator.of(navigationKey.currentContext!)
                                  .pushReplacementNamed(DashboardPage.routeName);
                            },
                            onDelete: _deleteEvent,
                            submitEvent: _submitEvent,
                            // changeImageEvent: _changeImageEvent,
                          )),
                      childCount: 1),
                ),
              ]),
            ),
          ),
        ),
        floatingActionButton: Padding(
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom + 70),
          child: CustomFloatingButton(
            scrollControllerMain: _scrollController,
          ),
        ));
  }

  void _notifyChangeUserProfile(User? user) {
    var menuProvider = Provider.of<MenuProvider>(context, listen: false);
    if (user != null) {
      menuProvider.user = user;
    }
  }

  Future<bool> _showDialogDeletingUser() async {
    return await Dialogs.showConfirmDialogMaterial('ELIMINAR CUENTA',
        '¿Estás seguro de que quieres eliminar tu cuenta? Toda tu información no podrá ser recuperada.');
  }

  void _deleteEvent() async {
    var userWantsDeleting = await _showDialogDeletingUser();
    if (userWantsDeleting) {
      var userID = Provider.of<MenuProvider>(context, listen: false).user!.id;
      await Provider.of<UserProvider>(context, listen: false)
          .deleteUser(userID);
    }
  }

  void _submitEvent(Profile user, String? currentPass, String? newPass) async {
    try {
      var userProvider = Provider.of<UserProvider>(context, listen: false);
      AccountRequest accountRequest =
          AccountRequest(email: user.email, username: user.email);
      ChangePasswordRequest? changePasswordRequest;
      UserRequest userRequest = UserRequest(
          name: user.name,
          firstSurname: user.firstSurname,
          secondSurname: user.secondSurname,
          birthdate: user.birthdate,
          gender: user.gender,
          weight: user.weight.toString(),
          height: user.height.toString(),
          intensityCategoryID: user.intensityCategory != null ? user.intensityCategory!.id : null,
          privacy: PrivacyData(
              profile: ProfileData(
                  birthdate: user.privacy!.birthdate!,
                  age: user.privacy!.age!,
                  facebook: user.privacy!.showWhatsapp!,
                  favoriteCategory: user.privacy!.favoriteCategory!,
                  firstSurname: user.privacy!.firstSurname!,
                  gender: user.privacy!.gender!,
                  height: user.privacy!.height!,
                  secondSurname: user.privacy!.secondSurname!,
                  instagram: user.privacy!.showInstagram!,
                  twitter: user.privacy!.showTwitter!,
                  weight: user.privacy!.weight!,
                  whatsapp: user.privacy!.showWhatsapp!),
              socialNetworks: SocialNetworkData(
                  facebook: user.privacy!.facebook,
                  whatsapp: user.privacy!.whatsapp,
                  twitter: user.privacy!.twitter,
                  instagram: user.privacy!.instagram)));
      if (currentPass!.isNotEmpty && newPass!.isNotEmpty) {
        changePasswordRequest = ChangePasswordRequest(
            currentPassword: currentPass,
            newPassword: newPass,
            confirmPassword: newPass);
      }
      var newUserProfile = await userProvider.updateProfileData(
          userRequest, accountRequest, changePasswordRequest);
      _notifyChangeUserProfile(newUserProfile);
      SnackBarHelper.showSnackBar(
          'Mi Cuenta',
          'Los datos de tu cuenta han sido modificados correctamente',
          TypeSnackBarEnum.success);
    } catch (error) {
      rethrow;
    }
  }

  void _changeImageEvent(File? file) async {
    try {
      var userProvider = Provider.of<UserProvider>(context, listen: false);
      var newUserProfile = await userProvider.setProfileImage(file);
      _notifyChangeUserProfile(newUserProfile);
    } catch (error) {
      rethrow;
    }
  }
}
